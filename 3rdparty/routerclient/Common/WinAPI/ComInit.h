#pragma once

class CComInit
{
public:
	CComInit()
	{
		::CoInitialize(NULL);
	}

	CComInit(DWORD _dwFlags)
	{
		::CoInitializeEx(NULL, _dwFlags);
	}

	~CComInit()
	{
		::CoUninitialize();
	}

};


#pragma once

#ifdef _WIN32
#include <windows.h>
#endif

#include <boost/system/system_error.hpp>
#include <boost/system/windows_error.hpp>
#include <string>

/**
 * Utility class to operate with current module
 */

class CProcessHelper
{
public:
	/// Get module name in format [path] + [exe filename] without extension
	static std::wstring GetCurrentModuleName();

	/// Get module name in format [exe filename] without extension
	static std::wstring GetCurrentModuleFileName();

	static std::wstring GetComputerName();

	static std::wstring ExpandEnvironment(LPCTSTR _lpszPath);

	static std::wstring GetCurrentFolderName();
};

#define THROW_SYSTEM_EXCEPTION() throw boost::system::system_error(boost::system::windows_error::windows_error_code(::GetLastError()))

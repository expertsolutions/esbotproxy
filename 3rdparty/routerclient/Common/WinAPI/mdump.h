#pragma once
#pragma warning (push)
#pragma warning (disable : 4091)
#include <dbghelp.h>
#pragma warning (pop)

#include "../Patterns/singleton.h"

class MiniDumper : public static_singleton<MiniDumper>
{
	friend class static_singleton<MiniDumper>;

private:
	static LONG WINAPI TopLevelFilter( struct _EXCEPTION_POINTERS *pExceptionInfo );

protected:
	MiniDumper();
};

typedef singleton_auto_pointer<MiniDumper> MDumper;
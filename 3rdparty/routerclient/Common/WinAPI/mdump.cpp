#include "stdafx.h"
#include "../Configuration/SystemConfiguration.h"
#include "Patterns/os_functions.h"
#include <ATLComTime.h>
#include <ATLStr.h>

#include "mdump.h"
using namespace ATL;

#pragma comment (lib, "dbghelp.lib" )

int MyReportHook(
				 int   nRptType,
				 char *szMsg,
				 int  *retVal
				 )
{
	::Sleep(1000);
	int * p = 0;
	*p=1000; //-V522
	return( TRUE );         // Allow the report to be made as usual
}

MiniDumper::MiniDumper( )
{

	_CrtSetReportMode(_CRT_WARN, _CRTDBG_MODE_FILE);
	_CrtSetReportFile(_CRT_WARN, _CRTDBG_FILE_STDOUT);
	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_FILE);
	_CrtSetReportFile(_CRT_ERROR, _CRTDBG_FILE_STDOUT);
	_CrtSetReportMode(_CRT_ASSERT, _CRTDBG_MODE_FILE);
	_CrtSetReportFile(_CRT_ASSERT, _CRTDBG_FILE_STDOUT);
	_CrtSetReportHook( MyReportHook );

	::SetUnhandledExceptionFilter( TopLevelFilter );
}

LONG MiniDumper::TopLevelFilter( struct _EXCEPTION_POINTERS *pExceptionInfo )
{
//	return EXCEPTION_EXECUTE_HANDLER;
	CString strDumpName = CString(CSystemConfiguration::get_module_name().c_str()) + ATL::COleDateTime::GetCurrentTime().Format(_T("_%Y-%m-%d_%H-%M-%S")) + _T(".dmp");

	LONG retval = EXCEPTION_CONTINUE_SEARCH;
	HWND hParent = NULL;						// find a better value for your app

	// create the file
	HANDLE hFile = ::CreateFile( strDumpName, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL );

	if (hFile!=INVALID_HANDLE_VALUE)
	{
		_MINIDUMP_EXCEPTION_INFORMATION ExInfo;

		ExInfo.ThreadId = core::support::get_current_thread_id();
		ExInfo.ExceptionPointers = pExceptionInfo;
		ExInfo.ClientPointers = NULL;

		// write the dump
		BOOL bOK = ::MiniDumpWriteDump( GetCurrentProcess(), GetCurrentProcessId(), hFile, MiniDumpWithFullMemory, &ExInfo, NULL, NULL );
		if (bOK)
		{
			retval = EXCEPTION_EXECUTE_HANDLER;
		}
		::CloseHandle(hFile);
	}
	return retval;
}

#include "stdafx.h"
#include "ProcessHelper.h"
#include <boost/filesystem/path.hpp>
#include <Psapi.h>

extern LPCTSTR c_lpszModuleName;

using namespace std;

/* static */
wstring CProcessHelper::GetCurrentModuleName()
{
	wchar_t szModulePath[1024];
    DWORD size = 1024;
	ZeroMemory(szModulePath, sizeof(szModulePath));
	::GetModuleFileName(0, szModulePath, size);
	return wstring(szModulePath);
}

/* static */
wstring CProcessHelper::GetCurrentModuleFileName()
{
	boost::filesystem::path pfn(GetCurrentModuleName());
	return wstring(pfn.filename().c_str());
}

/*static*/
wstring CProcessHelper::GetComputerName()
{
	DWORD dwNameLength = MAX_PATH;
	wchar_t szCounterName[MAX_PATH];
	ZeroMemory(szCounterName, sizeof(szCounterName));
	::GetComputerName(szCounterName, &dwNameLength);

	return wstring(szCounterName);
}

wstring CProcessHelper::ExpandEnvironment(LPCTSTR _lpszPath)
{
	wchar_t szResult[2048];
	ZeroMemory(szResult, sizeof(szResult));
	::ExpandEnvironmentStrings(_lpszPath, szResult, sizeof(szResult) / sizeof(TCHAR));
	return wstring(szResult);
}

wstring CProcessHelper::GetCurrentFolderName()
{
    boost::filesystem::path pfn(GetCurrentModuleName());
    return wstring(pfn.parent_path().c_str());
} 

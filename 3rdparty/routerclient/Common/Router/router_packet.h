#pragma once
#include "client_address.h"
#include "message.h"
#include "serilization_templates.h"

namespace core
{
	enum router_command : uint32_t
	{
		R2R_BEGIN = 0,
		R2R_END = 0,
		
		R2R_UPDATE_ROUTE_TABLE	= R2R_BEGIN,					// ���������� ������ ��������
		R2R_REGISTER_ROUTER		= R2R_UPDATE_ROUTE_TABLE + 1,	// ������ �� ����������� �������
		R2R_REGISTER_CLIENT		= R2R_REGISTER_ROUTER + 1,		// ������ �� ����������� �������
		R2R_REGISTER_ECHO		= R2R_REGISTER_CLIENT + 1,		// ����� �� ������� ������� ��� �����������
		R2R_ROUTE_ERROR			= R2R_REGISTER_ECHO + 1,		// ������ ��������

		R2R_STATUS_NOTICE		= R2R_ROUTE_ERROR + 1,			// ������ �������������� ��������
		R2R_SUBSCRIBE_NOTICE	= R2R_STATUS_NOTICE + 1,		// ��������� � �������� �� ������
		R2R_SUBSCRIBE_ADD		= R2R_SUBSCRIBE_NOTICE + 1,		// �������� �� ������
		R2R_SUBSCRIBE_REMOVE	= R2R_SUBSCRIBE_ADD + 1,		// �������
		R2R_GET_SUBSCRIBE		= R2R_SUBSCRIBE_REMOVE + 1,		// ���� � ��������

		R2R_MONITOR_NOTICE		= R2R_GET_SUBSCRIBE + 1,		// �������
		R2R_MONITOR_ADD			= R2R_MONITOR_NOTICE + 1,		// �������� �� ������
		R2R_MONITOR_REMOVE		= R2R_MONITOR_ADD + 1,			// �������
		USER_BEGIN				= R2R_MONITOR_REMOVE + 1		// �������� �������
	};

	template<typename ValueHolder>
	class router_packet_template
	{
	public:
		router_packet_template() {}

		template<typename FromAddress, typename ToAddress, typename MessageType>
		router_packet_template(FromAddress&& _from, ToAddress&& _to, uint32_t _command, MessageType&& _message) : m_value(std::forward<FromAddress>(_from), std::forward<ToAddress>(_to), _command,	std::forward<MessageType>(_message))
		{
		}

		static constexpr size_t header_size() noexcept
		{
			return client_address::size() * 2 + sizeof(uint32_t) * 2;
		}

		size_t size() const noexcept
		{
			return header_size() + m_value.m_message.size();
		}

		bool is_shutdown_packet() const noexcept
		{
			return command() == router_command::R2R_END && from() == client_address::Invalid() && to() == client_address::Invalid();
		}

		template<typename Archive>
		void save(Archive & ar, const uint32_t _version) const
		{
			ar & uint32_t(size());
			m_value.m_to.save(ar, _version);
			m_value.m_from.save(ar, _version);
			ar & m_value.m_command;
			m_value.m_message.save(ar, _version);
		}

		template<typename Archive>
		void load(Archive & ar, const uint32_t _size)
		{
			uint32_t size;
			ar & size;

			if (size > _size)
				throw incomplete_message_exception(size - _size);

			m_value.m_to.load(ar, _size);
			m_value.m_from.load(ar, _size);
			ar & m_value.m_command;
			m_value.m_message.load(ar, _size - static_cast<uint32_t>(header_size()));
		}

		uint32_t command() const noexcept { return m_value.m_command; }

		message& get_message() noexcept	{ return m_value.m_message; }

		const message& get_message()	const noexcept { return m_value.m_message; }
		const client_address& from()	const noexcept { return m_value.m_from;  }
		const client_address& to()		const noexcept { return m_value.m_to; }

		BOOST_SERIALIZATION_SPLIT_MEMBER()

	private:
		ValueHolder m_value;
	};

	namespace support
	{
		class by_value_holder
		{
			friend class router_packet_template<by_value_holder>;

		public:
			by_value_holder() { }

			template<typename FromAddress, typename ToAddress, typename MessageType>
			by_value_holder(FromAddress&& _from, ToAddress&& _to, uint32_t _command, MessageType&& _message) : m_from(std::forward<FromAddress>(_from)), m_to(std::forward<ToAddress>(_to)), m_command(_command),
				m_message(std::forward<MessageType>(_message))
			{

			}

		private:
			client_address	m_from;
			client_address	m_to;
			uint32_t		m_command = 0;
			message			m_message;
		};

		class by_ref_holder
		{
			friend class router_packet_template<by_ref_holder>;

		public:
			by_ref_holder(const client_address& _from, const client_address& _to, uint32_t _command, const message& _message) : m_from(_from), m_to(_to), m_command(_command), m_message(_message)
			{
			}

		private:
			const client_address&	m_from;
			const client_address&	m_to;
			uint32_t				m_command = 0;
			const message&			m_message;

		};
	};

	using router_packet = router_packet_template<support::by_value_holder>;
	using router_packet_ref = router_packet_template<support::by_ref_holder>;

	inline router_packet CreateShutdownPacket()
	{
		return router_packet(client_address::Invalid(), client_address::Invalid(), router_command::R2R_END, message());
	}
}

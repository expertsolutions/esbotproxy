#pragma once
#include <inttypes.h>
#include <string>
#include <memory>

namespace core
{
	class router_config_interface
	{
	public:
		virtual ~router_config_interface() {}

		virtual uint32_t			get_io_thread_count()	const noexcept = 0;
		virtual uint32_t			get_proc_thread_count()	const noexcept = 0;
		virtual const std::wstring&	get_host()				const noexcept = 0;
		virtual uint16_t			get_port()				const noexcept = 0;
		virtual bool				write_log()				const noexcept = 0;
		virtual uint32_t			keep_alive_timeout()	const noexcept = 0;
		virtual void				next_router() = 0;
	};

	using config_pointer = std::shared_ptr<router_config_interface>;


}

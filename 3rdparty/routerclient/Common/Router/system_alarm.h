#pragma once

#include "router_manager.h"
#include "../Patterns/time_functions.h"
#include <random>
#include <boost/asio/ip/host_name.hpp>

namespace core
{

	enum class alarm_severity
	{
		Critical,
		Major,
		Minor,
		Warning
	};

	class invalid_alarm_exception : public std::runtime_error
	{
	public:
		invalid_alarm_exception(const std::string& _message) : std::runtime_error(_message)
		{
		}
	};

	class system_alarm : public message
	{
	public:
		system_alarm() : message(MessageName), m_flags(0) 
		{
			AddParam(Machine, stow(boost::asio::ip::host_name()));
		}

		virtual ~system_alarm() {}

		system_alarm(const system_alarm& )				= default;
		system_alarm(system_alarm&&)					= default;
		system_alarm& operator = (const system_alarm&)	= default;
		system_alarm& operator = (system_alarm&&)		= default;

		void SetAlarmSource(const std::wstring& _module, const std::wstring& _submodule, const std::wstring& _subsystem)
		{
			(*this)[Module]		= _module;
			(*this)[SubModule]	= _submodule;
			(*this)[SubSystem]	= _subsystem;

			m_flags |= SourceSet;
		}

		void SetAlarmDescription(int64_t _errorCode, const std::wstring& _errorName, const std::wstring& _errorDescription, const std::wstring& _probErrorCause)
		{
			(*this)[ErrorCode]			= _errorCode;
			(*this)[ErrorName]			= _errorName;
			(*this)[ErrorDescription]	= _errorDescription;
			(*this)[ProbErrorCause]		= _probErrorCause;

			m_flags |= ErrorInfoSet;
		}

		void SetSeverity(alarm_severity _severity)
		{
			switch (_severity)
			{
			case alarm_severity::Critical:	(*this)[Severity] = L"Critical";	break;
			case alarm_severity::Major:		(*this)[Severity] = L"Major";		break;
			case alarm_severity::Minor:		(*this)[Severity] = L"Minor";		break;
			case alarm_severity::Warning:	(*this)[Severity] = L"Warning";		break;
			default:						(*this)[Severity] = L"Unknown";		break;
			}

			m_flags |= SeveritySet;
		}

		virtual void Activate(router_manager& _router, client_address address = client_address::All())
		{
			if (m_flags != AllFlagsSet)
				throw invalid_alarm_exception((boost::format("Incomplete alarm information. Severity %s, Source %s, ErrorInfo %s") % 
					get_flag_presence(SeveritySet) % get_flag_presence(SourceSet) % get_flag_presence(ErrorInfoSet)).str());

			auto local_time = boost::posix_time::microsec_clock::local_time();

            auto timeFormat = boost::wformat(L"%02d/%02d/%04d %02d:%02d:%02d") % local_time.date().day() % local_time.date().month() %
				local_time.date().year() % local_time.time_of_day().hours() % local_time.time_of_day().minutes() % local_time.time_of_day().seconds();

			(*this)[ActivationTime]			= timeFormat.str();
			(*this)[ActivationTimestamp]	= support::posixtime_to_uint64(local_time);
			_router.SendTo(*this, address);
		}

		virtual void Deactivate(router_manager& _router, client_address address = client_address::All())	= 0;
	
	public:
		static constexpr auto MessageName			= L"SYSTEM_ALARM";
		static constexpr auto Machine				= L"Machine";
		static constexpr auto Module				= L"Module";
		static constexpr auto SubModule				= L"SubModule";
		static constexpr auto SubSystem				= L"SubSystem";
		static constexpr auto ErrorCode				= L"ErrorCode";
		static constexpr auto ErrorName				= L"ErrorName";
		static constexpr auto ErrorDescription		= L"ErrorDescription";
		static constexpr auto ProbErrorCause		= L"ProbErrorCause";
		static constexpr auto Severity				= L"Severity";
		static constexpr auto ActivationTime		= L"ActivationTime";
		static constexpr auto ActivationTimestamp	= L"ActivationTimestamp";
		static constexpr auto DeactivationTime		= L"DeactivationTime";
		static constexpr auto DeactivationTimestamp = L"DeactivationTimestamp";
		static constexpr auto ImportanceTime		= L"ImportanceTime";
		static constexpr auto ImportanceTimestamp	= L"ImportanceTimestamp";
		static constexpr auto AlarmID				= L"AlarmID";

	private:
		enum AlarmFlags
		{
			SeveritySet = 1,
			SourceSet = 2,
			ErrorInfoSet = 4,
			AllFlagsSet = 7
		};

		string get_flag_presence(AlarmFlags _flag) const
		{
			return (m_flags & _flag) == 0 ? "MISSING" : "SET";
		}

		uint8_t m_flags;
	};

	class permanent_alarm : public system_alarm
	{
		static uint64_t get_random_id()
		{
			static std::random_device device;
			static std::uniform_int_distribution<uint64_t> dist(0, UINT64_MAX);

			return dist(device);
		}
	public:
		permanent_alarm() : m_alarmId(get_random_id())
		{
			(*this)[AlarmID] = m_alarmId;
		}

		permanent_alarm(const permanent_alarm&)					= default;
		permanent_alarm(permanent_alarm&&)						= default;
		permanent_alarm& operator = (const permanent_alarm&)	= default;
		permanent_alarm& operator = (permanent_alarm&&)			= default;

		bool operator == (const permanent_alarm& _alarm) const
		{
			return m_alarmId == _alarm.m_alarmId;
		}

		bool operator == (uint64_t _alarmId) const
		{
			return m_alarmId == _alarmId;
		}

		bool operator != (const permanent_alarm& _alarm) const
		{
			return m_alarmId != _alarm.m_alarmId;
		}

		virtual ~permanent_alarm() {}

		virtual void Deactivate(router_manager& _router, client_address address = client_address::All()) override
		{
			auto local_time = boost::posix_time::microsec_clock::local_time();

			auto timeFormat = boost::wformat(L"%02d/%02d/%04d %02d:%02d:%02d") % local_time.date().day() % local_time.date().month() %
				local_time.date().year() % local_time.time_of_day().hours() % local_time.time_of_day().minutes() % local_time.time_of_day().seconds();

			(*this)[DeactivationTime]		= timeFormat.str();
			(*this)[DeactivationTimestamp]	= core::support::posixtime_to_uint64(local_time);

			_router.SendTo(*this, address);
		}

	private:
		uint64_t m_alarmId;
	};

	class temporary_alarm : public system_alarm
	{
	public:
		temporary_alarm()										= default;
		temporary_alarm(const temporary_alarm&)					= default;
		temporary_alarm(temporary_alarm&&)						= default;
		temporary_alarm& operator = (const temporary_alarm&)	= default;
		temporary_alarm& operator = (temporary_alarm&&)			= default;

		virtual ~temporary_alarm() {}

		template<typename TimeType>
		void SetImportanceTime(const TimeType& _value)
		{
			using namespace std::chrono;
			(*this)[ImportanceTime] = core::support::duration_to_string(_value);
			(*this)[ImportanceTimestamp] = duration_cast<milliseconds>(_value).count();
		}

		virtual void Deactivate(router_manager&, client_address address = client_address::All()) override
		{
			throw invalid_alarm_exception("Temporary alarms can't be deactivated");
		}
	};
}



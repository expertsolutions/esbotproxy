#pragma once
#include <inttypes.h>
#include "message_parameter_types.h"
#include "../Patterns/time_functions.h"

namespace core
{
	namespace support
	{
		enum class variant_type : uint16_t
		{
			variant_null = 0,
			variant_int = 20,
			variant_uint = 21,
			variant_float = 4,
			variant_double = 5,
			variant_string = 8,
			variant_date = 7,
		};

		template<typename Archive>
		class vt_serialize_visitor : public boost::static_visitor<void>
		{
			Archive& m_archive;

		public:
			vt_serialize_visitor(Archive& _arch) : m_archive(_arch) {}

			void operator()(float value) const
			{
				m_archive & support::variant_type::variant_float;
			}

			void operator()(double value) const
			{
				m_archive & support::variant_type::variant_double;
			}

			void operator()(int64_t value) const
			{
				m_archive & support::variant_type::variant_int;
			}

			void operator()(const std::wstring& value) const
			{
				m_archive & support::variant_type::variant_string;
			}

			void operator()(const time_type& value) const
			{
				m_archive & support::variant_type::variant_date;
			}

			void operator()(const raw_data_type& value) const
			{
				m_archive & support::variant_type::variant_null;
			}
		};

		template<typename Archive>
		class value_serialize_visitor : public boost::static_visitor<void>
		{
			Archive& m_archive;
		public:
			value_serialize_visitor(Archive& _arch) : m_archive(_arch) {}

			~value_serialize_visitor()
			{
			}

			void operator()(float value) const
			{
				m_archive & value;

				uint32_t nFiller1(0);
				m_archive & nFiller1;
				serialize_filler();
			}

			void operator()(double value) const
			{
				m_archive & value;
				serialize_filler();
			}

			void operator()(int64_t value) const
			{
				m_archive & value;
				serialize_filler();
			}

			void operator()(const std::wstring& value) const
			{
				serialize_filler();
				support::wstring_read_write_support::save(m_archive, value);
				serialize_filler();
			}

			void operator()(const time_type& value) const
			{
				m_archive & core::support::to_variant_time(value);
				m_archive & core::support::get_milliseconds(value);
			}

			void operator()(const raw_data_type& value) const
			{
				serialize_filler();
				support::raw_data_read_write_support::save(m_archive, value);
				serialize_filler();
			}

			void serialize_filler() const
			{
				uint64_t nFiller(0);
				m_archive & nFiller;
			}

		};

	}
}
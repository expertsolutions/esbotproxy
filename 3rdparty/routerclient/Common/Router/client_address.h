#pragma once

#include <stdint.h>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/format.hpp>
#include "../Patterns/binary_serializer.h"

namespace core
{
	class client_address
	{
	public:
		client_address() noexcept : m_qwAddress(-1) {} //-V730
		explicit client_address(uint64_t _address) : m_dwClient(static_cast<uint32_t>(_address >> 32)), m_dwChild(static_cast<uint32_t>(_address)) {} //-V730

		client_address(uint32_t _clientId, uint32_t _childId) noexcept : m_dwClient(_clientId), m_dwChild(_childId) {} //-V730

		client_address(const client_address&)				= default;
		client_address(client_address&&)					= default;
		client_address& operator = (const client_address&)	= default;
		client_address& operator = (client_address&&)		= default;

		static client_address All()					noexcept { return client_address(0, 0); }
		static client_address AllAndSelf()			noexcept { return client_address(1, 0); }
		static client_address Any()					noexcept { return client_address(2, 0); }
		static client_address AnyAndSelf()			noexcept { return client_address(3, 0); }
		static client_address RemoteRegion()		noexcept { return client_address(4, 0); }
		static client_address Invalid()				noexcept { return client_address(); }
		static constexpr uint32_t NoChild()			noexcept { return 0; }

		bool operator == (const client_address& _client) const noexcept
		{
			return m_qwAddress == _client.m_qwAddress;
		}

		bool operator != (const client_address& _client) const noexcept
		{
			return m_qwAddress != _client.m_qwAddress;
		}

		bool operator < (const client_address& _client) const noexcept
		{
			return m_qwAddress < _client.m_qwAddress;
		}

		uint32_t	GetRegionID() const			noexcept { return m_dwClient >> 16; }
		uint32_t	GetClientID() const			noexcept { return m_dwClient; }
		uint32_t	GetChildID() const			noexcept { return m_dwChild; }
		uint32_t	SetClientID(uint32_t cl)	noexcept { return m_dwClient = cl; }
		uint32_t	SetChildID(uint32_t ch)		noexcept { return m_dwChild = ch; }

		uint64_t	GetAsQWORD() const			noexcept { return m_qwAddress; }
		uint64_t	SetAsQWORD(uint64_t qw)		noexcept { return m_qwAddress = qw; }
		int64_t		AsMessageParamterInt()  const { return int64_t(m_dwClient) << 32 | int64_t(m_dwChild); }

#ifdef _MSC_VER
		__declspec(property(get = GetClientID, put = SetClientID))	uint32_t	ClientID;
		__declspec(property(get = GetChildID, put = SetChildID))	uint32_t	ChildID;

		__declspec(property(get = GetClientID, put = SetClientID))	uint32_t	dwClientID;
		__declspec(property(get = GetChildID, put = SetChildID))	uint32_t	dwChildID;
		__declspec(property(get = GetAsQWORD, put = SetAsQWORD))	uint64_t	AsQWORD;
#endif

		template<typename Archive>
		void save(Archive & ar, const unsigned int _version) const
		{
			ar & m_qwAddress;
		}

		template<typename Archive>
		void load(Archive & ar, const unsigned int _version)
		{
			ar & m_qwAddress;
		}
		
		template<typename binary_stream, typename this_type>
		static binary_stream& serialize_binary(binary_stream& _stream, this_type&& _value)
		{
			_stream & _value.m_qwAddress;
			return _stream;
		}

		static constexpr size_t size() { return sizeof(m_qwAddress); }

		std::wstring to_string() const 
		{ 
			return (boost::wformat(L"0x%08X-%08X") % m_dwClient % m_dwChild).str(); 
		}

		std::wstring to_hex_string() const
		{
			return (boost::wformat(L"%08X%08X") % m_dwClient % m_dwChild).str();
		}

		BOOST_SERIALIZATION_SPLIT_MEMBER()

	private:
		union
		{
			uint64_t m_qwAddress;

			struct 
			{
				uint32_t	m_dwClient;
				uint32_t	m_dwChild;
			};

		};
	};
}

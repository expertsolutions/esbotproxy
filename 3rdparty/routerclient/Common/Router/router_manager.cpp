#include "stdafx.h"
#include "router_manager.h"
#include "../Patterns/string_functions.h"
#include <algorithm>
#include <boost/iostreams/device/array.hpp>
#include "internal_io_service.h"
#include "external_io_service.h"

const std::wstring core::router_manager::module_name(L"Router Manager");

using namespace core::support;

core::router_manager::router_manager(ConnectHandler _handler, config_pointer _config, log_pointer _log) : m_config(_config), m_log(_log), m_externalRunner(false), 
m_runner(std::make_shared<internal_io_service>(*this, m_config->get_io_thread_count(), m_config->get_proc_thread_count())), m_clientId(client_address::Invalid().GetClientID()), m_state(State::initalizing), 
m_connectHandler(_handler) //-V730
{
	reconnect();
}

core::router_manager::router_manager(support::router_io_service_ptr _io_runner, config_pointer _config, log_pointer _log) : m_config(_config), m_log(_log), m_externalRunner(true), m_runner(_io_runner), m_clientId(0), m_state(State::initalizing) //-V730
{
	reconnect();
}

core::router_manager::router_manager(io_service& _io_service, ConnectHandler _handler, config_pointer _config, log_pointer _log) : m_config(_config), m_log(_log), m_externalRunner(true), 
m_runner(std::make_shared<external_io_service>(*this, _io_service)), m_clientId(0), m_state(State::initalizing), m_connectHandler(_handler) //-V730
{
	reconnect();
}

core::router_manager::~router_manager()
{
	disconnect_from_router();
}

void core::router_manager::disconnect_from_router()
{
	if (m_state == State::connected || m_state == State::initalizing)
	{
		m_state = State::exiting;
		m_connection->disconnect();

		while (m_state != State::exited)
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
		}
	}
}

void core::router_manager::add_unsent_packets(packet_queue&& _unsent)
{
	// swap() used here, because there is no easy way to clear queue, without accessing underlying container
	if (!m_unsent.empty()) {
		core::packet_queue empty;
		m_unsent.swap(empty);
	}
	
	// filter unsent packets to not to send R2R_REGISTER_CLIENT twice, because it breaks logic
    while (!_unsent.empty()) 
    {
        auto data = _unsent.front();
        _unsent.pop();
        auto packet = support::deserialize<router_packet>(data, data.size());
        if (packet.command() != R2R_REGISTER_CLIENT) 
        {
            m_unsent.push(data);
        }
    }
}

void core::router_manager::send_handshake() const
{
	message msg;
	if(client_address::Invalid().GetClientID() != m_clientId)
		msg[L"client_id"] = m_clientId;

	m_connection->send(support::serialize(router_packet(client_address::All(), client_address::All(), router_command::R2R_REGISTER_CLIENT, std::move(msg))));
}

void core::router_manager::on_receive(router_packet&& _packet)
{
	m_runner->queuePacket(std::move(_packet));
}

bool core::router_manager::processPacket(router_packet& packet) 
{
	if (packet.is_shutdown_packet())
		return false;

	switch (packet.command())
	{
	case router_command::R2R_REGISTER_ECHO:
		process_connect(packet);
		break;
	case router_command::R2R_STATUS_NOTICE:
		process_status_change(packet);
		break;
	case router_command::R2R_SUBSCRIBE_NOTICE:
		process_subscribe_notice(packet);
		break;
	case router_command::R2R_ROUTE_ERROR:
		process_deliver_error(packet.get_message(), packet.from(), packet.to());
		break;
	default:
		process_message(packet);
		break;
	}

	return true;
}

void core::router_manager::process_connect(router_packet& packet)
{
	m_state = State::connected;

	m_clientId = packet.get_message()[L"client_id"].AsInt();
	log(LEVEL_INFO, L"[CONNECT] [%s:%s] connected. Client ID [0x%08X] set", m_config->get_host(), m_config->get_port(), m_clientId);

	if (m_connectHandler)
		m_connectHandler(m_config->get_host(), m_config->get_port());
	
	Lock lock(m_access);
	for(const auto& event : m_subscription)
	{
		send_subscribe(event, router_command::R2R_SUBSCRIBE_ADD);
	}

	while(!m_unsent.empty())
	{ 
		m_connection->send(std::move(m_unsent.front()));
		m_unsent.pop();
	}
}

void core::router_manager::process_status_change(router_packet& packet) const
{
	auto clientId = packet.get_message()[L"client_id"].AsInt();
	auto status = static_cast<ClientStatus>(packet.get_message()[L"status"].AsInt());
	log(LEVEL_INFO, L"[STATUS_CHANGE] Client ID [0x%08X] status change to [%s]", clientId, status == ClientStatus::CS_CONNECTED ? L"CONNECTED" : L"DISCONNECTED");

	if (m_statusChangeHandler)
	{
		m_statusChangeHandler(clientId, status);
	}
}

void core::router_manager::process_subscribe_notice(router_packet& packet) const
{
	const message& msg		= packet.get_message();

	int		nConsumers	= msg[L"consumers"].AsInt();
	auto	evtHash		= msg[L"event"].AsUInt();
	auto	status		= static_cast<SubscriptionStatus>(msg[L"status"].AsInt());

	log(LEVEL_INFO, L"[EVENT_SUBSCRIBE] Event [0x%08X] subscription status: [%i], Consumers: %i",  evtHash, int(status), nConsumers);

	if (nConsumers == 0)
		return;

	if (!m_eventSubscribeHandler)
		return;

	auto	consumers = msg[L"who"].AsVector<uint32_t>();
	for (auto client : consumers)
	{
		m_eventSubscribeHandler(evtHash, client_address(client, client_address::NoChild()), status, nConsumers);
	}
}

void core::router_manager::process_message(router_packet& packet) const
{
	using namespace std::chrono;
	auto start = system_clock::now();

	log_message(LEVEL_INFO, L"[RECEIVE]", packet.get_message(), packet.from(), packet.to());

	auto handler = m_handlers.find(packet.get_message().GetName());
	if (handler != m_handlers.end())
	{
		try
		{
			handler->second(packet.get_message(), packet.from(), packet.to());
		}
		catch (std::exception& ex)
		{
			m_log->write_log(LEVEL_WARNING, (boost::wformat(L"[RECEIVE] Exception [%s] while processing message %s") % stow(ex.what()) % packet.get_message().Dump()).str());
		}
		catch (...)
		{
			m_log->write_log(LEVEL_WARNING, (boost::wformat(L"[RECEIVE] Unknown exception while processing message %s") % packet.get_message().Dump()).str());
		}
	}
	else
	{
		try
		{
			if (m_defaultHandler)
			{
				m_defaultHandler(packet.get_message(), packet.from(), packet.to());
			}
		}
		catch (boost::bad_function_call&)
		{
			log(LEVEL_FINEST, L"[RECEIVE] Default handler for [%s] not found", packet.get_message().GetName());
		}
		catch(std::exception& ex)
		{
			m_log->write_log(LEVEL_WARNING, (boost::wformat(L"[RECEIVE] Exception [%s] while processing message %s") % stow(ex.what()) % packet.get_message().Dump()).str());
		}
		catch(...)
		{
			m_log->write_log(LEVEL_WARNING, (boost::wformat(L"[RECEIVE] Unknown exception while processing message %s") % packet.get_message().Dump()).str());
		}
	}

	log(LEVEL_FINE, L"Processing of [%s] completed in %i ms", packet.get_message().GetName(), duration_cast<milliseconds>(system_clock::now() - start).count());
}

void core::router_manager::on_connect()
{
	m_log->write_log(LEVEL_INFO, (boost::wformat(L"[CONNECT] [%s:%i] Sending handshake message...") % m_config->get_host() % m_config->get_port()).str());
	send_handshake();
}

void core::router_manager::on_disconnect(packet_queue&& _unsent)
{
	bool blReconnect = true;

	if (m_disconnectHandler)
	{
		m_disconnectHandler(m_config->get_host(), m_config->get_port(), blReconnect);
		if (!blReconnect)
			m_state = State::exiting;
	}

	switch (m_state)
	{
	case State::disconnected:
	case State::initalizing:
		m_config->next_router();
		break;
	case State::exiting:
		m_log->write_log(LEVEL_INFO, (boost::wformat(L"[SHUTDOWN] Disconnected from [%s:%i]. Router connection shut down.") % m_config->get_host() % m_config->get_port()).str());
		m_state = State::exited;
		return;
	case State::connected:
		m_state = State::disconnected;
		m_log->write_log(LEVEL_SEVERE, (boost::wformat(L"[DISCONNECT] Disconnected from [%s:%i]") % m_config->get_host() % m_config->get_port()).str());
		break;
	}

	if (blReconnect)
	{
		add_unsent_packets(std::move(_unsent));
		reconnect();
	}
}

void core::router_manager::reconnect()
{
	m_connection = std::make_unique<router_connection>(*this, m_runner->getService(), m_config->get_host(), m_config->get_port(), std::move(m_unsent), m_config->keep_alive_timeout());
	m_connection->connect();
}

void core::router_manager::on_error(boost::system::error_code _ec)
{
	log(LEVEL_SEVERE, L"[Router error] Code: 0x%08X, Description: %s", _ec.value(), stow(_ec.message()));
}


uint32_t core::router_manager::hash(const std::wstring& _name)
{
	uint32_t dwHash = 0;
	for(auto chr : _name)
		dwHash = (dwHash << 5) + dwHash + ((0x00FF) & chr);

	if (dwHash < router_command::USER_BEGIN)
		dwHash = router_command::USER_BEGIN;
	return dwHash;
}

void core::router_manager::SubscribeOnEvent(const std::wstring& _messageName)
{
	log(LEVEL_INFO, L"[SUBSCRIBE] Name: %s", _messageName);

	if (Connected())
	{
		send_subscribe(_messageName, router_command::R2R_SUBSCRIBE_ADD);
	}

	Lock lock(m_access);
	if (std::find(m_subscription.cbegin(), m_subscription.cend(), _messageName) == m_subscription.cend())
		m_subscription.push_back(_messageName);
}

void core::router_manager::send_subscribe(const std::wstring& _messageName, router_command _command) const
{
	message subscribe;
	subscribe[L"event"] = hash(_messageName);
	m_connection->send(support::serialize(router_packet(Self(), client_address::All(), _command, std::move(subscribe))));
}

void core::router_manager::UnsubscribeFromEvent(const std::wstring& _messageName)
{
	log(LEVEL_INFO, L"[UNSUBSCRIBE] Name: %s", _messageName);

	if (Connected())
	{
		send_subscribe(_messageName, router_command::R2R_SUBSCRIBE_REMOVE);
	}

	Lock lock(m_access);
	auto i = std::find(m_subscription.cbegin(), m_subscription.cend(), _messageName);
	if (i != m_subscription.cend())
		m_subscription.erase(i);
}

void core::router_manager::RegisterHandler(const std::wstring& _messageName, const MessageHandler& _handler)
{
	Lock lock(m_access);
	m_handlers.insert(std::pair<std::wstring, MessageHandler>(_messageName, _handler));
}

void core::router_manager::UnregisterHandler(const std::wstring& _messageName)
{
	Lock lock(m_access);
	m_handlers.erase(_messageName);
}

void core::router_manager::RegisterSubscription(const std::wstring& _messageName, const MessageHandler& _handler)
{
	RegisterHandler(_messageName, _handler);
	SubscribeOnEvent(_messageName);
}

void core::router_manager::UnregisterSubscription(const std::wstring& _messageName)
{
	UnregisterHandler(_messageName);
	UnsubscribeFromEvent(_messageName);
}

#pragma once

#include "default_router.h"

namespace core
{
	namespace test
	{
		using namespace std;
		using namespace std::chrono;

		class test_timeout_error : public runtime_error
		{
		public:
			test_timeout_error() : runtime_error("Test timer expired") {}
		};

		class router_test_client : public router_manager
		{
		public:
			router_test_client(router_manager::MessageHandler _func, milliseconds _waitPeriod, config_pointer _config = create_default_config(), log_pointer _log = create_default_log()) :
				router_manager(_config, _log), m_func(_func), m_done(false), m_start(core::support::now()), m_testDuration(_waitPeriod)
			{
				while (BoundAddress() == 0)
				{
					std::this_thread::sleep_for(std::chrono::milliseconds(100));
					check_time_expired();
				}
			}

			router_test_client(const wstring& _messageName, router_manager::MessageHandler _func, milliseconds _waitPeriod, config_pointer _config = create_default_config(), log_pointer _log = create_default_log()) :
				router_manager(_config, _log), m_func(_func), m_done(false), m_start(core::support::now()), m_testDuration(_waitPeriod)
			{
				while (!Connected())
				{
					std::this_thread::sleep_for(std::chrono::milliseconds(100));
					check_time_expired();
				}
				core::SubscribeMessage(*this, _messageName, this, &router_test_client::MessageHandler);
			}

			void check_time_expired() const
			{
				if(core::support::now() - m_start > m_testDuration)
					throw test_timeout_error();
			}

			void wait_test_result()
			{
				m_start = core::support::now();

				while(!m_done)
				{ 
					std::this_thread::sleep_for(std::chrono::milliseconds(100));
					check_time_expired();
				}
			}

			~router_test_client()
			{
			}

		private:
			void MessageHandler(message& _msg, const client_address& _from, const client_address& _to)
			{
				m_func(_msg, _from, _to);
				m_done = true;
			}

			router_manager::MessageHandler	m_func;
			atomic<bool>					m_done;
			time_type						m_start;
			milliseconds					m_testDuration;
		};
	}
}



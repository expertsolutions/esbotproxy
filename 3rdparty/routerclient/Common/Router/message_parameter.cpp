#include "stdafx.h"
#include "message_parameter.h"
#include <algorithm>
#include "message.h"
#include <boost/algorithm/string.hpp>

core::message_parameter::message_parameter(uint8_t * _data, raw_data_type::size_type _length)
{
	SetRawData(_data, _length);
}


core::message_parameter::message_parameter(const raw_data_type& _data) : m_value(_data)
{
}


core::message_parameter::message_parameter(raw_data_type&& _data) : m_value(std::move(_data))
{
}

std::wstring core::message_parameter::AsWideStr() const
{
	if (IsString())
		return GetValue<std::wstring>();
	else
	{
		std::wostringstream out;
		boost::apply_visitor(support::string_convert_visitor<std::wostringstream>(out), m_value);
		return out.str();
	}
}

bool core::message_parameter::Check(checked_type _type) const
{
	switch (_type)
	{
	case checked_type::Bool:
	case checked_type::Int:
	case checked_type::Int64:
		return IsInt64();
	case checked_type::String:
		return IsString();
	case checked_type::DateTime:
		return IsDateTime();
	case checked_type::RawData:
		return IsRawData();
	case checked_type::Float:
		return IsFloat();
	case checked_type::Double:
		return IsDouble();
	}

	return false;
}

core::message core::message_parameter::AsMessage() const 
{
	if (!IsRawData())
		throw invalid_value_type_exception();
	const auto& rawData = GetRawData();
	return message::deserialize(rawData, rawData.size());
}

std::vector<core::message> core::message_parameter::AsMessagesVector() const
{
	if (!IsRawData())
		throw invalid_value_type_exception();

	const auto& rawData = GetRawData();
	return from_raw_data<message>(rawData);
}

core::message_parameter& core::message_parameter::operator= (const core::message& _msg)
{
	SetValue(_msg.serialize());
	return *this;
}

std::wstring core::message_parameter::AsJsonStr() const
{
	auto result = AsWideStr();
	boost::replace_all(result, L"\n", L"");
	boost::replace_all(result, L"\r", L"");
	boost::replace_all(result, L"\\", L"\\\\");
	return result;
}


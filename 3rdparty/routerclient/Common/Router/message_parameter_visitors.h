#pragma once
#include <boost/variant.hpp>
#include "message_parameter_types.h"
#include "../Patterns/time_functions.h"

namespace core
{
	namespace support
	{
		class raw_data_type_size_visitor : public boost::static_visitor<raw_data_type::size_type>
		{
		public:
			raw_data_type::size_type operator()(const raw_data_type& _data) const noexcept
			{
				return _data.size();
			}

			template<typename T>
			raw_data_type::size_type operator()(const T&) const noexcept
			{
				return 0;
			}
		};

		class raw_data_content_visitor : public boost::static_visitor<const raw_data_type&>
		{
		public:
			raw_data_content_visitor() {}

			const raw_data_type& operator()(const raw_data_type& _data) const
			{
				return _data;
			}

			template<typename T>
			const raw_data_type& operator()(const T&) const
			{
				throw invalid_value_type_exception();
			}
		};

		template<typename Stream>
		class dump_visitor : public boost::static_visitor<void>
		{
			Stream& m_out;

		public:
			dump_visitor(Stream& _out) : m_out(_out) {}

			template<typename T>
			void operator()(const T& _value) const
			{
				m_out << _value;
			}

			void operator()(const std::wstring& _value) const
			{
				m_out << L'"' << _value << L'"';
			}

			void operator()(const time_type& _value) const
			{
				m_out << L'"' << _value << L'"';
			}

			void operator()(const int64_t _value) const
			{
				m_out << (boost::wformat(L"0x%08X-%08X") % uint32_t(_value >> 32) % static_cast<uint32_t>(_value)).str();
			}

			void operator()(const raw_data_type& _value) const
			{
				m_out << L"[Raw data array. Length: " << _value.size() << L"]";
			}
		};

		class type_info_visitor : public boost::static_visitor<std::wstring>
		{

		public:
			type_info_visitor() {}

			std::wstring operator()(double _value) const
			{
				return L"double";
			}

			std::wstring operator()(float _value) const
			{
				return L"float";
			}

			std::wstring operator()(const std::wstring& _value) const
			{
				return L"";
			}

			std::wstring operator()(const time_type& _value) const
			{
				return L"time";
			}

			std::wstring operator()(const int64_t _value) const
			{
				return L"int";;
			}

			std::wstring operator()(const raw_data_type& _value) const
			{
				return L"raw";
			}
		};

		class length_visitor : public boost::static_visitor<size_t>
		{
		public:
			static constexpr size_t s_nParameterLength = 0x68;

			template<typename T>
			size_t operator()(const T& _value) const noexcept
			{
				return s_nParameterLength;
			}

			size_t operator()(const std::wstring& _value) const noexcept
			{
				return s_nParameterLength + (_value.length() + 1) * sizeof(uint16_t);
			}

			size_t operator()(const raw_data_type& _value) const noexcept
			{
				return s_nParameterLength + _value.size();
			}

		};

		template<typename Stream>
		class string_convert_visitor : public boost::static_visitor<void>
		{
			Stream& m_out;
		public:
			string_convert_visitor(Stream& _out) : m_out(_out) {}

			template<typename T>
			void operator()(const T& _value) const
			{
				m_out << _value;
			}

			void operator()(const raw_data_type& _value) const
			{
				encode_to_hex(m_out, _value, L'0');
			}
		};


		inline raw_data_type raw_from_string(const std::wstring& _value)
		{
			raw_data_type result;

			for (unsigned int i = 0; i < _value.length(); i += 2) 
			{
				std::wstring byteString = _value.substr(i, 2);
				raw_data_type::value_type byte = static_cast<raw_data_type::value_type>(wcstoll(byteString.c_str(), nullptr, 16));
				result.push_back(byte);
			}
			return result;
		}

	}
}

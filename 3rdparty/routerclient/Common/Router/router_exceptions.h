#pragma once
#include <boost/format.hpp>
#include "../Patterns/string_functions.h"

namespace core
{
	class router_exception : public std::runtime_error
	{
	public:
		router_exception(const std::string _what) : std::runtime_error(_what) {}
	};

	class invalid_header_exception : public router_exception
	{
	public:
		invalid_header_exception() : router_exception("Invalid message header") {}
	};

	class incomplete_message_exception : public router_exception
	{
		size_t	m_size;
	public:
		incomplete_message_exception(size_t _size) : router_exception("Message incomplete"), m_size(_size) {}
		size_t bytes_to_read() const { return m_size; }
	};

	class message_parameter_exception : public router_exception
	{
	public:
		message_parameter_exception(const std::string _what) : router_exception(_what) {}
	};

	class invalid_value_type_exception : public message_parameter_exception
	{
	public:
		invalid_value_type_exception(const std::string& _what = "Type doesn't match") : message_parameter_exception(_what) {}
	};

	class message_parameter_missing_exception : public message_parameter_exception
	{
	public:
		message_parameter_missing_exception(const std::wstring& _parameterName) : message_parameter_exception((boost::format("Parameter [%s]. Parameter doesn't exist") % wtos(_parameterName)).str()) {}
	};

	class message_parameter_invalid_type_exception : public invalid_value_type_exception
	{
	public:
		message_parameter_invalid_type_exception(const std::wstring& _parameterName) : invalid_value_type_exception((boost::format("Parameter [%s]. Invalid type") % wtos(_parameterName)).str()) {}
	};

	class message_parameter_invalid_value_exception : public message_parameter_exception
	{
	public:
		message_parameter_invalid_value_exception(const std::wstring& _parameterName) : message_parameter_exception((boost::format("Parameter [%s]. Invalid value") % wtos(_parameterName)).str()) {}
	};

}
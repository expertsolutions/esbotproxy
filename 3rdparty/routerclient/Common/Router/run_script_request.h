#pragma once

#include "router_manager.h"

namespace core
{
	using namespace std;

	class run_script_request_base : public message
	{
	protected:
		run_script_request_base(const wstring& _messageName, const wstring& _path, const wstring& _monitorName) : message(_messageName)
		{
			AddParam(L"FileName", _path);
			AddParam(L"MonitorDisplayedName", _monitorName);
			AddParam(s_runRequestIdParameter, s_requestCounter.fetch_add(1));
		}

		run_script_request_base(const wstring& _messageName, const wstring& _path, const wstring& _monitorName, uint32_t _routerAddress) : message(_messageName)
		{
			AddParam(L"FileName", _path);
			AddParam(L"MonitorDisplayedName", _monitorName);
			AddParam(s_runRequestIdParameter, client_address(_routerAddress, static_cast<uint32_t>(s_requestCounter.fetch_add(1))));
		}

		template<typename Message>
		run_script_request_base(const wstring& _messageName, const wstring& _path, const wstring& _monitorName, Message&& _additionalParams) : message(std::forward<Message>(_additionalParams))
		{
			SetName(_messageName);
			AddParam(L"FileName", _path);
			AddParam(L"MonitorDisplayedName", _monitorName);
			AddParam(s_runRequestIdParameter, s_requestCounter.fetch_add(1));
		}

	public:
		client_address GetRunRequestId() { return this->operator[](s_runRequestIdParameter).AsAddress(); }

		static const wstring&		RunRequestID() { return s_runRequestIdParameter; }
		static core::checked_type	RunRequestIDType() { return core::checked_type::Int64; }

		static const wstring&		RunScriptOkMessageName() { return s_runScriptOkMessageName; }
		static const wstring&		RunScripFailMessageName() { return s_runScriptFailMessageName; }

	private:
		static atomic<uint64_t> s_requestCounter;
		static const wstring s_runRequestIdParameter;
		static const wstring s_runScriptOkMessageName;
		static const wstring s_runScriptFailMessageName;
	};

	class transit_agent_run_script_request : public run_script_request_base
	{
	public:
		transit_agent_run_script_request(const wstring& _path, const wstring& _monitorName) : run_script_request_base(L"ANY2TA_RUN_SCRIPT", _path, _monitorName)
		{
		}

		transit_agent_run_script_request(const wstring& _path, const wstring& _monitorName, uint32_t _routerAddress) : run_script_request_base(L"ANY2TA_RUN_SCRIPT", _path, _monitorName, _routerAddress)
		{
		}

		template<typename Message>
		transit_agent_run_script_request(const wstring& _path, const wstring& _monitorName, Message&& _additionalParams) : run_script_request_base(L"ANY2TA_RUN_SCRIPT", _path, _monitorName, std::forward<Message>(_additionalParams))
		{
		}
	};

	class dispatcher_run_script_request : public run_script_request_base
	{
	public:
		dispatcher_run_script_request(const wstring& _path, const wstring& _monitorName) : run_script_request_base(L"TA2D_RUNSCRIPT", _path, _monitorName)
		{
			AddParam(L"InitialMessage", wstring(L"MAIN"));
		}

		dispatcher_run_script_request(const wstring& _path, const wstring& _monitorName, uint32_t _routerAddress) : run_script_request_base(L"TA2D_RUNSCRIPT", _path, _monitorName, _routerAddress)
		{
			AddParam(L"InitialMessage", wstring(L"MAIN"));
		}

		template<typename Message>
		dispatcher_run_script_request(const wstring& _path, const wstring& _monitorName, Message&& _additionalParams) : run_script_request_base(L"TA2D_RUNSCRIPT", _path, _monitorName, std::forward<Message>(_additionalParams))
		{
			AddParam(L"InitialMessage", wstring(L"MAIN"));
		}
	};

	using run_script_request = transit_agent_run_script_request;
}
#include "stdafx.h"
#include "default_router_config.h"

core::default_router_config::default_router_config(const std::wstring& _nodeName) :
	m_threadCount(m_config->safe_get_config_int(L"RouterThreads", 4)),
	m_blWriteLog(m_config->safe_get_config_bool(L"RouterLog", false)),
	m_keepAliveTimeout(m_config->safe_get_config_int(L"RouterKeepAliveTimeout", 30000))
{
	int threadCount = (m_threadCount / 2) == 0 ? 1 : (m_threadCount / 2);

	try
	{
		auto routers_node = m_config->get_config_node(_nodeName);
		for (const auto& child : routers_node)
		{
			try
			{
				m_routers.emplace_back(
					m_config->get_config_attr<std::wstring>(child.second, L"Host"), 
					m_config->safe_get_config_attr<uint16_t>(child.second, L"Port", 50000),
					m_config->safe_get_config_attr<uint32_t>(child.second, L"IoThreadCount", threadCount),
					m_config->safe_get_config_attr<uint32_t>(child.second, L"ProcThreadCount", threadCount),
					m_config->safe_get_config_attr<bool>(child.second, L"RouterLog", m_blWriteLog),
					m_config->safe_get_config_attr<uint32_t>(child.second, L"KeepAliveTimeout", m_keepAliveTimeout)
					);
			}
			catch (configuration_error&)
			{ //-V565
			}
		}
	}
	catch (configuration_error&)
	{
		m_routers.emplace_back(std::wstring(L"127.0.0.1"), 50000, threadCount, threadCount, true, 30000);
	}
}

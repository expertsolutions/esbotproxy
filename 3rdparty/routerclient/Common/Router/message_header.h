#pragma once
#include <stdint.h>
#include <boost/serialization/serialization.hpp>
#include "router_exceptions.h"
#include "../Patterns/binary_serializer.h"

namespace core
{
	class message_header
	{
		friend class boost::serialization::access;

	public:
		message_header() {}
		message_header(uint32_t _size, uint32_t _messageId) : m_signature(m_messageSignature), m_totalSize(_size), m_messageId(_messageId) { }

		template<typename binary_stream, typename this_type>
		static binary_stream& serialize_binary(binary_stream& _stream, this_type&& _value)
		{
			_stream & _value.m_signature;
			_stream & _value.m_totalSize;
			_stream & _value.m_messageId;
			return _stream;
		}

		template<typename Archive>
		void serialize(Archive & ar, const unsigned int file_version)
		{
			ar & m_signature;
			ar & m_totalSize;
			ar & m_messageId;
		}

		bool		is_valid() const noexcept	{ return m_messageSignature == m_signature; }
		uint32_t	total_size() const noexcept { return m_totalSize;  }
	
	private:
		static constexpr uint64_t m_messageSignature = 0x34DF7A6B994DACFF;

		uint64_t m_signature = m_messageSignature;
		uint32_t m_totalSize = 0;
		uint32_t m_messageId = 0;
	};
}

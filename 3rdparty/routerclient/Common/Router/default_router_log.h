#pragma once
#include "../Log/log_interface.h"
#include "../Log/SystemLog.h"
#include <boost/algorithm/string.hpp>

namespace core
{
	class default_router_log : public log_interface
	{
		SystemLog m_log;
	public:
		default_router_log() {}
		virtual ~default_router_log() {}

		virtual bool check_severity(LogLevel _level) override { return _level >= m_log->GetLevel(); }
		virtual void write_log(LogLevel _level, const std::wstring& _logEntry) override
		{
			m_log->LogStringModule(_level, L"Router Manager", boost::replace_all_copy(_logEntry, L"%", L"%%"));
		}
	};

	inline log_pointer create_default_log() { return std::make_shared<default_router_log>(); }
}



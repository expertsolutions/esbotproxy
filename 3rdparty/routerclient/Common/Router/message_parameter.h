#pragma once

#include <string>
#include <memory>
#include <vector>
#include <stdint.h>
#include <boost/format.hpp>
#include <boost/variant.hpp>
#include "../Patterns/string_functions.h"
#include <limits>
#include <sstream>
#include <iomanip>
#include "../Patterns/time_functions.h"
#include "router_exceptions.h"
#include <boost/serialization/serialization.hpp>
#include "read_write_support.h"
#include "variant_support.h"
#include "message_parameter_types.h"
#include "message_parameter_visitors.h"
#include "client_address.h"
#include "../Patterns/binary_serializer.h"
#include "array_serilization.h"

namespace core
{
	class message;

	enum class checked_type
	{
		Int,
		Int64,
		String,
		Float,
		Double,
		RawData,
		Bool,
		DateTime
	};

	class message_parameter
	{
	public:
		message_parameter()											= default;
		message_parameter(const message_parameter&)					= default;
		message_parameter(message_parameter&&)						= default;
		~message_parameter()										= default;

		message_parameter& operator = (const message_parameter&)	= default;
		message_parameter& operator = (message_parameter&&)			= default;

		message_parameter(float _value)								: m_value(_value) {};
		message_parameter(double _value)							: m_value(_value) {};
		message_parameter(const time_type& _value)					: m_value(_value) {};
		message_parameter(const std::wstring& _value)				: m_value(_value) {};

		message_parameter(int64_t _value)							: m_value(_value) {};
		message_parameter(int8_t _value)							: message_parameter(int64_t(_value)) {}
		message_parameter(uint8_t _value)							: message_parameter(int64_t(_value)) {}
		message_parameter(int16_t _value)							: message_parameter(int64_t(_value)) {}
		message_parameter(uint16_t _value)							: message_parameter(int64_t(_value)) {}
		message_parameter(int32_t _value)							: message_parameter(int64_t(_value)) {}
		message_parameter(uint32_t _value)							: message_parameter(int64_t(_value)) {}
		message_parameter(uint64_t _value)							: message_parameter(int64_t(_value)) {}
		message_parameter(const client_address& _value)				: message_parameter(_value.AsMessageParamterInt()) {}

		template<typename Archive>
		message_parameter(Archive& _archive, uint32_t _size)
		{
			load(_archive, _size);
		}

		message_parameter(const raw_data_type& _data);
		message_parameter(raw_data_type&& _data);
		message_parameter(uint8_t * _data, raw_data_type::size_type _length);

		template<typename T, typename = typename std::enable_if<is_simple_type<T>::value, void>::type>
		message_parameter(const std::vector<T>& _data)
		{
			SetRawData(_data);
		}

		bool operator == (const message_parameter& _val) const
		{
			return m_value == _val.m_value;
		}

		bool IsInt64()				const { return m_value.which() == 1; }
		bool IsUInt64()				const { return IsInt64(); }
		bool IsInt()				const { return IsInt64(); }
		bool IsUInt()				const { return IsInt64(); }
		bool IsBool()				const { return IsInt64(); }
		bool IsFloat()				const { return m_value.which() == 2; }
		bool IsString()				const { return m_value.which() == 0; }
		bool IsDouble()				const { return m_value.which() == 3; }
		bool IsDateTime()			const { return m_value.which() == 4; }
		bool IsRawData()			const { return m_value.which() == 5; }
		bool IsEmpty()				const { return m_value.empty(); }
		bool IsMessageArray()		const { return IsRawData() && contains_array(GetRawData()); }

		bool		AsBool()		const { return AsInt64() != 0; }
		int32_t		AsInt()			const { return static_cast<int32_t>(GetValue<int64_t>()); }
		uint32_t	AsUInt()		const { return static_cast<uint32_t>(GetValue<int64_t>()); }
		int64_t		AsInt64()		const { return GetValue<int64_t>(); }
		uint64_t	AsUInt64()		const { return static_cast<uint64_t>(GetValue<int64_t>()); }
		time_type	AsDateTime()	const { return GetValue<time_type>(); }
		float		AsFloat()		const { return GetValue<float>(); }
		double		AsDouble()		const { return GetValue<double>(); }
		std::string AsStr()			const { return wtos(AsWideStr()); }

		template<typename T>
		std::vector<typename std::enable_if<is_simple_type<T>::value, T>::type>
		AsVector() const 
		{
			if (!IsRawData())
				throw invalid_value_type_exception();
			
			const auto& rawData = GetRawData();
			
			if (rawData.size() % sizeof(T))
				throw invalid_value_type_exception((boost::format("Raw data size doesn't match requested element size. Vector size: %i, Element size: %i") % rawData.size() % sizeof(T)).str());

			return std::vector<T>(reinterpret_cast<const T*>(rawData.data()), reinterpret_cast<const T*>(rawData.data() + rawData.size()));
		}

		client_address AsAddress()	const 
		{ 
			int64_t address = AsUInt64();
			return client_address(address);
		}

		message					AsMessage() const;
		std::vector<message>	AsMessagesVector() const;

		std::wstring AsWideStr()	const;
		std::wstring AsJsonStr() const;

		std::wstring ParameterType() const { return boost::apply_visitor(support::type_info_visitor(), m_value);  }
		bool		Check(checked_type _type) const;

		template<typename Archive>
		void load(Archive& _archive, const unsigned int _size);

		template<typename Archive>
		void save(Archive& _archive, const unsigned int _version) const;

		template<typename T>
		message_parameter& SetValue(const T& _value)
		{
			m_value = _value;
			return *this;
		}

		template<typename T>
		T GetValue() const
		{
			try
			{
				return boost::get<T>(m_value);
			}
			catch (std::exception&)
			{
				throw invalid_value_type_exception();
			}
		}

		template<typename T>
		message_parameter& operator = (const T& _value) 
		{
			return SetValue<typename std::conditional<std::numeric_limits<T>::is_integer, int64_t, T>::type>(_value);
		}

		message_parameter& operator = (const wchar_t * _value)
		{
			return SetValue<std::wstring>(_value);
		}

		message_parameter& operator = (const client_address& _value)
		{
			return SetValue<int64_t>(static_cast<int64_t>( _value.AsMessageParamterInt()));
		}

		template<typename T >
		typename std::enable_if<is_simple_type<T>::value, message_parameter&>::type 
		operator = (const std::vector<T>& _data)
		{
			SetRawData(_data);
			return *this;
		}

		message_parameter& operator = (const message& _value);

		raw_data_type::size_type GetRawDataSize() const noexcept
		{
			return boost::apply_visitor(support::raw_data_type_size_visitor(), m_value);
		}

		const raw_data_type& GetRawData() const
		{
			return boost::apply_visitor(support::raw_data_content_visitor(), m_value);
		}

		template<typename T>
		typename std::enable_if<is_simple_type<T>::value, void>::type
		SetRawData(const std::vector<T>& _data)
		{
			SetRawData(reinterpret_cast<const uint8_t *>(_data.data()), _data.size() * sizeof(T));
		}

		void SetRawData(const uint8_t * _data, raw_data_type::size_type _length)
		{
			m_value = raw_data_type(_data, _data + _length);
		}

		std::wstring Dump() const
		{
			std::wostringstream out;
			Dump(out);
			return out.str();
		}

		template<typename Stream>
		void Dump(Stream& _stream) const
		{
			boost::apply_visitor(support::dump_visitor<Stream>(_stream), m_value);
		}

		size_t size() const noexcept
		{
			return boost::apply_visitor(support::length_visitor(), m_value);
		}

		size_t GetLength() const noexcept
		{
			return size();
		}

	private:
		boost::variant<
			std::wstring, 
			int64_t, 
			float, 
			double, 
			time_type,
			raw_data_type>	m_value;

		template<typename Archive>
		void serialize_reserved(Archive& _archive) const
		{
			uint32_t wReserved;
			_archive & wReserved;
		}
	};

	template<typename Archive>
	void message_parameter::load(Archive& _archive, const unsigned int _size)
	{
		using namespace support;

		uint32_t nRawDataLength;
		_archive & nRawDataLength;

		uint32_t nRawDataPointer;
		_archive & nRawDataPointer;

		uint32_t nSplitter;
		_archive & nSplitter;

		variant_type vt;
		_archive & vt;

		serialize_reserved(_archive);

		if (nRawDataLength)
		{
			uint64_t nFiller1;
			_archive & nFiller1;
			m_value = support::raw_data_read_write_support::load(_archive, nRawDataLength);
			_archive & nFiller1;
		}
		else
		{
			switch (vt)
			{
			case variant_type::variant_date:
			{
				double value;
				_archive & value;
				uint64_t nMilliseconds;
				_archive & nMilliseconds;

				m_value = to_time_type(value, nMilliseconds);
			}
			break;
			case variant_type::variant_float:
			{
				float value;
				_archive & value;

				uint32_t nFiller1;
				_archive & nFiller1;

				uint64_t nFiller2;
				_archive & nFiller2;

				m_value = value;
			}
			break;
			case variant_type::variant_double:
			{
				double value;
				_archive & value;
				uint64_t nFiller;
				_archive & nFiller;

				m_value = value;
			}
			break;
			case variant_type::variant_string:
			{
				uint64_t nFiller;
				_archive & nFiller;
				m_value = support::wstring_read_write_support::load(_archive, _size - support::length_visitor::s_nParameterLength);
				_archive & nFiller;
			}
			break;
			case variant_type::variant_int:
			case variant_type::variant_uint:
			{
				int64_t value;
				_archive & value;

				uint64_t nFiller;
				_archive & nFiller;

				m_value = value;
			}
			break;
			default:
			{
				int32_t value;
				_archive & value;

				int32_t filler;
				_archive & filler;

				uint64_t nFiller;
				_archive & nFiller;

				m_value = int64_t(value);
			}
			break;
			}
		}

	}

	template<typename Archive>
	void message_parameter::save(Archive& _archive, const unsigned int _version) const
	{
		_archive & static_cast<uint32_t>(GetRawDataSize());

		_archive & uint64_t(0); // Raw data pointer + splitter

		boost::apply_visitor(core::support::vt_serialize_visitor<Archive>(_archive), m_value);
		
		serialize_reserved(_archive);

		boost::apply_visitor(core::support::value_serialize_visitor<Archive>(_archive), m_value);
	}

}

#pragma once
#include <string>
#include "../Configuration/SystemConfiguration.h"
#include <vector>
#include "router_config_interface.h"
#include "../Patterns/circular_vector.h"

namespace core
{
	class default_router_config : public router_config_interface
	{
		using router_list = circular_vector<std::tuple<std::wstring, uint16_t, uint32_t, uint32_t, bool, uint32_t>>;

	public:
		default_router_config(const std::wstring& _nodeName = L"Routers");
		virtual ~default_router_config() {}

		virtual const std::wstring&	get_host()				const noexcept override { return std::get<0>(m_routers.get()); }
		virtual uint16_t			get_port()				const noexcept override { return std::get<1>(m_routers.get()); }
		virtual uint32_t			get_io_thread_count()	const noexcept override { return std::get<2>(m_routers.get()); }
		virtual uint32_t			get_proc_thread_count()	const noexcept override { return std::get<3>(m_routers.get()); }
		virtual bool				write_log()				const noexcept override { return std::get<4>(m_routers.get()); }
		virtual uint32_t			keep_alive_timeout()	const noexcept override { return std::get<5>(m_routers.get()); }
		
		virtual void				next_router() override
		{
			m_routers.move_next();
		}

	private:
		SystemConfig			m_config;
		router_list				m_routers;
		uint32_t				m_threadCount;
		bool					m_blWriteLog;
		uint32_t				m_keepAliveTimeout;
	};

	inline config_pointer create_default_config()
	{
		return std::make_shared<default_router_config>();
	}
}

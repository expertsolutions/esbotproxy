#include "stdafx.h"
#include "run_script_request.h"

std::atomic<uint64_t> core::run_script_request_base::s_requestCounter(1);
const std::wstring core::run_script_request_base::s_runRequestIdParameter(L"RunRequestID");
const std::wstring core::run_script_request_base::s_runScriptOkMessageName(L"RUNSCRIPT_OK");
const std::wstring core::run_script_request_base::s_runScriptFailMessageName(L"RUNSCRIPT_FAIL");

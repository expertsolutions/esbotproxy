#include "stdafx.h"
#include "message.h"
#include "message_header.h"
#include <streambuf>
#include <sstream>
#include <boost/tokenizer.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;

wstring core::message::Dump() const
{
	wostringstream out;
	out << L"Name: " << m_strName << L"; ";
	
	for (const auto& param : m_parameters)
	{
		out << param.first << L" = ";
		param.second.Dump(out);
		out << L"; ";
	}

	return out.str();
}

void core::message::CheckParam(const wstring& _parameterName, CheckedType _type, ParamType _ptype) const
{
	using namespace std;

	auto parameter = m_parameters.find(_parameterName);
	if (parameter == m_parameters.cend())
	{
		if(_ptype == ParamType::Mandatory)
			throw message_parameter_missing_exception(_parameterName);
	}
	else if (_ptype == ParamType::Forbidden)
	{
		throw message_parameter_exception((boost::format("Parameter '%s' is forbidden") % wtos(_parameterName)).str());
	}
	else if (!parameter->second.Check(_type))
	{
		throw message_parameter_invalid_type_exception(_parameterName);
	}
}

int core::message::ParamAsEnum(const wstring& _parameterName, const wstring& _possibleValues, const wstring& _separators) const
{
	try
	{
		using namespace boost;
		auto parameter = m_parameters.at(_parameterName);
		char_separator<wchar_t> sep(_separators.c_str());
		tokenizer< char_separator<wchar_t>, wstring::const_iterator, wstring > tokens(_possibleValues, sep);
		int result = 0;
		auto value = parameter.AsWideStr();
		for(const auto& token : tokens)
		{
			if (token == value)
				return result;
			++result;
		}

		return result;
	}
	catch (out_of_range&)
	{
		throw message_parameter_invalid_value_exception(_parameterName);
	}
}

wstring core::message::ToJson(bool _blAppendType) const
{
	property_tree data;
	ToJson(data, _blAppendType);
	return support::ptree_parser::tree_to_json_string(data);
}

void core::message::ToJson(property_tree& _json, bool _blAppendType) const
{
	try
	{
		auto fromEndMark = m_strName.find(L'2');
		if (fromEndMark == string::npos)
			throw runtime_error("Invalid name format");

		auto toEndMark = m_strName.find(L'_', fromEndMark);
		if (toEndMark == string::npos)
			throw runtime_error("Invalid name format");
		
		_json.add(L"source", m_strName.substr(0, fromEndMark));
		_json.add(L"destination", m_strName.substr(fromEndMark + 1, toEndMark - fromEndMark - 1));
		_json.add(L"methodName", m_strName.substr(toEndMark + 1));
	}
	catch(runtime_error&)
	{
		_json.add(L"method_name", GetName());
	}

	for (const auto& param : m_parameters)
	{
		wstring name(param.first);
		if(name == L"json")
		{
			auto jsonData = support::ptree_parser::tree_from_string_json(param.second.AsWideStr());
			_json.insert(_json.end(), jsonData.begin(), jsonData.end());
		}
		else if (boost::istarts_with(name, L"json"))
		{
			auto jsonData = support::ptree_parser::tree_from_string_json(param.second.AsWideStr());
			_json.add_child(name.substr(4), jsonData);
		}
		if (boost::istarts_with(name, L"array"))
		{

		}
		else
		{
			if (_blAppendType)
			{
				_json.add(param.second.ParameterType() + param.first, param.second.AsWideStr());
			}
			else
				_json.add(param.first, param.second.AsWideStr());
		}
	}

}

wstring core::message::ToXml(bool _blAppendType) const
{
	property_tree data;
	ToXml(data, _blAppendType);

	auto result = support::ptree_parser::tree_to_xml_string(data);
	auto endLine = result.find(L'\n');
	return wstring(result.cbegin() + endLine + 1, result.cend());
}

void core::message::ToXml(property_tree& _xml, bool _blAppendType /* = false */) const
{
	wstring path = GetName() + L".<xmlattr>.";

	for (const auto& param : m_parameters)
	{
		if (_blAppendType)
		{
			_xml.put(path + param.second.ParameterType() + param.first, param.second.AsWideStr());
		}
		else
			_xml.put(path + param.first, param.second.AsWideStr());
	}
}

void core::message::FromJson(const wstring& _json, bool _removeType, bool allEscapeCharacters)
{
	return FromJson(core::support::ptree_parser::tree_from_string_json(_json), _removeType, allEscapeCharacters);
}

void core::message::FromJson(const property_tree& _json, bool _removeType, bool allEscapeCharacters)
{
//	SetName(_json.get<wstring>(L"method_name"));
	try
	{
		SetName(_json.get<wstring>(L"source") + L'2' + _json.get<wstring>(L"destination") + L'_' + _json.get<wstring>(L"methodName"));
	}
	catch(boost::property_tree::ptree_bad_path&)
	{
		SetName(_json.get<wstring>(L"method_name"));
	}

	parse_parameters(_json, _removeType, allEscapeCharacters);
}

void core::message::FromXml(const wstring& _xml, bool _removeType)
{
	FromXml(core::support::ptree_parser::tree_from_string_xml(_xml), _removeType);
}

void core::message::FromXml(const property_tree& _xml, bool _removeType /* = false */)
{
	const auto& i = _xml.front();

	SetName(i.first);
	try
	{
		parse_parameters(i.second.get_child(L"<xmlattr>"), _removeType, true);
	}
	catch (boost::property_tree::ptree_bad_path&)
	{ //-V565
	}
}

std::wstring remove_escapes(const wstring& _stream)
{
	wostringstream out;
	wchar_t val;
	for (size_t i = 0; i < _stream.length(); ++i)
	{
		val = _stream[i];
		if (val == L'\\')
		{
			
			wchar_t next = _stream[++i];

			if (next == L'u')
			{
				++i;
				uint16_t hexVal = static_cast<uint16_t>(wcstoul(_stream.substr(i, 4).c_str(), nullptr, 16));
				i += 3;
				out << wchar_t(hexVal);
			}
			else if (next == L'/')
			{
				out << L'/';
			}
			else
			{
				out << val << next;
			}
		}
		else
		{
			out << val;
		}

	}
	return out.str();
}

void core::message::parse_parameters(const property_tree& _tree, bool _removeType, bool allEscapeCharacters)
{
	for(const auto& i : _tree)
	{
		if (boost::istarts_with(i.first, L"int"))
		{
			if(_removeType)
				(*this)[i.first.substr(3)] = std::stoll(i.second.data());
			else
				(*this)[i.first] = std::stoll(i.second.data());
		}
		else if (boost::istarts_with(i.first, L"float"))
		{
			if (_removeType)
				(*this)[i.first.substr(5)] = std::stof(i.second.data());
			else
				(*this)[i.first] = std::stod(i.second.data());
		}
		else if (boost::istarts_with(i.first, L"double"))
		{
			if(_removeType)
				(*this)[i.first.substr(6)] = std::stod(i.second.data());
			else
				(*this)[i.first] = std::stod(i.second.data());
		}
		else if (boost::istarts_with(i.first, L"time"))
		{
			if (_removeType)
				(*this)[i.first.substr(4)] = support::time_from_string(i.second.data().c_str());
			else
				(*this)[i.first] = support::time_from_string(i.second.data().c_str());
		}
		else if (boost::istarts_with(i.first, L"raw"))
		{
			if (_removeType)
				(*this)[i.first.substr(3)] = support::raw_from_string(i.second.data().c_str());
			else
				(*this)[i.first] = support::raw_from_string(i.second.data().c_str());
		}
		else if (boost::istarts_with(i.first, L"json"))
		{
			auto json = support::ptree_parser::tree_to_json_string(i.second);
			if (!allEscapeCharacters)
				json = remove_escapes(json);

			if (_removeType)
				(*this)[i.first.substr(4)] = json;
			else
				(*this)[i.first] = json;
		}
		else
		{
			(*this)[i.first] = i.second.data();
		}
	}

}

#pragma once
#include "router_io_service.h"
#include <boost/asio.hpp>

namespace core
{
	namespace support
	{
		class external_io_service : public router_io_service
		{
		public:
			external_io_service(packet_processor& _processor, boost::asio::io_service& _service) : router_io_service(_processor), m_service(_service)
			{
			}

			virtual ~external_io_service()
			{
			}

			virtual boost::asio::io_service& getService() noexcept override
			{
				return m_service;
			}

			virtual void queuePacket(router_packet&& _packet) override
			{
				std::shared_ptr<router_packet> packet(make_shared<router_packet>(_packet));
				m_service.post([this, packet]()
				{
					processPacket(*packet);
				});
			}

		private:
			boost::asio::io_service& m_service;
		};

	}
}
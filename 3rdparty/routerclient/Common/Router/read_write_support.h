#pragma once
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/split_member.hpp>
#include <string>
#include <vector>
#include "../Patterns/string_functions.h"
#include <boost/serialization/binary_object.hpp>
#include <boost/serialization/array.hpp>
#include "message_parameter_types.h"

namespace core
{

	namespace support
	{

		class name_read_write_support
		{
		public:
			template<typename Archive>
			static std::wstring load(Archive& _archive)
			{
				char name[name_size()] = { 0 };
				boost::serialization::binary_object a_sink(name, sizeof(name));
				_archive & a_sink;
				return stow(name);
			}

			template<typename Archive>
			static void save(Archive& _archive, const std::wstring& _name)
			{
				std::string name(wtos(_name));
				name.resize(name_size(), 0);
				boost::serialization::binary_object a_sink(const_cast<char *>(name.data()), name_size());
				_archive & a_sink;
			}

			static constexpr size_t name_size() { return 64; }
		};

		class wstring_read_write_support
		{
		private:
			typedef uint16_t CHAR;

		public:
			template<typename Archive>
			static std::wstring load(Archive& _archive, size_t size)
			{
				auto count = size / sizeof(CHAR) - 1;
				std::wstring value(count, 0);

				if (sizeof(wchar_t) == sizeof(CHAR))
				{
					boost::serialization::binary_object a_sink(const_cast<wchar_t *>(value.data()), size - sizeof(wchar_t));
					_archive & a_sink;
				}
				else
				{
					for (size_t i = 0; i < count; ++i)
					{
						CHAR ch = 0;
						_archive & ch;
						value[i] = static_cast<wchar_t>(ch);
					}
				}

				serialize_filler(_archive);				
				return value;
			}

			template<typename Archive>
			static void save(Archive& _archive, const std::wstring& _value)
			{
				if (sizeof(wchar_t) == sizeof(CHAR))
				{
					boost::serialization::binary_object a_sink(const_cast<wchar_t *>(_value.data()), _value.length() * sizeof(wchar_t));
					_archive & a_sink;
				}
				else
				{
					auto count = _value.length();
					for (size_t i = 0; i < count; ++i)
					{
						_archive & static_cast<CHAR>(_value[i]);
					}
				}
				serialize_filler(_archive);
			}

			template<typename Archive>
			static void serialize_filler(Archive& _archive)
			{
				CHAR filler(0);
				_archive & filler;
			}

		};

		class raw_data_read_write_support
		{
		public:
			template<typename Archive>
			static raw_data_type load(Archive& _archive, int size)
			{
				raw_data_type value(size);
				boost::serialization::binary_object a_sink(const_cast<raw_data_type::value_type *>(value.data()), size);
				_archive & a_sink;
				return value;
			}

			template<typename Archive>
			static void save(Archive& _archive, const raw_data_type& _value)
			{
				boost::serialization::binary_object a_sink(const_cast<raw_data_type::value_type *>(_value.data()), _value.size());
				_archive & a_sink;
			}
		};
	}
}

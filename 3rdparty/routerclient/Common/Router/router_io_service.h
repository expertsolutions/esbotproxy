#pragma once
#include "router_packet.h"
#include <boost/asio.hpp>

namespace core
{
	namespace support
	{
		using namespace std;

		class packet_processor
		{
			friend class router_io_service;
		protected:
			virtual bool processPacket(router_packet& _packet) = 0;
		};

		class router_io_service
		{
		public:
			router_io_service(packet_processor& _processor) : m_processor(_processor)
			{
			}

			virtual ~router_io_service() {};

			virtual void queuePacket(router_packet&& _packet) = 0;
			virtual boost::asio::io_service& getService() noexcept = 0;

		protected:
			virtual bool processPacket(router_packet& _packet)
			{
				return m_processor.processPacket(_packet);
			}

			packet_processor& m_processor;
		};

		using router_io_service_ptr = shared_ptr<router_io_service>;
	}
}

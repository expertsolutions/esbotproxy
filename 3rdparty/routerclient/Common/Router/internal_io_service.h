#pragma once
#include "router_io_service.h"
#include <boost/asio.hpp>
#include <thread>
#include <memory>
#include "../Patterns/locked_queue.h"
#include "../Patterns/io_service_runner.h"

namespace core
{
	namespace support
	{
		using namespace std;

		class internal_io_service : public router_io_service
		{
		public:
			internal_io_service(packet_processor& _processor, uint32_t _ioThreadCount, uint32_t procThreadCount) : router_io_service(_processor), m_runner(_ioThreadCount)
			{
				create_processing_threads(procThreadCount);
				m_runner.run();
			}

			virtual ~internal_io_service()
			{
				m_runner.stop();
				stop_processing_threads();
			}

			virtual void queuePacket(router_packet&& _packet) override
			{
				m_incoming.emplace(_packet);
			}

			virtual boost::asio::io_service& getService() noexcept override
			{
				return m_runner.get_service();
			}

		private:
			void create_processing_threads(uint32_t _threadCount)
			{
				m_threads.reserve(_threadCount);

				for (size_t i = 0; i < _threadCount; ++i)
				{
					m_threads.emplace_back(&internal_io_service::processing_thread, this);
				}
			}


			void stop_processing_threads()
			{
				size_t threadCount = m_threads.size();
				for (size_t i = 0; i < threadCount; ++i)
				{
					m_incoming.emplace(CreateShutdownPacket());
				}

				for (auto& thread : m_threads)
				{
					if (thread.joinable())
						thread.join();
				}
			}

			void processing_thread()
			{
				bool continueProcessing = false;

				do
				{
					router_packet packet(m_incoming.pop_front());
					continueProcessing = processPacket(packet);
				} while (continueProcessing);
			}

			locked_queue<router_packet>				m_incoming;
			io_service_runner						m_runner;
			std::vector<std::thread>				m_threads;
		};

	}
}

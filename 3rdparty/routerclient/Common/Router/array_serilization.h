#pragma once
#include "serilization_templates.h"

namespace core
{
	constexpr uint64_t	array_signature = 0xFEABDC9845763201;
	constexpr auto		header_size = sizeof(array_signature) + sizeof(uint32_t);

	class array_deserilization_exception : public std::runtime_error
	{
	public:
		array_deserilization_exception() : std::runtime_error("Invalid array format") {}
	};

	template <typename Container>
	size_t container_size(const Container& _v)
	{
		return std::accumulate(_v.cbegin(), _v.cend(), size_t(0), [](size_t _total, const typename Container::value_type& _val)
		{
			return _total + _val.size();
		});
	}

	template <typename Container>
	raw_data_type to_raw_data(const Container& _v)
	{
		size_t size = container_size(_v) + header_size;

		raw_data_type result(size);
		array_sink sink{ result.data(), size };
		stream<boost::iostreams::array_sink> os{ sink };
		boost::archive::binary_oarchive writer(os, boost::archive::no_header | boost::archive::no_codecvt | boost::archive::no_tracking);

		writer & array_signature;
		writer & uint32_t(_v.size());

		for (const auto& _val : _v)
		{
			_val.save(writer, 1);
		}

		return result;
	}

	template<typename T>
	std::vector<T> from_raw_data(const raw_data_type& _data)
	{
		if (_data.size() < header_size)
			throw array_deserilization_exception();

		vector<T> result;
		result.reserve(*reinterpret_cast<const uint32_t *>(_data.data() + sizeof(array_signature)));

		size_t currentOffset = header_size;
		while (currentOffset < _data.size())
		{
			T val = support::deserialize<T>(_data, _data.size() - currentOffset, currentOffset);
			currentOffset += val.size();
			result.emplace_back(std::move(val));
		}

		return result;
	}

	inline bool contains_array(const raw_data_type& _data)
	{
		if (_data.size() < header_size)
			return false;

		return array_signature == *reinterpret_cast<const uint64_t *>(_data.data());
	}

}

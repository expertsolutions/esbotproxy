#pragma once
#include <boost/asio.hpp>
#include "../Patterns/locked_queue.h"
#include "router_packet.h"
#include <mutex>
#include <atomic>

namespace core
{
	using namespace boost::asio;
	using namespace boost::asio::ip;

	using packet_queue = std::queue<serilization_type>;

	enum class ClientStatus
	{
		CS_CONNECTED = 1,
		CS_DISCONNECTED = 2
	};

	enum class SubscriptionStatus
	{
		ES_ACTIVE = 0,
		ES_SUBSCRIBED = 1,
		ES_NOT_SUBSCRIBED = 2
	};

	class router_interface
	{
		friend class router_connection;
	public:
		virtual ~router_interface() {}

	protected:
		virtual void on_connect()								= 0;
		virtual void on_disconnect(packet_queue&& _unsent)		= 0;
		virtual void on_error(boost::system::error_code _ec)	= 0;
		virtual void on_receive(router_packet&& _packet)		= 0;
	};

	class router_connection
	{
	public:
		router_connection(router_interface& _manager, io_service& _io, const std::wstring& _host, uint16_t _port, packet_queue&& _unsent, uint32_t _keepAliveTimeout);
		~router_connection();

		router_connection(const router_connection&)					= delete;
		router_connection(router_connection&&)						= delete;
		router_connection& operator = (const router_connection&)	= delete;
		router_connection& operator = (router_connection&&)			= delete;


		void send(serilization_type&& _packet);
		void connect();
		void disconnect();
	private:
		void do_connect(tcp::resolver::iterator endpoint_iterator);
		void read_socket();
		void write_socket();
		void setup_keep_alive();

		void notify_disconnect(boost::system::error_code ec);

		router_interface&				m_manager;
		io_service&						m_io;
		tcp::socket						m_socket;
		tcp::resolver::query			m_routerAddress;
		packet_queue					m_outcoming;
		serilization_type				m_read;
		size_t							m_offset = 0;
		size_t							m_lastMessageEndPosition = 0;
		std::atomic<bool>				m_blDisconnectSent;
		std::atomic<bool>				m_blWriting;
		uint32_t						m_keepAliveTimeout;
		std::mutex						m_access;
	};
}

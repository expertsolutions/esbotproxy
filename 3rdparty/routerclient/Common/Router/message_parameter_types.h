#pragma once
#include <vector>
#include <chrono>
#include "../Patterns/binary_serializer.h"

namespace core
{
	using clock_type	= std::chrono::system_clock;
	using time_type		= std::chrono::time_point<clock_type>;
}
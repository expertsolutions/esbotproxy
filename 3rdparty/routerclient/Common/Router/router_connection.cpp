#include "stdafx.h"
#include "router_connection.h"
#include <boost/asio/ip/tcp.hpp>
#include "../Patterns/string_functions.h"
#include "../Log/SystemLog.h"

#ifdef _WIN32_WINNT
	#include "Mstcpip.h"
#endif

using namespace boost::asio;
using namespace boost::asio::ip;

core::router_connection::router_connection(router_interface& _manager, boost::asio::io_service& _io, const std::wstring& _host, uint16_t _port, packet_queue&& _unsent, uint32_t _keepAliveTimeout) :
	m_manager(_manager), m_io(_io), m_socket(_io), m_routerAddress(wtos(_host), std::to_string(_port)), m_outcoming(_unsent), m_read(2048, 0),
	m_blDisconnectSent(false), m_blWriting(false), m_keepAliveTimeout(_keepAliveTimeout)
{
}

core::router_connection::~router_connection()
{
}

void core::router_connection::connect()
{
	tcp::resolver resolver(m_io);
	auto endpoint_iterator = resolver.resolve(m_routerAddress);
	do_connect(endpoint_iterator);
}

void core::router_connection::disconnect()
{
#if defined _WIN32_WINNT && _WIN32_WINNT > 0x0501
	try
	{
		m_socket.cancel();
	}
	catch (...)
	{

	}
#endif // 
	m_socket.close();

}

void core::router_connection::do_connect(tcp::resolver::iterator endpoint_iterator)
{

	async_connect(m_socket, endpoint_iterator,
		[this](boost::system::error_code ec, tcp::resolver::iterator)
	{
		if (!ec)
		{
			setup_keep_alive();
			m_manager.on_connect();
			write_socket();
			read_socket();
		}
		else
		{
			notify_disconnect(ec);
		}
	});
}


void core::router_connection::send(serilization_type&& _packet)
{
	if (m_blDisconnectSent)
		return;

	{
		std::lock_guard<std::mutex> lock(m_access);
		m_outcoming.emplace(std::move(_packet));
	}

	write_socket();
}

void core::router_connection::write_socket()
{
	std::lock_guard<std::mutex> lock(m_access);
	if (!m_outcoming.empty() && !m_blWriting)
	{
		m_blWriting = true;
		async_write(m_socket, boost::asio::buffer(m_outcoming.front().data(), m_outcoming.front().size()),
			[this](boost::system::error_code ec, std::size_t /*length*/)
		{
			if (!ec)
			{
				{
					std::lock_guard<std::mutex> lock_m(m_access);
					m_blWriting = false;
					m_outcoming.pop();
				}
				write_socket();
			}
			else
			{
				notify_disconnect(ec);
			}
		});

	}

}

void core::router_connection::notify_disconnect(boost::system::error_code ec)
{
	if (!m_blDisconnectSent)
	{
		m_blDisconnectSent = true;
		m_manager.on_error(ec);
		m_manager.on_disconnect(std::move(m_outcoming));
	}
}

void core::router_connection::read_socket()
{
	if (m_blDisconnectSent)
		return;

	m_socket.async_read_some(
		buffer(m_read.data() + m_offset, m_read.size() - m_offset),
		[this](boost::system::error_code ec, std::size_t _length)
	{
		auto length = _length + m_offset - m_lastMessageEndPosition;
		if (!ec)
		{
			try
			{
				m_offset += _length;
				while (length > 0)
				{
					auto packet(core::support::deserialize<router_packet>(m_read, length, m_lastMessageEndPosition));
					auto sz = packet.size();
					length -= sz;
					m_lastMessageEndPosition += sz;
					m_manager.on_receive(std::move(packet));
				}

				m_lastMessageEndPosition = 0;
				m_offset = 0;
			}
			catch (incomplete_message_exception& ex)
			{
				if (m_offset + ex.bytes_to_read() > m_read.size())
					m_read.resize(m_offset + ex.bytes_to_read());
			}

			read_socket();
		}
		else
		{
			notify_disconnect(ec);
		}
	});
}

void core::router_connection::setup_keep_alive()
{
	if (m_keepAliveTimeout != 0)
	{
		// platform-specific         
#if BOOST_COMP_MSVC
		tcp_keepalive timeouts;
		timeouts.keepaliveinterval = m_keepAliveTimeout;
		timeouts.keepalivetime = m_keepAliveTimeout;
		timeouts.onoff = 1;
		DWORD bytesOut;
#if BOOST_VERSION >= 104700
        ::WSAIoctl(m_socket.native_handle(), SIO_KEEPALIVE_VALS, &timeouts, sizeof(timeouts), NULL, 0, &bytesOut, NULL, NULL);
#else // BOOST_VERSION < 104700
		::WSAIoctl(m_socket.native(), SIO_KEEPALIVE_VALS, &timeouts, sizeof(timeouts), NULL, 0, &bytesOut, NULL, NULL);
#endif // BOOST_VERSION >= 104700
#else
		try
		{
#if BOOST_VERSION >= 104700
            auto native_handle = m_socket.native_handle();
#else
            auto native_handle = m_socket.native();
#endif
			uint32_t optval = 1;
			auto optlen = sizeof(optval);
            if (setsockopt(native_handle, SOL_SOCKET, SO_KEEPALIVE, &optval, optlen) != 0)
				throw std::runtime_error("Failed to set SO_KEEPALIVE");

			optval = m_keepAliveTimeout;
			if (setsockopt(native_handle, SOL_TCP, TCP_KEEPIDLE, &optval, optlen) != 0)
				throw std::runtime_error("Failed to set TCP_KEEPIDLE");

			if (setsockopt(native_handle, SOL_TCP, TCP_KEEPINTVL, &optval, optlen) != 0)
				throw std::runtime_error("Failed to set TCP_KEEPINTVL");
		}
		catch (std::runtime_error& err)
		{
			throw;
			//perror(err.what());
			//close(s);
			//exit(EXIT_FAILURE);
		}
#endif

	}
}

#pragma once
#include <boost/iostreams/device/back_inserter.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/iostreams/stream.hpp>
#include <vector>
#include <numeric>
#include "../Patterns/binary_serializer.h"

namespace core
{
	using serilization_type = raw_data_type;
	using namespace boost::iostreams;

	namespace support
	{
		template<typename T>
		serilization_type	serialize(const T& _value)
		{
			auto nSize = _value.size();
			serilization_type result(nSize);

			array_sink sink{ result.data(), nSize };
			stream<boost::iostreams::array_sink> os{ sink };
			boost::archive::binary_oarchive writer(os, boost::archive::no_header | boost::archive::no_codecvt | boost::archive::no_tracking);

			_value.save(writer, 1);

			return result;
		}

		template<typename T>
		T deserialize(const serilization_type& _value, size_t _size, size_t offset = 0)
		{

			T result;
			array_source			value(const_cast<core::serilization_type::value_type *>(_value.data() + offset), _value.size());
			stream<array_source>	stream_in(value);

			boost::archive::binary_iarchive reader(stream_in, boost::archive::no_header | boost::archive::no_codecvt | boost::archive::no_tracking);
			//reader & result;
			result.load(reader, static_cast<uint32_t>(_size));

			return result;
		}
	}
}

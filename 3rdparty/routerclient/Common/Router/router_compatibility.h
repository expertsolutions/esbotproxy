#pragma once

#include "default_router.h"

using CLIENT_ADDRESS	= core::client_address;
using CRouterManager	= core::router_manager;
using CMessage			= core::message;

#define SUBSCRIBE_MESSAGE(manager, message, member_func)					{ core::SubscribeMessage(manager, message, this, &member_func); }
#define REGISTER_HANDLER(manager, message, member_func)						{ core::RegisterHandler(manager, message, this, &member_func); }
#define SUBSCRIBE_MESSAGE_OBJECT(manager, message, member_func, object)		{ core::SubscribeMessage(manager, message, object, &member_func); }
#define REGISTER_HANDLER_OBJECT(manager, message, member_func, object)		{ core::RegisterHandler(manager, message, object, &member_func); }
#define UNSUBSCRIBE_MESSAGE(manager, message)								{ manager.UnregisterSubscription(message); }
#pragma once

#include "router_connection.h"
#include "../Patterns/io_service_runner.h"
#include "../Patterns/locked_queue.h"
#include <atomic>
#include <memory>
#include <memory>
#include "../Log/log_interface.h"
#include "router_config_interface.h"
#include "router_io_service.h"

namespace core
{
	config_pointer create_default_config();
	log_pointer create_default_log();

	using io_runner_ptr = std::shared_ptr<io_service_runner>;
	using io_service = boost::asio::io_service;

	class default_router_log;
	class router_manager : public router_interface, public support::packet_processor
	{
	public:
		using MessageHandler		= std::function<void(message&, const client_address& , const client_address& )>;
		using StatusChangeHandler	= std::function<void(uint32_t _dwClient, ClientStatus	_status)>;
		using DisconnectHandler		= std::function<void(const std::wstring&, uint16_t, bool&)>;
		using ConnectHandler		= std::function<void(const std::wstring&, uint16_t)>;
		using DeliverErrorHandler	= std::function<void(const message&, const client_address& , const client_address& )>;
		using EventSubscribeHandler = std::function<void(uint32_t _dwEventHash, const client_address& _client, SubscriptionStatus _status, size_t _consumers)>;

	public:
		router_manager(ConnectHandler _handler, config_pointer _config = create_default_config(), log_pointer _log = create_default_log());

		router_manager(config_pointer _config = create_default_config(), log_pointer _log = create_default_log()) : router_manager([] (const std::wstring&, uint16_t) {}, _config, _log)
		{
		}

		router_manager(support::router_io_service_ptr _io_runner, config_pointer _config = create_default_config(), log_pointer _log = create_default_log());

		router_manager(io_service& _io_service, config_pointer _config = create_default_config(), log_pointer _log = create_default_log()) : router_manager(_io_service, [](const std::wstring&, uint16_t) {}, _config, _log)
		{

		}

		router_manager(io_service& _io_service, ConnectHandler _handler, config_pointer _config = create_default_config(), log_pointer _log = create_default_log());

		virtual ~router_manager();

		router_manager(const router_manager&)				= delete;
		router_manager(router_manager&&)					= delete;

		router_manager& operator = (const router_manager&)	= delete;
		router_manager& operator = (router_manager&&)		= delete;

		bool	Connected() const noexcept 
		{ 
			return m_state == State::connected && m_clientId != client_address::Invalid().GetClientID(); 
		}

		template<typename Message>
		void	SendFromTo(Message&& _message, uint32_t _childId, const client_address& _to);

		template<typename Message>
		void     SendFromTo(Message&& _message, const client_address& _from, const client_address& _to);

		template<typename Message>
		void	SendFromTo(Message&& _message, uint32_t _childId, uint32_t _clientId, uint32_t _clientChildId);

		template<typename Message>
		void	SendTo(Message&& _message, client_address _to);

		template<typename Message>
		void	SendToAll(Message&& _message, uint32_t _childId = client_address::NoChild());

		template<typename Message>
		void	SendToAllAndSelf(Message&& _message, uint32_t _childId = client_address::NoChild());

		template<typename Message>
		void	SendToAny(Message&& _message, uint32_t _childId = client_address::NoChild());

		template<typename Message>
		void	SendToAnyAndSelf(Message&& _message, uint32_t _childId = client_address::NoChild());

		template<typename Message>
		void	SendToRegion(const std::wstring& _regionName, Message&& _message, uint32_t _childId = client_address::NoChild());

		void	RegisterDefaultHandler(MessageHandler _handler)					{ m_defaultHandler			= _handler; }
		void	RegisterDeliverErrorHandler(DeliverErrorHandler _handler)		{ m_deliverErrorHandler		= _handler; }
		void	RegisterStatusHandler(StatusChangeHandler _handler)				{ m_statusChangeHandler		= _handler; }
		void	RegisterEventSubscribeHandler(EventSubscribeHandler _handler)	{ m_eventSubscribeHandler	= _handler; }
		void	RegisterDisconnectHandler(DisconnectHandler _handler)			{ m_disconnectHandler		= _handler; }
		void	RegisterConnectHandler(ConnectHandler _handler)					{ m_connectHandler			= _handler; }

		void	RegisterHandler(const std::wstring& _messageName, const MessageHandler& _handler);
		void	UnregisterHandler(const std::wstring& _messageName);
		void	RegisterSubscription(const std::wstring& _messageName, const MessageHandler& _handler);
		void	UnregisterSubscription(const std::wstring& _messageName);

		void	SubscribeOnEvent(const std::wstring& _messageName);
		void	UnsubscribeFromEvent(const std::wstring& _messageName);
		void	CleanupHandlers() 
		{
			Lock lock(m_access);
			m_handlers.clear();
			m_subscription.clear();
		}

		uint32_t BoundAddress() const { return m_clientId; }
		client_address Self(uint32_t _childId = client_address::NoChild())	const { return client_address(m_clientId, _childId); }

		boost::asio::io_service& get_io() { return m_runner->getService(); }

	protected:
		void on_connect()								override;
		void on_disconnect(packet_queue&& _unsent)		override;
		void on_error(boost::system::error_code _ec)	override;
		void on_receive(router_packet&& _packet)		override;


	private:
		template<typename value_type, typename Message, typename FromAddress = client_address, typename ToAddress = FromAddress>
		void Send(Message&& _message, FromAddress&& _from, ToAddress&& _to);

		void reconnect();

		template<typename... TArgs>
		void log(LogLevel level, const std::wstring& _formatMessage, TArgs&&... args) const
		{
			if (m_config->write_log() && m_log->check_severity(level))
			{
				boost::wformat fmt(_formatMessage);
				log_helper(level, fmt, std::forward<TArgs>(args)...);
			}
		}

		void log_message(LogLevel level, const std::wstring& _formatMessage, const message& _msg, const client_address& _from, const client_address& _to) const
		{
			if (m_config->write_log() && m_log->check_severity(level))
			{
				boost::wformat fmt(_formatMessage + std::wstring(L" [%s]->[%s] %s"));
				log_helper(level, fmt, _from.to_string(), _to.to_string(), _msg.Dump());

			}
		}

		template<typename TValue, typename... TArgs>
		void log_helper(LogLevel level, boost::wformat& _fmt, TValue&& arg, TArgs&&... args) const
		{
			_fmt % arg;
			log_helper(level, _fmt, std::forward<TArgs>(args)...);
		}

		void log_helper(LogLevel level, boost::wformat& message) const
		{
			m_log->write_log(level, message.str());
		}

		void disconnect_from_router();

		virtual bool processPacket(router_packet& packet) override;

		void process_connect(router_packet& packet);
		void process_status_change(router_packet& packet) const;
		void process_subscribe_notice(router_packet& packet) const;
		void process_message(router_packet& packet) const;

		template<typename Message, typename Address, typename ToAddress>
		void process_deliver_error(Message&& _message, Address&& _from, ToAddress&& _to) const;

		void add_unsent_packets(packet_queue&& _unsent);
		void send_handshake() const;
		void send_subscribe(const std::wstring& _messageName, router_command _command) const;

		static uint32_t hash(const std::wstring& _name);
		void processing_thread();
		void external_process_packet(router_packet _packet);

		config_pointer							m_config;
		log_pointer								m_log;
		bool									m_externalRunner;
		support::router_io_service_ptr			m_runner;
		std::unique_ptr<router_connection>		m_connection;
		uint32_t								m_clientId;


		std::unordered_map<std::wstring, MessageHandler>	m_handlers;
		std::vector<std::wstring>							m_subscription;
		packet_queue										m_unsent;

		MessageHandler							m_defaultHandler;
		StatusChangeHandler						m_statusChangeHandler;
		EventSubscribeHandler					m_eventSubscribeHandler;
		DisconnectHandler						m_disconnectHandler;
		ConnectHandler							m_connectHandler;
		DeliverErrorHandler						m_deliverErrorHandler;

		typedef std::recursive_mutex			Mutex;
		typedef std::lock_guard<Mutex>			Lock;

		Mutex									m_access;

		static const std::wstring				module_name;

		enum class State
		{
			initalizing,
			disconnected,
			connected,
			exiting,
			exited
		};

		std::atomic<State>						m_state;
	};

	using namespace std;
	
	template<typename Message, typename Address, typename ToAddress>
	inline void core::router_manager::process_deliver_error(Message&& _message, Address&& _from, ToAddress&& _to) const
	{
		log_message(LEVEL_INFO, L"[DELIVER_ERROR]", _message, _from, _to);

		if (m_deliverErrorHandler)
		{
			m_deliverErrorHandler(_message, _from, _to);
		}
	}


	template<typename Message>
	inline void router_manager::SendFromTo(Message&& _message, uint32_t _childId, const client_address& _to)
	{
		Send<typename conditional<is_lvalue_reference<Message>::value, router_packet_ref, router_packet>::type, Message>
			(std::forward<Message>(_message), Self(_childId), _to);
	}

	template<typename Message>
	inline void router_manager::SendFromTo(Message&& _message, const client_address& _from, const client_address& _to)
	{
		Send<typename conditional<is_lvalue_reference<Message>::value, router_packet_ref, router_packet>::type, Message>
			(std::forward<Message>(_message), _from, _to);
	}

	template<typename Message>
	inline void router_manager::SendFromTo(Message&& _message, uint32_t _childId, uint32_t _clientId, uint32_t _clientChildId)
	{
		SendFromTo(std::forward<Message>(_message), _childId, client_address(_clientId, _clientChildId));
	}

	template<typename Message>
	inline void router_manager::SendTo(Message&& _message, client_address _to)
	{
		Send<typename conditional<is_lvalue_reference<Message>::value, router_packet_ref, router_packet>::type, Message>
			(std::forward<Message>(_message), Self(client_address::NoChild()), std::forward<client_address>(_to));
	}

	template<typename Message>
	inline void router_manager::SendToAll(Message&& _message, uint32_t _childId)
	{
		Send<typename conditional<is_lvalue_reference<Message>::value, router_packet_ref, router_packet>::type, Message>
			(std::forward<Message>(_message), Self(_childId), client_address::All());
	}

	template<typename Message>
	inline void router_manager::SendToAllAndSelf(Message&& _message, uint32_t _childId)
	{
		Send<typename conditional<is_lvalue_reference<Message>::value, router_packet_ref, router_packet>::type, Message>
			(std::forward<Message>(_message), Self(_childId), client_address::AllAndSelf());
	}

	template<typename Message>
	inline void router_manager::SendToAny(Message&& _message, uint32_t _childId)
	{
		Send<typename conditional<is_lvalue_reference<Message>::value, router_packet_ref, router_packet>::type, Message>
			(std::forward<Message>(_message), Self(_childId), client_address::Any());
	}

	template<typename Message>
	inline void router_manager::SendToAnyAndSelf(Message&& _message, uint32_t _childId)
	{
		Send<typename conditional<is_lvalue_reference<Message>::value, router_packet_ref, router_packet>::type, Message>
			(std::forward<Message>(_message), Self(_childId), client_address::AnyAndSelf());
	}

	template<typename Message>
	void core::router_manager::SendToRegion(const std::wstring& _regionName, Message&& _message, uint32_t _childId)
	{
		if (_message.HasParam(L"$$REGION$$"))
		{
			Send<typename conditional<is_lvalue_reference<Message>::value, router_packet_ref, router_packet>::type, Message>
				(std::forward<Message>(_message), Self(_childId), client_address::RemoteRegion());
		}
		else
		{
			message msg(std::forward<Message>(_message));
			msg[L"$$REGION$$"] = _regionName;
			Send<typename conditional<is_lvalue_reference<Message>::value, router_packet_ref, router_packet>::type, Message>
				(msg, Self(_childId), client_address::RemoteRegion());
		}
	}

	template<typename value_type, typename Message, typename FromAddress, typename ToAddress >
	void core::router_manager::Send(Message&& _message, FromAddress&& _from, ToAddress&& _to)
	{
		log_message(LEVEL_INFO, L"[SEND]", _message, _from, _to);
		
		if (m_clientId == client_address::Invalid().GetClientID())
		{
			process_deliver_error(_message, _from, _to);
		}
		else
		{
			value_type packet(std::forward<FromAddress>(_from), std::forward<ToAddress>(_to), hash(_message.GetName()), std::forward<Message>(_message));

			if (Connected())
			{
				m_connection->send(support::serialize(packet));
			}
			else
			{
				Lock lock(m_access);
				m_unsent.emplace(support::serialize(packet));
			}
		}

	}


	template<typename Type, typename Func>	
	inline void SubscribeMessage(router_manager& _manager, const std::wstring& _messageName, Type * _pointer, Func _function)
	{
		_manager.RegisterSubscription(_messageName, std::bind(_function, _pointer, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
	}

	template<typename Type, typename Func>
	inline void RegisterHandler(router_manager& _manager, const std::wstring& _messageName, Type * _pointer, Func _function)
	{
		_manager.RegisterHandler(_messageName, std::bind(_function, _pointer, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
	}
}

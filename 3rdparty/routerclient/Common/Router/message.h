#pragma once
#include "message_parameter.h"
#include <unordered_map>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/split_member.hpp>
#include "message_header.h"
#include "read_write_support.h"
#include "serilization_templates.h"
#include "../Patterns/ptree_parser.h"

namespace core
{
	using namespace std;
	enum class parameter_type
	{
		Mandatory,
		Optional,
		Forbidden
	};

	class message
	{
	public:
		using ParamType			= parameter_type;
		using CheckedType		= checked_type;
		using parameter_map		= unordered_map<wstring, message_parameter>;
		using iterator			= parameter_map::iterator;
		using const_iterator	= parameter_map::const_iterator;

	public:
		message()										= default;
		message(const message&)							= default;
		message(message&&)								= default;
		
		message(const wstring& _name) : m_strName(_name) {} //-V730

		~message()										= default;

		message&			operator = (const message&)	= default;
		message&			operator = (message&&)		= default;

		message_parameter&	operator[](const wstring& _name)
		{
			return m_parameters[_name];
		}

		auto cbegin()	const { return m_parameters.cbegin(); }
		auto cend()		const { return m_parameters.cend(); }
		auto begin()	const { return m_parameters.begin(); }
		auto end()		const { return m_parameters.end(); }

		bool operator == (const wstring& _name) const { return m_strName == _name; }
		bool operator != (const wstring& _name) const { return m_strName != _name; }

		bool operator == (const message& _msg) const 
		{ 
			if (m_strName != _msg.GetName())
				return false;

			return _msg.m_parameters == m_parameters;
		}

		message& operator += (const message& _msg)
		{
			AddMessage(_msg, false);
			return *this;
		}

		const message_parameter& operator[](const wstring& _name) const;

		const wstring& GetName() const		{ return m_strName; }
		void SetName(const wstring& _name) { m_strName = _name; }
		wstring Dump() const;
		void Clear() 
		{
			m_strName.clear();
			m_parameters.clear();
		}

		void AddParam(wstring&& _name, message_parameter&& _param)
		{
			m_parameters.emplace(_name, _param);
		}
		
		void AddParam(const wstring& _name, const message_parameter& _param)
		{
			m_parameters[_name] = _param;
		}

		void Remove(const wstring& _name)
		{
			m_parameters.erase(_name);
		}
		
		bool IsParam(const wstring& _name) const
		{
			return m_parameters.find( _name) != std::cend(m_parameters);
		}

		void AddMessage(const message& _msg, bool _overwriteExistingParameters)
		{
			if (this != &_msg)
			{
				for (const auto& param : _msg)
				{
					if (_overwriteExistingParameters || !HasParam(param.first))
						m_parameters[param.first] = param.second;
				}
			}
		}

		template<typename T>
		message_parameter SafeReadParam(const wstring& _parameterName, CheckedType _type, T&& _value) const;

		void CheckParam(const wstring& _parameterName, CheckedType type, ParamType ptype) const;
		int ParamAsEnum(const wstring& _parameterName, const wstring& _possibleValues, const wstring& _separators = L";,") const;

		size_t size() const noexcept
		{
			size_t nSize = 0;
			for (const auto& parameter : m_parameters)
			{
				nSize += parameter.second.GetLength();
			}
			
			return 0x58 + nSize;
		}

		template<typename Archive>
		void load(Archive& _archive, const unsigned int _version);

		template<typename Archive>
		void save(Archive & ar, const unsigned int _version) const;

		core::serilization_type serialize() const
		{
			return ::core::support::serialize(*this);
		}

		static message deserialize(const core::serilization_type& _value, size_t _size)
		{
			return ::core::support::deserialize<message>(_value, _size);
		}

		bool HasParam(const wstring& _name) const
		{
			return m_parameters.find(_name) != m_parameters.end();
		}

		wstring ToXml(bool _blAppendType = false) const;
		void ToXml(property_tree& _xml, bool _blAppendType = false) const;
		void FromXml(const wstring& _xml, bool _removeType = false);
		void FromXml(const property_tree& _xml, bool _removeType = false);

		wstring ToJson(bool _blAppendType = false) const;
		void ToJson(property_tree& _json, bool _blAppendType = false) const;
		void FromJson(const wstring& _json, bool _removeType = false, bool allEscapeCharacters = true);
		void FromJson(const property_tree& _json, bool _removeType = false, bool allEscapeCharacters = true);

		BOOST_SERIALIZATION_SPLIT_MEMBER()

#ifdef _MSC_VER
		__declspec (property (get = GetName, put = SetName)) wstring Name;
#endif

	private:
		void parse_parameters(const property_tree& _tree, bool _removeType, bool allEscapeCharacters);

		wstring	m_strName;
		parameter_map	m_parameters;
	};

	template<typename Archive>
	void message::load(Archive& _archive, const unsigned int _size)
	{
		message_header header;
		header.serialize(_archive, 0);

		if (static_cast<int>(_size) < 0)
			throw incomplete_message_exception(static_cast<size_t>(-static_cast<int>(_size)));

		if (!header.is_valid())
			throw invalid_header_exception();

		if (header.total_size() > _size)
			throw incomplete_message_exception(header.total_size() - _size);

		m_strName = core::support::name_read_write_support::load(_archive);

		uint32_t nParameterCount = 0;
		_archive & nParameterCount;

		m_parameters.reserve(nParameterCount);

		for (uint32_t i = 0; i < nParameterCount; ++i)
		{
			uint32_t nParameterLength;
			_archive & nParameterLength;

			wstring parameterName(core::support::name_read_write_support::load(_archive));
			m_parameters.emplace(move(parameterName), message_parameter(_archive, nParameterLength));
		}
	}

	template<typename Archive>
	void message::save(Archive& _archive, const unsigned int _version) const
	{
		message_header header(static_cast<uint32_t>(size()), 0);
		
		header.serialize(_archive, 0);

		core::support::name_read_write_support::save(_archive, m_strName);
		uint32_t param_count = static_cast<uint32_t>(m_parameters.size());
		_archive & param_count;

		for(const auto& parameter : m_parameters)
		{
			uint32_t param_len = static_cast<uint32_t>(parameter.second.GetLength());
			_archive & param_len;
			core::support::name_read_write_support::save(_archive, parameter.first);
			parameter.second.save(_archive, 0);
		}

		_archive & uint32_t(0);
	}

	template<typename T>
	message_parameter message::SafeReadParam(const wstring& _parameterName, CheckedType _type, T&& _value) const
	{
		try
		{
			auto parameter = m_parameters.at(_parameterName);
			if (!parameter.Check(_type))
				throw message_parameter_invalid_type_exception(_parameterName);
			return parameter;
		}
		catch (out_of_range&)
		{
			return message_parameter(forward<T>(_value));
		}
	}
	
	inline
	const message_parameter& message::operator[](const wstring& _name) const
	{
		try
		{
			return m_parameters.at(_name);
		}
		catch (out_of_range&)
		{
			throw message_parameter_missing_exception(_name);
		}
	}

}


#include "stdafx.h"
#include "SystemLog.h"
#include "../Configuration/SystemConfiguration.h"

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/algorithm/string.hpp>

#include <boost/date_time/posix_time/posix_time_types.hpp>

namespace sinks = boost::log::sinks;
namespace keywords = boost::log::keywords;
namespace expr = boost::log::expressions;
using namespace std;

CSystemLog::CSystemLog()
{
}


CSystemLog::~CSystemLog()
{
}

BOOST_LOG_ATTRIBUTE_KEYWORD(timestamp, "TimeStamp", boost::posix_time::ptime)
BOOST_LOG_ATTRIBUTE_KEYWORD(severity, "Severity", LogLevel)

CSystemLog::log_initializer::log_initializer()// : m_loc("Russian_Russia.1251")
{
 	SystemConfig config;

	m_level = translateLogLevel(config->safe_get_config_value<wstring>(L"LogLevel", L"INFO"));

	m_sink = logging::add_file_log
		(
			keywords::file_name = config->safe_get_config_value<wstring>(L"LogFile", L"DefaultLog_%Y%m%d.log"),
			keywords::target_file_name = config->safe_get_config_value<wstring>(L"LogFile", L"DefaultLog_%Y%m%d.log"),
            keywords::time_based_rotation = sinks::file::rotation_at_time_point(0, 0, 0),
			keywords::format = expr::stream
			<< expr::format_date_time< boost::posix_time::ptime >("TimeStamp", "%H:%M:%S.%f") //-V747
			<< " | " << expr::attr<LogLevel>("Severity") << " "
			<< expr::message,
			keywords::rotation_size = 1024 * 1024 * 1024,
			keywords::open_mode = (std::ios::out | std::ios::app),
			keywords::auto_flush = true
			);

	logging::add_common_attributes();
}

LogLevel CSystemLog::log_initializer::translateLogLevel(const std::wstring& _level)
{
	if (boost::iequals(_level, L"fine"))
	{
		return LEVEL_FINE;
	}
	if (boost::iequals(_level, L"finest"))
	{
		return LEVEL_FINEST;
	}
	if (boost::iequals(_level, L"info"))
	{
		return LEVEL_INFO;
	}
	if (boost::iequals(_level, L"warning"))
	{
		return LEVEL_WARNING;
	}
	if (boost::iequals(_level, L"severe"))
	{
		return LEVEL_SEVERE;
	}
	if (boost::iequals(_level, L"fatal"))
	{
		return LEVEL_FATAL;
	}

	if (boost::iequals(_level, L"none"))
	{
		return LEVEL_FATAL;
	}

	if (boost::iequals(_level, L"all"))
	{
		return LEVEL_FINEST;
	}

	return LEVEL_INFO;
}

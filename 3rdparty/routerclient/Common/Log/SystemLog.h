#pragma once

#include "../Patterns/singleton.h"
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/format.hpp>
#include "../Patterns/string_functions.h"
#include "log_interface.h"

#ifndef MODULE_SPACE 
#define MODULE_SPACE L"20"
#endif

namespace src = boost::log::sources;
namespace logging = boost::log;


template< typename CharT, typename TraitsT >
inline std::basic_ostream< CharT, TraitsT >& operator<< (
	std::basic_ostream< CharT, TraitsT >& strm, LogLevel lvl)
{
	static const char* const str[] =
	{
		"FT",
		"FI",
		"IN",
		"WN",
		"SV",
		"FA"
	};

	if (static_cast<std::size_t>(lvl) < (sizeof(str) / sizeof(*str)))
		strm << str[lvl];
	else
		strm << static_cast<int>(lvl);
	return strm;
}

class CSystemLog : public static_singleton<CSystemLog>
{
	friend class static_singleton<CSystemLog>;

public:
	using log_sink = boost::shared_ptr<boost::log::sinks::synchronous_sink<boost::log::sinks::text_file_backend>>;

	template<typename... TArgs>
	void LogStringModule(LogLevel level, const std::wstring&_lpszModule, const std::wstring& formatMessage, TArgs&&... args)
	{
		if (level < m_init.getLevel())
			return;

		std::wstring strFormat = (boost::wformat(L"| %" MODULE_SPACE L"s |\t%s") % _lpszModule % formatMessage).str();
		LogString(level, strFormat, std::forward<TArgs>(args)...);
	}

	template<typename... TArgs>
	void LogString(LogLevel lvl, const std::wstring& fmt, TArgs&&... args)
	{
		if (lvl < m_init.getLevel())
			return;

		boost::wformat message(fmt);
		LogMessage(lvl, message, std::forward<TArgs>(args)...);
	}

	log_sink GetSink() const { return m_init.getSink();  }
	LogLevel GetLevel() const { return m_init.getLevel();  }

private:
	CSystemLog();
	~CSystemLog();

	class log_initializer
	{
	public:
		log_initializer();
		
		LogLevel getLevel() const 
		{
			return m_level;
		};

		CSystemLog::log_sink getSink() const
		{
			return m_sink;
		}

	private:
		LogLevel				m_level;
		CSystemLog::log_sink	m_sink;
		LogLevel	translateLogLevel(const std::wstring& _level);
	};

	void LogMessage(LogLevel level, boost::wformat& message) 
	{
		//boost::log::trivial::severity_level lvl = static_cast<boost::log::trivial::severity_level>(level);
		BOOST_LOG_SEV(m_lg, level) << wtos(message.str());
	}

	template<typename TValue, typename... TArgs>
	void LogMessage(LogLevel lvl, boost::wformat& message, TValue&& arg, TArgs&&... args) 
	{
		message % arg;
		LogMessage(lvl, message, std::forward<TArgs>(args)...);
	}


	log_initializer						m_init;
	src::severity_logger_mt<LogLevel>	m_lg;
};

typedef singleton_auto_pointer<CSystemLog> SystemLog;
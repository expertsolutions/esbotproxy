#pragma once
#include <memory>
#include <string>

enum LogLevel
{
	LEVEL_FINEST,
	LEVEL_FINE,
	LEVEL_INFO,
	LEVEL_WARNING,
	LEVEL_SEVERE,
	LEVEL_FATAL
};
 
class log_interface
{
public:
	virtual ~log_interface() {}

	virtual bool check_severity(LogLevel _level)							= 0;
	virtual void write_log(LogLevel _level, const std::wstring& _logEntry)	= 0;
};

using log_pointer = std::shared_ptr<log_interface>;

#pragma once

#include <boost/asio.hpp>
#include <memory>
#include <thread>

using io_service_ptr = std::shared_ptr<boost::asio::io_service>;

class io_service_runner
{
public:
	io_service_runner(size_t _nThreads);

	io_service_runner(const io_service_runner& _other) = delete;
	io_service_runner(io_service_runner&& _other) = delete;
	io_service_runner& operator=(const io_service_runner& _other) = delete;
	io_service_runner& operator=(io_service_runner&& _other) = delete;

	void run();
	void stop();

	boost::asio::io_service&		get_service() { return m_service; }

private:
	void work_function();

	boost::asio::io_service			m_service;
	size_t							m_nThreads;
	std::unique_ptr<std::thread>	m_worker;
	std::unique_ptr<boost::asio::io_service::work> m_work;
};


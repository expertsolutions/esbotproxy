#pragma once

#include <vector>

template <typename T>
class circular_vector
{
public:
	circular_vector() : m_current(m_items.begin()) {}

	template<typename... Args>
	void emplace_back(Args ... args)
	{
		m_items.emplace_back(std::forward<Args>(args)...);
		m_current = m_items.begin();
	}

	void move_next()
	{
		m_current++;
		if (m_current == m_items.end())
			m_current = m_items.begin();
	}

	const T& get() const
	{
		if (!m_items.size())
			throw std::runtime_error("Empty vector");
		return *m_current;
	}

	const T& get_and_move_next()
	{
		const T& value = get();
		move_next();
		return value;
	}

private:
	std::vector<T> m_items;
	typename std::vector<T>::iterator m_current;
};
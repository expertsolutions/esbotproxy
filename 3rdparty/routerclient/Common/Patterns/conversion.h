#pragma once

#include <boost/fusion/tuple.hpp>
#include <boost/fusion/algorithm.hpp>
#include <boost/tokenizer.hpp>
#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>
#include <tchar.h>

namespace
{

template <class StringType>
struct SeparatorHelper;

template <>
struct SeparatorHelper<std::string>
{
	static std::string GetSeparator() { return std::string(","); }
};

template <>
struct SeparatorHelper<std::wstring>
{
	static std::wstring GetSeparator() { return std::wstring(L","); }
};

template<class StringType>
class to_string
{
	mutable StringType& result;

public:
	to_string(StringType& _str) : result(_str)
	{
	}

	template<typename T>
	void operator()(T& x) const
	{
		if(result.length() != 0)
			result += SeparatorHelper<StringType>::GetSeparator();
		result += boost::lexical_cast<StringType>(x);
	}
};

template<typename IteratorType>
class from_string
{
	mutable			IteratorType i;

public:
	from_string(IteratorType _i) : i(_i)
	{
	}

	template<typename T>
	void operator()(T& x) const
	{
		IteratorType end;
		if(i == end)
			throw std::runtime_error(std::string("Not enough parameters in string"));
		x = boost::lexical_cast<T>(*i);
		++i;
	}
};


template<typename StringType, typename T>
StringType convert_to_string(const T& _value) 
{
	StringType result;
	boost::fusion::for_each(_value, to_string<StringType>(result));
	return result;
}

template<typename StringType, typename T>
void convert_from_string(const StringType& _string, T& _value, LPCTSTR _separators = _T(","))
{
	typedef boost::token_iterator_generator<boost::char_separator<StringType::value_type>, StringType::const_iterator, StringType >::type	CharIterator;
	CharIterator begin = boost::make_token_iterator<StringType>(_string.begin(), _string.end(), boost::char_separator<StringType::value_type>(_separators));
	boost::fusion::for_each(_value, from_string<CharIterator>(begin));
}

}

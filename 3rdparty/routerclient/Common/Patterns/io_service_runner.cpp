#include "stdafx.h"
#include "io_service_runner.h"
#include <vector>
#include <boost/bind.hpp>
#include <boost/thread.hpp>

io_service_runner::io_service_runner(size_t _nThreads) : m_nThreads(_nThreads)
{
}

void io_service_runner::run()
{
	m_work = std::make_unique<boost::asio::io_service::work>(m_service);
	m_worker = std::make_unique<std::thread>(&io_service_runner::work_function, this);
}

void io_service_runner::stop()
{
	m_work.reset();
	m_service.stop();
	if (m_worker->joinable())
		m_worker->join();
}


void io_service_runner::work_function() 
{
	// Create a pool of threads to run all of the io_services.
	std::vector<boost::thread> threads;
	threads.reserve(m_nThreads - 1);

	for (std::size_t i = 0; i < m_nThreads - 1; ++i)
	{
		threads.emplace_back(boost::bind(&boost::asio::io_service::run, &m_service));
	}

	m_service.run();

	// Wait for all threads in the pool to exit.
	for (std::size_t i = 0; i < threads.size(); ++i)
	{
		if (threads[i].joinable())
			threads[i].join();
	}
}

#pragma once

#include <string>
#include <boost/lexical_cast.hpp>
//#include <cvt/wstring>
#include <codecvt>
#include <locale.h>
#include <boost/locale.hpp>
#include <boost/predef.h>
#include <iomanip>

#if BOOST_COMP_MSVC
#include <atlstr.h>
using ATL::CString;
#endif

#if !BOOST_COMP_MSVC
inline std::wstring stow(const std::string& in)
{
	typedef std::codecvt_utf8<wchar_t> convert_typeX;
	std::wstring_convert<convert_typeX, wchar_t> converterX;
	return converterX.from_bytes(in);
}

inline std::string wtos(const std::wstring& in)
{
	typedef std::codecvt_utf8<wchar_t> convert_typeX;
	std::wstring_convert<convert_typeX, wchar_t> converterX;
	return converterX.to_bytes(in);
}
#else

inline std::wstring stow(const std::string& in)
{
	std::wstring result(in.length(), 0);
	::MultiByteToWideChar(CP_ACP, 0, in.data(), static_cast<int>(in.length()), const_cast<wchar_t*>(result.data()), static_cast<int>(result.length()));
	return result;
//	return boost::locale::conv::to_utf<wchar_t>(in, "Latin1");
}

inline std::string wtos(const std::wstring& in, size_t _resultSize = 0)
{
	size_t nConversionSize = _resultSize;
	if (_resultSize == 0)
	{
		nConversionSize = in.length();
	}

	std::string result(nConversionSize, 0);
	::WideCharToMultiByte(CP_ACP, 0, in.data(), static_cast<int>(in.length()), const_cast<char *>(result.data()), static_cast<int>(result.length()), nullptr, nullptr);
	return result;
}

inline std::string wtos(const CString& in, size_t _resultSize = 0, std::locale loc = std::locale())
{
	return wtos(std::wstring((LPCWSTR)in, _resultSize));
}

inline std::string wtos(LPCWSTR in, size_t _resultSize = 0, std::locale loc = std::locale())
{
	return wtos(std::wstring(in), _resultSize);
}
#endif

inline std::wstring from_utf8(const std::string& _val)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>> conv;
	return conv.from_bytes(_val);
}

inline std::string to_utf8(const std::wstring& _val)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>> conv;
	return conv.to_bytes(_val);
}

/*template<int size>
void safe_copy(char(&_dest)[size], const std::string& _value)
{
	strncpy_s(_dest, sizeof(_dest) - 1, _value.c_str(), _TRUNCATE);
}*/

template<typename Stream, typename Collection, typename CharType>
void encode_to_hex(Stream& _stream, const Collection& _collection, CharType _fill)
{
	_stream << std::hex << std::uppercase << std::setfill(_fill);
	std::for_each(std::cbegin(_collection), std::cend(_collection), [&](typename Collection::value_type _val) { _stream << std::setw(2) << int(_val); });
}

namespace boost
{
	template<>
	inline std::wstring lexical_cast<std::wstring, std::string>(const std::string& _value)
	{
		return stow(_value);
	}

	template<>
	inline std::string lexical_cast<std::string, std::wstring>(const std::wstring& _value)
	{
		return wtos(_value);
	}
}


#pragma once
#include <chrono>
#include <inttypes.h>
#include "../Log/SystemLog.h"

namespace core
{
	namespace profile
	{
		template<typename Function>
		int64_t do_profiling(Function&& _func)
		{
			using namespace std::chrono;
			auto start = high_resolution_clock::now();
			_func();
			return duration_cast<milliseconds>(high_resolution_clock::now() - start).count();
		}

		template<typename Function, typename... TArgs>
		void log_profiling(Function&& _func, LogLevel lvl, const std::wstring _moduleName, const std::wstring& _format, TArgs&&... args)
		{
			using namespace std::chrono;
			SystemLog log;
			auto start = high_resolution_clock::now();
			_func();
			log->LogStringModule(lvl, _moduleName, std::wstring(L"[PROFILE] [%" PRId64 "ms] ") + _format, duration_cast<milliseconds>(high_resolution_clock::now() - start).count(), std::forward<TArgs>(args)...);
		}

	}
}
#pragma once

#include <list>
#include <vector>
#include <mutex>

namespace core
{
	using namespace std;
	template<typename T, typename ActiveHolder = vector<T>, typename PassiveHolder = list<T>, typename Mutex = std::mutex>
	class resource_pool
	{
	protected:
		ActiveHolder			m_active;
		PassiveHolder			m_passive;
		Mutex					m_accessController;

		using lock = lock_guard<Mutex>;

	public:
		resource_pool()
		{
		}

		~resource_pool()
		{
		}

		void add(T _object)
		{
			lock lock(m_accessController);
			m_passive.push_back(_object);
		}

		void emplace(T&& _object)
		{
			m_passive.emplace_back(std::move(_object));
		}

		T& next_free()
		{
			lock lock(m_accessController);
			if (m_passive.size() == 0)
				throw std::runtime_error("No free resources");

			m_active.push_back(m_passive.front());
			m_passive.pop_front();
			return m_active[m_active.size() - 1];
		}

		void release(T _value)
		{
			lock lock(m_accessController);
			auto i = std::find(m_active.begin(), m_active.end(), _value);
			if (i == m_active.end())
				return;
			m_passive.push_back(*i);
			m_active.erase(i);
		}

		void erase(T& _value)
		{
			lock lock(m_accessController);
			auto i = std::find(m_active.begin(), m_active.end(), _value);
			if (i != m_active.end())
				m_active.erase(i);

			auto pi = std::find(m_passive.begin(), m_passive.end(), _value);
			if (pi != m_passive.end())
				m_passive.erase(pi);
		}

		decltype(auto) size() const noexcept { return m_passive.size();  }
	};


}

#pragma once
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <mutex>

namespace core
{
	using property_tree = boost::property_tree::wptree;

	namespace support
	{
		class ptree_parser
		{
			using Mutex = std::mutex;
			using Lock = std::lock_guard<Mutex>;

			template<typename TreeType, typename FuncPtr>
			static TreeType tree_from_string(const std::wstring& _data, FuncPtr _func)
			{
				static Mutex parserLock;
				using namespace boost::property_tree;

				std::wistringstream ss(_data);
				TreeType _wptree;
				Lock lock(parserLock);
				_func(ss, _wptree);
				return _wptree;
			}

		public:
			template<typename TreeType = property_tree>
			static TreeType tree_from_string_json(const std::wstring& _data)
			{
				return tree_from_string<TreeType>(_data, [](std::wistringstream& data, TreeType& _tree) { boost::property_tree::json_parser::read_json(data, _tree); });
			}

			template<typename TreeType = property_tree>
			static TreeType tree_from_string_xml(const std::wstring& _data)
			{
				return tree_from_string<TreeType>(_data, [](std::wistringstream& data, TreeType& _tree) { boost::property_tree::xml_parser::read_xml(data, _tree); });
			}

			static std::wstring tree_to_json_string(const property_tree& _tree);
			static std::wstring tree_to_xml_string(const property_tree& _tree);

			template <typename T, typename TreeType = property_tree>
			static T get_xml_attr(const TreeType& _ptree, const std::wstring& _name)
			{
				return _ptree.template get<T>((boost::wformat(L"<xmlattr>.%s") % _name).str());
			}
		};


	}
}

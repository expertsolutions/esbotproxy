#include "stdafx.h"
#include "ptree_parser.h"
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/algorithm/string.hpp>
#include <sstream>


std::wstring core::support::ptree_parser::tree_to_json_string(const property_tree& _tree)
{
	std::wostringstream os;
	boost::property_tree::json_parser::write_json(os, _tree, false);
	auto result = os.str();
	return result.substr(0, result.length() - 1);
}

std::wstring core::support::ptree_parser::tree_to_xml_string(const property_tree& _tree)
{
	std::wostringstream os;
	boost::property_tree::xml_parser::write_xml(os, _tree, false);
	return os.str();
}
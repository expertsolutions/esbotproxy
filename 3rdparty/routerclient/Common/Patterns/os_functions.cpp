#include "stdafx.h"
#include "string_functions.h"


namespace core
{
	namespace support
	{
		std::wstring get_computer_name()
		{
#ifdef WIN32
			WCHAR compname[4096] = { 0 };
			DWORD dwSize = sizeof(compname) / sizeof(compname[0]);
			::GetComputerNameW(compname, &dwSize);
			return compname;
#else
			char compname[HOST_NAME_MAX];
			gethostname(compname, HOST_NAME_MAX);
			return stow(compname);
#endif
		}

		uint32_t get_current_thread_id()
		{
#ifdef _WIN32
			return ::GetCurrentThreadId();
#else
			return pthread_self();
#endif
		}
	}

}


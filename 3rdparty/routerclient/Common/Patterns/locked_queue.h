#ifndef LOCKEDQUEUE_H
#define LOCKEDQUEUE_H

#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>


template <typename T>
class locked_queue
{
	using mutex			= std::mutex;
	using cvar			= std::condition_variable;
	using lock_guard	= std::lock_guard<mutex>;
	using unique_lock	= std::unique_lock<mutex>;

	std::queue<T>	m_queue;

	cvar			m_cond;
	mutex			m_mut;

public:
	locked_queue() 
	{

	}

	~locked_queue()
	{
	}

	size_t size()
	{
		lock_guard lock(m_mut);
		return m_queue.size();
	}

	void push(const T& _value)
	{
		lock_guard lock(m_mut);
		m_queue.push(_value);
		m_cond.notify_one();
	}

	template<typename... Args>
	void emplace(Args... args)
	{
		lock_guard lock(m_mut);
		m_queue.emplace(std::forward<Args>(args)...);
		m_cond.notify_one();
	}

	T& front()
	{
		unique_lock lock(m_mut);
		m_cond.wait(lock, std::bind(&locked_queue<T>::not_empty, this));
		return m_queue.front();
	}


	void pop()
	{
		unique_lock lock(m_mut);
		m_cond.wait(lock, std::bind(&locked_queue<T>::not_empty, this));
		m_queue.pop();
	}

	T pop_front()
	{
		unique_lock lock(m_mut);
		m_cond.wait(lock, std::bind(&locked_queue<T>::not_empty, this));
		T result(std::move(m_queue.front()));
		m_queue.pop();
		return result;
	}

	bool empty()
	{
		return m_queue.empty();
	}

	bool not_empty()
	{
		return !m_queue.empty();
	}

};

#endif //LOCKEDQUEUE_H

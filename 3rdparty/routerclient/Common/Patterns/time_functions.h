#pragma once

#include <chrono>
#include <ctime>
#include <sstream>
#include <iomanip>
#include <boost/format.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include "../Router/message_parameter_types.h"
#include "string_functions.h"

namespace core
{
	namespace support
	{
		using namespace std;
		using namespace std::chrono;

		inline uint64_t posixtime_to_uint64(const boost::posix_time::ptime& time)
		{
			static const std::tm unix_epoch = { 0, 0, 0, 1, 1, 1970, 0, 0, -1 };
			static const std::tm windows_epoch = { 0, 0, 0, 1, 1, 1601, 0, 0, -1 };

			struct utils
			{
				static auto get_epoch(const std::tm& epoch)
				{
					std::tm nonconst_epoch = epoch;
					static std::time_t tt =
#ifdef _WIN32
						_mkgmtime(&nonconst_epoch);
#else
						timegm(&nonconst_epoch);
#endif
					return std::chrono::system_clock::from_time_t(tt);
				}
			};

			auto duration = time - boost::posix_time::ptime();
			return (utils::get_epoch(unix_epoch) - utils::get_epoch(windows_epoch)).count() + duration.total_nanoseconds() / 100;
		}

		class time_format_exception : public std::runtime_error
		{
		public:
			time_format_exception(const std::string& _message) : std::runtime_error(_message) {}
		};

		inline int get_timezone_offset()
		{
			time_t currtime;
			struct tm timeinfo = { 0 };

			time(&currtime);
			timeinfo = *gmtime(&currtime);

			time_t utc = mktime(&timeinfo);
			timeinfo = *localtime(&currtime);

			time_t local = mktime(&timeinfo);
			double offsetFromUTC = difftime(utc, local) / 60;
			// Adjust for DST
			if (timeinfo.tm_isdst)
			{
				offsetFromUTC -= 60;
			}

			return -static_cast<int>(offsetFromUTC);
		}

		inline core::time_type to_local_time(const core::time_type& _time_point)
		{
			return _time_point + std::chrono::minutes(get_timezone_offset());
		}

		inline double to_variant_time(const core::time_type& _time_point)
		{
			std::time_t time = clock_type::to_time_t(_time_point);
			return time / 86400.0 + 25569;
		}

		inline core::time_type to_time_type(double _value, uint64_t _milliseconds = 0)
		{
			std::time_t time = static_cast<std::time_t>(((_value - 25569) * 86400.0) + 0.5);
			auto result = clock_type::from_time_t(time);
			return result + std::chrono::milliseconds(_milliseconds);
		}

		inline uint64_t get_milliseconds(const core::time_type& _time_point)
		{
			using namespace std::chrono;
			milliseconds ms = duration_cast<milliseconds>(_time_point.time_since_epoch());
			return ms.count() % 1000;
		}

		inline core::time_type now()
		{
			return core::time_type::clock::now();
		}

		inline core::time_type local_time()
		{
			return to_local_time(now());
		}

		namespace timezone_parser
		{

			inline minutes read_timezone_offset(wistringstream& _ss)
			{
				unsigned int hours_value = 0, minutes_value = 0;
				wchar_t separator = 0;

				_ss >> hours_value >> separator;
				if (hours_value > 23)
					throw time_format_exception("Invalid hours value in timezone information");

				if (separator != L':')
					throw time_format_exception("Invalid separator in timezone information");

				if(_ss.eof())
					throw time_format_exception("Missing minutes in timezone information");

				_ss >> minutes_value;
				if (minutes_value > 59)
					throw time_format_exception("Invalid minutes value in timezone information");

				return minutes(hours_value * 60 + minutes_value);
			}

		}

		inline core::time_type time_from_string(const std::wstring& _val, bool _applyTimezoneOffset = true)
		{
			std::tm tm = {};
			std::wistringstream ss(_val);
			wchar_t separator = 0;
			int millisecond_count = 0;
			minutes timezone_offset(0min);

			ss >> std::get_time(&tm, L"%Y-%m-%d %H:%M:%S");
			if (!ss.eof())
			{
				ss >> separator;

				if (separator == L'.')
				{
					ss >> millisecond_count;

					if(!ss.eof())
						ss >> separator;
				}
		
				if (!ss.eof() && _applyTimezoneOffset)
				{
					if (separator == L'+')
					{
						timezone_offset = -timezone_parser::read_timezone_offset(ss);
					}
					else if (separator == L'-')
					{
						timezone_offset = timezone_parser::read_timezone_offset(ss);
					}
					else if (separator != L'Z')
						throw time_format_exception((boost::format("Invalid time format %s in time_from_string function") % wtos(_val)).str());
				}
			}

			time_t utctime = mktime(&tm) + get_timezone_offset() * 60;

			return core::time_type::clock::from_time_t(utctime) + milliseconds(millisecond_count) + timezone_offset;
		}

		template<typename T>
		class char_stream_formatter
		{
		};

		template<>
		class char_stream_formatter<wchar_t>
		{
		public:
			static constexpr const wchar_t*	get_format()  { return L"%Y-%m-%d %H:%M:%S"; }
			static constexpr const wchar_t*	get_iso_format() { return L"%Y-%m-%dT%H:%M:%S"; }
			static constexpr wchar_t	get_fill()  { return L'0'; }
			static constexpr wchar_t	get_point()  { return L'.'; }
			static constexpr wchar_t	get_tz_separator()  { return L':'; }
			static constexpr wchar_t	get_plus_sign() { return L'+'; }
			static constexpr wchar_t	get_minus_sign() { return L'-'; }
			static constexpr wchar_t	get_null_tz() { return L'Z'; }
			static std::wostringstream	get_stream() { return std::wostringstream(); }
		};

		template<>
		class char_stream_formatter<char>
		{
		public:
			static constexpr const char*	get_format()  { return "%Y-%m-%d %H:%M:%S"; }
			static constexpr const char*	get_iso_format() { return "%Y-%m-%dT%H:%M:%S"; }
			static constexpr char		get_fill()  { return '0'; }
			static constexpr char		get_point()  { return '.'; }
			static constexpr char		get_tz_separator()  { return ':'; }
			static constexpr char		get_null_tz() { return 'Z'; }
			static constexpr char		get_plus_sign() { return '+'; }
			static constexpr char		get_minus_sign() { return '-'; }
			static std::ostringstream	get_stream() { return std::ostringstream(); }
		};

		template<typename Stream>
		Stream& write_gmt_time(Stream& _stream, const core::time_type& _time_point)
		{
			using formatter = core::support::char_stream_formatter<typename Stream::char_type>;

			std::time_t t = core::clock_type::to_time_t(_time_point);
			auto fractional_seconds = core::support::get_milliseconds(_time_point);

			struct tm time = *gmtime(&t);

			_stream << std::put_time(&time, formatter::get_format()) << formatter::get_point() << std::setw(3) << std::setfill(formatter::get_fill()) << fractional_seconds;
			return _stream;
		}

		template<typename Stream>
		Stream& write_iso_time(Stream& _stream, const core::time_type& _time_point)
		{
			using formatter = core::support::char_stream_formatter<typename Stream::char_type>;

			std::time_t t = core::clock_type::to_time_t(_time_point);
			auto fractional_seconds = core::support::get_milliseconds(_time_point);

#ifdef _WIN32
			struct tm time;
			localtime_s(&time, &t);
#else
			struct tm time = localtime(&t);
#endif
			_stream << std::put_time(&time, formatter::get_iso_format()) << formatter::get_point() << std::setw(3) << std::setfill(formatter::get_fill()) << fractional_seconds;
			return _stream;
		}

		template<typename Stream>
		Stream& operator << (Stream& _stream, const core::time_type& _time_point)
		{
			core::support::write_gmt_time<Stream>(_stream, _time_point);
			return _stream;
		}

		template<typename StringType>
		StringType time_to_string(const core::time_type& _time)
		{
			auto stream = char_stream_formatter<typename StringType::value_type>::get_stream();
			stream << _time;
			return stream.str();
		}

		template<typename Stream>
		Stream& write_tz_offset(Stream& _stream)
		{
			using formatter = core::support::char_stream_formatter<typename Stream::char_type>;

			minutes offset(get_timezone_offset());
			hours   total_hours(offset.count() / 60);
			offset = minutes(offset.count() % 60);

			if (total_hours.count() > 0)
				_stream << formatter::get_plus_sign() << setw(2) << setfill(formatter::get_fill()) << total_hours.count();
			else if (total_hours.count() == 0 && offset.count() == 0)
				_stream << formatter::get_null_tz();
			else
				_stream << formatter::get_minus_sign() << setw(2) << setfill(formatter::get_fill()) << -total_hours.count();

			_stream << formatter::get_tz_separator() << setw(2) << setfill(formatter::get_fill()) << offset.count();
			return _stream;
		}


		template<typename StringType>
		StringType time_to_iso_string(const core::time_type& _time)
		{
			using formatter = core::support::char_stream_formatter<typename StringType::value_type>;
			auto stream = formatter::get_stream();
			write_gmt_time(stream, _time);
			write_tz_offset(stream);
			return stream.str();
		}

		template<typename StringType>
		StringType time_to_iso_8601_string(const core::time_type& _time)
		{
			using formatter = core::support::char_stream_formatter<typename StringType::value_type>;
			auto stream = formatter::get_stream();
			write_iso_time(stream, _time);
			write_tz_offset(stream);
			return stream.str();
		}

		template <typename Container, typename Fun>
		void tuple_for_each(const Container& c, Fun fun)
		{
			for (auto& e : c)
				fun(std::get<0>(e), std::get<1>(e), std::get<2>(e));
		}

		template <typename TimeType>
		std::wstring duration_to_string(const TimeType& _time)
		{
			using namespace std::chrono;

			using T = std::tuple<milliseconds, int, const wchar_t *>;

			auto time = duration_cast<milliseconds>(_time);

			T formats[] = {
				T{ hours(1), 2, L"" },
				T{ minutes(1), 2, L":" },
				T{ seconds(1), 2, L":" },
				T{ milliseconds(1), 3, L"." }
			};

			std::wostringstream o;
			tuple_for_each(formats, [&time, &o](auto denominator, auto width, auto separator) {
				o << separator << std::setw(width) << std::setfill(L'0') << (time / denominator);
				time = time % denominator;
			});

			return o.str();
		}
	}
}


#pragma once
#include <boost/iostreams/device/array.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/archive/binary_iarchive.hpp>

namespace core
{
	namespace support
	{
		class binary_to_archive_converter
		{
			using binary_data = boost::iostreams::basic_array_source<char>;

			boost::iostreams::stream_buffer<binary_data> m_data;
			boost::archive::binary_iarchive m_archive;

		public:
			binary_to_archive_converter(const char * _data, size_t _length) : m_data(_data, _length), m_archive(m_data, boost::archive::no_header | boost::archive::no_codecvt | boost::archive::no_tracking)
			{
			}

			boost::archive::binary_iarchive& convert() { return m_archive; }
		};
	}
}

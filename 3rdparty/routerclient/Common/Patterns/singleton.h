#pragma once

//#include <atlsync.h>
#include <boost/noncopyable.hpp>

template <class T>
class static_singleton
{
public:
	static T * GetInstance()
	{
		static T instance;
		return &instance;
	}

	static uint32_t ReleaseInstance()
	{
		return 0;
	}

protected:
	static_singleton() {};
	~static_singleton() {}

	static_singleton(const static_singleton<T>&) = delete;
	static_singleton& operator = (const static_singleton<T>&) = delete;
};

//template<class T>
//T static_singleton<T>::instance;

template <class T>
class singleton : public boost::noncopyable
{
	static T *		m_instance;
	static int32_t		m_dwCounter;
public:
	static T * GetInstance()
	{
		if(m_instance == NULL)
			m_instance = new T();
//		::InterlockedIncrement(&m_dwCounter);
		++m_dwCounter;
		return m_instance;
	}

	static uint32_t ReleaseInstance()
	{
		//::InterlockedDecrement(&m_dwCounter);
		--m_dwCounter;
		if(m_dwCounter <= 0 && m_instance != NULL)
		{
			delete m_instance;
			m_instance = NULL;
		}
		return m_dwCounter;
	}
};

template <class T>
int32_t singleton<T>::m_dwCounter = 0;

template <class T>
T * singleton<T>::m_instance = NULL;

template <class T>
class singleton_auto_pointer
{
	T * m_instance;
public:
	singleton_auto_pointer()
	{
 		m_instance = T::GetInstance();
	}

	~singleton_auto_pointer()
	{
		T::ReleaseInstance();
	}

	T * operator -> ()
	{
		return m_instance;
	}
};

template <class T>
class singleton_locked_pointer : public singleton_auto_pointer<T>
{
public:
	singleton_locked_pointer()
	{
		(*this)->LockResource();
	}

	~singleton_locked_pointer()
	{
		(*this)->FreeResource();
	}
};

#pragma once


namespace core
{
	namespace support
	{
		std::wstring get_computer_name();
		uint32_t get_current_thread_id();
	}
}


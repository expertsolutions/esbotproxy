#pragma once
#include <type_traits>
#include <vector>

namespace core
{
	using namespace std;

	template <typename T> 
	using is_simple_type = integral_constant<bool, !is_same<T, bool>::value & (is_arithmetic<T>::value | is_enum<T>::value) >;

	template <typename T>
	using is_string_type = integral_constant<bool, is_same<T, std::string>::value | is_same<T, std::wstring>::value>;

	using raw_data_type = std::vector<char>;

	class serilization_error : public runtime_error
	{
	public:
		serilization_error() : runtime_error("Serilization error")
		{
		}
	};

	class skip
	{
		size_t m_skipSize;
	
	public:
		skip(size_t _skipSize) : m_skipSize(_skipSize)
		{
		}

		size_t length() const noexcept { return m_skipSize; }
	};

	template<typename string_type>
	class string_io
	{
		const string_type&	m_string;
		size_t				m_length;
	public:

		string_io(const string_type& _value) : m_string(_value), m_length(m_string.length())
		{

		}

		string_io(string_type& _value, size_t _length) : m_string(_value), m_length(_length)
		{
			if (_value.length() != m_length)
				_value.resize(_length);
		}

		size_t length() const noexcept { return m_length * sizeof(string_type::value_type); }
		size_t string_length() const noexcept { return m_string.length() * sizeof(string_type::value_type); }

		decltype(auto) begin()	const { return m_string.begin(); }
		decltype(auto) cbegin() const { return m_string.begin(); }

		decltype(auto) end()	const { return m_string.cend(); }
		decltype(auto) cend()	const { return m_string.cend(); }

		decltype(auto) data()	const { return reinterpret_cast<void *>(const_cast<typename string_type::value_type *>(m_string.data())); }
	};

	class binary_input_stream
	{
		const raw_data_type&	m_input;
		size_t					m_readOffset = 0;

	public:
		binary_input_stream(const raw_data_type& _input) : m_input(_input)
		{
		}

		void verify_size(size_t _elementSize) const
		{
			if (m_readOffset + _elementSize > m_input.size())
				throw serilization_error();
		}

		template<typename T>
		typename enable_if<is_simple_type<T>::value, binary_input_stream&>::type
		operator & (T& value)
		{
			verify_size(sizeof(T));
			value = *reinterpret_cast<const T*>(m_input.data() + m_readOffset);
			m_readOffset += sizeof(T);

			return *this;
		}

		binary_input_stream& operator & (skip _skip)
		{
			verify_size(_skip.length());
			m_readOffset += _skip.length();
			return *this;
		}

		template<typename T>
		typename enable_if<is_string_type<T>::value, binary_input_stream&>::type
		operator & (string_io<T> _io)
		{
			verify_size(_io.length());
			std::memcpy(_io.data(), m_input.data() + m_readOffset, _io.length());
			m_readOffset += _io.length();
			return *this;
		}

		bool is_writing() { return false; }
	};

	class binary_output_stream
	{
		raw_data_type	m_output;
		size_t			m_writeOffset = 0;
	public:
		binary_output_stream(size_t _expectedSize = 0) : m_output(_expectedSize)
		{
		}

		void resize(size_t elementSize)
		{
			if (m_output.capacity() < m_writeOffset + elementSize)
			{
				m_output.resize(m_writeOffset + elementSize);
			}
		}

		template<typename T>
		typename enable_if<is_simple_type<T>::value, binary_output_stream&>::type
		operator & (T value)
		{
			resize(sizeof(T));
			*reinterpret_cast<T*>(m_output.data() + m_writeOffset) = value;
			m_writeOffset += sizeof(value);

			return *this;
		}

		template<typename T>
		typename enable_if<is_string_type<T>::value, binary_output_stream&>::type
		operator & (string_io<T> _io)
		{
			resize(_io.length());
			std::memcpy(m_output.data() + m_writeOffset, _io.data(), _io.string_length());
			m_writeOffset += _io.length();
			return *this;
		}

		binary_output_stream& operator & (skip _skip)
		{
			resize(_skip.length());
			m_writeOffset += _skip.length();
			return *this;
		}

		const raw_data_type& get() const { return m_output; }

		bool is_writing() { return true; }
	};
}

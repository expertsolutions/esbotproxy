#pragma once

#include "../Patterns/singleton.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>
#include "../Patterns/ptree_parser.h"

namespace core
{
	using iproperty_tree	= boost::property_tree::wiptree;
	using config_node		= iproperty_tree;

	class configuration_error : public std::runtime_error
	{
	public:
		configuration_error(const char * _what) : std::runtime_error(_what) {}
	};

	template <typename T>
	T get_config_attr(const config_node& _ptree, const std::wstring& _name)
	{
		try
		{
			return support::ptree_parser::get_xml_attr<T>(_ptree, _name);
		}
		catch (boost::property_tree::ptree_error& _err)
		{
			throw configuration_error(_err.what());
		}
	}

	template <typename T>
	T safe_get_config_attr(const config_node& _ptree, const std::wstring& _name, const T& _default_value)
	{
		try
		{
			return _ptree.get<T>((boost::wformat(L"<xmlattr>.%s") % _name).str());
		}
		catch (boost::property_tree::ptree_error&)
		{
			return _default_value;
		}
	}

	inline bool get_config_bool(const config_node& _ptree, const std::wstring& _name)
	{
		return boost::iequals(get_config_attr<std::wstring>(_ptree, _name), L"true");
	}

	class configuration
	{
	public:
		configuration(const std::wstring& _configuration, bool _treatAsFilename = true);

		template <typename T>
		T safe_get_config_value(const std::wstring& _name, const T& _default) const
		{
			try
			{
				return get_config_value<T>(_name);
			}
			catch (configuration_error&)
			{
				return _default;
			}
		}

		int safe_get_config_int(const std::wstring& _name, int _default)
		{
			return safe_get_config_value<int>(_name, _default);
		}

		bool safe_get_config_bool(const std::wstring& _name, bool _default)
		{
			return safe_get_config_value<bool>(_name, _default);
		}

		template <bool>
		bool safe_get_config_value(const std::wstring& _name, bool _default) const
		{
			try
			{
				return get_config_value<bool>(_name);
			}
			catch(configuration_error&)
			{ 
				return _default;
			}
		}

		template<typename T>
		T get_config_value(const std::wstring& _name) const
		{
			try
			{
				return m_config.get<T>((boost::wformat(L"configuration.%s.<xmlattr>.value") % _name).str());
			}
			catch (boost::property_tree::ptree_error& _err)
			{
				throw configuration_error(_err.what());
			}
		}

		template<bool>
		bool get_config_value(const std::wstring& _name) const
		{
			return boost::iequals(get_config_value<std::wstring>(_name), L"true");
		}

		template <typename T>
		T get_config_attr(const config_node& _ptree, const std::wstring& _name) const
		{
			return core::get_config_attr<T>(_ptree, _name);
		}

		template <typename T>
		T safe_get_config_attr(const config_node& _ptree, const std::wstring& _name, const T& _default_value) const
		{
			return core::safe_get_config_attr<T>(_ptree, _name, _default_value);
		}

		bool get_config_bool(const config_node& _ptree, const std::wstring& _name) const
		{
			return core::get_config_bool(_ptree, _name);
		}

		const config_node& get_config_node(const std::wstring& _name) const
		{
			try
			{
				return m_config.get_child(std::wstring(L"configuration.") + _name);
			}
			catch (boost::property_tree::ptree_error& _err)
			{
				throw configuration_error(_err.what());
			}
		}

		const config_node& get_root_node() const
		{
			return m_config;
		}

	private:
		iproperty_tree m_config;

	};
}

class CSystemConfiguration : public core::configuration, public static_singleton<CSystemConfiguration>
{
	friend class static_singleton<CSystemConfiguration>;

public:
	static std::wstring get_module_name();

private:
	CSystemConfiguration();
	~CSystemConfiguration();

};


typedef singleton_auto_pointer<CSystemConfiguration> SystemConfig;

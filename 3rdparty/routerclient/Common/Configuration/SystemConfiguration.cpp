#include "stdafx.h"
#include "SystemConfiguration.h"
#include <boost/property_tree/xml_parser.hpp>
#include "../Patterns/string_functions.h"
#include <boost/predef.h>
#include <boost/dll/runtime_symbol_info.hpp>

core::configuration::configuration(const std::wstring& _configuration, bool _treatAsFilename)
{
	if (_treatAsFilename)
	{
		using namespace boost::property_tree;
		using namespace boost::property_tree::xml_parser;
		try
		{
			read_xml<iproperty_tree>(wtos(_configuration), m_config);
		}
		catch (boost::property_tree::xml_parser::xml_parser_error&)
		{
		}
	}
	else
	{
		m_config = core::support::ptree_parser::tree_from_string_xml<iproperty_tree>(_configuration);
	}
}

CSystemConfiguration::CSystemConfiguration() : configuration(get_module_name() + L".config")
{
}


CSystemConfiguration::~CSystemConfiguration()
{
}

std::wstring CSystemConfiguration::get_module_name()
{
#if BOOST_COMP_MSVC
	wchar_t szModulePath[1024];
        DWORD size = 1024;
	ZeroMemory(szModulePath, sizeof(szModulePath));
	::GetModuleFileName(0, szModulePath, size);
	return std::wstring(szModulePath);
#else
	return stow(boost::dll::program_location().string());
#endif
}

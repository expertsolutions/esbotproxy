//
// connection.cpp
// ~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2008 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
#include "stdafx.h"
#include "connection.hpp"
#include <vector>
#include <boost/bind.hpp>
#include <boost/logic/tribool.hpp>
#include <boost/scoped_array.hpp>
#include "server.hpp"
#include <fstream>
#include <iostream>
#include "../Patterns/string_functions.h"
//#include "UTFConverter.h"

namespace http_base {
	namespace server3 {

		std::atomic<uint32_t> connection::s_dwCurrentCounter(0);

		connection::connection(http_base::server3::server& server)
			: m_blSend(false),
			strand_(server.io_service()),
			socket_(server.io_service()), m_server(server)
		{
			//::InterlockedIncrement(&s_dwCurrentCounter);
			m_connectionId = ++s_dwCurrentCounter;
			buffer_.fill(0);
		}

		connection::~connection()
		{
		}

		boost::asio::ip::tcp::socket& connection::socket()
		{
			return socket_;
		}

		void connection::start()
		{
			buffer_.fill(0);
			socket_.async_read_some(boost::asio::buffer(buffer_),
				strand_.wrap(
				boost::bind(&connection::handle_read, shared_from_this(),
				boost::asio::placeholders::error,
				boost::asio::placeholders::bytes_transferred)));
		}

		void connection::disconnect(const boost::system::error_code& e)
		{
			m_server.on_disconnect(m_connectionId);

			boost::system::error_code ignored_ec;
			socket_.shutdown(boost::asio::ip::tcp::socket::shutdown_both, ignored_ec);
			try
			{
				socket_.close();
			}
			catch (...)
			{

			}
			m_server.remove_connection(m_connectionId);
		}


		void connection::send(const std::wstring& _data)
		{
			m_queue.push(to_utf8(_data));
			begin_write();
		}

		void connection::send(const std::string& _data)
		{
			m_queue.push(_data);
			begin_write();
		}

		void connection::begin_write()
		{
			if(m_queue.empty())
				return;

			if(m_blSend)
				return;

			m_blSend = true;
			buffer_out_.push_back(boost::asio::buffer(m_queue.front()));
			boost::asio::async_write(socket_, buffer_out_,
				strand_.wrap(
				boost::bind(&connection::handle_write, shared_from_this(),
				boost::asio::placeholders::error)));
		}

		void connection::handle_read(const boost::system::error_code& e, std::size_t bytes_transferred)
		{
			if (!e)
			{
				try
				{
					std::string data(buffer_.data(), buffer_.data() + bytes_transferred);
					m_packet += std::wstring(from_utf8(data));

					std::string ip_string = "";
					try
					{
						ip_string = this->socket().remote_endpoint().address().to_string();
					}
					catch (...)
					{
							
					}

					m_server.on_receive(m_connectionId, m_packet, ip_string);

					m_packet = L"";
				}
				catch (...)
				{
				}
				start();
			}
			else
			{
				disconnect(e);
			}
		}


		void connection::handle_write(const boost::system::error_code& e)
		{
			if (!e)
			{
				// Initiate graceful connection closure.

				if(m_queue.not_empty())
					m_queue.pop();
				buffer_out_.clear();
				m_blSend = false;
				begin_write();
			}
			else
			{
				disconnect(e);
			}
		}

	} // namespace server3
} // namespace http

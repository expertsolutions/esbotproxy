#pragma once
#include <string>
#include <map>
#include <boost/property_tree/ptree.hpp>
#include <vector>
#include "../Patterns/ptree_parser.h"

class http_worker
{
public:
	http_worker();
	~http_worker();

	std::pair<std::wstring, std::wstring> getContent(int32_t _dwConnectionId, const std::wstring& _packet);
	std::pair<std::wstring, std::wstring> parseData(const std::wstring& _packet) const;
	std::wstring getFullPacket(int32_t _dwConnectionId, const std::wstring& _packet);
	
	void cleanOldData(int32_t _dwConnectionId)
	{
		m_partialData.erase(_dwConnectionId);
	}

	static std::wstring getOk(const std::wstring& _contentType = L"text/plain", const std::wstring& _data = L"OK");
	static std::wstring getError(const std::wstring& _errorDescription);

	static std::wstring vector_to_json(const std::vector<std::wstring>& _v, wchar_t _splitter);



	core::property_tree on_receive(int32_t _dwConnectionId, const std::wstring& _packet);

private:
	std::map<int32_t, std::wstring>		m_partialData;
};

class http_error : public std::runtime_error
{
public:
	http_error(const char * _what) : runtime_error(_what)
	{}

};

class content_length_error : public http_error
{
public:
	content_length_error() : http_error("Content-Length element not found")
	{}
};

class invalid_content_error : public http_error
{
public:
	invalid_content_error() : http_error("HTTP request body not found")
	{}
};

class incomplete_content_error : public http_error
{
public:
	incomplete_content_error() : http_error("Data not found or incomplete")
	{}
};


//
// server.cpp
// ~~~~~~~~~~
//
// Copyright (c) 2003-2008 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
#include "stdafx.h"
#include "server.hpp"
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <vector>
#include <fstream>
#include <iostream>
#if BOOST_COMP_MSVC
#include "Mstcpip.h"
#endif

#define _SCL_SECURE_NO_WARNINGS


namespace http_base {
	namespace server3 {
		using namespace std;

		server::server(const std::string& address, const std::string& port, boost::asio::io_service& io_service)
			: io_service_(io_service), 
			acceptor_(io_service_), /*flash_acceptor_(io_service_),*/
			new_connection_(make_shared<connection>(*this))
		{
			// Open the acceptor with the option to reuse the address (i.e. SO_REUSEADDR).
			initialize_socket(acceptor_, address, port);
			acceptor_.async_accept(new_connection_->socket(),
				boost::bind(&server::handle_accept, this,
				boost::asio::placeholders::error));
		}

		void server::initialize_socket(boost::asio::ip::tcp::acceptor& _acceptor, const std::string& address, const std::string& port)
		{
			boost::asio::ip::tcp::resolver resolver(io_service_);
			boost::asio::ip::tcp::resolver::query query(address, port);
			boost::asio::ip::tcp::endpoint endpoint = *resolver.resolve(query);
			_acceptor.open(endpoint.protocol());
			_acceptor.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
			_acceptor.bind(endpoint);
			_acceptor.listen();
		}

		void server::handle_accept(const boost::system::error_code& e)
		{
			if (!e)
			{
				boost::recursive_mutex::scoped_lock lock(m_access);
				m_connections.push_back(new_connection_);

#if BOOST_COMP_MSVC
				tcp_keepalive timeouts;
				timeouts.keepaliveinterval = 5000;
				timeouts.keepalivetime = 5000;
				timeouts.onoff = 1;
				DWORD bytesOut;
#if BOOST_VERSION >= 104700
                ::WSAIoctl(new_connection_->socket().native_handle(), SIO_KEEPALIVE_VALS, &timeouts, sizeof(timeouts), NULL, 0, &bytesOut, NULL, NULL);
#else
				::WSAIoctl(new_connection_->socket().native(), SIO_KEEPALIVE_VALS, &timeouts, sizeof(timeouts), NULL, 0, &bytesOut, NULL, NULL);
#endif
#endif

				
				new_connection_->start();
				new_connection_.reset();
				new_connection_ = make_shared<connection>(*this);
				acceptor_.async_accept(new_connection_->socket(),
					boost::bind(&server::handle_accept, this,
					boost::asio::placeholders::error));
			}
		}

		void server::on_receive(int32_t _connectionId, const std::wstring& _packet, const std::string& _ipAddress)
		{
			m_dataHandler(_connectionId, _packet);
		}

		void server::on_disconnect(int32_t _connectionId)
		{
			m_disconnectHandler(_connectionId);
		}

		void server::remove_connection(int32_t _coonnectionId)
		{
			boost::recursive_mutex::scoped_lock lock(m_access);
			for(std::list<connection_ptr>::iterator i = m_connections.begin(); i != m_connections.end(); ++i)
			{
				if((*i)->is_equal(_coonnectionId))
				{
					m_connections.erase(i);
					return;
				}
			}
		}

		void server::send_data(int32_t _connectionId, const std::wstring& _data)
		{
			boost::recursive_mutex::scoped_lock lock(m_access);
			for(std::list<connection_ptr>::iterator i = m_connections.begin(); i != m_connections.end(); ++i)
			{
				if((*i)->is_equal(_connectionId))
				{
					(*i)->send(_data);
				}
			}
		}

		void server::send_data(int32_t _connectionId, const std::string& _data)
		{
			boost::recursive_mutex::scoped_lock lock(m_access);
			for (std::list<connection_ptr>::iterator i = m_connections.begin(); i != m_connections.end(); ++i)
			{
				if ((*i)->is_equal(_connectionId))
				{
					(*i)->send(_data);
				}
			}
		}

		void server::close_connection(int32_t _connectionId)
		{
			boost::recursive_mutex::scoped_lock lock(m_access);
			for (std::list<connection_ptr>::iterator i = m_connections.begin(); i != m_connections.end(); ++i)
			{
				if ((*i)->is_equal(_connectionId))
				{
					boost::system::error_code ignored_ec;
					(*i)->disconnect(ignored_ec);
					return;
				}
			}

		}


	} // namespace server3
} // namespace http

//
// connection.hpp
// ~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2008 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef HTTP_SERVER3_CONNECTION_HPP
#define HTTP_SERVER3_CONNECTION_HPP

#if _MSC_VER > 1500 && !defined _SCL_SECURE_NO_WARNINGS
#pragma message("NOTE: _SCL_SECURE_NO_WARNINGS macro defined for connection.hpp")
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include "../Patterns/locked_queue.h"
#include <atomic>

using connection_id = uint32_t;

namespace http_base {
	namespace server3 {

		class server;
		/// Represents a single connection from a client.
		class connection : public std::enable_shared_from_this<connection>
		{
		public:
			/// Construct a connection with the given io_service.
			explicit connection(http_base::server3::server& server);
			~connection();

			connection(connection&&) = delete; 
			connection(const connection&) = delete;

			connection& operator = (const connection&) = delete;
			connection& operator = (connection&&) = delete;
			/// Get the socket associated with the connection.
			boost::asio::ip::tcp::socket& socket();
			bool m_blSend;

			/// Start the first asynchronous operation for the connection.
			void start();

			bool operator == (connection_id _connectionId) const
			{
				return m_connectionId == _connectionId;
			}

			bool is_equal(connection_id _connectionId) const
			{
				return m_connectionId == _connectionId;
			}

			void send (const std::wstring& _data);
			void send(const std::string& _data);

			void disconnect(const boost::system::error_code& e);

			connection_id get_id() const { return m_connectionId;  }

		private:
			//void write_thread_func();
			void begin_write();
			/// Handle completion of a read operation.
			void handle_read(const boost::system::error_code& e,
				std::size_t bytes_transferred);

			/// Handle completion of a write operation.
			void handle_write(const boost::system::error_code& e);

			/// Strand to ensure the connection's handlers are not called concurrently.
			boost::asio::io_service::strand strand_;

			/// Socket for the connection.
			boost::asio::ip::tcp::socket socket_;

			/// Buffer for incoming data.
			boost::array<char, 16384>				buffer_;

			/// Output buffer
			std::vector<boost::asio::const_buffer>	buffer_out_;
			std::vector<std::string>				strings;
			locked_queue<std::string>				m_queue;

			std::wstring m_packet;
			std::string reply_;
			connection_id m_connectionId;

			static std::atomic<connection_id> s_dwCurrentCounter;

			http_base::server3::server& m_server;

		};

		typedef std::shared_ptr<connection> connection_ptr;

	} // namespace server3
} // namespace http

#undef _SCL_SECURE_NO_WARNINGS
#endif // HTTP_SERVER3_CONNECTION_HPP

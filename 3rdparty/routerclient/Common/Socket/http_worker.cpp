#include "stdafx.h"
#include "http_worker.h"
#include <boost/format.hpp>
#include <mutex>
#include <boost/algorithm/string.hpp>

http_worker::http_worker()
{
}


http_worker::~http_worker()
{
}

std::pair<std::wstring, std::wstring> http_worker::parseData(const std::wstring& _packet) const
{
	auto nContentPos = _packet.find(L"Content-Length: ");

	if (nContentPos == std::string::npos)
		throw content_length_error();

	//uint32_t nContentLength = std::wcstoul(_packet.c_str() + nContentPos + 16, NULL, 0);
	
	constexpr wchar_t strContentType[] = L"Content-Type: ";
	constexpr auto contentTypeLength = 14;

	auto nContentTypePos = _packet.find(strContentType);
	std::wstring contentType = L"application/json";

	if (nContentTypePos != std::string::npos)
	{
		auto nEnd = _packet.find(L"\r\n", nContentTypePos);
		contentType = std::wstring(_packet.cbegin() + nContentTypePos + contentTypeLength, _packet.cbegin() + nEnd);
	}

	auto nPos = _packet.find(L"\n\n", nContentPos);
	int nOffset = 2;

	if (nPos == std::string::npos)
	{
		nOffset = 4;
		nPos = _packet.find(L"\r\n\r\n", nContentPos);
	}

	if (nPos == std::string::npos)
		throw invalid_content_error();

	std::wstring httpData = _packet.substr(nPos + nOffset);
	
	if (httpData.length())
		return std::make_pair<std::wstring, std::wstring>(std::move(contentType), std::move(httpData));

	throw incomplete_content_error();
}

std::wstring http_worker::getFullPacket(int32_t _connectionId, const std::wstring& _packet)
{
	auto i = m_partialData.find(_connectionId);
	std::wstring campaignData;

	if (i != end(m_partialData))
	{
		campaignData = i->second + _packet;
	}
	else
	{
		campaignData = _packet;
	}

	m_partialData[_connectionId] = campaignData;
	return campaignData;
}

std::wstring http_worker::getOk(const std::wstring& _contentType, const std::wstring& _data)
{
	return (boost::wformat(L"HTTP/1.1 200 OK\r\nContent-Type: %s;\r\nContent-Length: %i\r\n\r\n%s") % _contentType % _data.length() % _data).str();
}

std::wstring http_worker::getError(const std::wstring& _errorDescription)
{
	return (boost::wformat(L"HTTP/1.1 400 Bad Request\r\nContent-Type: text/plain;\r\nContent-Length: %i\r\n\r\n%s") % _errorDescription.length() % _errorDescription).str();
}


std::wstring http_worker::vector_to_json(const std::vector<std::wstring>& _v, wchar_t _splitter)
{
	std::wstring result;

	result += L"[";
	for (const auto& i : _v)
	{
		if (result.length() != 1)
			result += L',';

		result += _splitter;
		result += i;
		result += _splitter;
	}
	result += L"]";

	return result;

}
std::pair<std::wstring, std::wstring> http_worker::getContent(int32_t _dwConnectionId, const std::wstring& _packet)
{
	using namespace core::support;

	std::wstring campaignData = getFullPacket(_dwConnectionId, _packet);
	auto content = parseData(campaignData);
	m_partialData.erase(_dwConnectionId);
	return content;
}

core::property_tree http_worker::on_receive(int32_t _dwConnectionId, const std::wstring& _packet)
{
	using namespace core::support;
	auto content = getContent(_dwConnectionId, _packet);

	if(content.first == std::wstring(L"application/json"))
		return ptree_parser::tree_from_string_json(content.second);
	else if (content.first == std::wstring(L"application/xml"))
		return ptree_parser::tree_from_string_xml(content.second);
	else
		throw invalid_content_error();
}

#pragma once

#if _MSC_VER > 1500 && !defined _SCL_SECURE_NO_WARNINGS
#pragma message("NOTE: _SCL_SECURE_NO_WARNINGS macro defined for text_message.h")
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <string>
#include <vector>
#include <boost/exception/all.hpp>

typedef boost::error_info<struct tag_function, wchar_t const *>	function_info;
typedef boost::error_info<struct tag_file_name, std::wstring>	user_info;

class no_match_exception : public boost::exception, public std::runtime_error
{
public:
	no_match_exception() : std::runtime_error("No match") {}
	~no_match_exception() {}
};

class message_format_exception : public boost::exception, public std::runtime_error
{
public:
	message_format_exception() : std::runtime_error("Invalid SIP message format") {}
	~message_format_exception() {}
};

class text_message
{

public:
	text_message() {};
	virtual ~text_message() {};

	explicit text_message(const std::wstring& _message);

	text_message(text_message&&)					= default;
	text_message(const text_message&)				= default;

	text_message& operator = (text_message&&)		= default;
	text_message& operator = (const text_message&)	= default;


	virtual std::wstring ToString() const;
	virtual void		 FromString(const std::wstring& _str);

	std::wstring& GetHeader()						{ return m_strRequestHeader; }
	const std::wstring& GetHeader() const			{ return m_strRequestHeader; }

	void SetContent(const std::wstring& _str) { m_content = _str;}

	std::wstring& GetContent()						{ return m_content; }
	const std::wstring& GetContent() const			{ return m_content; }

	std::wstring GetField(const std::wstring& _name) const;

	void SetHeader(const std::wstring& _str) 
	{ 
		m_strRequestHeader = _str; 
	}

	void SetField(const std::wstring& _field, const std::wstring& _value);

protected:
	std::wstring										m_strRequestHeader;
	std::vector<std::wstring>							m_fields;
	std::wstring										m_content;
};

#undef _SCL_SECURE_NO_WARNINGS 

//
// server.hpp
// ~~~~~~~~~~
//
// Copyright (c) 2003-2008 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef HTTP_SERVER3_SERVER_HPP
#define HTTP_SERVER3_SERVER_HPP

#if _MSC_VER > 1500 && !defined _SCL_SECURE_NO_WARNINGS
#pragma message("NOTE: _SCL_SECURE_NO_WARNINGS macro defined for server.hpp")
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <boost/asio.hpp>
#include <string>
#include <vector>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/signals2.hpp>
#include "connection.hpp"
#include <boost/thread/recursive_mutex.hpp>

using DataReceivedSignal	= boost::signals2::signal<void(connection_id, const std::wstring& _packet)>;
using DataReceivedHandler	= DataReceivedSignal::slot_function_type;
using DisconnectSignal		= boost::signals2::signal<void(connection_id)>;
using DisconnectHandler		= DisconnectSignal::slot_function_type;
using Connector				= boost::signals2::connection;

namespace http_base {
	namespace server3 {

		/// The top-level class of the HTTP server.
		class server : private boost::noncopyable
		{
		public:
			/// Construct the server to listen on the specified TCP address and port, and
			/// serve up files from the given directory.
			explicit server(const std::string& address, const std::string& port, boost::asio::io_service& io_service);
			void close_connection(int32_t _connectionId);
			void remove_connection(int32_t _coonnectionId);
			void on_receive(int32_t _connectionId, const std::wstring& _packet, const std::string& _ipAddress);
			void on_disconnect(int32_t _connectionId);
			boost::asio::io_service& io_service() const noexcept { return io_service_; }

			void send_data(int32_t _connectionId, const std::wstring& _data);
			void send_data(int32_t _connectionId, const std::string& _data);

			Connector RegisterDataReceivedHandler(const DataReceivedHandler& _handler)
			{
				return m_dataHandler.connect(_handler);
			}

			Connector RegisterDisconnectHandler(const DisconnectHandler& _handler)
			{
				return m_disconnectHandler.connect(_handler);
			}

		private:
			/// Handle completion of an asynchronous accept operation.
			void initialize_socket(boost::asio::ip::tcp::acceptor& _acceptor, const std::string& address, const std::string& port);
			void handle_accept(const boost::system::error_code& e);

			/// The io_service used to perform asynchronous operations.
			boost::asio::io_service& io_service_;

			/// Acceptor used to listen for incoming connections.
			boost::asio::ip::tcp::acceptor acceptor_;

			//boost::asio::ip::tcp::acceptor flash_acceptor_;

			/// The next connection to be accepted.
			connection_ptr new_connection_;

			boost::recursive_mutex		m_access;
			std::list<connection_ptr>	m_connections;
			DataReceivedSignal			m_dataHandler;
			DisconnectSignal			m_disconnectHandler;
		};

	} // namespace server3
} // namespace http

#undef _SCL_SECURE_NO_WARNINGS

#endif // HTTP_SERVER3_SERVER_HPP

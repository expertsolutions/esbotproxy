//#include "StdAfx.h"

#define _SCL_SECURE_NO_WARNINGS 

#include "text_message.h"
#include <sstream>
#include <boost/xpressive/xpressive.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/tokenizer.hpp>
#include "../Patterns/string_functions.h"

const std::wstring ENDL(L"\r\n");

text_message::text_message(const std::wstring& _message)
{
	FromString(_message);
}

std::wstring text_message::ToString() const
{
	std::wostringstream stream;
	stream << m_strRequestHeader << ENDL;
	
	for (auto& value : m_fields)
	{
		stream << value << ENDL;
	}

	stream << L"Content-Length: " << m_content.length() << ENDL;
	stream << ENDL;
	stream << m_content;

	return stream.str();
}

std::wstring text_message::GetField(const std::wstring& _name) const
{ 
	using namespace boost::xpressive;
	std::wstring pattern = _name + L"\\: *(.+)";
	const wsregex	e = wsregex::compile(pattern);
	wsmatch			result;

	for(auto& value : m_fields)
	{
		if(regex_match(value, result, e))
			return result[1];
	}

	throw no_match_exception() << user_info(L"text_message::GetField " + pattern);
}

void text_message::FromString(const std::wstring& _str)
{
	boost::tokenizer<boost::char_separator<wchar_t>, std::wstring::const_iterator, std::wstring> tok(_str.begin(), _str.end(), boost::char_separator<wchar_t>(L"\n"));
	auto begin = tok.begin();
	auto end = tok.end();

	if(!begin->empty())
	{
		m_strRequestHeader = *begin;
		boost::trim_if(m_strRequestHeader, boost::is_any_of("\r\n"));
		++begin;
	}
	else
	{
		throw message_format_exception() << function_info(L"text_message::FromString");
	}

	for(;begin != end; ++begin)
	{
		if (begin->empty() || *begin == L"\r")
			break;
		else
		{
			auto field = *begin;
			boost::trim_if(field, boost::is_any_of("\r\n"));
			m_fields.push_back(field);
		}
	}

	if(begin != end)
	{
		m_content = *begin + std::wstring(begin.base(), _str.end());
		boost::trim_if(m_content, boost::is_any_of("\r\n"));
	}
}

void text_message::SetField(const std::wstring& _field, const std::wstring& _value)
{
	m_fields.push_back(_field + L": " + _value);
}


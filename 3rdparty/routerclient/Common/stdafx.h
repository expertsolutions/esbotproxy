// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers

#include <string>
#include <streambuf>
#include <sstream>
#include <algorithm>

//#include <boost/tokenizer.hpp>
//#include <boost/property_tree/json_parser.hpp>
//#include <boost/property_tree/xml_parser.hpp>
//#include <boost/algorithm/string.hpp>
//#include <boost/asio/ip/tcp.hpp>
//#include <boost/iostreams/device/array.hpp>

#include <sstream>

#include <atlbase.h>
#include <atlcom.h>
#include <regex>

#include "html2plain.h"

#include <SAX/SAXContentHandler.h>
#include <SAX/SAXErrorHandler.h>
#include <SAX/SAXXmlParser.h>

namespace converter
{
    std::vector<std::wstring> split(const std::wstring& input, const std::wstring& regex) {
        // passing -1 as the submatch index parameter performs splitting
        std::wregex re(regex);
        std::wsregex_token_iterator
            first{ input.begin(), input.end(), re, -1 },
            last;
        return { first, last };
    }

    std::wstring ToLower(const std::wstring& data)
    {
        std::wstring result;
        std::transform(data.begin(), data.end(), std::back_inserter(result),
            [](wchar_t c) { return std::tolower(c); });

        return result;
    }

    class XMLParser : public ISAXXmlElementHandler
    {
    public:
        XMLParser()
            : _xmlParser(std::make_unique<CSAXXmlParser>())
            , _contentHandler(new CSAXContentHandler())
            , _errorHandler(new CSAXErrorHandler())
        {
            // Set the content handler.
            if (_xmlParser->PutContentHandler(_contentHandler))
            {
                _contentHandler->AttachElementHandler(this);
            }

            if (_xmlParser->PutErrorHandler(_errorHandler))
            {
                _errorHandler->AttachElementHandler(this);
            }
        }

        void OnXmlStartElement(const ISATXMLElement& xmlElement) override
        {
            if (xmlElement.GetName() == L"a")
            {
                _isReference = true;
                _reference_href = GetHref(xmlElement.GetParams());
            }
            else if (xmlElement.GetName() == L"br")
            {
                _text += L"\n";
            }
        }
        void OnXmlElementData(const wchar_t* aData, size_t aSize) override
        {
            if (_isReference)
            {
                _reference_text.append(aData, aSize);
            }
            else
            {
                _text.append(aData, aSize);
            }
        }
        void OnXmlEndElement(const ISATXMLElement& xmlElement) override
        {
            if (xmlElement.GetName() == L"a")
            {
                _isReference = false;
                _text += _reference_text + L" " + _reference_href;
            	_reference_text.clear();
            	_reference_href.clear();
            }
        }

        void OnXmlError(int line, int column, const std::wstring& errorText, unsigned long errorCode) override
        {
            _error = std::string("Error parsing XML document. Line ") + std::to_string(line)
                + ", char " + std::to_string(column)
                + ", reason: \"" + wtos(errorText) + "\"";
        }

        bool OnXmlAbortParse(const ISATXMLElement& xmlElement) override
        {
            return false;
        }

        std::wstring Parse(const std::wstring& text)
        {
            if (_xmlParser->Parse(text))
            {
                return _text;
            }

            //if (!_error.empty())
            //{
            //    throw std::runtime_error(_error);
            //}

            return _text;
        }

    private:

        static std::wstring MakeRef(const std::vector<std::wstring>& params)
        {
            std::wstring result;

            if (!params.empty())
            {
                result += params[0];
            }

            for (size_t i = 1; i < params.size(); ++i)
            {
                result += std::wstring(L"=" + params[i]);
            }

            return result;
        }

        std::wstring GetHref(const std::wstring& paramsString) const
        {
            const auto params = split(paramsString, L" ");
            for (const auto& param : params)
            {
                if (param.empty())
                {
                    continue;
                }
                const auto values = split(param, L"=");

                if (values.empty())
                {
                    continue;
                }

                const auto& key = values[0];
                if (ToLower(key) == L"href")
                {
                    return MakeRef({ values.cbegin() + 1, values.cend() });
                }
            }
            return std::wstring{};
        }

    private:
        std::unique_ptr<CSAXXmlParser> _xmlParser;
        CComPtr<CSAXContentHandler> _contentHandler;
        CComPtr<CSAXErrorHandler> _errorHandler;

        bool _isReference{};

        std::wstring _text;
        std::wstring _reference_text;
        std::wstring _reference_href;
        std::string _error;
    };

	std::wstring html2plain(const std::wstring& htmlText)
	{
        CoInitialize(NULL);

        XMLParser parser;
        std::wstring answer = parser.Parse(htmlText);

        CoUninitialize();
        return answer;
	}
}
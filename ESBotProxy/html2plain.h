#pragma once
#include <string>

namespace converter
{
	std::wstring html2plain(const std::wstring& htmlText);
}
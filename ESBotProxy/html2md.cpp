#include <sstream>

#include "html2md.h"
#include "../3rdparty/HtmlToMd\html2md.hpp"

namespace converter
{
	std::string html2md(const std::string& htmlText)
	{
		return html2md::Convert(htmlText);
	}
}
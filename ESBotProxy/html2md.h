#pragma once
#include <string>

namespace converter
{
	std::string html2md(const std::string& htmlText);
}
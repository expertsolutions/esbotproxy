#pragma once

#include <variant>

#include "Router\router_compatibility.h"
#include "Base.h"
#include "DataReceiver.h"
#include "queue.h"
#include "processor.h"
#include <ESBotProxyLib\bot_client.h>
#include <ESBotProxyLib\edna_client.h>

using BotResponse = bot::Response<bot::PrefixSessionId>;
using BotRequest = bot::Request<bot::PrefixSessionId>;
using BotRequestAck = bot::RequestAck<bot::PrefixSessionId>;

class CDataReceiverImpl : public CDataReceiver < CDataReceiverImpl >
{
	friend class CDataReceiver < CDataReceiverImpl >;
	using EdnaResponse = edna::Response;
	using EdnaRequest = edna::Request;
	using Response = std::variant<BotResponse, EdnaResponse>;

	auto ProcessorConsumer()
	{
		return [this](const Response& record)
		{
			return ProcessResponse(record);
		};
	}

public:
	CDataReceiverImpl(boost::asio::io_service& io);
	~CDataReceiverImpl();

private:
	int init_impl(CSystemLog* pLog);
	int start();
	bool ProcessResponse(const Response& record);
	bool ProcessResponse(const BotResponse& response);
	bool ProcessResponse(const EdnaResponse& response);

	void SendMessageToBot(BotRequest&& response);
	void SendMessageToEdna(EdnaRequest&& message);

private:
	//router api
	void on_routerConnect();
	void on_routerDeliverError(const CMessage&, const CLIENT_ADDRESS&, const CLIENT_ADDRESS&);
	void on_appStatusChange(DWORD dwClient, /*int*/core::ClientStatus	nStatus);
	void set_Subscribes();
	std::wstring get_ModuleName() const noexcept { return L"ESBotProxy"; }
	void stop();

private:
	void ConnectHandlers();

private:
	CSystemLog * m_log = nullptr;
	boost::asio::io_service& r_io;

public:
	std::wstring _botSessionIdPrefix;
	std::unique_ptr<bot::EdnaBotClient> _bot;
	std::unique_ptr<edna::EdnaClient> _edna;
	std::wstring _botSwitchToHumanDummyMessage;

	Processor<Queue<Response>> _processor;
};

/******************************* eof *************************************/


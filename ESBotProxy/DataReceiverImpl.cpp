#include <boost/asio/spawn.hpp>
#include <boost/asio/steady_timer.hpp>
#include "DataReceiverImpl.h"
#include "Configuration\SystemConfiguration.h"
#include "Log\SystemLog.h"
#include "WinAPI/ProcessHelper.h"
#include "ThreadHelper.h"
#include "utils.h"
#include "IdGenerator.h"

#include "md2html.h"
//#include "html2md.h"
#include "html2plain.h"

const int EXPIREDTIME = 30;
//

//static std::wstring ConvertToHtml(const std::wstring& markdownText)
//{
//	return stow(converter::md2html(wtos(markdownText)));
//}

//static std::wstring ConvertToMarkdown(const std::wstring& htmlText)
//{
//	return stow(converter::html2md(wtos(htmlText)));
//}

static std::wstring MakeHtmlFormat(std::wstring htmlText)
{
	boost::replace_all(htmlText, "<br>", "<br/>");
	boost::replace_all(htmlText, "&", "&amp;");

	return std::wstring(L"<html>") + htmlText + std::wstring(L"</html>");
}

static std::wstring ConvertToPlainText(const std::wstring& htmlText)
{
	return converter::html2plain(htmlText);
}

static std::wstring ConvertEdnaCtn(const std::wstring& ctn)
{
	static const size_t botCtnMaxSize = 10;

	if (ctn.size() <= botCtnMaxSize)
	{
		return ctn;
	}

	return std::wstring(ctn.cend() - botCtnMaxSize, ctn.cend());
}

static edna::Request Convert(const BotResponse& botResponse, const std::wstring& /*botSessionIdPrefix*/, const std::wstring& switchToHumanDummyMessage)
{
	edna::Request request;

	request.sessionId = botResponse.sessionId.GetScriptId();

	//request.receivedAt = botResponse.timestamp; //core::support::time_to_string<std::wstring>(core::time_type::clock::now());
	request.answerId = stow(CIdGenerator::GetInstance()->makeSId());
	//request.questionId = botResponse.questionNumber;
	request.questionIndex = botResponse.questionNumber;
	
	if (botResponse.errors.empty())
	{
		switch (botResponse.result)
		{
		case BotResponse::Result::Close:
		{
			request.code = edna::Code::UNCLEAR_QUESTION;
			request.text = ConvertToPlainText(MakeHtmlFormat(botResponse.MakeMessage()));
			break;
		}
		case BotResponse::Result::Answer:
		{
			request.code = edna::Code::SUCCESS;
			request.text = ConvertToPlainText(MakeHtmlFormat(botResponse.MakeMessage()));
			break;
		}
		case BotResponse::Result::Prompter:
		{
			request.code = edna::Code::SWITCH_TO_HUMAN;
			request.text = switchToHumanDummyMessage;
			break;
		}
		default:
		{
			request.code = edna::Code::UNCLEAR_QUESTION;
			request.text = ConvertToPlainText(MakeHtmlFormat(botResponse.MakeMessage()));
			break;
		}
		};
	}
	else
	{
		request.code = edna::Code::UNCLEAR_QUESTION;
		request.text = ConvertToPlainText(MakeHtmlFormat(botResponse.MakeError()));
	}

	return request;
}

static BotRequest Convert(const edna::Response& ednaResponse, const std::wstring& botSessionIdPrefix)
{
	BotRequest request;
	//request.sessionId = bot::SessionId{ ednaResponse.sessionId, botSessionIdPrefix };
	request.sessionId = bot::PrefixSessionId::CreatePrefixSessionId(ednaResponse.sessionId, botSessionIdPrefix);

	request.marketCode = L"VIP";
	//request.message = ConvertToHtml(ednaResponse.text);
	request.message = ednaResponse.text;
	request.ctn = ConvertEdnaCtn(ednaResponse.phone);
	request.authType = ednaResponse.authorized ? L"1" : L"0";
	request.segment = L"1";
	request.channelName = ednaResponse.channelType;
	request.channelType = ednaResponse.channelId;
	request.timestamp = ednaResponse.receivedAt;//core::support::time_to_string<std::wstring>(core::time_type::clock::now());
	//request.questionNumber = ednaResponse.questionId;
	request.questionNumber = ednaResponse.questionIndex;

	return request;
}

void CDataReceiverImpl::SendMessageToBot(BotRequest&& message)
{
	this->_bot->sendRequestAsync(std::move(message), [this](BotRequestAck&& ack)
		{
			this->m_log->LogString(LEVEL_INFO, L"Recieve answer: %s", ack.ToString());
		});
}

void CDataReceiverImpl::SendMessageToEdna(EdnaRequest&& message)
{
	this->_edna->sendRequestAsync(std::move(message), [this](edna::RequestAck&& ack)
		{
			this->m_log->LogString(LEVEL_INFO, L"%s", ack.ToString());
		});
}

bool CDataReceiverImpl::ProcessResponse(const BotResponse& response)
{
	this->SendMessageToEdna(Convert(response, _botSessionIdPrefix, _botSwitchToHumanDummyMessage));
	return true;
}
bool CDataReceiverImpl::ProcessResponse(const EdnaResponse& response)
{
	this->SendMessageToBot(Convert(response, _botSessionIdPrefix));
	return true;
}

bool CDataReceiverImpl::ProcessResponse(const Response& record)
{
	std::visit([this, &record](auto&& response)
		{
			this->ProcessResponse(std::forward<decltype(response)>(response));
		},
		record);

	return true;
}

CDataReceiverImpl::CDataReceiverImpl(boost::asio::io_service& io) 
	: m_log(NULL)
	, r_io(io)
{
}


CDataReceiverImpl::~CDataReceiverImpl()
{
}

void CDataReceiverImpl::ConnectHandlers()
{
}

void CDataReceiverImpl::set_Subscribes()
{
	//subscribe(L"M2MHS_UploadFile", &CDataReceiverImpl::M2MHS_UploadFileHandler);
	//subscribe(L"M2MHS_DownloadFile", &CDataReceiverImpl::M2MHS_DownloadFileHandler);
}

int CDataReceiverImpl::init_impl(CSystemLog* pLog)
{
	//const std::wstring html = L"";
	//const auto text = ConvertToPlainText(MakeHtmlFormat(html));

	if (!pLog)
		return -1111;

	CoInitialize(NULL);

	m_log = pLog;

	m_log->LogString(LEVEL_FINEST, L"CDataReceiverImpl initializing...");

	ConnectHandlers();

	boost::asio::spawn(r_io, [&](boost::asio::yield_context yield)
	{
		try
		{
			boost::asio::steady_timer timer(r_io);

			timer.expires_from_now(std::chrono::milliseconds(100));
			timer.async_wait(yield);

			start();
		}
		catch (std::exception& error)
		{
			m_log->LogString(LEVEL_WARNING, L"[start] exception: %s", stow(error.what()));
		}
		catch (...)
		{
			m_log->LogString(LEVEL_WARNING, L"[start] unknown exception");
		}
	});

	return 0;
}

static void CheckEdnaAddress(const std::wstring& serverUri)
{
	if (serverUri.find(L"https") != 0)
	{
		throw std::runtime_error("'https' address mandatory");
	}
}

int CDataReceiverImpl::start()
{
	SystemConfig settings;
	const std::wstring ednaExternalServerUrl = settings->get_config_value<std::wstring>(L"EdnaChatAdapterUrl");
	const std::wstring ednaLocalServerUrl = settings->get_config_value<std::wstring>(L"EdnaListeningUrl");

	const std::wstring botExternalServerUrl = settings->get_config_value<std::wstring>(L"BotChatAdapterUrl");
	const std::wstring botLocalServerUrl = settings->get_config_value<std::wstring>(L"BotListeningUrl");

	const std::wstring ednaCertificatePath = settings->get_config_value<std::wstring>(L"EdnaSslCertificate");
	const std::wstring ednaPrivateKeyPath = settings->get_config_value<std::wstring>(L"EdnaSslPrivateKey");

	const std::wstring ednaClientCertificatePath = settings->get_config_value<std::wstring>(L"EdnaClientSslCertificate");

	const std::wstring ednaAuthorizationToken = settings->get_config_value<std::wstring>(L"EdnaAuthorizationToken");
	_botSessionIdPrefix = settings->get_config_value<std::wstring>(L"BotSessionIdPrefix");
	_botSwitchToHumanDummyMessage = settings->safe_get_config_value<std::wstring>(L"BotSwitchToHumanDummyMessage", L"Wait for operator answer");

	std::string ednaCertificate, ednaPrivateKey, ednaClientCertificate;

	utils::GetFileStream(wtos(ednaCertificatePath), [&ednaCertificate](const char* filestream, size_t size)
		{
			ednaCertificate = std::string{ filestream, size };
		});

	utils::GetFileStream(wtos(ednaPrivateKeyPath), [&ednaPrivateKey](const char* filestream, size_t size)
		{
			ednaPrivateKey = std::string{ filestream, size };
		});

	utils::GetFileStream(wtos(ednaClientCertificatePath), [&ednaClientCertificate](const char* filestream, size_t size)
		{
			ednaClientCertificate = std::string{ filestream, size };
		});

	CheckEdnaAddress(ednaExternalServerUrl);

	_processor.StartConsuming(ProcessorConsumer());

	_edna = std::make_unique<edna::EdnaClient>(
		wtos(ednaExternalServerUrl), 
		wtos(ednaLocalServerUrl),
		ednaCertificate,
		ednaPrivateKey,
		ednaClientCertificate,
		ednaAuthorizationToken,
		std::wstring{});

	m_log->LogString(LEVEL_FINEST, L"Edna started");

	_bot = std::make_unique<bot::EdnaBotClient>(wtos(botExternalServerUrl), wtos(botLocalServerUrl), _botSessionIdPrefix);
	m_log->LogString(LEVEL_FINEST, L"Mbot started");

	_edna->setResponseHandler([this](EdnaResponse&& response)
		{
			_processor.Enqueue(Response{ std::move(response) });
			m_log->LogString(LEVEL_FINEST, L"Processor queue size: %i", _processor.Count());
		});

	_bot->setResponseHandler([this](BotResponse&& response)
		{
			_processor.Enqueue(Response{ std::move(response) });
			m_log->LogString(LEVEL_FINEST, L"Processor queue size: %i", _processor.Count());
		});

	return 0;
}

void CDataReceiverImpl::stop()
{
	_processor.StopConsuming();

	r_io.stop();

	_bot.reset();
	_edna.reset();
}

void CDataReceiverImpl::on_routerConnect()
{
	if (!m_log)
	{
		return;
	}

	m_log->LogStringModule(LEVEL_INFO, L"Receiver Core", L"Router has connected");
}

void CDataReceiverImpl::on_routerDeliverError(const CMessage& _msg, const CLIENT_ADDRESS &_from, const CLIENT_ADDRESS &_to)
{
	if (!m_log)
	{
		return;
	}

	m_log->LogStringModule(LEVEL_FINE, L"Router deliver error", _msg.Dump().c_str());
}

void CDataReceiverImpl::on_appStatusChange(DWORD dwClient, core::ClientStatus nStatus)
{
	if (nStatus == /*static_cast<int>*/(core::ClientStatus::CS_DISCONNECTED))
	{
		try
		{

		}
		catch (...)
		{

		}
	}

}

/******************************* eof *************************************/
#include <sstream>

#include "md2html.h"
#include "3rdparty\MdToHtml\markdown.h"

namespace converter
{
	std::string md2html(const std::string& mdText)
	{
		markdown::Document doc;
		doc.read(mdText);

		std::stringstream out;
		doc.write(out);
		return out.str();
	}
}
#pragma once
#include <string>

namespace converter
{
	std::string md2html(const std::string& mdText);
}
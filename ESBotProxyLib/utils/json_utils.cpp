#include "json_utils.h"

#include <array>

template<>
int convertJsonTo<int>(const web::json::value& value)
{
    return value.as_integer();
}

template<>
double convertJsonTo<double>(const web::json::value& value)
{
    return value.as_double();
}

#ifdef _WIN32
template<>
std::string convertJsonTo<std::string>(const web::json::value& value)
{
    return utility::conversions::to_utf8string(value.as_string());
}

template<>
std::wstring convertJsonTo<std::wstring>(const web::json::value& value)
{
    return value.as_string();
}
#else

//template<>
//std::wstring convertJsonTo<std::wstring>(const web::json::value& value)
//{
//    return utility::conversions::to_utf16string(value.as_string());
//}

template<>
std::string convertJsonTo<std::string>(const web::json::value& value)
{
    return value.as_string();
}
#endif


template<>
bool convertJsonTo<bool>(const web::json::value& value)
{
    return value.as_bool();
}

std::wstring jsonEscapeString(const std::wstring& str)
{
    static const std::array<wchar_t, 16> intToHex = { { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' } };

    std::wstring result;
    result.reserve(str.size() * 2);
    for (const auto &ch : str) {
        switch (ch) {
            case '\"':
                result += '\\';
                result += '\"';
                break;
            case '\\':
                result += '\\';
                result += '\\';
                break;
            case '/':
                result += '\\';
                result += '/';
                break;
            case '\b':
                result += '\\';
                result += 'b';
                break;
            case '\f':
                result += '\\';
                result += 'f';
                break;
            case '\r':
                result += '\\';
                result += 'r';
                break;
            case '\n':
                result += '\\';
                result += 'n';
                break;
            case '\t':
                result += '\\';
                result += 't';
                break;
            default:

                // If a control character then must unicode escaped.
                if (ch >= 0 && ch <= 0x1F) {
                    result += '\\';
                    result += 'u';
                    result += '0';
                    result += '0';
                    result += intToHex[(ch & 0xF0) >> 4];
                    result += intToHex[ch & 0x0F];
                } else {
                    result += ch;
                }
        }
    }
    return result;
}

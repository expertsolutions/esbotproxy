#pragma once

#include <cpprest/json.h>

template <typename T>
T convertJsonTo(const web::json::value& value)
{
    return T{};
}

template<>
int convertJsonTo<int>(const web::json::value& value);

template<>
double convertJsonTo<double>(const web::json::value& value);

template<>
std::string convertJsonTo<std::string>(const web::json::value& value);

template<>
std::wstring convertJsonTo<std::wstring>(const web::json::value& value);

template<>
bool convertJsonTo<bool>(const web::json::value& value);

template <typename T>
T getOrThrow(const web::json::object& json, const utility::char_t* name)
{
    auto it = json.find(name);
    if (it != json.cend()) {
        return convertJsonTo<T>(it->second);
    } else {

#ifdef _WIN32
        char buffer[128];
        sprintf_s(buffer, "Mandatory field \"%S\" not found", name);
        throw std::exception(buffer);
#else
        throw std::runtime_error(std::string("Mandatory field \"") + name + "\" not found");
#endif
    }
}

template <typename T>
T getOrDefault(const web::json::object& json, const utility::char_t* name, const T& default_value = T())
{
    auto it = json.find(name);
    if (it != json.cend()) {
        if (it->second.is_null())
        {
            return default_value;
        }
        return convertJsonTo<T>(it->second);
    } else {
        return default_value;
    }
}

template <typename T, typename Sink>
std::vector<T> getArray(const web::json::object& json, const utility::char_t* name, Sink sink)
{
    std::vector<T> result;
    const auto cit = json.find(name);
    if (cit == json.cend() || cit->second.is_null())
    {
        return {};
    }

    return sink(cit->second);
}

std::wstring jsonEscapeString(const std::wstring& str);

#pragma once

#include <cpprest/json.h>
#include "bot_response.h"

namespace bot
{
    template <class SessionId>
    class ResponseParser
    {
    public:
        static Response<SessionId> FromJson(web::json::value json, const std::optional<XString>& salt);
    };
}



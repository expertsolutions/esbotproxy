#pragma once

#include "bot_session_id.h"
#include "base_client.h"
#include "voice_request.h"
#include "voice_http.h"
#include "voice_response.h"
#include "voice_response_parser.h"
#include "request_ack_parser.h"

namespace voice
{
    struct VoiceSetup
    {
        using SessionId = bot::SimpleSessionId;
        using Request = voice::Request;
        using HttpRequest = voice::HttpRequest;
        using RequestAck = voice::RequestAck;
        using Response = voice::Response;
        using ResponseParser = voice::ResponseParser;
        using RequestAckParser = voice::RequestAckParser;
        using HttpResponse = voice::HttpResponse;

        static constexpr wchar_t ComponentName[] = L"VoiceClient";
    };

    using VoiceClient = base::ClientBase<VoiceSetup>;
}
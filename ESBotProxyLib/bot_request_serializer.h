#pragma once

#include <cpprest/json.h>

#include "bot_request.h"

namespace bot
{
	template <class SessionId>
	class RequestSerializer
	{
	public:
		static web::json::value toJson(const Request<SessionId>& request);
	};
}


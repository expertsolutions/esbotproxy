#pragma once
#include <optional>
#include "ESBotProxyLib/utils/json_utils.h"

namespace edna
{
    enum class Code
    {
        UNCLEAR_QUESTION = 0,
        SWITCH_TO_HUMAN,
        SUCCESS
    };

    inline std::string ToString(const Code code)
    {
        switch (code)
        {
        case Code::UNCLEAR_QUESTION:
            return "UNCLEAR_QUESTION";
        case Code::SWITCH_TO_HUMAN:
            return "SWITCH_TO_HUMAN";
        case Code::SUCCESS:
        default:
            return "SUCCESS";
        }
    }

	struct Request
	{
        std::wstring sessionId;
        std::wstring text;
        //std::string receivedAt;
        std::wstring answerId;
        int questionIndex{};
        Code code{ Code::UNCLEAR_QUESTION };
	};

    // bot request => edna ack request
	struct RequestAck
	{
        std::wstring text;
        std::wstring sessionId;
        int questionIndex{};
        int code{};

        RequestAck() = default;
        RequestAck(
            const std::wstring& text,
            const std::wstring& sessionId,
            const Request& request,
            const int code)
            : text(text)
            , sessionId(sessionId)
            , questionIndex(request.questionIndex)
            , code(code)
        {
        }

        std::wstring ToString() const
        {
            std::wstringstream str;
            str << L"edna::RequestAck {text: " << text
                << L", sessionId: " << sessionId
                << L", questionIndex: " << questionIndex
                << L", code: " << code;

            return str.str();
        }

        static std::wstring GetSessionId(const std::optional<std::wstring>& /*salt*/, const Request& request)
        {
            return request.sessionId;
        }
	};

	struct RequestAckParser
	{
        // got empty json?
		static RequestAck FromJson(const web::json::value& /*json*/, const Request& request, const std::optional<std::wstring>& /*salt*/)
		{
			RequestAck ack;
			//auto& object = json.as_object();

            ack.sessionId = request.sessionId;
            //ack.questionId = request.questionId;
            ack.questionIndex = request.questionIndex;

			return ack;
		}
	};
}

#pragma once

#include "bot_session_id.h"
#include "base_client.h"
#include "bot_request.h"
#include "bot_http.h"
#include "bot_response.h"
#include "bot_response_parser.h"
#include "request_ack_parser.h"

namespace bot
{
    template <class TSessionId>
    struct BotSetup
    {
        using SessionId = TSessionId;
        using Request = bot::Request<SessionId>;
        using HttpRequest = bot::HttpRequest<SessionId>;
        using RequestAck = bot::RequestAck<SessionId>;
        using Response = bot::Response<SessionId>;
        using ResponseParser = bot::ResponseParser<SessionId>;
        using RequestAckParser = bot::RequestAckParser<SessionId>;
        using HttpResponse = bot::HttpResponse;

        static constexpr wchar_t ComponentName[] = L"BotClient";
    };

    using MBotClient = base::ClientBase<BotSetup<bot::CompositeSessionId>>;
    using EdnaBotClient = base::ClientBase<BotSetup<bot::PrefixSessionId>>;
}

//#include <memory>
//
//#include <cpprest/http_client.h>
//#include <cpprest/http_listener.h>
//
//#include <ESBotProxyLib/interfaces/i_bot_client.h>
//#include "Log/SystemLog.h"
//
//using http_listener = web::http::experimental::listener::http_listener;
//using http_client = web::http::client::http_client;
//using http_request = web::http::http_request;
//using http_response = web::http::http_response;
//
//
//
//class BotHttpRequest;
//
//class BotClient: public IBotClient
//{
//public:
//    BotClient(
//        const std::string& requestUrl, 
//        const std::string& listenUrl,
//        const std::wstring& sessionIdPrefix);
//    ~BotClient();
//
//    void sendRequestAsync(BotRequest&& request, std::function<void(BotRequestAck&&)> callback) override;
//    void setBotResponseHandler(std::function<void(BotResponse&&)> handler) override;
//
//private:
//    void handleRequest(http_request request);
//    void trySendRequest(int triesLeft, std::shared_ptr<BotHttpRequest> request);
//
//    std::wstring _sessionIdPrefix;
//    std::unique_ptr<http_listener> _listener;
//    std::unique_ptr<http_client> _client;
//    std::function<void(BotResponse&&)> _responseHandler;
//    const int _retries;
//    SystemLog _log;
//};
//

#pragma once

namespace voice
{
    /*
    	std::string request{
		R"--({
 "request_id": "1gf",
 "appeal_id": "6065845",
 "request_time": "2022-03-05T19:40:44.704+03:00",
 "message_number": 1,
 "sender_id": 256,
 "sender_type": "client",
 "meta_data": {
 "region": "01",
 "phone": "89214789012",
 "login": "test_login"
 },
 "cross_links": [
 {
 "system_code": "phone",
 "id": "+79999998877"
 },
{
"system_code": "phone",
"id": "+79999998877"
}
],
"full_name": "Ivanov",
"channel_id": 10,
"channel_name": "MP",
"skill_id": 49,
"skill_name": "Segent_name",
"agent": {},
"data": [
{
"uid": "",
"type": "text",
"content": "How are you?"
}
]
		})--"
	};

    */


    struct Request
    {
        struct Metadata
        {
            std::wstring region;
            std::wstring phone;
            std::wstring login;
        };

        struct CrossLinks
        {
            std::wstring systemCode;
            std::wstring id;
        };

        struct Agent
        {};

        struct Data
        {
            std::wstring uid;
            std::wstring type;
            std::wstring content;
        };

        std::wstring requestId;
        std::wstring appealId;
        std::wstring requestTime;
        int messageNumber;
        int senderId;
        std::wstring senderType;
        Metadata metadata;
        std::vector<CrossLinks> crossLinks;
        std::wstring fullName;
        int channelId;
        std::wstring channelName;
        int skillId;
        std::wstring skillName;
        std::vector<Agent> agent;
        std::vector<Data> data;
        std::wstring replyTo;

        Request() {}

        Request(Request&&) = default;
        Request& operator=(Request&&) = default;

        Request(const Request&) = delete;
        Request& operator=(const Request&) = delete;
    };
}


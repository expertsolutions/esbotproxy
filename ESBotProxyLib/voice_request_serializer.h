#pragma once

#include <cpprest/asyncrt_utils.h>
#include <cpprest/json.h>

#include "Common/IdGenerator.h"
#include "voice_request.h"

namespace voice
{
	class RequestSerializer
	{
	public:
		static web::json::value toJson(const Request& request)
		{
            web::json::value result;

            result[L"request_id"] = web::json::value(request.requestId);
            result[L"appeal_id"] = web::json::value(utility::conversions::to_string_t(request.appealId));
            result[L"request_time"] = web::json::value(utility::conversions::to_string_t(request.requestTime));
            result[L"message_number"] = web::json::value(request.messageNumber);
            result[L"replyTo"] = web::json::value(utility::conversions::to_string_t(request.replyTo));

            result[L"sender_id"] = 111; //unused

            web::json::value metaData;
            metaData[L"region"] = web::json::value(utility::conversions::to_string_t(request.metadata.region));
            metaData[L"phone"] = web::json::value(utility::conversions::to_string_t(request.metadata.phone));
            metaData[L"login"] = web::json::value(utility::conversions::to_string_t(request.metadata.login));

            result[L"meta_data"] = metaData;
            result[L"full_name"] = web::json::value(utility::conversions::to_string_t(request.fullName));
            result[L"channel_id"] = web::json::value(request.channelId);
            result[L"channel_name"] = web::json::value(utility::conversions::to_string_t(request.channelName));
            result[L"sender_type"] = web::json::value(utility::conversions::to_string_t(request.senderType));

            web::json::value dataArray = web::json::value::array();
            for (auto i = 0; i < request.data.size(); ++i)
            {
                web::json::value data;
                data[L"uid"] = web::json::value(utility::conversions::to_string_t(request.data[i].uid));
                data[L"type"] = web::json::value(utility::conversions::to_string_t(request.data[i].type));
                data[L"content"] = web::json::value(utility::conversions::to_string_t(request.data[i].content));

                dataArray[0] = data;
            }

            result[L"data"] = dataArray;

            return result;
		}
	};
}


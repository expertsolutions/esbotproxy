#pragma once
#include "edna_request.h"
#include "edna_request_serializer.h"

namespace edna
{
	struct HttpRequest
	{
		HttpRequest(Request&& request, std::function<void(RequestAck&&)> callback)
			: _request(std::move(request))
			, _callback(callback)

		{
			_body = RequestSerializer::toJson(_request);
		}

		const Request& GetRequest() const { return _request; }
		const web::json::value& GetBody() const { return _body; }
		std::function<void(RequestAck&&)> Callback() const { return _callback; }

	private:
		Request _request;
		web::json::value _body;
		std::function<void(RequestAck&&)> _callback;
	};

	struct HttpResponse
	{
		template <class Response>
		static web::json::value CreateResponse(const Response& response, const std::string& message = {})
		{
			web::json::value httpResponse;

            httpResponse[L"sessionId"] = web::json::value(utility::conversions::to_string_t(response.sessionId));
            httpResponse[L"questionIndex"] = web::json::value(response.questionIndex);
            httpResponse[L"text"] = web::json::value(utility::conversions::to_string_t(message));
            httpResponse[L"threadsClientId"] = web::json::value(response.threadsClientId);
			//httpResponse[L"receivedAt"] = web::json::value(response.receivedAt);

			return httpResponse;
		}

		static web::json::value CreateError(const std::string& message)
		{
			web::json::value httpResponse;
            httpResponse[L"text"] = web::json::value(utility::conversions::to_string_t(message));
			return httpResponse;
		}
	};
}

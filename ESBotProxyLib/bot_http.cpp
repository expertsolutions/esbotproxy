#include "bot_http.h"

#include "bot_request_serializer.h"

namespace bot
{
    template <class SessionId>
    HttpRequest<SessionId>::HttpRequest(Request<SessionId>&& request, std::function<void(RequestAck<SessionId>&&)> callback)
        : _botRequest(std::move(request))
        , _callback(callback)
    {
        _body = RequestSerializer<SessionId>::toJson(_botRequest);
    }

    template <class SessionId>
    HttpRequest<SessionId>::~HttpRequest()
    {
    }

    template class HttpRequest<CompositeSessionId>;
    template class HttpRequest<PrefixSessionId>;
    template class HttpRequest<SimpleSessionId>;
}


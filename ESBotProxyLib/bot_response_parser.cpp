#include "bot_response_parser.h"
#include "bot_session_id.h"

#include "utils/json_utils.h"

namespace bot
{
    template <class SessionId>
    Response<SessionId> ResponseParser<SessionId>::FromJson(web::json::value json, const std::optional<XString>& salt)
    {
        using Response = Response<SessionId>;

        Response result;
        auto& object = json.as_object();

        result.requestId = getOrThrow<XString>(object, TEXT("request_id"));

        const auto sessionId = getOrThrow<XString>(object, TEXT("appeal_id"));
        if (sessionId.empty())
        {
            throw std::runtime_error("appeal_id field could not be empty");
        }

        result.sessionId = SessionId::CreateSessionId(sessionId, salt);

        result.questionNumber = getOrThrow<int>(object, TEXT("message_number"));
        result.timestamp = getOrThrow<XString>(object, TEXT("request_time"));
        const auto resultOpt= Response::StringToResult(getOrDefault<XString>(object, TEXT("result"), XString(TEXT("answer"))));
        if (!resultOpt.has_value())
        {
            throw std::runtime_error("unknown result field");
        }
        result.result = *resultOpt;

        result.errors = getArray<typename Response::Error>(object, TEXT("errors"),
            [](const web::json::value& items) -> std::vector<typename Response::Error>
            {
                if (!items.is_array())
                {
                    return {};
                }

                std::vector<typename Response::Error> result;

                for (auto iter = items.as_array().cbegin(); iter != items.as_array().cend(); ++iter)
                {
                    typename Response::Error error;

                    const auto item = iter->as_object();
                    error.code = getOrDefault<int>(item, TEXT("code"), 0);
                    error.text = getOrDefault<XString>(item, TEXT("text"), TEXT(""));

                    result.push_back(error);
                }

                return result;
            });

        result.data = getArray<typename Response::Data>(object, TEXT("data"),
            [](const web::json::value& items) -> std::vector<typename Response::Data>
            {
                if (!items.is_array())
                {
                    return {};
                }

                std::vector<typename Response::Data> result;

                for (auto iter = items.as_array().cbegin(); iter != items.as_array().cend(); ++iter)
                {
                    typename Response::Data data;

                    const auto item = iter->as_object();
                    data.score = getOrDefault<XString>(item, TEXT("score"), TEXT(""));
                    data.uid = getOrDefault<XString>(item, TEXT("uid"), TEXT(""));
                    data.type = getOrDefault<XString>(item, TEXT("type"), TEXT("text"));
                    data.content = getOrDefault<XString>(item, TEXT("content"), TEXT(""));

                    result.push_back(data);
                }

                return result;
            });

        return result;
    }

    template class ResponseParser<CompositeSessionId>;
    template class ResponseParser<PrefixSessionId>;
    template class ResponseParser<SimpleSessionId>;
}


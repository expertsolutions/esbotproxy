#pragma once

#include <functional>
#include <cpprest/json.h>


#include "voice_request.h"
#include "voice_request_ack.h"
#include "voice_request_serializer.h"

namespace voice
{
    class HttpRequest
    {
    public:
        HttpRequest(Request&& request, std::function<void(RequestAck&&)> callback)
			: _request(std::move(request))
			, _callback(callback)
		{
			_body = RequestSerializer::toJson(_request);
		}

        const web::json::value& GetBody() const { return _body; }
        const Request& GetRequest() const { return _request; }
        std::function<void(RequestAck&&)> Callback() const { return _callback; }

    private:
        Request _request;
        web::json::value _body;
        std::function<void(RequestAck&&)> _callback;
    };

	struct HttpResponse
	{
		template <class Response>
		static web::json::value CreateResponse(const Response& response, const std::string& message = {})
		{
			web::json::value httpResponse;
			//httpResponse[L"Code"] = web::json::value(0);
			//httpResponse[L"SessionId"] = web::json::value(utility::conversions::to_string_t(response.sessionId.GetBotSessionId()));
			//httpResponse[L"QuestionNumber"] = web::json::value(response.questionNumber);

			httpResponse[L"request_id"] = web::json::value(utility::conversions::to_string_t(response.requestId));
			httpResponse[L"appeal_id"] = web::json::value(utility::conversions::to_string_t(response.appealId));
			httpResponse[L"message_number"] = web::json::value(response.questionNumber);
			httpResponse[L"Code"] = web::json::value(0);

			return httpResponse;
		}

		static web::json::value CreateError(const std::string& message)
		{
			web::json::value httpResponse;
			httpResponse[L"Code"] = 1;
			httpResponse[L"Description"] = web::json::value(utility::conversions::to_string_t(message));

			return httpResponse;
		}
	};

}



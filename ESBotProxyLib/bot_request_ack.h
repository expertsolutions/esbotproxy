#pragma once

#include <string>
#include <iostream>

namespace bot
{
    template <class SessionId>
    struct RequestAck
    {
        XString description;
        SessionId/*CompositeSessionId*/ sessionId;
        int questionNumber;
        int code;

        RequestAck() = default;

        RequestAck(RequestAck&&) = default;
        RequestAck& operator=(RequestAck&&) = default;

        RequestAck(const RequestAck&) = delete;
        RequestAck& operator=(const RequestAck&) = delete;

        RequestAck(
            const XString& description,
            const SessionId/*CompositeSessionId*/& sessionId,
            const Request<SessionId>& request,
            const int code)
            : description(description)
            , sessionId(sessionId)
            , questionNumber(request.questionNumber)
            , code(code)
        {}

        XString ToString() const
        {
#ifdef _WIN32
            std::wstringstream str;
#else
            std::stringstream str;
#endif
            str << TEXT("RequestAck {description: '") << description
                << TEXT("', sessionId: ") << sessionId.GetSessionId()
                << TEXT(", questionNumber: ") << questionNumber
                << TEXT(", code: ") << code
                << TEXT("}");

            return str.str();
        }

        //static XString GetSessionId(const XString& /*salt*/, const Request& request)
        //{
        //    return request.sessionId.GetSessionId();
        //    //return request.sessionId.GetBotSessionId();
        //}

        static SessionId/*CompositeSessionId*/ GetSessionId(const std::optional<XString>& /*salt*/, const Request<SessionId>& request)
        {
            return request.sessionId;
            //return request.sessionId.GetBotSessionId();
        }

    };
}

#pragma once

#include "bot_session_id.h"

// Represents request made to ChatAdapter from script

namespace bot
{
    template <class SessionId>
    struct Request
    {
        SessionId sessionId;
        //CompositeSessionId sessionId;
        XString marketCode;
        XString message;
        XString ctn;
        XString authType;
        XString segment;
        XString channelName;
        XString timestamp;
        XString replyTo;
        int channelType;
        int questionNumber;
        XString cityId;
        XString districtId;
        XString subscriberNo;
        XString pl;

        Request() {}

        Request(Request<SessionId>&&) = default;
        Request& operator=(Request<SessionId>&&) = default;

        Request(const Request<SessionId>&) = delete;
        Request& operator=(const Request<SessionId>&) = delete;
    };
}


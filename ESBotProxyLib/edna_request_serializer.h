#pragma once

#include <cpprest/json.h>

#include "edna_request.h"

namespace edna
{
    class RequestSerializer
    {
    public:
        static web::json::value toJson(const Request& request)
        {
            web::json::value result;

            result[L"sessionId"] = web::json::value(utility::conversions::to_string_t(request.sessionId));
            result[L"formattedText"] = web::json::value(utility::conversions::to_string_t(request.text));
            //result[L"receivedAt"] = web::json::value(utility::conversions::to_string_t(request.receivedAt));
            result[L"answerId"] = web::json::value(utility::conversions::to_string_t(request.answerId));
            result[L"questionIndex"] = web::json::value(request.questionIndex);
            result[L"code"] = web::json::value(utility::conversions::to_string_t(ToString(request.code)));

            return result;
        }
    };
}



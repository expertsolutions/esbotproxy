#pragma once
#include <map>
#include <optional>
#include <boost/algorithm/string.hpp>

#include "bot_session_id.h"

namespace bot
{
    template <class SessionId>
    struct Response
    {
        enum class Result
        {
            Answer = 0,
            Prompter,
            Close
        };

        struct Data
        {
            XString score;
            XString uid;
            XString type;
            XString content;
        };

        struct Error
        {
            int code{};
            XString text;
        };

        static std::optional<Result> StringToResult(const XString& result)
        {
            static std::map<XString, Result> results
            {
                {TEXT("answer"), Result::Answer},
                {TEXT("prompter"), Result::Prompter},
                {TEXT("close"), Result::Close}
            };

            if (const auto cit = results.find(result); cit != results.cend())
            {
                return cit->second;
            }

            return std::nullopt;
        }

        // message unique identifier
        XString requestId;

        // chat session unique identifier
        //CompositeSessionId sessionId;
        SessionId sessionId;

        // question number during current session
        int questionNumber;

        // response code: 0 - success, 1 - error during request handling
        //int code;
        std::vector<Error> errors;

        // error description. Empty if there is no errors
        //std::string description;

        // response type: 0 - question was not understood, 1 - has response, 2 - transfer to agent
        //int responseType;
        Result result;

        // response text in html
        //std::string message;
        std::vector<Data> data;

        XString timestamp;

        // split number for routing
        XString split;

        Response() = default;

        Response(Response&&) = default;
        Response& operator=(Response&&) = default;

        Response(const Response&) = delete;
        Response& operator=(const Response&) = delete;

        XString MakeMessage() const
        {
            XString content;
            if (!data.empty())
            {
                content = data.at(0).content;
                boost::replace_all(content, TEXT("\""), TEXT("\\\""));
            }
            return content;
        }

        XString MakeError() const
        {
            if (!errors.empty())
            {
                return errors.at(0).text;
            }
            return XString{};
        }
    };

    
}

#pragma once

#include <memory>
#include <optional>

#include <boost/asio/buffer.hpp>
#include <boost/asio/ssl/context.hpp>

#include <cpprest/http_client.h>
#include <cpprest/http_listener.h>

#include "Log/SystemLog.h"

#ifdef _WIN32
using XString = std::wstring;
inline std::wstring ConvertToXString(const std::wstring& str)
{
    return str;
}

inline std::wstring ConvertToXString(const std::string& str)
{
    return stow(str);
}

inline std::wstring ConvertToXString2(const std::string& str)
{
    return stow(str);
}


#else
#define TEXT(STRING) STRING
using XString = std::string;
inline std::string ConvertToXString(const std::wstring& str)
{
    return wtos(str);
}

inline std::wstring ConvertToXString(const std::string& str)
{
    return stow(str);
}

inline std::string ConvertToXString2(const std::string& str)
{
    return str;
}

#endif

//#ifdef _WIN32
//#else

namespace base
{
    using http_listener = web::http::experimental::listener::http_listener;
    using http_listener_config = web::http::experimental::listener::http_listener_config;
    using http_client = web::http::client::http_client;
    using http_client_config = web::http::client::http_client_config;
    using http_request = web::http::http_request;
    using http_response = web::http::http_response;

    template <class Setup>
    class ClientBase
    {
        using SessionId = typename Setup::SessionId;
        using Request = typename Setup::Request;
        using RequestAck = typename Setup::RequestAck;
        using Response = typename Setup::Response;
        using HttpRequest = typename Setup::HttpRequest;
        using HttpResponse = typename Setup::HttpResponse;
        using ResponseParser = typename Setup::ResponseParser;
        using RequestAckParser = typename Setup::RequestAckParser;

        const std::wstring COMPONENT_NAME{ Setup::ComponentName };

    public:
        ClientBase(const std::string& requestUrl,
            const std::string& listenUrl,
            const std::optional<XString>& sessionIdPrefix/* = {}*/)
            : _sessionIdPrefix{ sessionIdPrefix }
            , _retries{ 3 }
        {
            // can throw
            _client = std::make_unique<http_client>(utility::conversions::to_string_t(requestUrl));
            _listener = std::make_unique<http_listener>(utility::conversions::to_string_t(listenUrl));

            _listener->support(web::http::methods::POST, std::bind(&ClientBase::handleRequest, this, std::placeholders::_1));
            _listener->open()
                .then([listenUrl, this](pplx::task<void> task)
                {
                    try 
                    {
                        task.get();
                    }
                    catch (std::exception& e)
                    {
                        _log->LogStringModule(LEVEL_FATAL, COMPONENT_NAME, L"Could not start HTTP listener: %s", stow(e.what()));
                        throw;
                    }
                })
                .wait();
        }

        ClientBase(
            const std::string& requestUrl, 
            const std::string& listenUrl, 
            const std::string& certificate, 
            const std::string& privateKey,
            const std::string& clientRootCertificate,
            const XString& authorizationToken,
            const std::optional<XString>& sessionIdPrefix/* = {}*/)
            : _retries{ 3 }
            , _authorizationToken{ authorizationToken }
            , _sessionIdPrefix{ sessionIdPrefix }
        {
            http_listener_config server_config;
            server_config.set_ssl_context_callback([this, certificate, privateKey](boost::asio::ssl::context& ctx) {
                ctx.set_options(boost::asio::ssl::context::default_workarounds);

                //ctx.use_certificate_chain_file(certificate);
                //ctx.use_private_key_file(privateKey, boost::asio::ssl::context::pem);

                boost::asio::const_buffer cert(certificate.c_str(), certificate.size());
                boost::asio::const_buffer key(privateKey.c_str(), privateKey.size());

                ctx.use_certificate_chain(cert);
                ctx.use_private_key(key, boost::asio::ssl::context::pem);
                });

            _listener = std::make_unique<http_listener>(utility::conversions::to_string_t(listenUrl), server_config);

            http_client_config client_config;
            client_config.set_ssl_context_callback(
                [this, clientRootCertificate](boost::asio::ssl::context& ctx)
                { 
                    boost::asio::const_buffer root(clientRootCertificate.c_str(), clientRootCertificate.size());
                    ctx.add_certificate_authority(root);

                    //ctx.load_verify_file(clientRootCertificate);
                });

            _client = std::make_unique<http_client>(utility::conversions::to_string_t(requestUrl), client_config);

            _listener->support(web::http::methods::POST, std::bind(&ClientBase::handleRequest, this, std::placeholders::_1));
            _listener->open()
                .then([listenUrl, this](pplx::task<void> task)
                    {
                        try
                        {
                            task.get();
                        }
                        catch (std::exception & e)
                        {
                            _log->LogStringModule(LEVEL_FATAL, COMPONENT_NAME, L"Could not start HTTP listener: %s", stow(e.what()));
                            throw;
                        }
                    })
                .wait();
        }

        ~ClientBase()
        {
            try
            {
                _listener->close().wait();
            }
            catch (std::exception& e)
            {
                _log->LogStringModule(LEVEL_FATAL, COMPONENT_NAME, L"Could not stop HTTP listener: %s", stow(e.what()));
            }
        }

        void sendRequestAsync(Request&& request, std::function<void(RequestAck&&)> callback)
        {
            auto httpRequest = std::make_shared<HttpRequest>(std::move(request), callback);
            trySendRequest(_retries, httpRequest);
        }
        void setResponseHandler(std::function<void(Response&&)> handler)
        {
            _responseHandler = handler;
        }

    private:
        void handleRequest(http_request request)
        {
            _log->LogStringModule(LEVEL_INFO, COMPONENT_NAME, L"New incoming HTTP request: %s", ConvertToXString(request.absolute_uri().to_string()));
            request.extract_json()
                .then([request, this](web::json::value value)
                {
                    _log->LogStringModule(LEVEL_INFO, COMPONENT_NAME, L"Incoming HTTP request body: %s", ConvertToXString(value.serialize()));

                    try
                    {
                        auto response = ResponseParser::FromJson(value, _sessionIdPrefix);
                        web::json::value httpResponse = HttpResponse::CreateResponse(response);

                        if (_responseHandler)
                        {
                            _responseHandler(std::move(response));
                        }
                        request.reply(web::http::status_codes::OK, httpResponse);
                    }
                    catch (std::exception& error)
                    {
                        _log->LogStringModule(LEVEL_SEVERE, COMPONENT_NAME, L"Error while processing incoming HTTP request: %s", error.what());

                        web::json::value httpResponse = HttpResponse::CreateError(error.what());
                        request.reply(web::http::status_codes::InternalError, httpResponse);
                    }
                    catch (...)
                    {
                        _log->LogStringModule(LEVEL_SEVERE, COMPONENT_NAME, L"Unknown rrror while processing incoming HTTP request");

                        web::json::value httpResponse = HttpResponse::CreateError("unknown");
                        request.reply(web::http::status_codes::InternalError, httpResponse);
                    }

                })
                .then([request, this](pplx::task<void> task)
                {
                    try
                    {
                        task.get();
                    }
                    catch (std::exception & e)
                    {
                        _log->LogStringModule(LEVEL_SEVERE, COMPONENT_NAME, L"Error while handling incoming HTTP request: %S", stow(e.what()));

                        web::json::value httpResponse = HttpResponse::CreateError(e.what());

                        //web::json::value response;
                        //response[L"Code"] = 1;
                        //response[L"Description"] = web::json::value(utility::conversions::to_string_t(e.what()));

                        // TODO: add possible exception handling
                        request.reply(web::http::status_codes::InternalError, httpResponse);
                    }
                });
        }
        
        void trySendRequest(int triesLeft, std::shared_ptr<HttpRequest> httpRequest)
        {
            _log->LogStringModule(LEVEL_INFO, COMPONENT_NAME, L"Trying to send HTTP request (try %d of %d): %s", _retries - triesLeft + 1, _retries, ConvertToXString(httpRequest->GetBody().serialize()));

            http_request msg(web::http::methods::POST);
            msg.set_body(httpRequest->GetBody());
            
            if (!_authorizationToken.empty())
            {
                auto& headers = msg.headers();
                headers.add(TEXT("Authorization"), XString(TEXT("Bearer ")) + _authorizationToken);
            }

            auto task = _client->request(msg);
            task
                .then([this](http_response response)
                {
                    _log->LogStringModule(LEVEL_INFO, COMPONENT_NAME, L"Got HTTP response: %s", ConvertToXString(response.to_string()));
                    if (response.status_code() == web::http::status_codes::OK)
                    {
                        return response.extract_json();
                    }
                    else
                    {
                        throw web::http::http_exception(response.reason_phrase());
                    }
                })
                .then([=](web::json::value json)
                {
                    _log->LogStringModule(LEVEL_INFO, COMPONENT_NAME, L"HTTP response body: %s", ConvertToXString(json.serialize()));
                    auto ack = RequestAckParser::FromJson(json, httpRequest->GetRequest(), _sessionIdPrefix);
                    httpRequest->Callback()(std::move(ack));
                    _log->LogStringModule(LEVEL_INFO, COMPONENT_NAME, L"HTTP response process completed");
                })
                .then([=](pplx::task<void> task)
                {
                    try
                    {
                        task.get();
                    }
                    catch (std::exception & e)
                    {
                        _log->LogStringModule(LEVEL_SEVERE, COMPONENT_NAME, L"Error while sending HTTP request: %S", stow(e.what()));
                        if (triesLeft > 1)
                        {
                            this->trySendRequest(triesLeft - 1, httpRequest);
                        }
                        else
                        {
                            const auto& request = httpRequest->GetRequest();
                            //request->callback()(BotRequestAck{ stow(e.what()), botRequest.sessionId.GetRealSessionId(_sessionIdPrefix), botRequest.questionNumber, 1 });
                            httpRequest->Callback()(RequestAck{ ConvertToXString2(e.what()), RequestAck::GetSessionId(_sessionIdPrefix, request), request, 1 });
                        }
                    }
                });
        }

        std::optional<XString> _sessionIdPrefix;
        std::unique_ptr<http_listener> _listener;
        std::unique_ptr<http_client> _client;
        std::function<void(Response&&)> _responseHandler;
        const int _retries;
        SystemLog _log;
        const XString _authorizationToken;
    };
}

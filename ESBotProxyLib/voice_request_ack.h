#pragma once

#include <string>
#include "utils/json_utils.h"

namespace voice
{
    struct RequestAck
    {
        std::wstring description;
        std::wstring appealId;
        std::wstring requestId;
        int messageNumber;
        int code;

        RequestAck() = default;

        RequestAck(RequestAck&&) = default;
        RequestAck& operator=(RequestAck&&) = default;

        RequestAck(const RequestAck&) = delete;
        RequestAck& operator=(const RequestAck&) = delete;

        RequestAck(
            const std::wstring& description,
            const std::wstring& appealId,
            const Request& request,
            const int code)
            : description(description)
            , appealId(appealId)
            , requestId(request.requestId)
            , messageNumber(request.messageNumber)
            , code(code)
        {}

        std::wstring ToString() const
        {
            std::wstringstream str;
            str << "RequestAck {description: '" << description
                << "', appealId: " << appealId
                << "', requestId: " << requestId
                << ", messageNumber: " << messageNumber
                << ", code: " << code
                << "}";

            return str.str();
        }

        static std::wstring GetSessionId(const std::optional<std::wstring>& /*salt*/, const Request& request)
        {
            return request.appealId;
        }
    };
    struct RequestAckParser
    {
        // got empty json?
        static RequestAck FromJson(const web::json::value& json, const Request& request, const std::optional<std::wstring>& /*salt*/)
        {
            RequestAck ack;
            auto& object = json.as_object();

            ack.appealId = request.appealId;
            ack.requestId = request.requestId;
            ack.messageNumber = request.messageNumber;

            ack.code = getOrDefault<int>(object, L"code", -1);
            //ack.description = 


            return ack;
        }
    };

}
#pragma once
#include "ESBotProxyLib/utils/json_utils.h"

namespace edna
{
    // edna response => bot
	struct Response
	{
        std::wstring sessionId;
        std::wstring text;
        std::wstring receivedAt;
        std::wstring channelType;
        int channelId{};
        std::wstring phone;
        //int questionId{};
        int questionIndex{};
        int threadsClientId{};
        bool authorized{};
	};

	struct ResponseParser
	{
        static Response FromJson(const web::json::value& json, const std::optional<std::wstring>& /*salt*/)
        {
            Response result;
            const auto& object = json.as_object();

            result.sessionId = getOrDefault<std::wstring>(object, L"sessionId", L"");
            result.text = getOrDefault<std::wstring>(object, L"text", L"");
            result.receivedAt = getOrDefault<std::wstring>(object, L"receivedAt", L"");
            //result.questionId = getOrDefault<int>(object, "questionId", 0);
            result.questionIndex = getOrDefault<int>(object, L"questionIndex", 0);
            result.threadsClientId = getOrDefault<int>(object, L"threadsClientId", 0);

            const auto& channelInfo = object.find(L"channelInfo");
            if (channelInfo != object.end())
            {
                result.channelType = getOrDefault<std::wstring>(channelInfo->second.as_object(), L"channelType", L"");
                result.authorized = getOrDefault<bool>(channelInfo->second.as_object(), L"authorized", false);
                result.channelId = getOrDefault<int>(channelInfo->second.as_object(), L"id", 99);
            }

            const auto& clientData = object.find(L"clientData");
            if (clientData != object.end())
            {
                result.phone = getOrDefault<std::wstring>(clientData->second.as_object(), L"phone", L"");
            }

            return result;
        }
	};
}

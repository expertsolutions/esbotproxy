#pragma once

#include <cpprest/json.h>

#include "bot_http.h"
#include "bot_request_ack.h"

namespace bot
{
    template <class SessionId>
    class RequestAckParser
    {
    public:
        //static RequestAck FromJson(const web::json::value& json);
        static RequestAck<SessionId> FromJson(const web::json::value& json, const Request<SessionId>& /*request*/, const std::optional<XString>& salt);
    };
}



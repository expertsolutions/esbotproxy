//#include "bot_client.h"
//
//#include "bot_http.h"
//#include "request_ack_parser.h"
//#include "bot_response_parser.h"
//
//#define COMPONENT_NAME L"BotClient"
//
//BotClient::BotClient(const std::string& requestUrl, const std::string& listenUrl, const std::wstring& sessionIdPrefix)
//    : _retries(3)
//    , _sessionIdPrefix{ sessionIdPrefix }
//{
//    // can throw
//    _client = std::make_unique<http_client>(utility::conversions::to_string_t(requestUrl));
//    _listener = std::make_unique<http_listener>(utility::conversions::to_string_t(listenUrl));
//
//    _listener->support(web::http::methods::POST, std::bind(&BotClient::handleRequest, this, std::placeholders::_1));
//    _listener->open()
//        .then([listenUrl, this] (pplx::task<void> task) {
//            try {
//                task.get();
//            }
//            catch (std::exception &e) {
//                _log->LogStringModule(LEVEL_FATAL, COMPONENT_NAME, L"Could not start HTTP listener: %s", stow(e.what()));
//                throw;
//            }
//        })
//        .wait();
//}
//
//BotClient::~BotClient()
//{
//    try {
//        _listener->close().wait();
//    }
//    catch (std::exception &e) {
//        _log->LogStringModule(LEVEL_FATAL, COMPONENT_NAME, L"Could not stop HTTP listener: %s", stow(e.what()));
//    }
//}
//
//void BotClient::sendRequestAsync(BotRequest&& request, std::function<void(BotRequestAck&&)> callback)
//{
//    auto httpRequest = std::make_shared<BotHttpRequest>(std::move(request), callback);
//    trySendRequest(_retries, httpRequest);
//}
//
//void BotClient::setBotResponseHandler(std::function<void(BotResponse&&)> handler)
//{
//    _responseHandler = handler;
//}
//
//void BotClient::handleRequest(web::http::http_request request)
//{
//    _log->LogStringModule(LEVEL_INFO, COMPONENT_NAME, L"New incoming HTTP request: %s", request.absolute_uri().to_string());
//    request.extract_json()
//        .then([request, this](web::json::value value) {
//            _log->LogStringModule(LEVEL_INFO, COMPONENT_NAME, L"Incoming HTTP request body: %s", value.serialize());
//            auto botResponse = BotResponseParser::FromJson(value, _sessionIdPrefix);
//
//            web::json::value httpResponse;
//            httpResponse[L"Code"] = web::json::value(0);
//            httpResponse[L"SessionId"] = web::json::value(utility::conversions::to_string_t(botResponse.sessionId.GetBotSessionId()));
//            httpResponse[L"QuestionNumber"] = web::json::value(botResponse.questionNumber);
//
//            if (_responseHandler) {
//                _responseHandler(std::move(botResponse));
//            }
//            request.reply(web::http::status_codes::OK, httpResponse);
//        })
//        .then([request, this](pplx::task<void> task) {
//            try {
//                task.get();                              
//            }
//            catch (std::exception &e) {
//                _log->LogStringModule(LEVEL_SEVERE, COMPONENT_NAME, L"Error while handling incoming HTTP request: %S", e.what());
//                web::json::value response;
//                response[L"Code"] = 1;
//                response[L"Description"] = web::json::value(utility::conversions::to_string_t(e.what()));
//
//                // TODO: add possible exception handling
//                request.reply(web::http::status_codes::InternalError, response);
//            }
//        });
//}
//
//void BotClient::trySendRequest(int triesLeft, std::shared_ptr<BotHttpRequest> request)
//{
//    _log->LogStringModule(LEVEL_INFO, COMPONENT_NAME, L"Trying to send HTTP request (try %d of %d): %s", _retries - triesLeft + 1, _retries, request->body().serialize());
//    auto task = _client->request(web::http::methods::POST, L"", request->body());
//
//    task
//        .then([this](http_response response) {
//            _log->LogStringModule(LEVEL_INFO, COMPONENT_NAME, L"Got HTTP response: %s", response.to_string());
//            if (response.status_code() == web::http::status_codes::OK) {
//                return response.extract_json();
//            } else {
//                throw web::http::http_exception(response.reason_phrase());
//            }
//        })
//        .then([=](web::json::value json) {
//            _log->LogStringModule(LEVEL_INFO, COMPONENT_NAME, L"HTTP response body: %s", json.serialize());
//            auto ack = RequestAckParser::FromJson(json);
//            request->callback()(std::move(ack));
//        })
//        .then([=](pplx::task<void> task) {
//            try {
//                task.get();
//            }
//            catch (std::exception &e) {
//                _log->LogStringModule(LEVEL_SEVERE, COMPONENT_NAME, L"Error while sending HTTP request: %S", e.what());
//                if (triesLeft > 1) {
//                    this->trySendRequest(triesLeft - 1, request);
//                } else {
//                    auto& botRequest = request->botRequest();
//                    request->callback()(BotRequestAck{ stow(e.what()), botRequest.sessionId.GetRealSessionId(_sessionIdPrefix), botRequest.questionNumber, 1 });
//                }
//            }
//        });
//}

#pragma once

#include <functional>
#include <cpprest/json.h>


#include "bot_request.h"
#include "bot_request_ack.h"

namespace bot
{
	template <class SessionId>
    class HttpRequest
    {
    public:
        HttpRequest(Request<SessionId>&& request, std::function<void(RequestAck<SessionId>&&)> callback);
        ~HttpRequest();

        const web::json::value& GetBody() const { return _body; }
        const Request<SessionId>& GetRequest() const { return _botRequest; }
        std::function<void(RequestAck<SessionId>&&)> Callback() const { return _callback; }

    private:
        Request<SessionId> _botRequest;
        web::json::value _body;
        std::function<void(RequestAck<SessionId>&&)> _callback;
    };

	struct HttpResponse
	{
		template <class Response>
		static web::json::value CreateResponse(const Response& response, const std::string& message = {})
		{
			web::json::value httpResponse;
            //httpResponse[L"Code"] = web::json::value(0);
			//httpResponse[L"SessionId"] = web::json::value(utility::conversions::to_string_t(response.sessionId.GetBotSessionId()));
			//httpResponse[L"QuestionNumber"] = web::json::value(response.questionNumber);

			httpResponse[TEXT("request_id")] = web::json::value(utility::conversions::to_string_t(response.requestId));
			httpResponse[TEXT("appeal_id")] = web::json::value(utility::conversions::to_string_t(response.sessionId.GetSessionId()));
			httpResponse[TEXT("message_number")] = web::json::value(response.questionNumber);
			httpResponse[TEXT("Code")] = web::json::value(0);

			return httpResponse;
		}

		static web::json::value CreateError(const std::string& message)
		{
			web::json::value httpResponse;
            httpResponse[TEXT("Code")] = 1;
            httpResponse[TEXT("Description")] = web::json::value(utility::conversions::to_string_t(message));

			return httpResponse;
		}
	};

}



#pragma once

#include <string>
#include <stdexcept>
#include <optional>

#ifdef _WIN32
#ifndef _WINSOCK
#define _WINSOCK
#include <Windows.h>
#endif
using XString = std::wstring;
#else
#define TEXT(STRING) STRING
using XString = std::string;
#endif

namespace bot
{
    //class SessionId
    //{
    //    XString CreateSessionId(const XString& realSessionId, const XString& salt)
    //    {
    //        return salt + realSessionId;
    //    }

    //public:
    //    SessionId() = default;

    //    explicit SessionId(const XString& realSessionId, const XString& salt)
    //        : _sessionId(CreateSessionId(realSessionId, salt))
    //    {
    //    }

    //    explicit SessionId(const XString& sessionId)
    //        : _sessionId(sessionId)
    //    {
    //    }

    //    XString GetBotSessionId() const
    //    {
    //        return _sessionId;
    //    }

    //    static XString GetRealSessionId(const XString& sessionId, const XString& salt)
    //    {
    //        if (!CheckSessionId(sessionId, salt))
    //        {
    //            std::runtime_error("wrong bot session id");
    //        }

    //        return XString{ sessionId.begin() + salt.size(),  sessionId.end() };
    //    }

    //    XString GetRealSessionId(const XString& salt) const
    //    {
    //        return GetRealSessionId(_sessionId, salt);
    //    }

    //    bool IsEmpty() const
    //    {
    //        return _sessionId.empty();
    //    }

    //    static bool CheckSessionId(const XString& sessionId, const XString& salt)
    //    {
    //        if (salt.empty())
    //        {
    //            return true;
    //        }
    //        auto pos = sessionId.find(salt);
    //        return pos != XString::npos;
    //    }

    //private:
    //    XString _sessionId;
    //};

    class SimpleSessionId
    {
    public:
        static SimpleSessionId CreateSimpleSessionId(const XString& scriptId)
        {
            return SimpleSessionId(scriptId);
        }

        static SimpleSessionId CreateSessionId(const XString& sessionId, const std::optional<XString>& /*salt*/)
        {
            return SimpleSessionId(sessionId);
        }

        XString GetSessionId() const
        {
            return _scriptId;
        }

        XString GetScriptId() const
        {
            return _scriptId;
        }

        SimpleSessionId() = default;
        SimpleSessionId(const SimpleSessionId&) = default;

        SimpleSessionId& operator=(const SimpleSessionId&) = default;

    private:
        SimpleSessionId(const XString& scriptId)
            : _scriptId{ scriptId }
        {}
    private:
        XString _scriptId;
    };

    class PrefixSessionId
    {
    public:
        static PrefixSessionId CreatePrefixSessionId(const XString& scriptId, const XString& salt)
        {
            return PrefixSessionId(scriptId, salt);
        }

        static PrefixSessionId CreateSessionId(const XString& sessionId, const std::optional<XString>& salt)
        {
            if (!salt.has_value())
            {
                throw std::runtime_error("Empty salt when CreateSessionId");
            }

            auto pos = sessionId.find(*salt);
            XString scriptId = XString{ sessionId.begin() + pos,  sessionId.end() };

            return PrefixSessionId(scriptId, *salt);
        }

        XString GetSessionId() const
        {
            return _salt + _scriptId;
        }

        XString GetScriptId() const
        {
            return _scriptId;
        }

        PrefixSessionId() = default;
        PrefixSessionId(const PrefixSessionId&) = default;

        PrefixSessionId& operator=(const PrefixSessionId&) = default;

    private:
        PrefixSessionId(const XString& scriptId, const XString& salt)
            : _scriptId{ scriptId }
            , _salt{ salt }
        {}
    private:
        XString _scriptId;
        XString _salt;
    };


    class CompositeSessionId
    {
#ifdef _WIN32
        static constexpr wchar_t Delim[] = L".";
#else
        static constexpr char Delim[] = ".";
#endif

    public: 
        static CompositeSessionId CreateCompositeSessionId(const XString& scriptId, const std::optional<XString>& salt)
        {
            if (!salt.has_value())
            {
                throw std::logic_error("Empty salt");
            }

            return CompositeSessionId(scriptId, *salt);
        }

        static CompositeSessionId CreateSessionId(const XString& sessionId, const std::optional<XString>& /*salt*/)
        {
            auto pos = sessionId.find(Delim);
            XString scriptId = XString{ sessionId.cbegin(), sessionId.cbegin() + pos };
            XString salt = XString{ sessionId.cbegin() + pos + 1, sessionId.cend() };

            return CompositeSessionId(scriptId, salt);
        }

        XString GetSessionId() const
        {
            return _scriptId + Delim + _salt;
        }

        XString GetScriptId() const
        {
            return _scriptId;
        }

        CompositeSessionId() = default;
        CompositeSessionId(const CompositeSessionId&) = default;

        CompositeSessionId& operator=(const CompositeSessionId&) = default;

    private:
        CompositeSessionId(const XString& scriptId, const XString& salt)
            : _scriptId{ scriptId }
            , _salt{ salt }
        {}
    private:
        XString _scriptId;
        XString _salt;
    };
}

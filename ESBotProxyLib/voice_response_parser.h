#pragma once

#include <cpprest/json.h>
#include "utils/json_utils.h"

#include "voice_response.h"

namespace voice
{
    class ResponseParser
    {
    public:
        static Response FromJson(web::json::value json, const std::optional<std::wstring>& /*salt*/)
        {
            Response result;
            auto& object = json.as_object();

            result.requestId = getOrThrow<std::wstring>(object, L"request_id");

            const auto appealId = getOrThrow<std::wstring>(object, L"appeal_id");
            if (appealId.empty())
            {
                throw std::exception("appeal_id field could not be empty");
            }

            result.appealId = appealId;

            result.questionNumber = getOrThrow<int>(object, L"message_number");
            result.timestamp = getOrThrow<std::wstring>(object, L"request_time");
            const auto resultOpt = Response::StringToResult(getOrDefault<std::wstring>(object, L"result", L"answer"));
            if (!resultOpt.has_value())
            {
                throw std::exception("unknown result field");
            }
            result.result = *resultOpt;

            result.errors = getArray<Response::Error>(object, L"errors",
                [](const web::json::value& items) -> std::vector<Response::Error>
                {
                    if (!items.is_array())
                    {
                        return {};
                    }

                    std::vector<Response::Error> result;

                    for (auto iter = items.as_array().cbegin(); iter != items.as_array().cend(); ++iter)
                    {
                        Response::Error error;

                        const auto item = iter->as_object();
                        error.code = getOrDefault<int>(item, L"code", 0);
                        error.text = getOrDefault<std::wstring>(item, L"text", L"");

                        result.push_back(error);
                    }

                    return result;
                });

            result.data = getArray<Response::Data>(object, L"data",
                [](const web::json::value& items) -> std::vector<Response::Data>
                {
                    if (!items.is_array())
                    {
                        return {};
                    }

                    std::vector<Response::Data> result;

                    for (auto iter = items.as_array().cbegin(); iter != items.as_array().cend(); ++iter)
                    {
                        Response::Data data;

                        const auto item = iter->as_object();
                        data.score = getOrDefault<std::wstring>(item, L"score", L"");
                        data.uid = getOrDefault<std::wstring>(item, L"uid", L"");
                        data.type = getOrDefault<std::wstring>(item, L"type", L"text");
                        data.content = getOrDefault<std::wstring>(item, L"content", L"");

                        result.push_back(data);
                    }

                    return result;
                });

            result.replyToId = getOrThrow<std::wstring>(object, L"replyTo_id");

            return result;

        }
    };
}



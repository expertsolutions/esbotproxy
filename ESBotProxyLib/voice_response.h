#pragma once
#include <map>
#include <optional>

//#include "voice_session_id.h"

namespace voice
{
    struct Response
    {
        enum class Result
        {
            Answer = 0,
            Prompter,
            Close
        };

        struct Data
        {
            std::wstring score;
            std::wstring uid;
            std::wstring type;
            std::wstring content;
        };

        struct Error
        {
            int code{};
            std::wstring text;
        };

        static std::optional<Result> StringToResult(const std::wstring& result)
        {
            static std::map<std::wstring, Result> results
            {
                {L"answer", Result::Answer},
                {L"prompter", Result::Prompter},
                {L"close", Result::Close}
            };

            if (const auto cit = results.find(result); cit != results.cend())
            {
                return cit->second;
            }

            return std::nullopt;
        }

        // message unique identifier
        std::wstring requestId;

        // chat session unique identifier
        std::wstring appealId;

        // question number during current session
        int questionNumber;

        // response code: 0 - success, 1 - error during request handling
        //int code;
        std::vector<Error> errors;

        // error description. Empty if there is no errors
        //std::wstring description;

        // response type: 0 - question was not understood, 1 - has response, 2 - transfer to agent
        //int responseType;
        Result result;

        // response text in html
        //std::wstring message;
        std::vector<Data> data;

        std::wstring timestamp;

        // split number for routing
        std::wstring split;

        // content request id unique identifier
        std::wstring replyToId;

        Response() = default;

        Response(Response&&) = default;
        Response& operator=(Response&&) = default;

        Response(const Response&) = delete;
        Response& operator=(const Response&) = delete;

        std::wstring MakeMessage() const
        {
            if (!data.empty())
            {
                return data.at(0).content;
            }
            return std::wstring{};
        }

        std::wstring MakeError() const
        {
            if (!errors.empty())
            {
                return errors.at(0).text;
            }
            return std::wstring{};
        }
    };

    
}
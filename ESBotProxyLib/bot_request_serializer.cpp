#include "bot_request_serializer.h"
#include "bot_session_id.h"

#include <cpprest/asyncrt_utils.h>
#include "Common/IdGenerator.h"

namespace bot
{
    template <class SessionId>
    web::json::value RequestSerializer<SessionId>::toJson(const Request<SessionId>& request)
    {
        web::json::value result;

        //result[L"SessionId"] = web::json::value(utility::conversions::to_string_t(request.sessionId.GetBotSessionId()));
        //result[L"MarketCode"] = web::json::value(utility::conversions::to_string_t(request.marketCode));
        //result[L"QuestionNumber"] = web::json::value(request.questionNumber);
        //result[L"Message"] = web::json::value(utility::conversions::to_string_t(request.message));
        //result[L"CTN"] = web::json::value(utility::conversions::to_string_t(request.ctn));
        //result[L"AuthType"] = web::json::value(utility::conversions::to_string_t(request.authType));
        //result[L"Segment"] = web::json::value(utility::conversions::to_string_t(request.segment));
        //result[L"ChannelName"] = web::json::value(utility::conversions::to_string_t(request.channelName));
        //result[L"ChannelType"] = web::json::value(request.channelType);
        //result[L"TimeStamp"] = web::json::value(utility::conversions::to_string_t(request.timestamp));
        
        result[TEXT("request_id")] = web::json::value(utility::conversions::to_string_t(IdGenerator()->makeSId()));// Generate::GUID
        result[TEXT("appeal_id")] = web::json::value(utility::conversions::to_string_t(request.sessionId.GetSessionId()));
        result[TEXT("request_time")] = web::json::value(utility::conversions::to_string_t(request.timestamp));
        result[TEXT("message_number")] = web::json::value(request.questionNumber);
        result[TEXT("replyTo")] = web::json::value(utility::conversions::to_string_t(request.replyTo));
        
        result[TEXT("sender_id")] = 111; //unused

        web::json::value metaData;
        metaData[TEXT("regionId")] = web::json::value(utility::conversions::to_string_t(request.marketCode));
        metaData[TEXT("ctn")] = web::json::value(utility::conversions::to_string_t(request.ctn));
        metaData[TEXT("authType")] = web::json::value(utility::conversions::to_string_t(request.authType));
        metaData[TEXT("segment")] = web::json::value(utility::conversions::to_string_t(request.segment));
        metaData[TEXT("channelName")] = web::json::value(utility::conversions::to_string_t(request.channelName));
        metaData[TEXT("channelType")] = web::json::value(request.channelType);
        
        metaData[TEXT("city_id")] = web::json::value(utility::conversions::to_string_t(request.cityId));
        metaData[TEXT("district_id")] = web::json::value(utility::conversions::to_string_t(request.districtId));
        metaData[TEXT("subscriber_no")] = web::json::value(utility::conversions::to_string_t(request.subscriberNo));
        metaData[TEXT("pl")] = web::json::value(utility::conversions::to_string_t(request.pl));

        result[TEXT("meta_data")] = metaData;
        result[TEXT("full_name")] = web::json::value(utility::conversions::to_string_t(TEXT("OCPNAME"))); // ToDo: client name from OCP
        result[TEXT("channel_id")] = web::json::value(request.channelType);
        result[TEXT("channel_name")] = web::json::value(utility::conversions::to_string_t(request.channelName));
        result[TEXT("sender_type")] = web::json::value(utility::conversions::to_string_t(TEXT("client")));

        web::json::value data;
        data[TEXT("uid")] = web::json::value(utility::conversions::to_string_t(TEXT("")));
        data[TEXT("type")] = web::json::value(utility::conversions::to_string_t(TEXT("text")));
        data[TEXT("content")] = web::json::value(utility::conversions::to_string_t(request.message));

        web::json::value dataArray = web::json::value::array();
        dataArray[0] = data;

        result[TEXT("data")] = dataArray;

        return result;
    }

    template class RequestSerializer<CompositeSessionId>;
    template class RequestSerializer<PrefixSessionId>;
    template class RequestSerializer<SimpleSessionId>;
}


#pragma once
#include "base_client.h"
#include "edna_request.h"
#include "edna_http.h"
#include "edna_response.h"

namespace edna
{
	struct EdnaSetup
	{
		using SessionId = std::wstring;
		using Request = edna::Request;
		using HttpRequest = edna::HttpRequest;
		using RequestAck = edna::RequestAck;
		using Response = edna::Response;
		using ResponseParser = edna::ResponseParser;
		using RequestAckParser = edna::RequestAckParser;
		using HttpResponse = edna::HttpResponse;

		static constexpr wchar_t ComponentName[] = L"EdnaClient";
	};

	using EdnaClient = base::ClientBase<EdnaSetup>;
}
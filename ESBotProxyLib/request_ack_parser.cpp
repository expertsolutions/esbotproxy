#include "request_ack_parser.h"
#include "bot_session_id.h"

#include "utils/json_utils.h"

namespace bot
{
    template <class SessionId>
    RequestAck<SessionId> RequestAckParser<SessionId>::FromJson(const web::json::value& json, const Request<SessionId>& /*request*/, const std::optional<XString>& salt)
    {
        RequestAck<SessionId> ack;
        auto& object = json.as_object();

        //ack.sessionId = getOrThrow<std::string>(object, L"SessionId");
        //if (ack.sessionId.empty()) {
        //    throw std::exception("SessionId could not be empty");
        //}
        //ack.questionNumber = getOrThrow<int>(object, L"QuestionNumber");
        //ack.code = getOrThrow<int>(object, L"Code");
        //ack.description = getOrDefault<std::string>(object, L"Description");

        ack.sessionId = SessionId::CreateSessionId(getOrThrow<XString>(object, TEXT("appeal_id")), salt);
        //if (ack.sessionId.empty()) {
        //    throw std::runtime_error("appeal_id could not be empty");
        //}
        ack.questionNumber = getOrThrow<int>(object, TEXT("message_number"));
        ack.code = getOrThrow<int>(object, TEXT("code"));
        //ack.description = getOrDefault<std::string>(object, L"Description");

        return ack;
    }

    //RequestAck RequestAckParser::FromJson(const web::json::value& json, const Request& /*request*/)
    //{
    //    RequestAck ack;
    //    auto& object = json.as_object();

    //    ack.sessionId = getOrThrow<std::string>(object, L"SessionId");
    //    if (ack.sessionId.empty()) {
    //        throw std::exception("SessionId could not be empty");
    //    }
    //    ack.questionNumber = getOrThrow<int>(object, L"QuestionNumber");
    //    ack.code = getOrThrow<int>(object, L"Code");
    //    ack.description = getOrDefault<std::string>(object, L"Description");

    //    return ack;
    //}

    template class RequestAckParser<CompositeSessionId>;
    template class RequestAckParser<PrefixSessionId>;
    template class RequestAckParser<SimpleSessionId>;
}

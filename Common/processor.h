#pragma once
#include <condition_variable>
#include <mutex>
#include <memory>
#include <atomic>
#include <thread.h>

template<typename Queue>
class Processor
{
public:
	using QueueValueType = typename Queue::ValueType;

	// create empty processor to start consuming later
	Processor() = default;

	template<typename Consumer>
	explicit Processor(Consumer&& consumer)
	{
		StartConsuming(std::forward<Consumer>(consumer));
	}

	~Processor()
	{
		StopConsuming();
	}

	template<typename Consumer>
	void StartConsuming(Consumer&& consumer)
	{
		thread.Start([this, consumer = std::forward<Consumer>(consumer)](Threading::StopFlag& stopped)
		{
			running = true;
			while (running)
			{
				if (stopped)
					running = false;

				Consume(consumer);
				WaitForData();
			}
		});
	}

	void StopConsuming()
	{
		{
			std::unique_lock<std::mutex> lock(mtx);
			running = false;
			cv.notify_all();
		}
		thread.Stop();
	}

	template<typename Data>
	void Enqueue(Data&& data)
	{
		queue.Enqueue(std::forward<Data>(data));
		std::unique_lock<std::mutex> lock(mtx);
		needToAwake = true;
		cv.notify_one();
	}

	struct ValueOnly;

	template<typename Consumer>
	auto Consume(Consumer&& consumer, ValueOnly* = nullptr)  -> std::void_t<std::invoke_result_t<Consumer, QueueValueType>>
	{
		while (running)
		{
			auto optionalValue = queue.Dequeue();
			if (!optionalValue)
				break;

			if (!consumer(std::move(*optionalValue)))
				break;
		}
	}

	template<typename Consumer>
	auto Consume(Consumer&& consumer) -> std::void_t<std::invoke_result_t<Consumer, Queue&, const std::atomic<bool>&>>
	{
		consumer(queue, running);
	}

	auto Consume()
	{
		return queue.Dequeue();
	}

	void WaitForData()
	{
		std::unique_lock<std::mutex> lock(mtx);
		cv.wait(lock, [this] { return needToAwake || !running || !queue.IsEmpty(); });
		needToAwake = false;
	}

	void StopWaiting()
	{
		std::unique_lock<std::mutex> lock(mtx);
		needToAwake = true;
		cv.notify_all();
	}

	size_t Count() const
	{
		return queue.Count();
	}

protected:
	std::atomic<bool> running{ true };
	std::atomic<bool> needToAwake{ false };
	std::mutex mtx;
	std::condition_variable cv;
	Queue queue;
	Threading::Thread thread;
};
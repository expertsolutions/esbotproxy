#pragma once

namespace utils
{
	template<class value_type, class T>
	std::basic_string<value_type> toStr(T aa)
	{
		std::basic_ostringstream<value_type> out;

		out << aa;
		return out.str();
	}

	template<class T>
	std::wstring toStrW(T aa)
	{
		return toStr<wchar_t>(aa);
	}

	template<class T>
	std::string toStrS(T aa)
	{
		return toStr<char>(aa);
	}

	template<class T, class string_type>
	T toNum(std::basic_string<string_type> _in)
	{
		T num(0);
		std::basic_istringstream<string_type> inn(_in);
		inn >> num;
		return num;
	}

	template<class T, class string_type>
	T toNumX(std::basic_string<string_type> _in)
	{
		T num;
		if (_in.empty())
			num = 0;
		else
			std::basic_istringstream<string_type>(_in) >> std::hex >> num;
		return num;
	}

	template <class T>
	static std::basic_string<T> toLower(const std::basic_string<T>& aText)
	{
		std::basic_string<T> result;
		std::transform(aText.cbegin(), aText.cend(), std::inserter(result, result.begin()), ::tolower);

		return result;
	}

	template <class T>
	static std::basic_string<T> toUpper(const std::basic_string<T>& aText)
	{
		std::basic_string<T> result;
		std::transform(aText.cbegin(), aText.cend(), std::inserter(result, result.begin()), ::toupper);

		return result;
	}

	static std::wstring NormalizeToWindowsPath(const std::wstring& aPath)
	{
		std::wstring result(aPath);
		boost::replace_all(result, L"/", L"\\");

		return result;
	}

	static std::wstring NormalizeToWebPath(const std::wstring& aPath)
	{
		std::wstring result(aPath);
		boost::replace_all(result, L"\\", L"/");

		return result;
	}

	template <class TReadFunction>
	void GetFileStream(const std::string& aAudioPath, TReadFunction read)
	{
		int length;
		char* buffer;

		std::ifstream is;
		is.open(aAudioPath, std::ios::binary);
		// get length of file:
		is.seekg(0, std::ios::end);
		length = is.tellg();
		is.seekg(0, std::ios::beg);
		// allocate memory:
		buffer = new char[length];
		// read data as a block:
		is.read(buffer, length);

		is.close();

		read(buffer, length);
		delete[] buffer;
	}
}


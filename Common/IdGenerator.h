#pragma once
#include <random>
#include <functional>
//#include "Base.h"

#include "Patterns/singleton.h"


class CIdGenerator : public singleton <CIdGenerator>
{
	friend class singleton <CIdGenerator>;
private:
	CIdGenerator();

public:
	int64_t makeId();
    std::string makeSId();
	int64_t makeId(unsigned int clientId);
    //std::wstring makeSId(unsigned int clientId);

private:
	std::random_device _rd;
	std::mt19937_64 _gen;
};


using IdGenerator = singleton_auto_pointer<CIdGenerator>;

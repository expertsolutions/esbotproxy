#pragma once

#include <list>
#include <mutex>
#include <optional>

template <class Value>
class Queue
{
public:
	using ValueType = Value;

	template <class... Args>
	void Enqueue(Args&&... args)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		_queue.emplace_back(std::forward<Args>(args)...);
	}

	std::optional<Value> Dequeue()
	{
		std::optional<Value> result;
		
		std::lock_guard<std::mutex> lock(_mutex);
		if (_queue.empty())
		{
			return std::nullopt;
		}

		result = std::move(_queue.front());
		_queue.pop_front();
		return result;
	}

	bool IsEmpty() const
	{
		std::lock_guard<std::mutex> lock(_mutex);
		return _queue.empty();
	}

	size_t Count() const
	{
		std::lock_guard<std::mutex> lock(_mutex);
		return _queue.size();
	}

private:
	mutable std::mutex _mutex;
	std::list<Value> _queue;
};
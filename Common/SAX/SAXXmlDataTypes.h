#pragma once
#include <string>
#include <map>
#include <memory>
#include <vector>
#include <numeric>
#include <algorithm>
#include <optional>
#include <locale>

inline std::wstring stow(const std::string& in, std::locale loc = std::locale())
{
	std::wstring out(in.length(), 0);
	std::string::const_iterator i = in.begin(), ie = in.end();
	std::wstring::iterator j = out.begin();

	for (; i != ie; ++i, ++j)
		*j = std::use_facet< std::ctype< wchar_t > >(loc).widen(*i);

	return out;
}

inline std::string wtos(const std::wstring& in, std::locale loc = std::locale())
{
	std::string out(in.length(), 0);
	std::wstring::const_iterator i = in.begin(), ie = in.end();
	std::string::iterator j = out.begin();

	for (; i != ie; ++i, ++j)
		*j = std::use_facet< std::ctype< char > >(loc).narrow(*i);

	return out;
}

class ISATXMLElement
{
public:
	ISATXMLElement() = default;

	using AttrContainer = std::map<std::wstring, std::wstring>;

	ISATXMLElement(const std::wstring& aName,
		const std::wstring& aText,
		const AttrContainer& aAttribs,
		int aLine,
		const std::wstring& aXML,
		const std::wstring& aParams,
		const std::wstring& aUri)
		: m_sName(aName)
		, m_sText(aText)
		, m_Attribs(aAttribs)
		, m_iLine(aLine)
		, m_sXML(aXML)
		, m_sParams(aParams)
		, m_sUri(aUri)
	{}

	ISATXMLElement(const std::wstring& aName,
		const std::wstring& aText,
		int aLine,
		const std::wstring& aUri)
		: m_sName(aName)
		, m_sText(aText)
		, m_iLine(aLine)
		, m_sUri(aUri)
	{}

protected:
	std::wstring	m_sName;
	std::wstring	m_sText;
	AttrContainer	m_Attribs;
	int             m_iLine;
	std::wstring	m_sXML;
	std::wstring    m_sParams;
	std::wstring    m_sUri;


public:
	// Set 
	void SetName(const std::wstring& _name){m_sName = _name;}
	void SetText(const std::wstring& _text){m_sText = _text;}
	void SetAttr(const std::wstring&_attrname, const std::wstring& _attrvalue){m_Attribs[_attrname] = _attrvalue;}
	void SetLine(const int& _line){m_iLine = _line;}
	void SetXML (const std::wstring& _xml ){m_sXML  = _xml;}
	void SetParams(const std::wstring& params){ m_sParams = params; }
	void SetUri(const std::wstring& _uri) { m_sUri = _uri; }

	// Get
	std::wstring  GetName() const {return m_sName;}
	std::wstring  GetText() const {return m_sText;}
	AttrContainer GetAttr() const {return m_Attribs;} 
	int           GetLine() const {return m_iLine;}
	std::wstring  GetXML () const {return m_sXML;}
	std::wstring  GetParams() const { return m_sParams; }
	std::wstring  GetUri () const {return m_sUri;}

	std::wstring GetAttrVal(const std::wstring& sName) const
	{
		AttrContainer::const_iterator it = m_Attribs.find(sName);
		if (it == m_Attribs.end())
		{
			throw std::runtime_error(std::string("Tag '") + wtos(m_sName) + "' does not have attribute " + wtos(sName));
		}
		return it->second;
	}
	bool GetAttrVal(const std::wstring& sName, std::wstring& sValue) const
	{
		AttrContainer::const_iterator it = m_Attribs.find(sName);
		if (it == m_Attribs.end())
		{
			return false;
		}
		sValue = it->second;
		return true;
	}

	std::optional<std::wstring> GetAttrValSafe(const std::wstring& sName) const
	{
		std::optional<std::wstring> result = std::nullopt;

		AttrContainer::const_iterator cit = m_Attribs.find(sName);
		if (cit != m_Attribs.cend())
		{
			result = cit->second;
		}
		return result;
	}


	std::wstring ToString()const
	{
		std::wstring result;
		result += L"{Name: " + m_sName;
		result += L", Text: " + m_sText;
		result += L", Line: " + std::to_wstring(m_iLine);
		result += L", Uri: " + m_sUri;
		result += L"}";
		return result;
	}
	void AppendText(const wchar_t* aData, size_t aSize)
	{
		if (!m_sText.empty())
		{
			m_sText += '\n';
		}
		m_sText.append(aData, aSize);
	}

	void AppendText(const std::wstring& _text)
	{
		m_sText.append(_text);
	}

	void AppendXML(const wchar_t* aData, size_t aSize)
	{
		if (!m_sXML.empty())
		{
			m_sXML += '\n';
		}
		m_sXML.append(aData, aSize);
	}

	void AppendXML(const std::wstring& _text)
	{
		m_sXML.append(_text);
	}
};
#pragma once

#include "SAXXmlElementHandler.h"

class CSAXErrorHandler : public MSXML2::ISAXErrorHandler  
{
public:
	CSAXErrorHandler();
	virtual ~CSAXErrorHandler();

	virtual long __stdcall          QueryInterface(const struct _GUID &,void ** );
	ULONG STDMETHODCALLTYPE AddRef( void)
	{
		return ::InterlockedIncrement(&m_lRefCount);
	}

	ULONG STDMETHODCALLTYPE Release(void)
	{
		LONG lResult = ::InterlockedDecrement(&m_lRefCount);
		if(m_lRefCount == 0)
			delete this;
		return lResult;
	}

	virtual HRESULT STDMETHODCALLTYPE error( 
		/* [in] */ MSXML2::ISAXLocator __RPC_FAR *pLocator,
		/* [in] */ unsigned short * pwchErrorMessage,
		/* [in] */ HRESULT errCode);

	virtual HRESULT STDMETHODCALLTYPE fatalError( 
		/* [in] */ MSXML2::ISAXLocator __RPC_FAR *pLocator,
		/* [in] */ unsigned short * pwchErrorMessage,
		/* [in] */ HRESULT errCode);

	virtual HRESULT STDMETHODCALLTYPE ignorableWarning( 
		/* [in] */ MSXML2::ISAXLocator __RPC_FAR *pLocator,
		/* [in] */ unsigned short * pwchErrorMessage,
		/* [in] */ HRESULT errCode);

	// Attach/detach XML events handler.
	void AttachElementHandler(ISAXXmlElementHandler* pElementHandler);
	void DetachElementHandler();

private:
	void ReportError(
		/* [in] */ MSXML2::ISAXLocator*    pLocator,
		/* [in] */ unsigned short* pwchErrorMessage,
		/* [in] */ HRESULT         errCode);

	ISAXXmlElementHandler*  m_attachElementHandler;
	LONG m_lRefCount;
};
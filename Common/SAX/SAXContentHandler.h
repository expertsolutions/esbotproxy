
#pragma once

#include "SAXXmlElementHandler.h"

class CSAXContentHandler : public MSXML2::ISAXContentHandler
{
public:
	CSAXContentHandler();
	virtual ~CSAXContentHandler();

	virtual long __stdcall          QueryInterface(const struct _GUID &, void **);
	
	ULONG STDMETHODCALLTYPE AddRef( void)
	{
		return ::InterlockedIncrement(&m_lRefCount);
	}

	ULONG STDMETHODCALLTYPE Release(void)
	{
		LONG lResult = ::InterlockedDecrement(&m_lRefCount);
		if(m_lRefCount == 0)
			delete this;
		return lResult;
	}

	virtual HRESULT STDMETHODCALLTYPE putDocumentLocator( 
		/* [in] */ MSXML2::ISAXLocator __RPC_FAR *pLocator);

	virtual HRESULT STDMETHODCALLTYPE startDocument(void);

	virtual HRESULT STDMETHODCALLTYPE endDocument(void);

	virtual HRESULT __stdcall startPrefixMapping( 
		/* [in] */ unsigned short * pwchPrefix,
		/* [in] */ int cchPrefix,
		/* [in] */ unsigned short * pwchUri,
		/* [in] */ int cchUri);

	virtual HRESULT STDMETHODCALLTYPE endPrefixMapping( 
		/* [in] */ unsigned short __RPC_FAR *pwchPrefix,
		/* [in] */ int cchPrefix);

	virtual HRESULT STDMETHODCALLTYPE startElement( 
		/* [in] */ unsigned short __RPC_FAR *pwchNamespaceUri,
		/* [in] */ int cchNamespaceUri,
		/* [in] */ unsigned short __RPC_FAR *pwchLocalName,
		/* [in] */ int cchLocalName,
		/* [in] */ unsigned short __RPC_FAR *pwchRawName,
		/* [in] */ int cchRawName,
		/* [in] */ MSXML2::ISAXAttributes __RPC_FAR *pAttributes);

	virtual HRESULT STDMETHODCALLTYPE endElement( 
		/* [in] */ unsigned short __RPC_FAR *pwchNamespaceUri,
		/* [in] */ int cchNamespaceUri,
		/* [in] */ unsigned short __RPC_FAR *pwchLocalName,
		/* [in] */ int cchLocalName,
		/* [in] */ unsigned short __RPC_FAR *pwchRawName,
		/* [in] */ int cchRawName);

	virtual HRESULT STDMETHODCALLTYPE characters( 
		/* [in] */ unsigned short __RPC_FAR *pwchChars,
		/* [in] */ int cchChars);

	virtual HRESULT STDMETHODCALLTYPE ignorableWhitespace( 
		/* [in] */ unsigned short __RPC_FAR *pwchChars,
		/* [in] */ int cchChars);

	virtual HRESULT STDMETHODCALLTYPE processingInstruction( 
		/* [in] */ unsigned short __RPC_FAR *pwchTarget,
		/* [in] */ int cchTarget,
		/* [in] */ unsigned short __RPC_FAR *pwchData,
		/* [in] */ int cchData);

	virtual HRESULT STDMETHODCALLTYPE skippedEntity( 
		/* [in] */ unsigned short __RPC_FAR *pwchName,
		/* [in] */ int cchName);    

	// Attach/detach XML events handler.
	void AttachElementHandler(ISAXXmlElementHandler*  pElementHandler);
	void DetachElementHandler();

private:
	int  m_depth;
	LONG m_lRefCount;

	ISAXXmlElementHandler*  m_attachElementHandler;

	MSXML2::ISAXLocator*    m_pLocator;
};

/******************************* eof *************************************/
#pragma once
#include <thread>
#include <atomic>

namespace Threading
{
	using StopFlag = std::atomic<bool>;
	class Thread final
    {
    public:
		Thread(Thread && ) = delete;
		Thread & operator = (Thread && ) = delete;

		Thread(Thread const &) = delete;
		Thread & operator = (Thread const &) = delete;

		Thread() = default;

		template<typename Lambda>
		Thread(Lambda && lam)
		{
			Start(std::forward<Lambda>(lam));
		}

		~Thread()
		{
			Stop();
		}

		template<typename Lambda>
		void Start(Lambda && lam)
		{
			if (thr == nullptr)
			{
				stopped = false;
				thr = std::make_unique<std::thread>(std::forward<Lambda>(lam), std::ref(stopped));			}
		}

		void Stop(const bool doJoin = true)
		{
			if (thr)
			{
				stopped = true;
				if (doJoin)
				{
					if (thr->joinable())
						thr->join();
					thr.reset();
				}
			}
		}

		bool IsRunning() const
		{
			return !stopped;
		}

		void Detach()
		{
			if (thr)
			{
				if (thr->joinable())
					thr->detach();
				thr.reset();
			}
		}

	private:
		StopFlag stopped{ true };
		std::unique_ptr<std::thread> thr;
	};
}
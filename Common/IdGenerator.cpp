#include <string>
#include "IdGenerator.h"

CIdGenerator::CIdGenerator() : _gen(_rd())
{

}

int64_t CIdGenerator::makeId()
{
	std::uniform_int_distribution<int64_t> dis;

	return dis(_gen);
}

std::string CIdGenerator::makeSId()
{
    return std::to_string(makeId());
}

int64_t CIdGenerator::makeId(unsigned int clientId)
{ 
	int64_t mccid = clientId;
	mccid = (mccid << 32) | (makeId() >> 32);

	return mccid;
}

//std::wstring CIdGenerator::makeSId(unsigned int clientId)
//{
//	return std::to_wstring(makeId(clientId));
//}

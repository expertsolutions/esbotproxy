#include "DataReceiverImpl.h"
#include "IdGenerator.h"
#include "utils.h"

void CDataReceiverImpl::S2VC_ClassifyHandler(const CMessage& aMsg, const CLIENT_ADDRESS& aFrom, const CLIENT_ADDRESS& aTo)
{
    aMsg.CheckParam(L"Text", core::message::CheckedType::String, core::message::ParamType::Mandatory);
    aMsg.CheckParam(L"ScriptID", core::message::CheckedType::Int64, core::message::ParamType::Mandatory);

    voice::Request request;
    //const auto scriptAddress = aFrom.AsMessageParamterInt();
    const auto text = aMsg.SafeReadParam(L"Text", core::message::CheckedType::String, L"").AsWideStr();
    const auto scriptId = aMsg.SafeReadParam(L"ScriptID", core::message::CheckedType::Int64, 0).AsInt64();
    const auto vnHost = aMsg.SafeReadParam(L"VnHost", core::message::CheckedType::String, L"Default").AsWideStr();

    //request.appealId = utils::toStrW(scriptAddress);

    request.appealId = stow(CIdGenerator::GetInstance()->makeSId());

    request.requestId = utils::toStrW(scriptId);//CIdGenerator::GetInstance()->makeSId();
    request.requestTime = core::support::time_to_iso_8601_string<std::wstring>(core::support::local_time());
    request.messageNumber = CIdGenerator::GetInstance()->makeId() % 100 + 1;
    request.senderId = this->GetClientId();
    request.senderType = L"client";

    request.metadata.region = L"01";
    request.metadata.phone = L"89214789012";
    request.metadata.login = L"test_login";

    request.crossLinks.push_back({L"phone", L"79999998877"});
    request.fullName = L"Ivanov";

    request.channelId = 10; // ToDo: ?
    request.channelName = L"IVR"; // ToDo: ??

    request.skillId = 49;
    request.skillName = L"Segent_name";

    request.data.push_back({ L"", L"text", text });
    //request.replyTo = _localServerUrl;

    SendMessageToCrt(vnHost, std::move(request));
}

/******************************* eof *************************************/
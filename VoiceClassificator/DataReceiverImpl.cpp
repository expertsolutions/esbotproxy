#include <boost/asio/spawn.hpp>
#include <boost/asio/steady_timer.hpp>
#include "DataReceiverImpl.h"
#include "Configuration\SystemConfiguration.h"
#include "Log\SystemLog.h"
#include "WinAPI/ProcessHelper.h"
#include "ThreadHelper.h"
#include "utils.h"
#include "IdGenerator.h"

const int EXPIREDTIME = 30;

void CDataReceiverImpl::SendMessageToCrt(const std::wstring& vnHost, voice::Request&& message)
{
	const auto cit = _clients.find(vnHost);
	if (cit == _clients.cend())
	{
		throw std::runtime_error("Cannot find vnHost: " + wtos(vnHost));
	}

	message.replyTo = cit->second.listener;

	cit->second.client->sendRequestAsync(std::move(message), [this](voice::RequestAck&& ack)
		{
			this->m_log->LogString(LEVEL_INFO, L"Recieve answer: %s", ack.ToString());

			this->SendS2VC_ClassifyAck(std::move(ack));
		});
}

CDataReceiverImpl::CDataReceiverImpl(boost::asio::io_service& io) 
	: m_log(NULL)
	, r_io(io)
{
}

CDataReceiverImpl::~CDataReceiverImpl()
{
}

void CDataReceiverImpl::ConnectHandlers()
{
}

void CDataReceiverImpl::set_Subscribes()
{
	subscribe(L"S2VC_Classify", &CDataReceiverImpl::S2VC_ClassifyHandler);
}

int CDataReceiverImpl::init_impl(CSystemLog* pLog)
{
	if (!pLog)
		return -1111;

	CoInitialize(NULL);

	m_log = pLog;

	m_log->LogString(LEVEL_FINEST, L"CDataReceiverImpl initializing...");

	ConnectHandlers();

	boost::asio::spawn(r_io, [&](boost::asio::yield_context yield)
	{
		try
		{
			boost::asio::steady_timer timer(r_io);

			timer.expires_from_now(std::chrono::milliseconds(100));
			timer.async_wait(yield);

			start();
		}
		catch (std::exception& error)
		{
			m_log->LogString(LEVEL_WARNING, L"[start] exception: %s", stow(error.what()));
		}
		catch (...)
		{
			m_log->LogString(LEVEL_WARNING, L"[start] unknown exception");
		}
	});

	return 0;
}

static void CheckEdnaAddress(const std::wstring& serverUri)
{
	if (serverUri.find(L"https") != 0)
	{
		throw std::runtime_error("'https' address mandatory");
	}
}

int CDataReceiverImpl::start()
{
	SystemConfig settings;

	std::set<std::wstring> adapters;
	std::set<std::wstring> listeners;

	auto vnHosts = settings->get_config_node(L"VnHosts");
	for (const auto& child : vnHosts)
	{
		auto result = _clients.try_emplace(
			child.first,
			VnHost{
				settings->safe_get_config_attr<std::wstring>(child.second, L"ChatAdapterUrl", L""),
				settings->safe_get_config_attr<std::wstring>(child.second, L"VoiceListeningUrl", L""),
				nullptr
			}
		);

		if (!result.second)
		{
			throw std::runtime_error("Dublicated VnHost name: " + wtos(child.first));
		}

		if (!adapters.emplace(result.first->second.adapter).second)
		{
			throw std::runtime_error("Dublicated Chat Adapter URL: " + wtos(result.first->second.adapter));
		}
		if (!listeners.emplace(result.first->second.listener).second)
		{
			throw std::runtime_error("Dublicated Voice Listening URL: " + wtos(result.first->second.listener));
		}
	}

	for (auto& [name, vnHost] : _clients)
	{
		m_log->LogString(LEVEL_INFO, L"Connecting to adapter: \"%s\", listener = \"%s\"", vnHost.adapter, vnHost.listener);

		vnHost.client = std::make_unique<voice::VoiceClient>(
			wtos(vnHost.adapter),
			wtos(vnHost.listener),
			std::wstring{});

		vnHost.client->setResponseHandler([this, name](voice::Response&& response)
			{
				this->SendS2VC_ClassifyCompleted(std::move(response));
				m_log->LogString(LEVEL_FINEST, L"[%s] CRT response", name);
			});
	}

	//const std::wstring externalServerUrl = settings->get_config_value<std::wstring>(L"ChatAdapterUrl");
	//_localServerUrl = settings->get_config_value<std::wstring>(L"VoiceListeningUrl");

	//_voiceClient = std::make_unique<voice::VoiceClient>(
	//	wtos(externalServerUrl),
	//	wtos(_localServerUrl),
	//	std::wstring{});

	m_log->LogString(LEVEL_FINEST, L"VoiceClassificator started");

	//_voiceClient->setResponseHandler([this](voice::Response&& response)
	//	{
	//		this->SendS2VC_ClassifyCompleted(std::move(response));
	//		m_log->LogString(LEVEL_FINEST, L"CRT response");
	//	});

	return 0;
}

void CDataReceiverImpl::stop()
{
	r_io.stop();

	//_voiceClient.reset();
	_clients.clear();
}

void CDataReceiverImpl::on_routerConnect()
{
	if (!m_log)
	{
		return;
	}

	m_log->LogStringModule(LEVEL_INFO, L"Receiver Core", L"Router has connected");
}

void CDataReceiverImpl::on_routerDeliverError(const CMessage& _msg, const CLIENT_ADDRESS &_from, const CLIENT_ADDRESS &_to)
{
	if (!m_log)
	{
		return;
	}

	m_log->LogStringModule(LEVEL_FINE, L"Router deliver error", _msg.Dump().c_str());
}

void CDataReceiverImpl::on_appStatusChange(DWORD dwClient, core::ClientStatus nStatus)
{
	if (nStatus == /*static_cast<int>*/(core::ClientStatus::CS_DISCONNECTED))
	{
		try
		{

		}
		catch (...)
		{

		}
	}

}

void CDataReceiverImpl::SendS2VC_ClassifyAck(voice::RequestAck&& ack)
{
	//const int64_t clientAddress = utils::toNum<int64_t>(ack.appealId);
	const int64_t scriptId = utils::toNum<int64_t>(ack.requestId);

	CMessage msg(L"S2VC_ClassifyAck");
	msg[L"ScriptID"] = scriptId;
	msg[L"Code"] = ack.code;
	msg[L"Description"] = ack.description;
	
	CLIENT_ADDRESS addrTo(scriptId);
	this->SendTo(msg, addrTo);
}

void CDataReceiverImpl::SendS2VC_ClassifyCompleted(voice::Response&& response)
{
	//const int64_t clientAddress = utils::toNum<int64_t>(response.appealId);
	const int64_t scriptId = utils::toNum<int64_t>(response.replyToId);

	CMessage msg(L"S2VC_ClassifyCompleted");
	msg[L"ScriptID"] = scriptId;
	msg[L"Result"] = static_cast<int>(response.result);
	msg[L"ClassId"] = response.MakeMessage();

	CLIENT_ADDRESS addrTo(scriptId);
	this->SendTo(msg, addrTo);
}

/******************************* eof *************************************/
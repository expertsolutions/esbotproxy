#include "application.h"

#define BOOST_APPLICATION_FEATURE_NS_SELECT_BOOST

#include <iostream>
#include <fstream>
#include <future>
#include <boost/program_options.hpp>
#include <boost/application.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/spawn.hpp>

#include "boost/archive/iterators/base64_from_binary.hpp"
#include "boost/archive/iterators/binary_from_base64.hpp"
#include "boost/archive/iterators/transform_width.hpp"
#include <boost/archive/iterators/insert_linebreaks.hpp>
#include <boost/archive/iterators/ostream_iterator.hpp>
#include <sstream>
#include <string>


// provide setup example for windows service
#if defined(BOOST_WINDOWS_API)
#   include <3rdparty/Boost.Application/example/setup/windows/setup/service_setup.hpp>
#endif

#include "Log/SystemLog.h"
#include "Patterns/string_functions.h"

#include "DataReceiverImpl.h"
#include "ThreadHelper.h"

#ifdef  WIN32
#include "WinAPI/mdump.h"
#endif

namespace po = boost::program_options;


class myapp
{

public:

    myapp(boost::application::context& context)
        : context_(context)
    {
    }

    void worker()
    {
        boost::asio::io_service io;

        singleton_auto_pointer<CSystemLog> log;
        log->LogString(LEVEL_INFO, L"Initializing...");

#ifdef  WIN32
        singleton_auto_pointer<MiniDumper> g_MiniDump;
        CoInitialize(nullptr);
#endif //  WIN32


        m_receiver = std::make_unique<CDataReceiverImpl>(io);
        if (!m_receiver->Init(log->GetInstance()))
        {
            log->LogString(LEVEL_INFO, L"Starting run");
            CThreadWrapper t(std::thread(boost::bind(&boost::asio::io_service::run, &io)));

            boost::application::csbl::shared_ptr<boost::application::status> st =
                context_.find<boost::application::status>();

            int count = 0;
            while (st->state() != boost::application::status::stopped)
            {
                boost::this_thread::sleep(boost::posix_time::seconds(1));
            }

            m_receiver->Stop();
        }

#ifdef  WIN32
        CoUninitialize();
#endif
    }

    // param
    int operator()()
    {
        worker();
        return 0;
    }

    // windows/posix

    bool stop()
    {
        singleton_auto_pointer<CSystemLog> log;
        log->LogString(LEVEL_INFO, L"Stoping the application...");

        if (m_receiver)
        {
            m_receiver->Stop();
        }

        return true; // return true to stop, false to ignore
    }

    // windows specific (ignored on posix)

    bool pause()
    {
        singleton_auto_pointer<CSystemLog> log;
        log->LogString(LEVEL_INFO, L"Pause the application...");
        return true; // return true to pause, false to ignore
    }

    bool resume()
    {
        singleton_auto_pointer<CSystemLog> log;
        log->LogString(LEVEL_INFO, L"Resume the application...");

        return true; // return true to resume, false to ignore
    }

private:

    //std::ofstream my_log_file_;

    boost::application::context& context_;

    std::unique_ptr<CDataReceiverImpl> m_receiver;

};


int CreateMain(int argc, char * c_argv[])
{
    //////////////////////////////////////////////////

    bool bRunAsConsole = false;
    boost::system::error_code ec;

#if defined(BOOST_WINDOWS_API)
#if !defined(__MINGW32__)
    // get our executable path name
    boost::filesystem::path executable_path_name = boost::application::path().location();

    po::variables_map vm;
    po::options_description install("service options");
    install.add_options()
        ("help", "produce a help message")
        ("console", "run as console")
        (",i", "install service")
        (",u", "unistall service")
        ("name", po::value<std::string>()->default_value(executable_path_name.stem().string()), "service name")
        ("display", po::value<std::string>()->default_value(""), "service display name (optional, installation only)")
        ("description", po::value<std::string>()->default_value(""), "service description (optional, installation only)")
        ;


    po::store(po::parse_command_line(argc, c_argv, install), vm);

    if (vm.count("help"))
    {
        std::cout << "[I] Setup changed the current configuration." << std::endl;
        std::cout << install << std::endl;
        return 0;
    }

    if (vm.count("console"))
    {
        bRunAsConsole = true;
    }

    if (vm.count("-i"))
    {
        std::cout << "[I] Setup changed the current configuration." << std::endl;

        try
        {
            boost::application::example::install_windows_service(
                boost::application::setup_arg(stow(vm["name"].as<std::string>())),
                boost::application::setup_arg(stow(vm["display"].as<std::string>())),
                boost::application::setup_arg(stow(vm["description"].as<std::string>())),
                boost::application::setup_arg(executable_path_name),
                boost::application::setup_arg(L""),
                boost::application::setup_arg(L""),
                boost::application::setup_arg(L"auto"),
                boost::application::setup_arg(L""),
                boost::application::setup_arg(L"")).install(ec);

            std::cout << ec.message() << std::endl;
        }
        catch (boost::system::system_error& ec)
        {
            std::cout << ec.what() << std::endl;
        }
        return 0;
    }

    if (vm.count("-u"))
    {
        std::cout << "[I] Setup changed the current configuration." << std::endl;

        boost::application::example::uninstall_windows_service(
            boost::application::setup_arg(vm["name"].as<std::string>()),
            boost::application::setup_arg(executable_path_name)).uninstall(ec);

        std::cout << ec.message() << std::endl;
        return 0;
    }
#endif
#endif


    boost::application::context app_context;
    myapp app(app_context);

    //insert path aspect
    boost::application::path path;
    path.location();
    app_context.insert<boost::application::path>(
        boost::application::csbl::make_shared<boost::application::path>(path));


    // add termination handler
    boost::application::handler<>::callback termination_callback
        = boost::bind(&myapp::stop, &app);

    app_context.insert<boost::application::termination_handler>(
        boost::application::csbl::make_shared<boost::application::termination_handler_default_behaviour>(termination_callback));

#if defined(BOOST_WINDOWS_API)

    // windows only : add pause handler
    boost::application::handler<>::callback pause_callback
        = boost::bind(&myapp::pause, &app);

    app_context.insert<boost::application::pause_handler>(
        boost::application::csbl::make_shared<boost::application::pause_handler_default_behaviour>(pause_callback));

    // windows only : add resume handler

    boost::application::handler<>::callback resume_callback
        = boost::bind(&myapp::resume, &app);

    app_context.insert<boost::application::resume_handler>(
        boost::application::csbl::make_shared<boost::application::resume_handler_default_behaviour>(resume_callback));

#endif

    int result = 0;
    if (bRunAsConsole)
        result = boost::application::launch< boost::application::common>(app, app_context, ec);
    else
    	result = boost::application::launch< boost::application::server>(app, app_context, ec);


    if (ec)
    {
        singleton_auto_pointer<CSystemLog> log;
        log->LogString(LEVEL_INFO, L"Process terminated with error: <%i> - %s", ec.value(), ec.message().c_str());
    }

    return result;
}

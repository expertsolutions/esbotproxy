#pragma once

#include <variant>

#include "Router\router_compatibility.h"
#include "Base.h"
#include "DataReceiver.h"
//#include "queue.h"
//#include "processor.h"
#include "ESBotProxyLib\voice_client.h"
//#include "ESBotProxyLib\edna_client.h"

using VoiceClientPtr = std::unique_ptr<voice::VoiceClient>;

struct VnHost
{
	std::wstring adapter;
	std::wstring listener;
	VoiceClientPtr client;
};


class CDataReceiverImpl : public CDataReceiver < CDataReceiverImpl >
{
	friend class CDataReceiver < CDataReceiverImpl >;


	//auto ProcessorConsumer()
	//{
	//	return [this](const Response& record)
	//	{
	//		return ProcessResponse(record);
	//	};
	//}

public:
	CDataReceiverImpl(boost::asio::io_service& io);
	~CDataReceiverImpl();

	void S2VC_ClassifyHandler(const CMessage& aMsg, const CLIENT_ADDRESS& aFrom, const CLIENT_ADDRESS& aTo);

private:
	int init_impl(CSystemLog* pLog);
	int start();
	//bool ProcessResponse(const Response& record);
	//bool ProcessResponse(const BotResponse& response);
	//bool ProcessResponse(const EdnaResponse& response);

	void SendMessageToCrt(const std::wstring& /*vnHost*/, voice::Request&& request);

private:
	//router api
	void on_routerConnect();
	void on_routerDeliverError(const CMessage&, const CLIENT_ADDRESS&, const CLIENT_ADDRESS&);
	void on_appStatusChange(DWORD dwClient, /*int*/core::ClientStatus	nStatus);
	void set_Subscribes();
	std::wstring get_ModuleName() const noexcept { return L"VoiceClassificator"; }
	void stop();

	void SendS2VC_ClassifyAck(voice::RequestAck&& ack);
	void SendS2VC_ClassifyCompleted(voice::Response&& response);

private:
	void ConnectHandlers();

private:
	CSystemLog * m_log = nullptr;
	boost::asio::io_service& r_io;

public:
	//std::map<std::wstring, VoiceClientPtr> _clients;
	//std::unique_ptr<voice::VoiceClient> _voiceClient;
	//std::wstring _localServerUrl;
	std::map<std::wstring, VnHost> _clients;
};

/******************************* eof *************************************/


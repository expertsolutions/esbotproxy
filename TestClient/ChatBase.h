#pragma once

#include "Base.h"

using ChatId = std::wstring;
using MessageId = std::wstring;

using TMessageStatus = TMessageStatus_t<ChatId>;

using ChatTextMessageHandler = Base::ChatTextMessageHandler<ChatId>;
using ChatTextMessageHandler_s = std::function<std::wstring(const CMessage & msg, const TMessageStatus&)>;

using ConnectionStatusMesageHandler = std::function<void(SessionScriptId sessionScriptId, CONNECTION_STATUS error, const std::wstring & message)>;

#pragma once
#include <string>

namespace TgBot {

	class WebhookInfo
	{
	public:

		typedef std::shared_ptr<WebhookInfo> Ptr;

		std::string url;
		bool hasCustomSertificate;
		int32_t pendingUpdateCount;
		int32_t lastErrorDate;
		std::string lastErrorMessage;
	};

}
#pragma once

namespace utils
{
	template<class value_type, class T>
	std::basic_string<value_type> toStr(T aa)
	{
		std::basic_ostringstream<value_type> out;

		out << aa;
		return out.str();
	}

	template<class T>
	std::wstring toStrW(T aa)
	{
		return toStr<wchar_t>(aa);
	}

	template<class T>
	std::string toStrS(T aa)
	{
		return toStr<char>(aa);
	}

	template<class T, class string_type>
	T toNum(std::basic_string<string_type> _in)
	{
		T num(0);
		std::basic_istringstream<string_type> inn(_in);
		inn >> num;
		return num;
	}

	template<class T, class string_type>
	T toNumX(std::basic_string<string_type> _in)
	{
		T num;
		if (_in.empty())
			num = 0;
		else
			std::basic_istringstream<string_type>(_in) >> std::hex >> num;
		return num;
	}

	template <class T>
	static std::basic_string<T> toLower(const std::basic_string<T>& aText)
	{
		std::basic_string<T> result;
		std::transform(aText.cbegin(), aText.cend(), std::inserter(result, result.begin()), ::tolower);

		return result;
	}

	template <class T>
	static std::basic_string<T> toUpper(const std::basic_string<T>& aText)
	{
		std::basic_string<T> result;
		std::transform(aText.cbegin(), aText.cend(), std::inserter(result, result.begin()), ::toupper);

		return result;
	}

}


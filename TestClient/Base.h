/************************************************************************/
/* Name     : MCHAT\Base.h                                              */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 05 Jan 2017                                               */
/************************************************************************/

#pragma once

#include "Router/router_compatibility.h"

//
using MessageId = std::wstring;
using MCCID = unsigned __int64;
using SessionScriptId = unsigned __int64;
using SessionId = unsigned __int64;
using MCPId = unsigned __int64;
using RequestId = std::wstring;

template <class TChatId>
struct TMessageStatus_t
{
	TChatId chatId;
	std::wstring methodName;
	std::wstring timeStamp;
};

enum class ENUM_MESSAGE_CODE
{
	MD_SESSION_CLOSED_BY_OPERATOR = 1,
	MD_SESSION_CLOSED_BY_ABONENT = 2,
	MD_SESSION_CLOSED_BY_TIMEOUT = 3,
	MD_OPERATOR_CHATSESSION = 10,
	MD_ABONENT_CHATSESSION = 11,
	MD_TYPENING = 12,
	MD_AUTOREPLY = 13
};

enum class MESSAGE_STATUS
{
	NEW = 1,
	DELEYIED,
	SENT,
	TRANSFER,
	RECIEVED
};

enum class CONNECTION_STATUS
{
	SUCCESS_ANS = 1,
	ERROR_ANS,
	NO_ANS
};

namespace Base
{
	template <class TChatId>
	using ChatTextMessageHandler = std::function<std::wstring(const CMessage& msg, TMessageStatus_t<TChatId>& status)>;
	using ChatTextMessageHandler_s = std::function<std::wstring(const CMessage& msg, const std::string& status)>;
	using ServerRequestMessageHandler = std::function<std::string(const CMessage& msg)>;
}

namespace MessageContentType
{
	const wchar_t TEXT[] = L"TEXT";
	const wchar_t FILE[] = L"FILE";
}

/******************************* eof *************************************/
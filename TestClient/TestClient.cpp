// TestClient.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <boost/thread.hpp>
#include "boost/asio/ssl.hpp"
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/spawn.hpp>

#include "Log/SystemLog.h"
#include "Patterns/string_functions.h"
#include "Patterns\time_functions.h"


#include "tgbot/net/HttpParser.h"

#include "Codec.h"
#include "MessageParser.h"
#include "Base.h"

#include "Socket/SslSpawnClient.h"

template <class Handle>
void MakeClient(const std::string& uri, const std::string& port, const std::string& sertificate, const CMessage& msg, CSystemLog* log, const Handle& handle)
{
	using Proxy = ssl_spawn_client::ClientProxy;

	Proxy::TConnectionParams params;
	params._uri = uri;
	params._port = port;
	params._partyId = "";
	params._clientId = 123;
	params._msg = msg;
	params._xApiEnvironment = "";
	params._statusHandler = handle;
	params._sertificate = sertificate;

	boost::asio::io_service io;

	Proxy::MakeClient(std::move(params), &io, log);

	io.run();
}

CMessage CreateSimpleTextMessage()
{
	CMessage msg(L"init");
	msg[L"field"] = L"value";
	return msg;
}

int main()
{
	singleton_auto_pointer<CSystemLog> log;
	log->LogString(LEVEL_INFO, L"Starting run");


	std::wstring session_id = L"78503A1D3135430DB28B221D70E33354";
	std::string uri = "192.168.0.5";
	std::string port = "41103";
	std::string sertificate = "D:\\Programming\\Projects\\esbotproxy\\x64\\Debug\\Cert\\ca-cert.pem";

	//CREATE SESSION
	if (true)
	{
		const auto msg = CreateSimpleTextMessage();

		MakeClient(uri, port, sertificate, msg, log->GetInstance(), [](SessionScriptId sessionScriptId, CONNECTION_STATUS error, const std::wstring& message)
			//MakeMySslClient(uri, port, msg, log->GetInstance(), [](SessionScriptId sessionScriptId, CONNECTION_STATUS error, const std::wstring& message)
			{
				std::cout << wtos(message) << std::endl;
			});
	}

	::Sleep(10000);
}
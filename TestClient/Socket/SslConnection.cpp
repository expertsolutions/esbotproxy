/************************************************************************/
/* Name     : MCHAT\SslConnection.cpp                                   */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCHAT                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 26 Oct 2020                                               */
/************************************************************************/
#include "stdafx.h"
#include "SslConnection.h"

bool ChatSslConnection::HandShake(boost::asio::yield_context yield)
{
	return this->Process([&]()
	{
		boost::system::error_code errorCode;

		LogStringModule(LEVEL_INFO, L"6.1");
		this->_socket.set_verify_mode(boost::asio::ssl::verify_peer);
		//this->_socket.set_verify_mode(boost::asio::ssl::verify_none);

		LogStringModule(LEVEL_INFO, L"6.2");
		this->_socket.set_verify_callback([&](bool preverified,
			boost::asio::ssl::verify_context& ctx)
		{
			// The verify callback can be used to check whether the certificate that is
			// being presented is valid for the peer. For example, RFC 2818 describes
			// the steps involved in doing this for HTTPS. Consult the OpenSSL
			// documentation for more details. Note that the callback is called once
			// for each certificate in the certificate chain, starting from the root
			// certificate authority.

			// In this example we will simply print the certificate's subject name.
			char subject_name[256];
			X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
			X509_NAME_oneline(X509_get_subject_name(cert), subject_name, 256);

			std::cout << subject_name << std::endl;

			this->LogStringModule(LEVEL_INFO, L"Sertificat verification answer: \"%s\"\nVerified = \"%s\"", stow(subject_name).c_str(), preverified ? L"true" : L"false");

			return preverified;
		});
		this->LogStringModule(LEVEL_FINEST, L"6.3");
		this->_socket.async_handshake(boost::asio::ssl::stream_base::server, yield[errorCode]);

		this->LogStringModule(LEVEL_FINEST, L"6.4");

		if (errorCode)
			throw errorCode;
	});
}

//void ChatSslConnection::startListening()
//{
//	boost::asio::spawn(_strand,
//		[self = shared_from_this()](boost::asio::yield_context yield)
//	{
//		try
//		{
//			self->_timer.expires_from_now(std::chrono::seconds(3));
//
//			std::string in_data;
//			boost::system::error_code errorCode;
//			boost::asio::streambuf buff_response;
//
//			bool result = self->HandShake(yield);
//
//			if (!result)
//			{
//				return;
//			}
//
//			size_t bytes_transferred = 0;
//
//			result = self->Process([&]()
//				{
//					do
//					{
//						char reply_[C_TCP_PACK_SIZE] = {};
//						int len = self->_socket.async_read_some(boost::asio::buffer(reply_), yield);
//						if (errorCode)
//						{
//							throw errorCode;
//						}
//
//						bytes_transferred += len;
//
//						in_data.append(reply_, len);
//					} while (in_data.find("\r\n\r\n") == std::string::npos);
//				});
//
//			if (!result)
//			{
//				return;
//			}
//
//			size_t num_additional_bytes = bytes_transferred - (in_data.find("\r\n\r\n") + strlen("\r\n\r\n"));
//			TgBot::HttpParser::HeadersMap headers;
//			std::wstring serverResponse = stow(TgBot::HttpParser::getInstance().parseResponse(in_data, headers));
//
//			auto header_it = headers.find("content-length");
//			if (header_it != headers.end())
//			{
//				auto content_length = stoull(header_it->second);
//				if (content_length > num_additional_bytes)
//				{
//					result = self->Process([&]()
//						{
//							do
//							{
//								char reply_[C_TCP_PACK_SIZE] = {};
//								int len = self->_socket.async_read_some(boost::asio::buffer(reply_), yield);
//								if (errorCode)
//								{
//									throw errorCode;
//								}
//
//								num_additional_bytes += len;
//
//								in_data.append(reply_, len);
//
//							} while (content_length > num_additional_bytes);
//						});
//
//					if (!result)
//					{
//						return;
//					}
//				}
//			}
//
//			std::wstring answer(stow(in_data));
//
//			self->LogStringModule(LEVEL_FINEST, L"Read: %s", answer.c_str());
//			serverResponse = stow(TgBot::HttpParser::getInstance().parseResponse(wtos(answer), headers));
//
//			CMessage msg;
//			result = self->Process([&]()
//				{
//					std::string contentType = "text";
//
//					auto cit = headers.find("content-type");
//					if (cit != headers.cend())
//					{
//						contentType = cit->second;
//					}
//
//					if (!serverResponse.empty())
//					{
//						if (contentType.find("json") != std::string::npos)
//						{
//							msg = CMessageParser::ToMessage(serverResponse);
//						}
//						else
//						{
//							msg = CMessageParser::ToMessageFromPlainText(serverResponse);
//						}
//
//					}
//				});
//
//			if (!result)
//			{
//				return;
//			}
//
//			TMessageStatus tms = self->ParseMessageStatus(headers.find("status")->second);
//
//			msg.SetName(tms.methodName);
//			std::wstring response = self->_handler(std::move(msg), std::move(tms));
//
//			self->Process([&]()
//				{
//					self->LogStringModule(LEVEL_FINEST, L"Write: %s", response.c_str());
//					boost::asio::async_write(self->_socket, boost::asio::buffer(wtos(response), response.size()), yield);
//					self->LogStringModule(LEVEL_FINEST, L"Write OK");
//
//					self->CloseSocket();
//				});
//		}
//		catch (std::exception & e)
//		{
//			self->LogStringModule(LEVEL_FINEST, L"Exception: %s", stow(e.what()).c_str());
//
//			self->CloseSocket();
//			self->_timer.cancel();
//		}
//		catch (...)
//		{
//			self->LogStringModule(LEVEL_WARNING, L"Connection failed with error: unhandled exception caught");
//		}
//	});
//
//	boost::asio::spawn(_strand,
//		[self = shared_from_this()](boost::asio::yield_context yield)
//	{
//		while (self->IsOpen())
//		{
//			boost::system::error_code ignored_ec;
//			self->_timer.async_wait(yield[ignored_ec]);
//			if (self->_timer.expires_from_now() <= std::chrono::seconds(0))
//			{
//				self->LogStringModule(LEVEL_FINEST, L"Socket timeout");
//				if (self->IsOpen())
//				{
//					self->CloseSocket();
//					self->LogStringModule(LEVEL_FINEST, L"Close socket by timeout");
//				}
//
//			}
//		}
//	});
//}

/******************************* eof *************************************/
/************************************************************************/
/* Name     : MVEON\Common\Socket\SslSpawnServer.h                      */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 19 Feb 2017                                               */
/************************************************************************/

#pragma once

#include <string>
#include <functional>
#include "Router/router_compatibility.h"
#include "ChatBase.h"

namespace ssl_spawn_server
{
	namespace detail
	{
		struct TConnectionServerParams
		{
			std::string _uri;
			std::string _port;
			//std::string _token;
			//int _iServerTimeout;
			std::string _sertificate;
			std::string _private_key;
			uint32_t  _clientId;
			//std::string _server_uri;
			//std::string _server_port;
			ChatTextMessageHandler_s _handler;
		};

		template <class Connection>
		bool makeMySslServer(boost::asio::io_service* io_service, TConnectionServerParams params, CSystemLog* pLog);
	}

	struct ServerProxy
	{
		using TConnectionParams = ssl_spawn_server::detail::TConnectionServerParams;

		TConnectionParams Params;

		template <class Connection, class TParams>
		static bool MakeServer(boost::asio::io_service* io_service, TParams&& params, CSystemLog *pLog)
		{
			return ssl_spawn_server::detail::makeMySslServer<Connection>(io_service, std::forward<TParams>(params), pLog);
		}
	};

}
/******************************* eof *************************************/
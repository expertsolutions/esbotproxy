/************************************************************************/
/* Name     : MVEON\Common\Socket\SpawnClient.cpp                       */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 05 Jan 2017                                               */
/************************************************************************/
#include "stdafx.h"
#include <boost/asio/spawn.hpp>
#include <boost/asio/steady_timer.hpp>
#include "tgbot/net/HttpParser.h"
#include "SpawnClient.h"
#include "MessageParser.h"
#include "Log/SystemLog.h"

namespace spawn_client
{
	const int TCP_PACK_SIZE = 1024;
	static unsigned int _count = 0;

	namespace detail
	{
		bool SpawnClient::HandShake(TSocket& /*socket*/, boost::asio::yield_context /*yield*/, boost::system::error_code& /*errorCode*/)
		{
			return true;
		}

		std::unique_ptr<SpawnClient::TSocket> SpawnClient::CreateSocket(boost::asio::io_service* io_service, const spawn_client::TConnectionParams&) const
		{
			auto socket = std::make_unique<boost::asio::ip::tcp::socket>(*io_service );
			return std::move(socket);
		}

	} // detail
}



/******************************* eof *************************************/
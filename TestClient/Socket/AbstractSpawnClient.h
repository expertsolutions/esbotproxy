#pragma once

#include <boost/asio.hpp>
#include <boost/asio/spawn.hpp>

#include "ChatBase.h"

namespace abstract_spawn_client
{
	//const int TCP_PACK_SIZE = 1024;

	//inline bool ReadCompletePredicate(const int data_len)
	//{
	//	if (!data_len)
	//		return true;
	//	return 	data_len < TCP_PACK_SIZE;
	//};

	//inline std::string AddPartyId(const std::string aPartyId)
	//{
	//	if (aPartyId.empty())
	//	{
	//		return std::string{};
	//	}

	//	return std::string{ "/" } + aPartyId;
	//};

	template <class Setup>
	class AbstractSpawnClient
	{
		using TConnectionParams = typename Setup::TConnectionParams;
		using TSocket = typename Setup::TSocket;
		using TClientPtr = typename Setup::TClientPtr;
	public:

		bool MakeClient(TClientPtr clientPtr, TConnectionParams&& params, boost::asio::io_service* io_service, CSystemLog *pLog);

		virtual bool HandShake(TSocket& socket, boost::asio::yield_context yield, boost::system::error_code& errorCode) = 0;
		virtual std::unique_ptr<TSocket> CreateSocket(boost::asio::io_service* io_service, const TConnectionParams& params) const = 0;
	};
}
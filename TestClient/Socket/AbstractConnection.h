/************************************************************************/
/* Name     : MCHAT\AbstractConnection.h                                */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCHAT                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 23 May 2017                                               */
/************************************************************************/

#pragma once
#include <memory>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/spawn.hpp>
#include "boost/asio/ssl.hpp"

#include "Log/SystemLog.h"
#include "IdGenerator.h"
#include "MessageParser.h"

//#include "MessageCollector.h"
#include "tgbot/net/HttpParser.h"
#include "ChatBase.h"

const int C_TCP_PACK_SIZE = 2048;

class AbstractConnection
{
public:
	explicit AbstractConnection(boost::asio::io_service& io, CSystemLog* log)
		:_timer(io),
		_strand(io),
		_log(log)
	{
	}

	virtual ~AbstractConnection() = default;

	template<typename... TArgs>
	void LogStringModule(LogLevel level, const std::wstring& formatMessage, TArgs&&... args)
	{
		_log->LogStringModule(level, _connection_name, formatMessage, std::forward<TArgs>(args)...);
	}

	virtual void startListening() = 0;

protected:
	boost::asio::steady_timer _timer;
	boost::asio::io_service::strand _strand;
	CSystemLog* _log;
	std::wstring _connection_name;
};

class AbstractChatConnection : public AbstractConnection, public std::enable_shared_from_this<AbstractChatConnection>
{
public:
	AbstractChatConnection(
		boost::asio::io_service& io, 
		CSystemLog* log,
		ChatTextMessageHandler_s handler)
		: AbstractConnection(io, log)
		, _handler(handler)
	{
		_connection_name = L"connection " + std::to_wstring(++_count);
	}
protected:
	TMessageStatus ParseMessageStatus(std::string status);

	virtual bool HandShake(boost::asio::yield_context /*yield*/) = 0;
	virtual void CloseSocket() = 0;
	virtual bool IsOpen() const = 0;

	template <class Socket>
	void startListeningImpl(Socket& socket)
	{
		boost::asio::spawn(_strand,
			[self = shared_from_this(), &socket](boost::asio::yield_context yield)
		{
			try
			{
				self->_timer.expires_from_now(std::chrono::seconds(3));

				std::string in_data;
				boost::system::error_code errorCode;
				boost::asio::streambuf buff_response;

				bool result = self->HandShake(yield);

				if (!result)
				{
					return;
				}

				size_t bytes_transferred = 0;

				result = self->Process([&]()
				{
					do
					{
						char reply_[C_TCP_PACK_SIZE] = {};
						int len = socket.async_read_some(boost::asio::buffer(reply_), yield);
						if (errorCode)
						{
							throw errorCode;
						}

						bytes_transferred += len;

						in_data.append(reply_, len);
					} while (in_data.find("\r\n\r\n") == std::string::npos);
				});

				if (!result)
				{
					return;
				}

				size_t num_additional_bytes = bytes_transferred - (in_data.find("\r\n\r\n") + strlen("\r\n\r\n"));
				TgBot::HttpParser::HeadersMap headers;
				std::wstring serverResponse = stow(TgBot::HttpParser::getInstance().parseResponse(in_data, headers));

				auto header_it = headers.find("content-length");
				if (header_it != headers.end())
				{
					auto content_length = stoull(header_it->second);
					if (content_length > num_additional_bytes)
					{
						result = self->Process([&]()
						{
							do
							{
								char reply_[C_TCP_PACK_SIZE] = {};
								int len = socket.async_read_some(boost::asio::buffer(reply_), yield);
								if (errorCode)
								{
									throw errorCode;
								}

								num_additional_bytes += len;

								in_data.append(reply_, len);

							} while (content_length > num_additional_bytes);
						});

						if (!result)
						{
							return;
						}
					}
				}

				std::wstring answer(stow(in_data));

				self->LogStringModule(LEVEL_FINEST, L"Read: %s", answer.c_str());
				serverResponse = stow(TgBot::HttpParser::getInstance().parseResponse(wtos(answer), headers));

				CMessage msg;
				result = self->Process([&]()
				{
					std::string contentType = "text";

					auto cit = headers.find("content-type");
					if (cit != headers.cend())
					{
						contentType = cit->second;
					}

					if (!serverResponse.empty())
					{
						if (contentType.find("json") != std::string::npos)
						{
							msg = CMessageParser::ToMessage(serverResponse);
						}
						else
						{
							msg = CMessageParser::ToMessageFromPlainText(serverResponse);
						}

					}
				});

				if (!result)
				{
					return;
				}

				TMessageStatus tms = self->ParseMessageStatus(headers.find("status")->second);

				msg.SetName(tms.methodName);
				std::wstring response = self->_handler(std::move(msg), std::move(tms));

				self->Process([&]()
				{
					self->LogStringModule(LEVEL_FINEST, L"Write: %s", response.c_str());
					boost::asio::async_write(socket, boost::asio::buffer(wtos(response), response.size()), yield);
					self->LogStringModule(LEVEL_FINEST, L"Write OK");

					self->CloseSocket();
				});
			}
			catch (std::exception & e)
			{
				self->LogStringModule(LEVEL_FINEST, L"Exception: %s", stow(e.what()).c_str());

				self->CloseSocket();
				self->_timer.cancel();
			}
			catch (...)
			{
				self->LogStringModule(LEVEL_WARNING, L"Connection failed with error: unhandled exception caught");
			}
		});

		boost::asio::spawn(_strand,
			[self = shared_from_this()](boost::asio::yield_context yield)
		{
			while (self->IsOpen())
			{
				boost::system::error_code ignored_ec;
				self->_timer.async_wait(yield[ignored_ec]);
				if (self->_timer.expires_from_now() <= std::chrono::seconds(0))
				{
					self->LogStringModule(LEVEL_FINEST, L"Socket timeout");
					if (self->IsOpen())
					{
						self->CloseSocket();
						self->LogStringModule(LEVEL_FINEST, L"Close socket by timeout");
					}

				}
			}
		});
	}

	template <class Fn>
	bool Process(const Fn& fn)
	{
		std::string errorMsg;
		try
		{
			fn();
		}
		catch (boost::system::error_code & error)
		{
			if (error != boost::asio::error::eof)
			{
				errorMsg = error.message();
			}
		}
		catch (std::runtime_error & error)
		{
			errorMsg = error.what();
		}
		catch (...)
		{
			errorMsg = "Socket failed with error: unhandled exception caught";
		}

		if (!errorMsg.empty())
		{
			this->CloseSocket();
			this->_timer.cancel();
			this->LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(errorMsg));

			return false;
		}

		return true;
	}

protected:
	ChatTextMessageHandler_s _handler;
	static std::atomic<unsigned int> _count;
};

/******************************* eof *************************************/
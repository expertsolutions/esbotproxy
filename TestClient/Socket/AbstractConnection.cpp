/************************************************************************/
/* Name     : MCHAT\AbstractConnection.cpp                              */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCHAT                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 26 Oct 2020                                               */
/************************************************************************/

#include "stdafx.h"
#include "AbstractConnection.h"

std::atomic<unsigned int> AbstractChatConnection::_count = 0;

TMessageStatus AbstractChatConnection::ParseMessageStatus(std::string status)
{
	TMessageStatus result;

	int pos = status.find("/chatsessions");
	if (pos < 0)
		throw (std::runtime_error("Invalit http header status"));

	result.methodName = L"START_SESSION";

	//pos += strLen(L"/chatsessions") - 1;
	pos += 13;
	status = status.substr(pos, status.size() - pos);

	pos = status.find("/");
	if (pos != std::string::npos)
	{
		status = status.substr(pos + 1, status.size() - pos - 1);

		pos = status.find("/");
		if (pos >= 0)
		{
			std::string chatId = status.substr(0, pos);
			result.chatId = stow(chatId);
			result.methodName = L"CHATMESSAGE";
		}
		else if (status.find("?timestamp") != std::string::npos)
		{
			result.methodName = L"GET_SESSION_STATUS";
			pos = status.find("?timestamp");

			std::wstring chatId = stow(status).substr(0, pos);
			result.chatId = chatId;
			//pos += strLen(L"?timestamp");
			pos += 11;
			result.timeStamp = stow(status.substr(pos, status.size() - pos));
		}
		else
		{
			result.methodName = L"GET_SESSION_STATUS";
			//pos = status.find("?timestamp");

			std::wstring chatId = stow(status).substr(0, pos);
			result.chatId = chatId;
			////pos += strLen(L"?timestamp");
			//pos += 11;
			//result.timeStamp = stow(status.substr(pos, status.size() - pos));
		}
	}

	return result;
}

/******************************* eof *************************************/
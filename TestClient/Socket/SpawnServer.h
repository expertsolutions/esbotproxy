/************************************************************************/
/* Name     : MVEON\Common\Socket\SpawnServer.h                         */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCC                                                       */
/* Company  : Expert Solutions                                          */
/* Date     : 05 Jan 2017                                               */
/************************************************************************/

#pragma once

#include <string>
#include <functional>
#include "Router/router_compatibility.h"
#include "ChatBase.h"

namespace spawn_server
{
	namespace detail
	{
		struct TConnectionServerParams
		{
			std::string _uri;
			std::string _port;
			uint32_t  _clientId;

			std::string _token;
			int _iServerTimeout;
			std::string _sertificate;
			std::string _private_key;

			ChatTextMessageHandler_s _handler;
		};

		template <class Connection>
		bool makeServer(boost::asio::io_service* io_service, TConnectionServerParams params, CSystemLog* pLog);
	}

	struct ServerProxy
	{
		using TConnectionParams = spawn_server::detail::TConnectionServerParams;

		TConnectionParams Params;

		template <class Connection, class TParams>
		static bool MakeServer(boost::asio::io_service* io_service, TParams&& params, CSystemLog *pLog)
		{
			return spawn_server::detail::makeServer<Connection>(io_service, std::forward<TParams>(params), pLog);
		}
	};

}



/******************************* eof *************************************/
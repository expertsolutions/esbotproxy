/************************************************************************/
/* Name     : MVEON\Common\Socket\SpawnClient.h                         */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 05 Jan 2017                                               */
/************************************************************************/

#pragma once

#include <string>
#include <functional>
#include "Router/router_compatibility.h"
#include "ChatBase.h"
#include "AbstractSpawnClient.h"

class CSystemLog;

namespace spawn_client
{
	struct TConnectionParams
	{
		std::string _uri;
		std::string _port;
		std::string _partyId;
		uint32_t  _clientId;

		//std::string _request;
		std::string _response;
		CMessage    _msg;
		std::string _xApiEnvironment;

		ConnectionStatusMesageHandler _statusHandler;
	};

	namespace detail
	{
		class SpawnClient;
	}
	struct SpawnClientSetup
	{
		using TConnectionParams = spawn_client::TConnectionParams;
		using TSocket = boost::asio::ip::tcp::socket;
		using TClientPtr = std::shared_ptr<detail::SpawnClient>;
	};

	namespace detail
	{
		class SpawnClient : 
			public abstract_spawn_client::AbstractSpawnClient<SpawnClientSetup>, 
			public std::enable_shared_from_this<SpawnClient>
		{
			using TSocket = typename SpawnClientSetup::TSocket;
		public:
			bool HandShake(TSocket& socket, boost::asio::yield_context yield, boost::system::error_code& errorCode) override;
			std::unique_ptr<TSocket> CreateSocket(boost::asio::io_service* io_service, const spawn_client::TConnectionParams& params) const override;
		};
	}

	struct ClientProxy
	{
		using TConnectionParams = spawn_client::TConnectionParams;

		TConnectionParams Params;

		template <class TParams>
		static bool MakeClient(TParams&& params, boost::asio::io_service* io_service, CSystemLog *pLog)
		{
			auto client = std::make_shared<detail::SpawnClient>();
			return client->MakeClient(client->shared_from_this(), std::forward<TParams>(params), io_service, pLog);
		}
	};
}

/******************************* eof *************************************/
/************************************************************************/
/* Name     : MVEON_TESTS\Socket\MakeSslClientToVeon.h                  */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 19 Feb 2017                                               */
/************************************************************************/

#pragma once

#include "boost/asio/ssl.hpp"

#include "ChatBase.h"
#include "AbstractSpawnClient.h"

namespace ssl_spawn_client
{
	struct TConnectionParams
	{
		std::string _uri;
		std::string _port;
		std::string _partyId;
		uint32_t  _clientId;

		std::string _request;
		std::string _response;
		CMessage    _msg;
		std::string _xApiEnvironment;
		std::string _sertificate;

		ConnectionStatusMesageHandler _statusHandler;
	};

	namespace detail
	{
		class SslSpawnClient;
	}

	struct SslSpawnClientSetup
	{
		using TConnectionParams = ssl_spawn_client::TConnectionParams;
		using TSocket = boost::asio::ssl::stream<boost::asio::ip::tcp::socket>;
		using TClientPtr = std::shared_ptr<detail::SslSpawnClient>;
	};

	namespace detail
	{
		class SslSpawnClient : 
			public abstract_spawn_client::AbstractSpawnClient<SslSpawnClientSetup>,
			public std::enable_shared_from_this<SslSpawnClient>
		{
			using TSocket = typename SslSpawnClientSetup::TSocket;
		public:

			SslSpawnClient();

			bool HandShake(TSocket& socket, boost::asio::yield_context yield, boost::system::error_code& errorCode) override;
			std::unique_ptr<TSocket> CreateSocket(boost::asio::io_service* io_service, const ssl_spawn_client::TConnectionParams&) const override;

		private:
			mutable boost::asio::ssl::context _context;
		};
	}

	struct ClientProxy
	{
		using TConnectionParams = ssl_spawn_client::TConnectionParams;

		TConnectionParams Params;

		template <class TParams>
		static bool MakeClient(TParams&& params, boost::asio::io_service* io_service, CSystemLog *pLog)
		{
			auto client = std::make_shared<detail::SslSpawnClient>();
			return client->MakeClient(client->shared_from_this(), std::forward<TParams>(params), io_service, pLog);
		}
	};
}

/******************************* eof *************************************/
#pragma once

#include <variant>

#include "Router/router_compatibility.h"

#include "Base.h"
#include "DataReceiver.h"
#include "queue.h"
#include "processor.h"
#include "ESBotProxyLib/bot_client.h"
//#include "ESBotProxyLib/edna_client.h"

class CDataReceiverImpl : public CDataReceiver < CDataReceiverImpl >
{
	friend class CDataReceiver < CDataReceiverImpl >;
	//using EdnaResponse = edna::Response;
	//using EdnaRequest = edna::Request;
	using BotResponse = bot::Response<bot::CompositeSessionId>;
	using BotRequest = bot::Request<bot::CompositeSessionId>;
	using BotRequestAck = bot::RequestAck<bot::CompositeSessionId>;
	
	//using Response = std::variant<BotResponse, EdnaResponse>;

	//auto ProcessorConsumer()
	//{
	//	return [this](const Response& record)
	//	{
	//		return ProcessResponse(record);
	//	};
	//}

public:
	CDataReceiverImpl(boost::asio::io_service& io);
	~CDataReceiverImpl();

	void S2MBOT_RequestHandler(const CMessage& aMsg, const CLIENT_ADDRESS& aFrom, const CLIENT_ADDRESS& aTo);

private:
	int init_impl(CSystemLog* pLog);
	int start();
	//bool ProcessResponse(const Response& record);
	//bool ProcessResponse(const BotResponse& response);
	//bool ProcessResponse(const EdnaResponse& response);

	void SendMessageToBot(BotRequest&& response);
	//void SendMessageToEdna(EdnaRequest&& message);

	void SendBotRequestAck(BotRequestAck&& ack);
	void SendBotResponse(BotResponse&& response);

private:
	//router api
	void on_routerConnect();
	void on_routerDeliverError(const CMessage&, const CLIENT_ADDRESS&, const CLIENT_ADDRESS&);
    void on_appStatusChange(uint32_t dwClient, /*int*/core::ClientStatus	nStatus);
	void set_Subscribes();
	std::wstring get_ModuleName() const noexcept { return L"ESBotProxy"; }
	void stop();

private:
	void ConnectHandlers();

private:
	CSystemLog * m_log = nullptr;
	boost::asio::io_service& r_io;

public:
	//std::wstring _botSessionIdPrefix;
	std::unique_ptr<bot::MBotClient> _bot;
	//std::unique_ptr<edna::EdnaClient> _edna;
	//std::wstring _botSwitchToHumanDummyMessage;

	//Processor<Queue<Response>> _processor;
	XString _replyToServerUrl;
};

/******************************* eof *************************************/


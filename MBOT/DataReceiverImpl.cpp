#include <boost/asio/spawn.hpp>
#include <boost/asio/steady_timer.hpp>
#include "DataReceiverImpl.h"
#include "Configuration/SystemConfiguration.h"
#include "Log/SystemLog.h"
//#include "WinAPI/ProcessHelper.h"
#include "ThreadHelper.h"
#include "utils.h"
#include "IdGenerator.h"

const int EXPIREDTIME = 30;

void CDataReceiverImpl::SendMessageToBot(BotRequest&& message)
{
	this->_bot->sendRequestAsync(std::move(message), [this](BotRequestAck&& ack)
		{
            this->m_log->LogString(LEVEL_INFO, L"%s", ConvertToXString(ack.ToString()));
			this->SendBotRequestAck(std::move(ack));
		});
}

CDataReceiverImpl::CDataReceiverImpl(boost::asio::io_service& io) 
	: m_log(NULL)
	, r_io(io)
{
}

CDataReceiverImpl::~CDataReceiverImpl()
{
}

void CDataReceiverImpl::ConnectHandlers()
{
}

void CDataReceiverImpl::set_Subscribes()
{
	subscribe(L"S2MBOT_REQUEST", &CDataReceiverImpl::S2MBOT_RequestHandler);
}

int CDataReceiverImpl::init_impl(CSystemLog* pLog)
{
	//const std::wstring html = L"";
	//const auto text = ConvertToPlainText(MakeHtmlFormat(html));

	if (!pLog)
		return -1111;
#ifdef _WIN32
    CoInitialize(NULL);
#endif

	m_log = pLog;

	m_log->LogString(LEVEL_FINEST, L"CDataReceiverImpl initializing...");

	ConnectHandlers();

	start();

	return 0;
}

int CDataReceiverImpl::start()
{
	SystemConfig settings;

	const std::wstring botExternalServerUrl = settings->get_config_value<std::wstring>(L"BotChatAdapterUrl");
	const std::wstring botLocalServerUrl = settings->get_config_value<std::wstring>(L"BotListeningUrl");
    _replyToServerUrl = ConvertToXString(settings->get_config_value<std::wstring>(L"ReplyToUrl"));

    _bot = std::make_unique<bot::MBotClient>(wtos(botExternalServerUrl), wtos(botLocalServerUrl), TEXT(""));
	m_log->LogString(LEVEL_FINEST, L"Mbot started");
	
	_bot->setResponseHandler([this](BotResponse&& response)
		{
			this->SendBotResponse(std::move(response));
		});

	return 0;
}

void CDataReceiverImpl::stop()
{
	r_io.stop();

	_bot.reset();
}

void CDataReceiverImpl::on_routerConnect()
{
	if (!m_log)
	{
		return;
	}

	m_log->LogStringModule(LEVEL_INFO, L"Receiver Core", L"Router has connected");
}

void CDataReceiverImpl::on_routerDeliverError(const CMessage& _msg, const CLIENT_ADDRESS &_from, const CLIENT_ADDRESS &_to)
{
	if (!m_log)
	{
		return;
	}

	m_log->LogStringModule(LEVEL_FINE, L"Router deliver error", _msg.Dump().c_str());
}

void CDataReceiverImpl::on_appStatusChange(uint32_t dwClient, core::ClientStatus nStatus)
{
	if (nStatus == /*static_cast<int>*/(core::ClientStatus::CS_DISCONNECTED))
	{
		try
		{

		}
		catch (...)
		{

		}
	}
}

void CDataReceiverImpl::SendBotRequestAck(BotRequestAck&& ack)
{
	try {
		CMessage message(L"MBOT2S_REQUEST_ACK");

		auto scriptId = ack.sessionId.GetScriptId();

        message[L"SessionId"] = ConvertToXString(scriptId);
        message[L"QuestionNumber"] = ack.questionNumber;
		message[L"Code"] = ack.code;
        message[L"Description"] = ConvertToXString(ack.description);

		CLIENT_ADDRESS clientAddress(std::stoull(scriptId));
		m_log->LogString(LEVEL_INFO, L"Sending ack to %s: %s", clientAddress.to_string(), message.ToJson());
		SendTo(std::move(message), clientAddress);
	}
	catch (std::exception & e) {
		m_log->LogString(LEVEL_SEVERE, L"Error while trying to send request ack to script: %s", stow(e.what()));
	}
}


void CDataReceiverImpl::SendBotResponse(BotResponse&& response)
{
	try {
		CMessage message(L"MBOT2S_RESPONSE");
        message[L"SessionId"] = ConvertToXString(response.sessionId.GetScriptId());
		message[L"QuestionNumber"] = response.questionNumber;
		message[L"Result"] = static_cast<int>(response.result);
		//message[L"Description"] = response.description;
		//message[L"ResponseType"] = response.responseType;
		//message[L"Split"] = response.split;
        message[L"TimeStamp"] = ConvertToXString(response.timestamp);
		if (response.errors.empty())
		{
            message[L"Message"] = ConvertToXString(response.MakeMessage());
		}
		else
		{
            message[L"Message"] = ConvertToXString(response.MakeError());
		}

		const int64_t clientAddress = utils::toNum<int64_t>(response.sessionId.GetScriptId());
		CLIENT_ADDRESS addrTo(clientAddress);
		m_log->LogString(LEVEL_INFO, L"Sending response to %s: %s", addrTo.to_string(), message.ToJson());
		SendTo(std::move(message), addrTo);
	}
	catch (std::exception & e) {
		m_log->LogString(LEVEL_SEVERE, L"Error while trying to send response to script: %s", stow(e.what()));
	}
}


/******************************* eof *************************************/

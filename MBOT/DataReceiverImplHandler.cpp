#define _WINSOCK
#include "ESBotProxyLib/bot_session_id.h"
#include "DataReceiverImpl.h"
#include "IdGenerator.h"

void CDataReceiverImpl::S2MBOT_RequestHandler(const CMessage& aMsg, const CLIENT_ADDRESS& aFrom, const CLIENT_ADDRESS& aTo)
{
    aMsg.CheckParam(L"SessionId", core::message::CheckedType::String, core::message::ParamType::Mandatory);
    aMsg.CheckParam(L"QuestionNumber", core::message::CheckedType::Int, core::message::ParamType::Mandatory);
    aMsg.CheckParam(L"Message", core::message::CheckedType::String, core::message::ParamType::Mandatory);
    aMsg.CheckParam(L"ChannelName", core::message::CheckedType::String, core::message::ParamType::Mandatory);
    aMsg.CheckParam(L"ChannelType", core::message::CheckedType::Int, core::message::ParamType::Mandatory);
    aMsg.CheckParam(L"TimeStamp", core::message::CheckedType::String, core::message::ParamType::Mandatory);

    bot::Request<bot::CompositeSessionId> request;

    auto asString = [](const auto& param)
    {
#ifdef _WIN32
        return param.AsWideStr();
#else
        return param.AsStr();
#endif
    };


    request.sessionId = bot::CompositeSessionId::CreateCompositeSessionId( asString(aMsg[L"SessionId"]),
            ConvertToXString2(CIdGenerator::GetInstance()->makeSId()) );
    request.marketCode = asString(aMsg[L"MarketCode"]);
    request.message = asString(aMsg[L"Message"]);
    request.ctn = asString(aMsg[L"CTN"]);
    request.authType = asString(aMsg[L"AuthType"]);
    request.segment = asString(aMsg[L"Segment"]);
    request.channelName = asString(aMsg[L"ChannelName"]);
    request.timestamp = asString(aMsg[L"TimeStamp"]);
    request.channelType = aMsg[L"ChannelType"].AsInt();
    request.questionNumber = aMsg[L"QuestionNumber"].AsInt();

    if (aMsg.HasParam(L"CityId"))
    {
        request.cityId = asString(aMsg[L"CityId"]);
    }

    if (aMsg.IsParam(L"DistrictId"))
    {
        request.districtId = asString(aMsg[L"DistrictId"]);
    }

    if (aMsg.IsParam(L"SubscriberNo"))
    {
        request.subscriberNo = asString(aMsg[L"SubscriberNo"]);
    }

    if (aMsg.IsParam(L"Pl"))
    {
        request.pl = asString(aMsg[L"Pl"]);
    }

    request.replyTo = _replyToServerUrl;

    SendMessageToBot(std::move(request));

}


/******************************* eof *************************************/

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include "request_ack_parser.h"

BOOST_AUTO_TEST_SUITE(request_ack_parser_suite)

BOOST_AUTO_TEST_CASE(AllFieldsTest)
{
    web::json::value value;
    value[U("Description")] = web::json::value(U("error"));
    value[U("SessionId")] = web::json::value(U("1231241241"));
    value[U("Code")] = web::json::value(1);
    value[U("QuestionNumber")] = web::json::value(1);

    auto ack = RequestAckParser::fromJson(value);

    BOOST_CHECK(ack.description == L"error");
    BOOST_CHECK(ack.sessionId == L"1231241241");
    BOOST_TEST(ack.code == 1);
    BOOST_TEST(ack.questionNumber == 1);
}

BOOST_AUTO_TEST_CASE(RequiredFieldsTest)
{
    web::json::value value;
    value[U("SessionId")] = web::json::value(U("1231241241"));
    value[U("Code")] = web::json::value(0);
    value[U("QuestionNumber")] = web::json::value(1);

    auto ack = RequestAckParser::fromJson(value);

    BOOST_TEST(ack.description.empty());
    BOOST_CHECK(ack.sessionId == L"1231241241");
    BOOST_TEST(ack.code == 0);
    BOOST_TEST(ack.questionNumber == 1);
}

BOOST_AUTO_TEST_CASE(NoSessionIdThrowsTest)
{
    web::json::value value;
    value[U("Description")] = web::json::value(U("error"));
    value[U("Code")] = web::json::value(0);
    value[U("QuestionNumber")] = web::json::value(1);

    BOOST_CHECK_THROW(RequestAckParser::fromJson(value), std::exception);
}

BOOST_AUTO_TEST_CASE(NoCodeThrowsTest)
{
    web::json::value value;
    value[U("SessionId")] = web::json::value(U("1231241241"));
    value[U("Description")] = web::json::value(U("error"));
    value[U("QuestionNumber")] = web::json::value(1);

    BOOST_CHECK_THROW(RequestAckParser::fromJson(value), std::exception);
}

BOOST_AUTO_TEST_CASE(NoQuestionNumberThrowsTest)
{
    web::json::value value;
    value[U("SessionId")] = web::json::value(U("1231241241"));
    value[U("Description")] = web::json::value(U("error"));
    value[U("Code")] = web::json::value(0);

    BOOST_CHECK_THROW(RequestAckParser::fromJson(value), std::exception);
}

BOOST_AUTO_TEST_CASE(EmptySessionIdThrowsTest)
{
    web::json::value value;
    value[U("SessionId")] = web::json::value(U(""));
    value[U("Description")] = web::json::value(U("error"));
    value[U("Code")] = web::json::value(0);
    value[U("QuestionNumber")] = web::json::value(1);

    BOOST_CHECK_THROW(RequestAckParser::fromJson(value), std::exception);
}

BOOST_AUTO_TEST_CASE(WrongTypeThrowsTest)
{
    web::json::value value;
    value[U("SessionId")] = web::json::value(U(""));
    value[U("Description")] = web::json::value(U("error"));
    value[U("Code")] = web::json::value(U("0"));
    value[U("QuestionNumber")] = web::json::value(1);

    BOOST_CHECK_THROW(RequestAckParser::fromJson(value), std::exception);
}


BOOST_AUTO_TEST_SUITE_END()
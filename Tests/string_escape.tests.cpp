#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include "utils/json_utils.h"

BOOST_AUTO_TEST_SUITE(string_escape_suite)

BOOST_AUTO_TEST_CASE(HtmlStringEscapedTest)
{
    std::wstring input = LR"raw(��������� � ���� ������ ����� ������: <br>� ��������� ���������� <a href="https://moskva.beeline.ru/customers/products/mobile/devices/my-beeline">"��� ������"</a>;<br>� <a href="https://moskva.beeline.ru/login">������ ��������</a> 110*9#����� ��� ��������� ������ � ������, �� ������: 067405, ������ ������� �� ��������: 110*05#�����. <br>)raw";

    std::wstring expected = LR"raw(��������� � ���� ������ ����� ������: <br>� ��������� ���������� <a href=\"https:\/\/moskva.beeline.ru\/customers\/products\/mobile\/devices\/my-beeline\">\"��� ������\"<\/a>;<br>� <a href=\"https:\/\/moskva.beeline.ru\/login\">������ ��������<\/a> 110*9#����� ��� ��������� ������ � ������, �� ������: 067405, ������ ������� �� ��������: 110*05#�����. <br>)raw";

    auto result = jsonEscapeString(input);
    BOOST_CHECK(result == expected);
}

BOOST_AUTO_TEST_CASE(BackslashedEscaped)
{
    std::wstring input = LR"raw(asd\\\123\33fffff)raw";

    std::wstring expected = LR"raw(asd\\\\\\123\\33fffff)raw";

    auto result = jsonEscapeString(input);
    BOOST_CHECK(result == expected);
}

BOOST_AUTO_TEST_CASE(ReturnEscaped)
{
    std::wstring input = L"Test\rreturn";

    std::wstring expected = L"Test\\rreturn";

    auto result = jsonEscapeString(input);
    BOOST_CHECK(result == expected);
}

BOOST_AUTO_TEST_CASE(CiryllicEscaped)
{
    std::wstring input = L"������ <img />";

    std::wstring expected = L"������ <img \\/>";

    auto result = jsonEscapeString(input);
    BOOST_CHECK(result == expected);
}


BOOST_AUTO_TEST_SUITE_END()
#include "stdafx.h"

#include <future>
#include <boost/test/unit_test.hpp>

#include <ESBotProxyLib/edna_client.h>
//#include "D:\Programming\cpprestsdk\Release\tests\functional\http\utilities\include\http_asserts.h"

// Memory leaks will be detected due to SystemLog singleton

static const std::string EXTERNAL_SERVER_URL{ "http://localhost:51102/chatbot" };
static const std::string LOCAL_SERVER_URL{ "http://localhost:41102/mbot" };

static const std::string EXTERNAL_SERVER_HTTPS_URL{ "https://192.168.0.2:443/chatbot" };
static const std::string LOCAL_SERVER_HTTPS_URL{ "https://192.168.0.2:443/mbot" };

static const char* GetSslCertificate()
{
    const char* self_signed_cert = R"(
-----BEGIN CERTIFICATE-----
MIIFgTCCA2kCCQCzOzVTtIAt5TANBgkqhkiG9w0BAQUFADB1MQswCQYDVQQGEwJG
UjESMBAGA1UECAwJT2NjaXRhbmllMREwDwYDVQQHDAhUb3Vsb3VzZTEUMBIGA1UE
CgwLVGVjaCBTY2hvb2wxEjAQBgNVBAsMCUVkdWNhdGlvbjEVMBMGA1UEAwwMMTcy
LjE2LjcyLjM4MB4XDTIxMDcxNDEzNDgzNloXDTIxMDkxMjEzNDgzNlowgY8xCzAJ
BgNVBAYTAkZSMRYwFAYDVQQIDA1JbGUgZGUgRnJhbmNlMQ4wDAYDVQQHDAVQYXJp
czEQMA4GA1UECgwHUEMgQm9vazERMA8GA1UECwwIQ29tcHV0ZXIxEjAQBgNVBAMM
CWxvY2FsaG9zdDEfMB0GCSqGSIb3DQEJARYQcGNib29rQGdtYWlsLmNvbTCCAiIw
DQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAOD/dBj39b0AmScfka6ziTqG5nKb
a11EklwT37yW7qsD+0EbcZxkxCCwD2BXNh8yaB2NSweefszxg38QVSQ7g95JzMVM
NlQ2iDfrLbyOUJokphX0hl3WIm9Hd73T/YHheDIAD3/10PZX2VPxSbodIjtGNLxL
6x3UASL1DGrqzgD038w9O/s3OdwwhI64NSESMky00+IN0BePhBbyaIj2ehSEVa2O
roDEm0bSCDhJh75+pMyLYIiOVqzAWMpr5rHg/WhDi3XpSm9UTwZ6TlSrHBh67CDH
KD43nTJrKNThGw1br5ZV3Sgrf8L1kywBD6eLwabaByk7QjVFH8dSOgAvnFw2Wmde
p4wqzFTP5Jry7EW6AyOaG75/b0KKhDy8pbBezUO8/ef+fTO6W8jGv/ppIrubf1vr
XQ+L2T+90JhQIMjDHqEGzPxALuUG7NIK3uIDxp46PHrescxk5r6VtM32YM+17fs7
738iPoqslCBlZ08gNfxS5EbG25YbO+kiqf0frQG1FZ9LlpNGyq8T4kzXMLMEJVrJ
Nrt3b5dZOL88duDTlxqe2qNfHnBPGvG6tWNtAg0jeQnwLdR3U2L7JFOJLly8SsqD
HudQxSo0gLKLuNFfCnRek8A7Qo5SWfEs42hkmVv9kU20fqKu48PbAdWbbzdvE35H
wk/Ud27tlve4FnDbAgMBAAEwDQYJKoZIhvcNAQEFBQADggIBADHxk5X6yJWoTZ8y
Q+spJ1R+hvKO1hXXrXp1UnrnSRnl6qxSIowplCR2bx6c+rjMnHE0KFmr3mNtYpVx
5ZIUQElcSIY5Yep+g3pQwq/+tDzAw8Yu/5+Jv6GkurFuDg/qlYxLIgS8zGAgguPA
seKqvWys2VoboRi62cLC1WwCNb9tsXjHikVXOP+SNzNCbtYPtOxkiJ8bj0KB5wXb
60Ka7pXVVGSJO2QsStqNZrOP+yAN3lUj99gQvWDcL5h27sautnK+Mc4Vhw7fEcY4
p6Sw+DKcbnRnjjj+eJ7jBg0Ck/cyzsfgSIg6JAXjRbw6K9tuFc5IAikRSOrc2zkp
piot5SfWov01uOSzVzLNh9zqdaRQZxtG6UaltVOt99lokpyz5WQAOQRobYnYEdb3
kMMyAOuTtdDZlVKNYpjfMFjgxqtfshM8F57E5w40IkYRVHVLq842/Bgba6KtsY2Y
+IGINwAnz9Vk620uf05uwm4rHimUYt2ZkYQzqp6jg+vJUVNIVtHs+bfZ03Ic23Wu
jm8QCv+hFJu4anXXh7ijVvsDcslV6xQQcXO7ukhdu3xFkPJQfcrEd1C4wHefU6dC
79bxIU9u9RFqpIbiYYHQ+/aBPvGM9Ks3gYW4ZKi5FqnUzrTG/KOiYVjvTPav1QP3
o7Q2VZ7eID06sfvnv+lKFwu3n65/
-----END CERTIFICATE-----
        )";
    return self_signed_cert;
}

static const char* GetSslPrivateKey()
{
    const char* private_key = R"(
-----BEGIN PRIVATE KEY-----
MIIJQgIBADANBgkqhkiG9w0BAQEFAASCCSwwggkoAgEAAoICAQDg/3QY9/W9AJkn
H5Gus4k6huZym2tdRJJcE9+8lu6rA/tBG3GcZMQgsA9gVzYfMmgdjUsHnn7M8YN/
EFUkO4PeSczFTDZUNog36y28jlCaJKYV9IZd1iJvR3e90/2B4XgyAA9/9dD2V9lT
8Um6HSI7RjS8S+sd1AEi9Qxq6s4A9N/MPTv7NzncMISOuDUhEjJMtNPiDdAXj4QW
8miI9noUhFWtjq6AxJtG0gg4SYe+fqTMi2CIjlaswFjKa+ax4P1oQ4t16UpvVE8G
ek5UqxwYeuwgxyg+N50yayjU4RsNW6+WVd0oK3/C9ZMsAQ+ni8Gm2gcpO0I1RR/H
UjoAL5xcNlpnXqeMKsxUz+Sa8uxFugMjmhu+f29CioQ8vKWwXs1DvP3n/n0zulvI
xr/6aSK7m39b610Pi9k/vdCYUCDIwx6hBsz8QC7lBuzSCt7iA8aeOjx63rHMZOa+
lbTN9mDPte37O+9/Ij6KrJQgZWdPIDX8UuRGxtuWGzvpIqn9H60BtRWfS5aTRsqv
E+JM1zCzBCVayTa7d2+XWTi/PHbg05cantqjXx5wTxrxurVjbQINI3kJ8C3Ud1Ni
+yRTiS5cvErKgx7nUMUqNICyi7jRXwp0XpPAO0KOUlnxLONoZJlb/ZFNtH6iruPD
2wHVm283bxN+R8JP1Hdu7Zb3uBZw2wIDAQABAoICABadNlMIIRfVBo+rGZ5ghytt
wZ3FRz5BlSBSEwvGc8iWhttdYcbhTeEnMPRy74qlK1fFCsSCRqZco+XSg2w32yIt
crpl1ZQTQw+zwI4SE8HaayMLx7GIwz9CBKP8uZ8jRUxpOyWWVGFeJQbN2qYtJXKl
tMk0LDZ8R3z2+j+IyBnKgu/pO60dOOCLs9cQZq6jQJD7H8y5rK+hihowVRdnBpAn
WOEv1RQ2LpEorRDTOySHYBL680HmZInKb2IxBCfewDmJiGvafdPAuoyhIFxcMl+3
gyIsQjw2pMMbiCgVU/28uGI40Rinfpjey+MBXExvvJADm/Kl9Rqbjv0eo5u/LUzb
Dqel6jXCSwj0W3eWbcHPJEbZ29Nf03De9h4FUu69Sf0YE6JfO1N8rvrNH99nDFQc
GALIzrKpjdEYVwZH5EGGBhDR48vB/in0QgMyk7sjIjNl2E0GsZVfLCMhxHsfzgvK
a0zR3UJo9Px3O3MtjbtJmCcjme/NQh+FVu8xHR1vr15LFOzx8/padk6780BmjV1u
iAmdAUd7u0Dxg8wiYESMcSmhgDouaRoSIKpn1Hrv+j3A1hdSHQgw7nieAVlEcbRF
tiQw54IqULu8jjwIuOz51egBH1mjhRHgL6arvFNJ/mztlp/FtueFdfgGq6llm9zN
Z5xMcjxUGv5BrWAv9wJBAoIBAQD8tiMSmb/lfOOJ0hJrIf9pKOEmbUWVoVHfJv3a
hy3+31nn0CeTSMpHGTiOrMsNZ0214q+5b0WYg/AjXPUfb7C6JBoP//DZGL4Pjd0C
sw9UbC0S/tlEtHPfdjfoUO9M7LID5fVUwCoStPkqfEfx4Mt0ff+bBl7GNw2W1Up/
k2BJ9+ayXq7y/BUOEWOyQefQpUGCACkMBKfJfH9fpz5RNLlX+pHFTNpxPkgbB6Rt
PaWhHyuoF/urY7UcvPjEdIXIepkTASHRESlscgimWZkJg8is6O2gridcYaHcQaNO
+kb+cYuAsIzJEPbYHMg0GAzMMsKi0Pc7Q5pmrMxCunvZO3jhAoIBAQDj7P5bmu1n
DXrbpSvD3HQ3+dtM6EslAI2qgnp78kdymEe6BeXtm0wHjG0A+lSEC4XSLNNWrQXA
ynZEcKjeH3PmU9f/hYJCISck69ykGRn3tKogc4CbF89W3DUkuA3o85F0WZcYHLSe
stNz3z4yvfDx5AfrtYKs5I0zqWgvpigOEYalx//RoVwytocHOnp8geIkOwfNe+Z6
0HBwdvkKzPscZ6Min96DVlM3qUJS1eFM/KiK5F9e7nzc87UCVnGc/KVomer1JTaS
orkxbF87q3cyWWtrQd7e2d+0Lgjx9wENKsVpMOq5gAuCTko/+TYCD41FqAObauWH
n7cLkfxruzU7AoIBAQC2zU37k4pTiA7UUEiQhKxMNoz/jgXK4kHmCT+SRzzHZGuV
m2Y1TW89FQprcqUDUhnNm2f/3HTciXw5KHIMRm/xhlHwBvUDJbm+aKFZ5YsOR54n
YoBqCZrUJ6DzhTBFVebDmSHpJ4SHua+Pww0L889Yuhge3PHLsmE7HZjhRgteFlwj
5QfqrtzQK1YRyUjWDeSgFFuc3PU+c7dzyvfyKBB2isPNk/Vj+2iPXOzdNL3OGk0r
MAYWCW8mla/h9GQeDEkUjToYELEFSEvngWKJlnGgWnIiTDgexGUgfb6TT8nxHtOJ
yKxeec4FsHRF8qu3PnS68CwXC6Tx5sdu8ZTLJBEhAoIBADnRl+DDF3Ap8j2f089W
sEC26FPFJbLYRhe4C7w/hLJkQOXGs0IoQAw2KjyyRBhPF/5uEpqNP8VdVs0byyq1
zd/i6wtN4fJGmLh8ALe8CmNU+EdNJZWyX42IRQ7whc7PYtmiFHswZP1RHeTp/8w9
dM7X6s53eBPF0rAFTIGx471V4tNRPer+xPW0aeKCiinlhOlt9BfDU39aOvu8ooaD
4zucarzQ01B76JZZIJME4Kd3/IVnU24wj4o5ZHSYWy/Yw1kamojfjM177ADGng3z
/EPQfThzOEdN+7qoZuqCn0iozfVWTGUMP18p3xoFbRbuvQarjWayolFaScPsn5zw
m5sCggEABA8wlBRS01oUnD2D6smaR8/7brTI2oTQL4W4LIQeP4AyLuLtuUQK48hz
w8HhPqvni/IyW0sU5UM57GnFgBP9mfa9z7JjTiRDD8Pb8AWO2BkfCbXXcbZumxSP
thG5cuwm0gsD6Ve4w0FmXJGDEWMTHXBqBvctJrbuBHPBdn96cjTFPVe6VSBqgL6G
lbw33HBTjyHhBTlkhfmwG6GTHJybKwR83KxetaL5mx5RQLaRVSekCvuoGv1vT/3f
zITYQXPcnVuWvYSaszFGnSKHdTA7wUxeuvtE+YP+gsR22lZbvuCqAXcjzRi2TDin
8R8OEDYpY0xZhxXt7kZFwig0aUGgjg==
-----END PRIVATE KEY-----
        )";

    return private_key;
}

static const char* GetRootCertificate()
{
    const char* root = R"(
-----BEGIN CERTIFICATE-----
MIIFvTCCA6WgAwIBAgIJAMjFv/YLTje4MA0GCSqGSIb3DQEBBQUAMHUxCzAJBgNV
BAYTAkZSMRIwEAYDVQQIDAlPY2NpdGFuaWUxETAPBgNVBAcMCFRvdWxvdXNlMRQw
EgYDVQQKDAtUZWNoIFNjaG9vbDESMBAGA1UECwwJRWR1Y2F0aW9uMRUwEwYDVQQD
DAwxNzIuMTYuNzIuMzgwHhcNMjEwNzE0MTM0ODA3WhcNMjIwNzE0MTM0ODA3WjB1
MQswCQYDVQQGEwJGUjESMBAGA1UECAwJT2NjaXRhbmllMREwDwYDVQQHDAhUb3Vs
b3VzZTEUMBIGA1UECgwLVGVjaCBTY2hvb2wxEjAQBgNVBAsMCUVkdWNhdGlvbjEV
MBMGA1UEAwwMMTcyLjE2LjcyLjM4MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIIC
CgKCAgEA3iCI7R0xAaol4i/lURH6Fi/M9vIB4oZIpK7kaQZCWbh1ir+6pXwAqCqa
uMcnc/qFfho9j4USzxoVoCP2d+fAboDqx98Ej6J/mO3hPIK43qq1RoV5kdvV4u7U
/ocbzWZfT9MJNMxFpvpYd8cqirn2wm6dXd1I5f52YbS0RGBJR9mYBb8MXv8Xhia3
TyhQSfUUVV7OSMQJSWIUUoM/1mEW+i1jMRIvP5h9bPNiai0BuHkfUVBCinIlHivz
hEbuw28URuTjbK15Fr9K4jsROJ7WkGFj3nf4hD39fZ0J8nmhQvOm3g+bG4f/mbKY
X17G51pEHuWdhYOx532NcoSs2wtMk1kWBRbX1aHq3u5OfeWXVib718r2p15kxbAr
C4AGhP2CHZ47HzDKEdIWW1j3st/XpS2G5wlDoipUT6vymeD9kS0ib1MjgGuJazOf
DJZC3yFKNW4MfUfpXVqnAitodicZXWsY8ENmdkrYmhqqxMncz3SEf4cbJGvmw+HD
6p5j25mS7/395pstbeNfB1Iz5jlUe2KY45J/vllSOgWOCyB37DF9cprZ7G2nckuy
D6iY7ttnzXfhRFORSaauZ7xfZx3kUBa3OEnfEtR0ZtHBnC+IYpLWk5G0gCmO6IqX
z3+rejTj4MMKCXHQYv6aYBrOP7GkpcCQ491iwMf32phNNkv6xBMCAwEAAaNQME4w
HQYDVR0OBBYEFNcZmxvQs+spQFXmBeI35NxKNDvOMB8GA1UdIwQYMBaAFNcZmxvQ
s+spQFXmBeI35NxKNDvOMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEFBQADggIB
AAgtXBGkaZe3xUoCwQEmOxcjhV9SUY5YOK4vG1wYetGD8dBQ4S4LKjBh8Fwlr06Q
biW6SN/aQVPhXd6mtHxOJFk+jauRQWyfevQV14n+XYGGcwomwtyfmizf48GPc3e7
3c6309krh0vIoDujE2Gjy90LYOxTr1SLzy9SgFfuji7W83m+5GbnZIqf7ro0j8a9
zCiJIvPf97kkx7o65+Mx38e3ZA+w7lzM09hFaDyi0otHtCiFsOsIcP41qi0sq4w7
S6YGZsgmumEHa0+EQJBIlR85qqyYXv8u32ubgVnPtesTF8B/NrGejVWHAWdjCqlP
N2BwhLMz5K+N/7Nr+6wKiuVzIFjVF0HOyoqHqitqoWD3poxcV2hwPh5mCak7ZXsW
ykIsM2hXiugbe4MskKJqrOOJ2eBfJgAODmxDM8DeXlqm2rGqW076XBJVJ3HJE+gB
VoD+J0I3PIYajForCJZ8IbE65GkoqnnfDbkmf7ZeRQnhRM8MRFmF+jtO4NVidpsT
yBKJGJysOKAz4qnHSjnJ9Fkpbp+wBhQ7MLy6m6LA+NMjTjR9JvBkC8P+cGWtTUuf
jwGLw+gZXffCyds0EXcN8n7HZd1z9MI4xWCY6TReVUSpFVXxE90ldqQss2iyPKqO
ae/oDHEKtabH7kPblJQJqCQuoI56WrAzUfeUI53zlivE
-----END CERTIFICATE-----
    )";

    return root;
}

struct HttpServerFixture
{
    HttpServerFixture()
        : _listener(utility::conversions::to_string_t(EXTERNAL_SERVER_URL))
    {
        _listener.open().wait();
    }

    ~HttpServerFixture()
    {
        _listener.close().wait();
    }

    base::http_listener _listener;
};

struct HttpClientFixture
{
    HttpClientFixture()
        : _client(utility::conversions::to_string_t(LOCAL_SERVER_URL))
    {
    }

    ~HttpClientFixture()
    {
    }

    base::http_client _client;
};

struct HttpsClientFixture
{
private:
    static base::http_client_config CreateClientConfig()
    {
        const std::string sertificate(GetSslCertificate());
        boost::asio::const_buffer cert(sertificate.c_str(), sertificate.size());

        base::http_client_config client_config;
        client_config.set_validate_certificates(false);
        //client_config.set_ssl_context_callback(
        //    [&](boost::asio::ssl::context& ctx)
        //    { 
        //        ctx.add_certificate_authority(cert); 
        //    });

        return client_config;
    }

public:

    HttpsClientFixture()
        : _clientConfig(CreateClientConfig())
        , _client(utility::conversions::to_string_t(LOCAL_SERVER_HTTPS_URL), _clientConfig)
    {
    }

    ~HttpsClientFixture()
    {

    }
    
    base::http_client_config _clientConfig;
    base::http_client _client;
};

BOOST_AUTO_TEST_SUITE(edna_client_suite)

BOOST_FIXTURE_TEST_CASE(EdnaSendWorksCorrectlyTest, HttpServerFixture)
{
    edna::Request ednaRequest;

    ednaRequest.sessionId = L"123456";
    ednaRequest.text = L"message";
    //ednaRequest.receivedAt = L"2018-11-13T13:13:11.756Z";
    ednaRequest.answerId = L"qwerty";
    ednaRequest.questionIndex = 1;
    ednaRequest.code = edna::Code::SWITCH_TO_HUMAN;

    _listener.support(web::http::methods::POST, [](web::http::http_request request) {
        web::json::value value;
        value[L"sessionId"] = web::json::value(L"123456");
        value[L"questionIndex"] = web::json::value(1);

        request.reply(web::http::status_codes::OK, value);
        });

    edna::EdnaClient client(EXTERNAL_SERVER_URL, LOCAL_SERVER_URL, std::wstring{});

    std::promise<void> p;
    auto f = p.get_future();

    edna::RequestAck ack;
    client.sendRequestAsync(std::move(ednaRequest), [&p, &ack](edna::RequestAck&& receivedAck) {
        ack = std::move(receivedAck);
        p.set_value();
        });

    auto status = f.wait_for(std::chrono::seconds(5));
    if (status != std::future_status::ready)
    {
        BOOST_FAIL("RequestAck was not received in 5 seconds");
    }

    BOOST_CHECK(ack.sessionId == L"123456");
    BOOST_CHECK(ack.code == 0);
    BOOST_CHECK(ack.questionIndex == 1);
}

//BOOST_AUTO_TEST_CASE(SendDestinationNotAvailableTest, *boost::unit_test::disabled())
//{
//    bot::Request botRequest;
//    botRequest.sessionId = L"123456";
//    botRequest.marketCode = L"VIP";
//    botRequest.message = L"Hi";
//    botRequest.ctn = L"88005553535";
//    botRequest.authType = L"1";
//    botRequest.segment = L"1";
//    botRequest.channelName = L"email";
//    botRequest.timestamp = L"2017-04-12'T'15:41:12.123+0300";
//    botRequest.channelType = 12;
//    botRequest.questionNumber = 1;
//
//    BotClient client(EXTERNAL_SERVER_URL, LOCAL_SERVER_URL);
//
//    std::atomic<bool> ackReceived = false;
//    bot::RequestAck ack;
//    client.sendRequestAsync(std::move(botRequest), [&ackReceived, &ack](bot::RequestAck&& receivedAck) {
//        ack = std::move(receivedAck);
//        ackReceived = true;
//    });
//
//    auto waitTime = std::chrono::seconds(90);
//    auto startTime = std::chrono::system_clock::now();
//
//    while (!ackReceived && (std::chrono::system_clock::now() - startTime < waitTime)) {
//        std::this_thread::sleep_for(std::chrono::milliseconds(50));
//    }
//
//    if (!ackReceived) {
//        BOOST_FAIL("RequestAck was not received in 90 seconds");
//    }
//
//    BOOST_CHECK(ack.sessionId == L"123456");
//    BOOST_CHECK(ack.code == 1);
//    BOOST_CHECK(ack.questionNumber == 1);
//    BOOST_CHECK(!ack.description.empty());
//}
//
//BOOST_FIXTURE_TEST_CASE(SendRetriesWorkingTest, HttpServerFixture)
//{
//    bot::Request botRequest;
//    botRequest.sessionId = L"123456";
//    botRequest.marketCode = L"VIP";
//    botRequest.message = L"Hi";
//    botRequest.ctn = L"88005553535";
//    botRequest.authType = L"1";
//    botRequest.segment = L"1";
//    botRequest.channelName = L"email";
//    botRequest.timestamp = L"2017-04-12'T'15:41:12.123+0300";
//    botRequest.channelType = 12;
//    botRequest.questionNumber = 1;
//
//    _listener.support(web::http::methods::POST, [](web::http::http_request request) {
//        static int tryNumber = 0;
//        if (++tryNumber < 3) {
//            request.reply(web::http::status_codes::InternalError);
//        } else {
//            web::json::value value;
//            value[L"SessionId"] = web::json::value(L"123456");
//            value[L"QuestionNumber"] = web::json::value(1);
//            value[L"Code"] = web::json::value(0);
//
//            request.reply(web::http::status_codes::OK, value);
//        }
//    });
//
//    BotClient client(EXTERNAL_SERVER_URL, LOCAL_SERVER_URL);
//
//    std::atomic<bool> ackReceived = false;
//    bot::RequestAck ack;
//    client.sendRequestAsync(std::move(botRequest), [&ackReceived, &ack](bot::RequestAck&& receivedAck) {
//        ack = std::move(receivedAck);
//        ackReceived = true;
//    });
//
//    auto waitTime = std::chrono::seconds(20);
//    auto startTime = std::chrono::system_clock::now();
//
//    while (!ackReceived && (std::chrono::system_clock::now() - startTime < waitTime)) {
//        std::this_thread::sleep_for(std::chrono::milliseconds(50));
//    }
//
//    if (!ackReceived) {
//        BOOST_FAIL("RequestAck was not received in 20 seconds");
//    }
//
//    BOOST_CHECK(ack.sessionId == L"123456");
//    BOOST_CHECK(ack.code == 0);
//    BOOST_CHECK(ack.questionNumber == 1);
//    BOOST_CHECK(ack.description.empty());
//}
//
BOOST_FIXTURE_TEST_CASE(ReceiveWorksCorrectlyTest, HttpClientFixture)
{
    web::json::value requestData;
    requestData[L"sessionId"] = web::json::value(L"123456");
    //requestData[L"questionId"] = web::json::value(1);
    requestData[L"questionIndex"] = web::json::value(1);
    requestData[L"text"] = web::json::value(L"test");
    requestData[L"receivedAt"] = web::json::value(L"2017-04-12'T'15:41:12.123+0300");
    requestData[L"threadsClientId"] = web::json::value(123);

    edna::Response response;

    edna::EdnaClient client(EXTERNAL_SERVER_URL, LOCAL_SERVER_URL, std::wstring{});
    client.setResponseHandler([&response](edna::Response&& actualResponse) {
        response = std::move(actualResponse);
    });

    _client.request(web::http::methods::POST, L"", requestData).wait();
    
    BOOST_CHECK(response.sessionId/*.GetBotSessionId()*/ == L"123456");
    //BOOST_CHECK(response.questionId == 1);
    BOOST_CHECK(response.questionIndex == 1);
    BOOST_CHECK(response.text == L"test");
    BOOST_CHECK(response.receivedAt == L"2017-04-12'T'15:41:12.123+0300");
    BOOST_CHECK(response.threadsClientId == 123);
}
//
//BOOST_FIXTURE_TEST_CASE(ReceiveWithEmptySessionIdNotPassedTest, HttpClientFixture)
//{
//    web::json::value requestData;
//    requestData[L"SessionId"] = web::json::value(L"");
//    requestData[L"QuestionNumber"] = web::json::value(1);
//    requestData[L"Code"] = web::json::value(0);
//    requestData[L"ResponseType"] = web::json::value(1);
//    requestData[L"Message"] = web::json::value(L"test");
//    requestData[L"TimeStamp"] = web::json::value(L"2017-04-12'T'15:41:12.123+0300");
//    requestData[L"Split"] = web::json::value(L"123");
//
//    std::atomic<bool> called = false;
//    BotClient client(EXTERNAL_SERVER_URL, LOCAL_SERVER_URL);
//    client.setResponseHandler([&called](bot::Response&& response) {
//        called = true;
//    });
//
//    _client.request(web::http::methods::POST, L"", requestData).wait();
//
//    BOOST_CHECK(called == false);
//}
//
//BOOST_FIXTURE_TEST_CASE(ReceiveRespondsWithCorrectBodyTest, HttpClientFixture)
//{
//    web::json::value requestData;
//    requestData[L"SessionId"] = web::json::value(L"123");
//    requestData[L"QuestionNumber"] = web::json::value(12);
//    requestData[L"Code"] = web::json::value(0);
//    requestData[L"ResponseType"] = web::json::value(1);
//    requestData[L"Message"] = web::json::value(L"test");
//    requestData[L"TimeStamp"] = web::json::value(L"2017-04-12'T'15:41:12.123+0300");
//    requestData[L"Split"] = web::json::value(L"123");
//
//    BotClient client(EXTERNAL_SERVER_URL, LOCAL_SERVER_URL);
//
//    web::json::value resp;
//    _client.request(web::http::methods::POST, L"", requestData)
//        .then([](web::http::http_response response) {
//            return response.extract_json();
//        })
//        .then([&resp](web::json::value value) {
//            resp = value;
//        })
//        .wait();
//
//
//    BOOST_CHECK(resp[L"SessionId"].as_string() == L"123");
//    BOOST_CHECK(resp[L"Code"].as_integer() == 0);
//    BOOST_CHECK(resp[L"QuestionNumber"].as_integer() == 12);
//}
//
//BOOST_FIXTURE_TEST_CASE(ReceiveNonPostNotWorkingTest, HttpClientFixture)
//{
//    web::json::value requestData;
//    requestData[L"SessionId"] = web::json::value(L"123");
//    requestData[L"QuestionNumber"] = web::json::value(12);
//    requestData[L"Code"] = web::json::value(0);
//    requestData[L"ResponseType"] = web::json::value(1);
//    requestData[L"Message"] = web::json::value(L"test");
//    requestData[L"TimeStamp"] = web::json::value(L"2017-04-12'T'15:41:12.123+0300");
//    requestData[L"Split"] = web::json::value(L"123");
//
//    BotClient client(EXTERNAL_SERVER_URL, LOCAL_SERVER_URL);
//
//    web::http::http_response resp;
//    _client.request(web::http::methods::PUT, L"", requestData)
//        .then([&resp](web::http::http_response response) {
//            resp = response;
//        })
//        .wait();
//
//    BOOST_CHECK(resp.status_code() == web::http::status_codes::MethodNotAllowed);
//
//    _client.request(web::http::methods::GET, L"")
//        .then([&resp](web::http::http_response response) {
//            resp = response;
//        })
//        .wait();
//
//    BOOST_CHECK(resp.status_code() == web::http::status_codes::MethodNotAllowed);
//}
//
//BOOST_FIXTURE_TEST_CASE(ReceiveWithEmptySessionIdRespondsCorrectlyTest, HttpClientFixture)
//{
//    web::json::value requestData;
//    requestData[L"SessionId"] = web::json::value(L"");
//    requestData[L"QuestionNumber"] = web::json::value(12);
//    requestData[L"Code"] = web::json::value(0);
//    requestData[L"ResponseType"] = web::json::value(1);
//    requestData[L"Message"] = web::json::value(L"test");
//    requestData[L"TimeStamp"] = web::json::value(L"2017-04-12'T'15:41:12.123+0300");
//    requestData[L"Split"] = web::json::value(L"123");
//
//    BotClient client(EXTERNAL_SERVER_URL, LOCAL_SERVER_URL);
//
//    web::http::http_response httpResponce;
//    web::json::value responseData;
//    _client.request(web::http::methods::POST, L"", requestData)
//        .then([&httpResponce](web::http::http_response response) {
//            httpResponce = response;
//            return response.extract_json();
//        })
//        .then([&responseData](web::json::value value) {
//            responseData = value;
//        })
//        .wait();
//
//    BOOST_TEST(httpResponce.status_code() == web::http::status_codes::InternalError);
//    BOOST_TEST(responseData[L"Code"].as_integer() == 1);
//    BOOST_CHECK(responseData[L"Description"].as_string() == L"SessionId field could not be empty");
//}
//BOOST_FIXTURE_TEST_CASE(ReceiveNoSessionIdRespondsCorrectlyTest, HttpClientFixture)
//{
//    web::json::value requestData;
//    requestData[L"QuestionNumber"] = web::json::value(12);
//    requestData[L"Code"] = web::json::value(0);
//    requestData[L"ResponseType"] = web::json::value(1);
//    requestData[L"Message"] = web::json::value(L"test");
//    requestData[L"TimeStamp"] = web::json::value(L"2017-04-12'T'15:41:12.123+0300");
//    requestData[L"Split"] = web::json::value(L"123");
//
//    BotClient client(EXTERNAL_SERVER_URL, LOCAL_SERVER_URL);
//
//    web::http::http_response httpResponce;
//    web::json::value responseData;
//    _client.request(web::http::methods::POST, L"", requestData)
//        .then([&httpResponce](web::http::http_response response) {
//        httpResponce = response;
//        return response.extract_json();
//    })
//        .then([&responseData](web::json::value value) {
//        responseData = value;
//    })
//        .wait();
//
//    BOOST_TEST(httpResponce.status_code() == web::http::status_codes::InternalError);
//    BOOST_TEST(responseData[L"Code"].as_integer() == 1);
//    BOOST_CHECK(responseData[L"Description"].as_string() == L"Mandatory field \"SessionId\" not found");
//}
//

BOOST_FIXTURE_TEST_CASE(ReceiveWorksCorrectlyHttpsTest, HttpsClientFixture)
{
    web::json::value requestData;
    requestData[L"sessionId"] = web::json::value(L"123456");
    //requestData[L"questionId"] = web::json::value(1);
    requestData[L"questionIndex"] = web::json::value(1);
    requestData[L"text"] = web::json::value(L"test");
    requestData[L"receivedAt"] = web::json::value(L"2017-04-12'T'15:41:12.123+0300");
    requestData[L"threadsClientId"] = web::json::value(123);

    edna::Response response;

    const std::wstring authorizationToken = L"{DDFE07A5-AFCF-434B-9B12-7D8BE0F75666}";

    edna::EdnaClient client(
        EXTERNAL_SERVER_HTTPS_URL, 
        LOCAL_SERVER_HTTPS_URL, 
        GetSslCertificate(), 
        GetSslPrivateKey(), 
        GetRootCertificate(),
        authorizationToken,
        std::wstring{});

    client.setResponseHandler([&response](edna::Response&& actualResponse) {
        response = std::move(actualResponse);
        });

    _client.request(web::http::methods::POST, L"", requestData).wait();

    BOOST_CHECK(response.sessionId/*.GetBotSessionId()*/ == L"123456");
    //BOOST_CHECK(response.questionId == 1);
    BOOST_CHECK(response.questionIndex == 1);
    BOOST_CHECK(response.text == L"test");
    BOOST_CHECK(response.receivedAt == L"2017-04-12'T'15:41:12.123+0300");
    BOOST_CHECK(response.threadsClientId == 123);
}


//BOOST_FIXTURE_TEST_CASE(DISABLED_CpprestTest, HttpsClientFixture)
//{
//    const std::string self_signed_cert = R"(
//-----BEGIN CERTIFICATE-----
//MIIDlzCCAn+gAwIBAgIJAP9ZV+1X94UjMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNV
//BAYTAkNOMQswCQYDVQQIDAJTSDELMAkGA1UEBwwCU0gxEjAQBgNVBAoMCU1JQ1JP
//U09GVDERMA8GA1UECwwISFBDLVBBQ0sxEjAQBgNVBAMMCWxvY2FsaG9zdDAeFw0x
//NTA4MTkwOTA0MjhaFw00MzAxMDMwOTA0MjhaMGIxCzAJBgNVBAYTAkNOMQswCQYD
//VQQIDAJTSDELMAkGA1UEBwwCU0gxEjAQBgNVBAoMCU1JQ1JPU09GVDERMA8GA1UE
//CwwISFBDLVBBQ0sxEjAQBgNVBAMMCWxvY2FsaG9zdDCCASIwDQYJKoZIhvcNAQEB
//BQADggEPADCCAQoCggEBALLv7AAPa+4wYpa+3tqc9HIHhh8kv/MpV2Dm+oKG27iH
//zOugMNAPqLzMAaWCzDRyw27I+jPS3pzAAu6rQ0v2H6XNrie1YEEV27j1WOUS9iFy
//vcf6Y+ywUKXvFlN/VM/ZFz9Z8U3jc7Y6unIyoUs8UdX/RRITspb2m7SUxlmLJ+4c
//qiLrHwstNB2NHIZN72oc8DaS5eBqBdT9h6NO62RSBTrAlR7Vk9eU/5trYkd5+PoC
//pispvU+7Fe24uVerGgU6Yoyd7DMj+3BpbG3g/VkOlGhgH0DNtbKu3v/XOmnzdZn6
//dzoOoGFNpG1NeH2Xv0vnvEZP6WG4h/TFSafBJMONNnMCAwEAAaNQME4wHQYDVR0O
//BBYEFO1mAjAmLk1J0iT93xfczAE5mxgzMB8GA1UdIwQYMBaAFO1mAjAmLk1J0iT9
//3xfczAE5mxgzMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEFBQADggEBAFB8AACf
//5O+sPe3PZ8IPgwZb+BCXdoXc2rngR/gOaYO019TZyNLuHRzW9FtplzW25IbQ9Jnc
//b+jmY2Ill7Zf3TX4OhHEwscJ1G2LBaqZfQlwSbYJmCzvRNSzSbF3RigNQD5Qhdph
//vVBdvVGTZnVeatjTOFKUyfhcXf4DMb6eMfaU6il/VJCSMW0j3hYNQjPm3V/PLxnG
//fd9T4hpCUd8MK2XG4RqJAzh6x/6v0fc6mRHBS5+qTWYSDGFwITrU1pP2L9qFegpm
//aNAom7bdENU8uivd+vrLnG2fKvFSssjVfaXpFLKAICfTJY9A3/CWnZ1AcbE5El7A
//adctopihoUrlAb0=
//-----END CERTIFICATE-----
//        )";
//    const std::string private_key = R"(
//-----BEGIN PRIVATE KEY-----
//MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQCy7+wAD2vuMGKW
//vt7anPRyB4YfJL/zKVdg5vqChtu4h8zroDDQD6i8zAGlgsw0csNuyPoz0t6cwALu
//q0NL9h+lza4ntWBBFdu49VjlEvYhcr3H+mPssFCl7xZTf1TP2Rc/WfFN43O2Orpy
//MqFLPFHV/0USE7KW9pu0lMZZiyfuHKoi6x8LLTQdjRyGTe9qHPA2kuXgagXU/Yej
//TutkUgU6wJUe1ZPXlP+ba2JHefj6AqYrKb1PuxXtuLlXqxoFOmKMnewzI/twaWxt
//4P1ZDpRoYB9AzbWyrt7/1zpp83WZ+nc6DqBhTaRtTXh9l79L57xGT+lhuIf0xUmn
//wSTDjTZzAgMBAAECggEAenzd8lScL1qTwlk6ODAE7SHVX/BKLWv5Um4KwdsLAVCE
//qC7p+yMdANAtuFzG6Ig+29Fb5KnOlUKjPzmhQZhjpZ4cPzZbg3IxDHV2uqi2L8NZ
//wlDWoik3q770a4fYSMd0sHsjQYwXo4CkLJQX8WaDJpgtcehl8g0yHPVSqe0mEkoL
//dxdqaZnxprchscxefWaGaysIxEO+V+ZOBaPNf4i8PmBKoMNczWZbLcdKhRL7aLeW
//ngPQp1xSWYoN8fPoonpL2qTSop3Nsc2INpwGcYPAj3vxdasC3+DZ8JEJI2AmxpVB
//13BLkd3nDzOwimZIlu9Fv+NMJ1vb9XdC249ZOqo68QKBgQDigkws1W429nqDtEtQ
//Dr5ebHTdP4gZlNt6vWx5obGLCMBAzoyubfNCCBTCYsCPj8hXxNfiPArPFFkIgEx9
//+w0n7BlaYL6SD2xD4q+YzA1/j4Loakxc7N9z8Cyu+/YHifvLhzwqgFnkLfFnVq9N
//TF8TatHUYcrbcpawJLz0wr/cnQKBgQDKPAYNTzqPLOOBaE4DfnJNn2zctGU8G5Xp
//0L/ED8O1t9AjjV2xVO8PDPNDZAxMzgnIbWeU9iWRSLbr7NloXElKh/QlITjAbSXe
//HsUruq1SmDgiaUhEtDaaJ1SqSZZWY2BZqNXMdILOCgvZGnOyyBR2U49zuNaRHyhm
//kmZMdIIKTwKBgQDezAk/hEQfvfuuNpZpzcbEu+uLgKVPfFMSfOYJEdnAB0CLvl80
//Z6QBzE8XEOmVjHkkk9NBjYuYOsyEhyY2OM2s+hfKBSUOKCt27q+IHRYd5bx+/afV
//M41rzc8141ISAlBw1rmAmLVSszojSmmuH7PZNpXkULineCPuaISQQEtWJQKBgQDD
//laVsvdEuowUsJEo+ys2VELhiAv1dUnh79u1fmrd2SV085P1WAYRqE+Y4qMvUg/em
//JVjmEeBnT+HI7fmdGpOvRyjxt92BDI5w8WVTU2lI1fqEHTpNZ9Te5WbWgfCpf9ax
//H74VzCCtT74Bq7l1kFdp0IqOKpcpJu8VtETHcG5LtQKBgQC4Tx7El1Xb4hsI4dvE
//h43j3KBb3evlz6vaqgz0BArahYAz2UkkOYDSOPs4K6aOxxXjO0BjqQqCx/tCPcU5
//AvLsTlswO+wDLXM1DoKxzFBZL5o8927niqW+vZpzyGc1uPmC1MG7+MDKdZsR+e+9
//XzJTD4slrGSJrcpLt/g/Jqqdjg==
//-----END PRIVATE KEY-----
//        )";
//
//    auto body = utility::string_t{ L"body content" };
//    web::http::http_headers all_headers;
//    all_headers.add(L"Accept", L"text/plain");
//    all_headers.add(L"Accept-Charset", L"utf-8");
//    all_headers.add(L"Accept-Encoding", L"gzip, deflate");
//    //all_headers.add(U("Accept-Language"), U("en-US"));
//    //all_headers.add(U("Accept-Datetime"), U("Thu, 31 May 2007 20:35:00 GMT"));
//    //all_headers.add(U("Authorization"), U("Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ=="));
//    //all_headers.add(U("Cache-Control"), U("no-cache"));
//    //all_headers.add(U("Cookie"), U("$Version=1; Skin=new;"));
//    //all_headers.add(U("Content-Length"), body.size());
//    //all_headers.add(U("Content-MD5"), U("Q2hlY2sgSW50ZWdyaXR5IQ=="));
//    //all_headers.add(U("Content-Type"), U("application/x-www-form-urlencoded"));
//    //all_headers.add(U("Date"), U("Tue, 15 Nov 1994 08:12:31 GMT"));
//    //all_headers.add(U("Expect"), U("100-continue"));
//    //all_headers.add(U("Forwarded"),
//    //    U("for=192.0.2.60;proto=http;by=203.0.113.43Forwarded: for=192.0.2.43, for=198.51.100.17"));
//    //all_headers.add(U("From"), U("user@example.com"));
//    //all_headers.add(U("Host"), U("en.wikipedia.org"));
//    //all_headers.add(U("If-Match"), U("\"737060cd8c284d8af7ad3082f209582d\""));
//    //all_headers.add(U("If-Modified-Since"), U("Sat, 29 Oct 1994 19:43:31 GMT"));
//    //all_headers.add(U("If-None-Match"), U("\"737060cd8c284d8af7ad3082f209582d\""));
//    //all_headers.add(U("If-Range"), U("\"737060cd8c284d8af7ad3082f209582d\""));
//    //all_headers.add(U("If-Unmodified-Since"), U("Sat, 29 Oct 1994 19:43:31 GMT"));
//    //all_headers.add(U("Max-Forwards"), U("10"));
//    //all_headers.add(U("Origin"), U("http://www.example-social-network.com"));
//    //all_headers.add(U("Pragma"), U("no-cache"));
//    //all_headers.add(U("Proxy-Authorization"), U("Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ=="));
//    //all_headers.add(U("Range"), U("bytes=500-999"));
//    //all_headers.add(U("Referer"), U("http://en.wikipedia.org/wiki/Main_Page"));
//    //all_headers.add(U("TE"), U("trailers, deflate"));
//    //all_headers.add(U("User-Agent"), U("Mozilla/5.0 (X11; Linux x86_64; rv:12.0) Gecko/20100101 Firefox/21.0"));
//    //all_headers.add(U("Upgrade"), U("HTTP/2.0, SHTTP/1.3, IRC/6.9, RTA/x11"));
//    //all_headers.add(U("Via"), U("1.0 fred, 1.1 example.com (Apache/1.1)"));
//    //all_headers.add(U("Warning"), U("199 Miscellaneous warning"));
//
//    //const std::string self_signed_cert = GetSslCertificate();
//    //const std::string private_key = GetSslPrivateKey();
//    const std::string root_cert = GetRootCertificate();
//
//    boost::asio::const_buffer cert(self_signed_cert.c_str(), self_signed_cert.size());
//    boost::asio::const_buffer key(private_key.c_str(), private_key.size());
//    boost::asio::const_buffer root(root_cert.c_str(), root_cert.size());
//
//    web::http::experimental::listener::http_listener_config server_config;
//    server_config.set_ssl_context_callback([&](boost::asio::ssl::context& ctx) {
//        //ctx.set_options(boost::asio::ssl::context::default_workarounds |
//        //    boost::asio::ssl::context::no_sslv2 | boost::asio::ssl::context::no_tlsv1 |
//        //    boost::asio::ssl::context::no_tlsv1_1);
//        ctx.set_options(boost::asio::ssl::context::default_workarounds);
//        ctx.use_certificate_chain(cert);
//        ctx.use_private_key(key, boost::asio::ssl::context::pem);
//        ctx.set_password_callback([](std::size_t /*size*/, boost::asio::ssl::context_base::password_purpose /*purpose*/) { return std::string{}; });
//        });
//
//    web::http::experimental::listener::http_listener listener(utility::conversions::to_string_t(LOCAL_SERVER_HTTPS_URL), server_config);
//
//    listener.support(web::http::methods::GET, [&](web::http::http_request request) 
//    {
//        //web::http::http_asserts::assert_request_equals(request, web::http::methods::GET, U("/"));
//
//        //for (auto&& h : all_headers)
//        //{
//        //    VERIFY_IS_TRUE(request.headers().has(h.first));
//        //    VERIFY_ARE_EQUAL(h.second, request.headers().find(h.first)->second);
//        //}
//
//        //VERIFY_ARE_EQUAL(body, request.extract_string(true).get());
//
//        request.reply(web::http::status_codes::OK);
//    });
//
//    listener.open().wait();
//
//    web::http::client::http_client_config client_config;
//#if !defined(_WIN32) && !defined(__cplusplus_winrt) || defined(CPPREST_FORCE_HTTP_CLIENT_ASIO)
//    client_config.set_ssl_context_callback(
//        [&](boost::asio::ssl::context& ctx) 
//        { 
//            //ctx.add_certificate_authority(cert); 
//            ctx.load_verify_file("D:\\Programming\\Projects\\esbotproxy\\x64\\Debug\\Cert\\ca-cert.pem");
//        });
//#else
//    // in this build configuration, with WinHTTP-based http_client, this test will fail unless the self-signed
//    // cert is added to the Windows certificate store (or certificate validation is disabled in client_config)
//#endif
//    web::http::client::http_client client(utility::conversions::to_string_t(LOCAL_SERVER_HTTPS_URL), client_config);
//    web::http::http_request msg(web::http::methods::GET);
//    msg.set_request_uri(L"/");
//
//    msg.headers() = all_headers;
//    //msg.set_body(body);
//
//    //client.request(msg).wait();
//
//    //tests::functional::http::utilities::http_asserts::assert_response_equals(client.request(msg).get(), web::http::status_codes::OK);
//    client.request(msg).get();
//
//    listener.close().wait();
//
//    ::Sleep(10000);
//
//}

BOOST_AUTO_TEST_SUITE_END()
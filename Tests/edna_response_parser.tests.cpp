#include <boost/test/unit_test.hpp>

//#include "ESBotProxyLib/edna_response.h"

namespace utests
{
    BOOST_AUTO_TEST_SUITE(edna_response_parser_suite)

    BOOST_AUTO_TEST_CASE(ShouldParseResponse)
    {
        const std::string jsonRequest = R"(
{
  "action":"MESSAGE",
  "threadsClientId":1,
  "sessionId":"1",
  "questionId":43,
  "questionIndex":43,
  "receivedAt":"2018-11-13T13:13:11.876Z",
  "text":"Message",
  "channelInfo":{
      "channelType":"MOBILE",
      "authorized":true,
      "id":8
  },
  "platform":"Android",
  "attachments":[
  {
      "url":"hhtp://...",
      "name":"test.jpg",
      "type":"image/jpeg",
      "size":256
  }
  ],
  "clientData":{
    "phone":"88885553535"
  },
  "sender":"ThreadsAPI"
}meta
)";

        auto value = web::json::value{}.parse(jsonRequest);
        auto response = edna::ResponseParser::FromJson(value, std::wstring{});

        BOOST_CHECK(response.sessionId/*.GetBotSessionId()*/ == L"1");
        BOOST_CHECK(response.text == L"Message");
        BOOST_CHECK(response.receivedAt == L"2018-11-13T13:13:11.876Z");
        BOOST_CHECK(response.channelType == L"MOBILE");
        BOOST_CHECK(response.channelId == 8);
        BOOST_CHECK(response.phone == L"88885553535");
        //BOOST_CHECK(response.questionId == 43);
        BOOST_CHECK(response.questionIndex == 43);
        BOOST_CHECK(response.threadsClientId == 1);
        BOOST_CHECK(response.authorized == true);
    }

    BOOST_AUTO_TEST_SUITE_END()
}


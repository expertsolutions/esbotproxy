//#define BOOST_TEST_DYN_LINK
#include "stdafx.h"
#include <boost/test/unit_test.hpp>
//#include <boost/test/included/unit_test.hpp>

#include <ESBotProxyLib/bot_client.h>

// Memory leaks will be detected due to SystemLog singleton

//#define EXTERNAL_SERVER_URL "http://localhost:51102/chatbot"
//#define LOCAL_SERVER_URL "http://localhost:41102/mbot"

static const std::string EXTERNAL_SERVER_URL{ "http://localhost:51102/chatbot" };
static const std::string LOCAL_SERVER_URL{ "http://localhost:41102/mbot" };
static const std::wstring SESSION_ID_PREFIX{ L"EDNA_" };

struct HttpServerFixture
{
    HttpServerFixture()
        : _listener(utility::conversions::to_string_t(EXTERNAL_SERVER_URL))
    {
        _listener.open().wait();
    }

    ~HttpServerFixture()
    {
        _listener.close().wait();
    }

    base::http_listener _listener;
};

struct HttpClientFixture
{
    HttpClientFixture()
        : _client(utility::conversions::to_string_t(LOCAL_SERVER_URL))
    {
    }

    ~HttpClientFixture()
    {
    }

    base::http_client _client;
};


BOOST_AUTO_TEST_SUITE(bot_client_suite)

//BOOST_AUTO_TEST_CASE(CtorThrowsWithWrongLocalUrlsTest)
//{
//    BOOST_CHECK_THROW(BotClient(EXTERNAL_SERVER_URL, "http://google.ru/mbot"), std::exception);
//}


BOOST_FIXTURE_TEST_CASE(SendWorksCorrectlyTest, HttpServerFixture)
{
    bot::SessionId botSessionId = bot::SessionId(L"123456", SESSION_ID_PREFIX);

    bot::Request botRequest;
    botRequest.sessionId = botSessionId;
    botRequest.marketCode = L"VIP";
    botRequest.message = L"Hi";
    botRequest.ctn = L"88005553535";
    botRequest.authType = L"1";
    botRequest.segment = L"1";
    botRequest.channelName = L"email";
    botRequest.timestamp = L"2017-04-12'T'15:41:12.123+0300";
    botRequest.channelType = 12;
    botRequest.questionNumber = 1;

    _listener.support(web::http::methods::POST, [&botSessionId](web::http::http_request request) {
        web::json::value value;
        value[L"appeal_id"] = web::json::value(botSessionId.GetBotSessionId());
        value[L"message_number"] = web::json::value(1);
        value[L"code"] = web::json::value(0);

        request.reply(web::http::status_codes::OK, value);
    });

    bot::BotClient client(EXTERNAL_SERVER_URL, LOCAL_SERVER_URL, SESSION_ID_PREFIX);

    std::atomic<bool> ackReceived = false;
    bot::RequestAck ack;
    client.sendRequestAsync(std::move(botRequest), [&ackReceived, &ack](bot::RequestAck&& receivedAck) {

        ack = std::move(receivedAck);
        ackReceived = true;
    });

    auto waitTime = std::chrono::seconds(5);
    auto startTime = std::chrono::system_clock::now();

    while (!ackReceived && (std::chrono::system_clock::now() - startTime < waitTime)) {
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }

    if (!ackReceived) {
        BOOST_FAIL("RequestAck was not received in 5 seconds");
    }

    BOOST_CHECK(ack.sessionId == botSessionId.GetBotSessionId());
    BOOST_CHECK(ack.code == 0);
    BOOST_CHECK(ack.questionNumber == 1);
    BOOST_CHECK(ack.description.empty());
}

BOOST_AUTO_TEST_CASE(SendDestinationNotAvailableTest, *boost::unit_test::disabled())
{
    bot::Request botRequest;
    botRequest.sessionId = bot::SessionId(L"123456", SESSION_ID_PREFIX);
    botRequest.marketCode = L"VIP";
    botRequest.message = L"Hi";
    botRequest.ctn = L"88005553535";
    botRequest.authType = L"1";
    botRequest.segment = L"1";
    botRequest.channelName = L"email";
    botRequest.timestamp = L"2017-04-12'T'15:41:12.123+0300";
    botRequest.channelType = 12;
    botRequest.questionNumber = 1;

    bot::BotClient client(EXTERNAL_SERVER_URL, LOCAL_SERVER_URL, SESSION_ID_PREFIX);

    std::atomic<bool> ackReceived = false;
    bot::RequestAck ack;
    client.sendRequestAsync(std::move(botRequest), [&ackReceived, &ack](bot::RequestAck&& receivedAck) {
        ack = std::move(receivedAck);
        ackReceived = true;
    });

    auto waitTime = std::chrono::seconds(90);
    auto startTime = std::chrono::system_clock::now();

    while (!ackReceived && (std::chrono::system_clock::now() - startTime < waitTime)) {
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }

    if (!ackReceived) {
        BOOST_FAIL("RequestAck was not received in 90 seconds");
    }

    BOOST_CHECK(ack.sessionId == L"123456");
    BOOST_CHECK(ack.code == 1);
    BOOST_CHECK(ack.questionNumber == 1);
    BOOST_CHECK(!ack.description.empty());
}

BOOST_FIXTURE_TEST_CASE(SendRetriesWorkingTest, HttpServerFixture)
{
    bot::Request botRequest;

    auto botSessionId = bot::SessionId(L"123456", SESSION_ID_PREFIX);

    botRequest.sessionId = botSessionId;
    botRequest.marketCode = L"VIP";
    botRequest.message = L"Hi";
    botRequest.ctn = L"88005553535";
    botRequest.authType = L"1";
    botRequest.segment = L"1";
    botRequest.channelName = L"email";
    botRequest.timestamp = L"2017-04-12'T'15:41:12.123+0300";
    botRequest.channelType = 12;
    botRequest.questionNumber = 1;

    _listener.support(web::http::methods::POST, [&botSessionId](web::http::http_request request) {
        static int tryNumber = 0;
        if (++tryNumber < 3) {
            request.reply(web::http::status_codes::InternalError);
        } else {
            web::json::value value;

            value[L"appeal_id"] = web::json::value(botSessionId.GetBotSessionId());
            value[L"message_number"] = web::json::value(1);
            value[L"code"] = web::json::value(0);

            request.reply(web::http::status_codes::OK, value);
        }
    });

    bot::BotClient client(EXTERNAL_SERVER_URL, LOCAL_SERVER_URL, SESSION_ID_PREFIX);

    std::atomic<bool> ackReceived = false;
    bot::RequestAck ack;
    client.sendRequestAsync(std::move(botRequest), [&ackReceived, &ack](bot::RequestAck&& receivedAck) {
        ack = std::move(receivedAck);
        ackReceived = true;
    });

    auto waitTime = std::chrono::seconds(20);
    auto startTime = std::chrono::system_clock::now();

    while (!ackReceived && (std::chrono::system_clock::now() - startTime < waitTime)) {
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }

    if (!ackReceived) {
        BOOST_FAIL("RequestAck was not received in 20 seconds");
    }

    BOOST_CHECK(ack.sessionId == botSessionId.GetBotSessionId());
    BOOST_CHECK(ack.code == 0);
    BOOST_CHECK(ack.questionNumber == 1);
    BOOST_CHECK(ack.description.empty());
}

BOOST_FIXTURE_TEST_CASE(ReceiveWorksCorrectlyTest, HttpClientFixture)
{
    web::json::value requestData;

    std::wstring botSessionId = SESSION_ID_PREFIX + L"123456";

    requestData[L"request_id"] = web::json::value(L"123");
    requestData[L"appeal_id"] = web::json::value(botSessionId);
    requestData[L"message_number"] = web::json::value(1);
    requestData[L"result"] = web::json::value(L"answer");
    requestData[L"request_time"] = web::json::value(L"2017-04-12'T'15:41:12.123+0300");

    bot::Response botResponse;

    bot::BotClient client(EXTERNAL_SERVER_URL, LOCAL_SERVER_URL, SESSION_ID_PREFIX);
    client.setResponseHandler([&botResponse](bot::Response&& response) {
        botResponse = std::move(response);
    });

    _client.request(web::http::methods::POST, L"", requestData).wait();
    
    BOOST_CHECK(botResponse.sessionId.GetBotSessionId() == botSessionId);
    BOOST_CHECK(botResponse.questionNumber == 1);
    BOOST_CHECK(botResponse.timestamp == L"2017-04-12'T'15:41:12.123+0300");
}
//
BOOST_FIXTURE_TEST_CASE(ReceiveWithEmptySessionIdNotPassedTest, HttpClientFixture)
{
    web::json::value requestData;
    requestData[L"request_id"] = web::json::value(L"123");
    requestData[L"appeal_id"] = web::json::value(L"");
    requestData[L"message_number"] = web::json::value(1);
    requestData[L"result"] = web::json::value(L"answer");
    requestData[L"request_time"] = web::json::value(L"2017-04-12'T'15:41:12.123+0300");

    std::atomic<bool> called = false;
    bot::BotClient client(EXTERNAL_SERVER_URL, LOCAL_SERVER_URL, SESSION_ID_PREFIX);
    client.setResponseHandler([&called](bot::Response&& response) {
        called = true;
    });

    _client.request(web::http::methods::POST, L"", requestData).wait();

    BOOST_CHECK(called == false);
}
//
BOOST_FIXTURE_TEST_CASE(ReceiveRespondsWithCorrectBodyTest, HttpClientFixture)
{
    web::json::value requestData;

    std::wstring botSessionId = SESSION_ID_PREFIX + L"123";

    requestData[L"request_id"] = web::json::value(L"123");
    requestData[L"appeal_id"] = web::json::value(botSessionId);
    requestData[L"message_number"] = web::json::value(12);
    requestData[L"result"] = web::json::value(L"answer");
    requestData[L"request_time"] = web::json::value(L"2017-04-12'T'15:41:12.123+0300");

    bot::BotClient client(EXTERNAL_SERVER_URL, LOCAL_SERVER_URL, SESSION_ID_PREFIX);

    web::json::value resp;
    _client.request(web::http::methods::POST, L"", requestData)
        .then([](web::http::http_response response) {
            return response.extract_json();
        })
        .then([&resp](web::json::value value) {
            resp = value;
        })
        .wait();


    BOOST_CHECK(resp[L"appeal_id"].as_string() == botSessionId);
    BOOST_CHECK(resp[L"Code"].as_integer() == 0);
    BOOST_CHECK(resp[L"message_number"].as_integer() == 12);
}
//
BOOST_FIXTURE_TEST_CASE(ReceiveNonPostNotWorkingTest, HttpClientFixture)
{
    web::json::value requestData;

    std::wstring botSessionId = SESSION_ID_PREFIX + L"123";

    requestData[L"request_id"] = web::json::value(L"123");
    requestData[L"appeal_id"] = web::json::value(botSessionId);
    requestData[L"message_number"] = web::json::value(12);
    requestData[L"result"] = web::json::value(L"answer");
    requestData[L"request_time"] = web::json::value(L"2017-04-12'T'15:41:12.123+0300");

    bot::BotClient client(EXTERNAL_SERVER_URL, LOCAL_SERVER_URL, SESSION_ID_PREFIX);

    web::http::http_response resp;
    _client.request(web::http::methods::PUT, L"", requestData)
        .then([&resp](web::http::http_response response) {
            resp = response;
        })
        .wait();

    BOOST_CHECK(resp.status_code() == web::http::status_codes::MethodNotAllowed);

    _client.request(web::http::methods::GET, L"")
        .then([&resp](web::http::http_response response) {
            resp = response;
        })
        .wait();

    BOOST_CHECK(resp.status_code() == web::http::status_codes::MethodNotAllowed);
}
//
BOOST_FIXTURE_TEST_CASE(ReceiveWithEmptySessionIdRespondsCorrectlyTest, HttpClientFixture)
{
    web::json::value requestData;
    requestData[L"request_id"] = web::json::value(L"123");
    requestData[L"appeal_id"] = web::json::value(L"");
    requestData[L"message_number"] = web::json::value(12);
    requestData[L"result"] = web::json::value(L"answer");
    requestData[L"request_time"] = web::json::value(L"2017-04-12'T'15:41:12.123+0300");

    bot::BotClient client(EXTERNAL_SERVER_URL, LOCAL_SERVER_URL, {});

    web::http::http_response httpResponce;
    web::json::value responseData;
    _client.request(web::http::methods::POST, L"", requestData)
        .then([&httpResponce](web::http::http_response response) {
            httpResponce = response;
            return response.extract_json();
        })
        .then([&responseData](web::json::value value) {
            responseData = value;
        })
        .wait();

    BOOST_TEST(httpResponce.status_code() == web::http::status_codes::InternalError);
    BOOST_TEST(responseData[L"Code"].as_integer() == 1);
    BOOST_CHECK(responseData[L"Description"].as_string() == L"appeal_id field could not be empty");
}
//
BOOST_FIXTURE_TEST_CASE(ReceiveWithoutSeparatorSessionIdRespondsCorrectlyTest, HttpClientFixture)
{
    web::json::value requestData;
    requestData[L"request_id"] = web::json::value(L"123");
    requestData[L"appeal_id"] = web::json::value(L"123");
    requestData[L"message_number"] = web::json::value(12);
    requestData[L"result"] = web::json::value(L"answer");
    requestData[L"request_time"] = web::json::value(L"2017-04-12'T'15:41:12.123+0300");

    bot::BotClient client(EXTERNAL_SERVER_URL, LOCAL_SERVER_URL, SESSION_ID_PREFIX);

    web::http::http_response httpResponce;
    web::json::value responseData;
    _client.request(web::http::methods::POST, L"", requestData)
        .then([&httpResponce](web::http::http_response response) {
        httpResponce = response;
        return response.extract_json();
            })
        .then([&responseData](web::json::value value) {
                responseData = value;
            })
                .wait();

            BOOST_TEST(httpResponce.status_code() == web::http::status_codes::InternalError);
            BOOST_TEST(responseData[L"Code"].as_integer() == 1);
            BOOST_CHECK(responseData[L"Description"].as_string() == L"Wrong \"appeal_id\" format");
}
//
BOOST_FIXTURE_TEST_CASE(ReceiveNoSessionIdRespondsCorrectlyTest, HttpClientFixture)
{
    web::json::value requestData;
    requestData[L"request_id"] = web::json::value(L"123");
    requestData[L"message_number"] = web::json::value(12);
    requestData[L"result"] = web::json::value(L"answer");
    requestData[L"request_time"] = web::json::value(L"2017-04-12'T'15:41:12.123+0300");

    bot::BotClient client(EXTERNAL_SERVER_URL, LOCAL_SERVER_URL, SESSION_ID_PREFIX);

    web::http::http_response httpResponce;
    web::json::value responseData;
    _client.request(web::http::methods::POST, L"", requestData)
        .then([&httpResponce](web::http::http_response response) {
        httpResponce = response;
        return response.extract_json();
    })
        .then([&responseData](web::json::value value) {
        responseData = value;
    })
        .wait();

    BOOST_TEST(httpResponce.status_code() == web::http::status_codes::InternalError);
    BOOST_TEST(responseData[L"Code"].as_integer() == 1);
    BOOST_CHECK(responseData[L"Description"].as_string() == L"Mandatory field \"appeal_id\" not found");
}



BOOST_AUTO_TEST_SUITE_END()
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

//#include "ESBotProxyLib/bot_client.h"

BOOST_AUTO_TEST_SUITE(bot_request_parser_suite)
//
//BOOST_AUTO_TEST_CASE(AllFieldsMessageTest)
//{
//    core::message msg(L"TEST_MESSAGE");
//    msg[L"SessionId"] = L"394859230485394";
//    msg[L"MarketCode"] = L"VIP";
//    msg[L"QuestionNumber"] = 1;
//    msg[L"Message"] = L"Text";
//    msg[L"CTN"] = L"88005553535";
//    msg[L"AuthType"] = L"0";
//    msg[L"Segment"] = L"0";
//    msg[L"ChannelName"] = L"email";
//    msg[L"ChannelType"] = 12;
//    msg[L"TimeStamp"] = L"2017-04-12'T'15:41:12.123+0300";
//
//    auto request = BotRequestParser::fromRouterMessage(msg);
//
//    BOOST_CHECK(request.sessionId == L"394859230485394");
//    BOOST_CHECK(request.marketCode == L"VIP");
//    BOOST_TEST(request.questionNumber == 1);
//    BOOST_CHECK(request.message == L"Text");
//    BOOST_CHECK(request.ctn == L"88005553535");
//    BOOST_CHECK(request.authType == L"0");
//    BOOST_CHECK(request.segment == L"0");
//    BOOST_CHECK(request.channelName == L"email");
//    BOOST_TEST(request.channelType == 12);
//    BOOST_CHECK(request.timestamp == L"2017-04-12'T'15:41:12.123+0300");
//}

//BOOST_AUTO_TEST_CASE(RequiredFieldsMessageTest)
//{
//    core::message msg(L"TEST_MESSAGE");
//    msg[L"SessionId"] = L"394859230485394";
//    msg[L"QuestionNumber"] = 1;
//    msg[L"Message"] = L"Text";
//    msg[L"ChannelName"] = L"email";
//    msg[L"ChannelType"] = 12;
//    msg[L"TimeStamp"] = L"2017-04-12'T'15:41:12.123+0300";
//
//    auto request = BotRequestParser::fromRouterMessage(msg);
//
//    BOOST_CHECK(request.sessionId == L"394859230485394");
//    BOOST_CHECK(request.marketCode == L"");
//    BOOST_TEST(request.questionNumber == 1);
//    BOOST_CHECK(request.message == L"Text");
//    BOOST_CHECK(request.ctn == L"");
//    BOOST_CHECK(request.authType == L"");
//    BOOST_CHECK(request.segment == L"");
//    BOOST_CHECK(request.channelName == L"email");
//    BOOST_TEST(request.channelType == 12);
//    BOOST_CHECK(request.timestamp == L"2017-04-12'T'15:41:12.123+0300");
//}
//
//BOOST_AUTO_TEST_CASE(MissingRequiredFieldThrowsTest)
//{
//    core::message msg(L"TEST_MESSAGE");
//    msg[L"SessionId"] = L"394859230485394";
//    msg[L"QuestionNumber"] = 1;
//    msg[L"Message"] = L"Text";
//    msg[L"ChannelType"] = 12;
//    msg[L"TimeStamp"] = L"2017-04-12'T'15:41:12.123+0300";
//
//    BOOST_CHECK_THROW(BotRequestParser::fromRouterMessage(msg), std::exception);
//}
//
//BOOST_AUTO_TEST_CASE(WrongFieldTypeThrowsTest)
//{
//    core::message msg(L"TEST_MESSAGE");
//    msg[L"SessionId"] = L"394859230485394";
//    msg[L"QuestionNumber"] = L"1";
//    msg[L"Message"] = L"Text";
//    msg[L"ChannelType"] = 12;
//    msg[L"TimeStamp"] = L"2017-04-12'T'15:41:12.123+0300";
//
//    BOOST_CHECK_THROW(BotRequestParser::fromRouterMessage(msg), std::exception);
//}

BOOST_AUTO_TEST_SUITE_END()
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

//#include "ESBotProxyLib/bot_client.h"

BOOST_AUTO_TEST_SUITE(bot_response_parser_suite)

BOOST_AUTO_TEST_CASE(AllFieldsTest)
{
//    const std::string jsonRequest = R"(
//{
//   "appeal_id":"844624001866316494",
//   "data":[
//      {
//         "content":"data",
//         "score":"",
//         "type":"text",
//         "uid":"7940948270014051155/noMatch"
//      }
//   ],
//   "errors":[
//      
//   ],
//   "message_number":1,
//   "replyTo_id":"7940948270014051155",
//   "request_id":"6eb802bc-c607-48d4-93b9-a5a2ca3fa610",
//   "request_time":"2022-04-28T10:07:41.750+03:00",
//   "response_data":{
//      
//   },
//   "result":"answer"
//}
//)";
    const std::string jsonRequest = R"(
    {"appeal_id":"844624019046371904",
    "data":[
        {
            "content":"��������",
            "score":"",
            "type":"text",
            "uid":"1793478501517115122/noMatch"
        }],
    "errors":[],
    "message_number":1,
    "replyTo_id":"1793478501517115122",
    "request_id":"6057c60a-bc75-41e8-bcb4-87558bb15183",
    "request_time":"2022-06-06T11:54:04.111+03:00",
    "response_data":{},
    "result":"answer"}
)";

//    const std::string jsonRequest = R"(
//{
//   "request_id":"154124124",
//   "appeal_id":"6084",
//   "message_number":1,
//   "replyTo_id":"421321",
//   "result":"answer",
//   "request_time":"2017-04-12'T'15:41:12.123+0300",
//   "response_data":{
//      "additional_data1":"test",
//      "additional_data2":"test2"
//   },
//   "data":[
//      {
//         "score":"1.2",
//         "uid":"1",
//         "type":"text",
//         "content":"answer1."
//      },
//      {
//         "score":"58.2",
//         "uid":"2",
//         "type":"text",
//         "content":"answer2."
//      }
//   ],
//   "errors":[
//      
//   ]
//}
//)";

    try
    {
        auto value = web::json::value{}.parse(jsonRequest);
        auto response = bot::ResponseParser::FromJson(value, std::wstring{});

        BOOST_CHECK(response.sessionId.GetBotSessionId() == L"6084");
        BOOST_CHECK(response.errors.empty() == true);
        BOOST_CHECK(response.questionNumber == 1);
        BOOST_CHECK(response.result == bot::Response::Result::Answer);
        BOOST_CHECK(response.timestamp == L"2017-04-12'T'15:41:12.123+0300");
        BOOST_CHECK(response.MakeMessage() == L"answer1.");
    }
    catch (std::exception& error)
    {
        BOOST_FAIL(error.what());
    }
    catch (...)
    {
        BOOST_FAIL("unknown exception");
    }



    //web::json::value value;
    //value[U("Description")] = web::json::value(U(""));
    //value[U("SessionId")] = web::json::value(U("1231241241"));
    //value[U("Code")] = web::json::value(0);
    //value[U("QuestionNumber")] = web::json::value(1);
    //value[U("ResponseType")] = web::json::value(1);
    //value[U("Message")] = web::json::value(U("ima bot!"));
    //value[U("TimeStamp")] = web::json::value(U("2017-04-12'T'15:41:12.123+0300"));
    //value[U("Split")] = web::json::value(U("123"));

    //auto response = BotResponseParser::fromJson(value);

    //BOOST_CHECK(response.description == L"");
    //BOOST_CHECK(response.sessionId == L"1231241241");
    //BOOST_TEST(response.code == 0);
    //BOOST_TEST(response.questionNumber == 1);
    //BOOST_CHECK(response.message == L"ima bot!");
    //BOOST_CHECK(response.timestamp == L"2017-04-12'T'15:41:12.123+0300");
    //BOOST_CHECK(response.split == L"123");
}

BOOST_AUTO_TEST_CASE(RequestAckParse)
{
    const std::string jsonRequest = R"(
{
   "request_id":"234-4456-3322",
   "appeal_id":"6084",
   "message_number":1,
   "code":0
}
)";

    auto value = web::json::value{}.parse(jsonRequest);
    auto response = bot::RequestAckParser::FromJson(value, {});

    BOOST_CHECK(response.sessionId == L"6084");
    BOOST_CHECK(response.questionNumber == 1);
    BOOST_CHECK(response.code == 0);
}

//BOOST_AUTO_TEST_CASE(RussianTextTest)
//{
//    web::json::value value;
//    value[U("Description")] = web::json::value(U(""));
//    value[U("SessionId")] = web::json::value(U("1231241241"));
//    value[U("Code")] = web::json::value(0);
//    value[U("QuestionNumber")] = web::json::value(1);
//    value[U("ResponseType")] = web::json::value(1);
//    value[U("Message")] = web::json::value(U("� ����� �������"));
//    value[U("TimeStamp")] = web::json::value(U("2017-04-12'T'15:41:12.123+0300"));
//    value[U("Split")] = web::json::value(U("123"));
//
//    auto response = BotResponseParser::fromJson(value);
//
//    BOOST_CHECK(response.description == L"");
//    BOOST_CHECK(response.sessionId == L"1231241241");
//    BOOST_TEST(response.code == 0);
//    BOOST_TEST(response.questionNumber == 1);
//    BOOST_CHECK(response.message == L"� ����� �������");
//    BOOST_CHECK(response.timestamp == L"2017-04-12'T'15:41:12.123+0300");
//    BOOST_CHECK(response.split == L"123");
//}
//
//BOOST_AUTO_TEST_CASE(RequiredFieldsTest)
//{
//    web::json::value value;
//    value[U("SessionId")] = web::json::value(U("1231241241"));
//    value[U("Code")] = web::json::value(1);
//    value[U("QuestionNumber")] = web::json::value(1);
//    value[U("ResponseType")] = web::json::value(1);
//    value[U("TimeStamp")] = web::json::value(U("2017-04-12'T'15:41:12.123+0300"));
//
//    auto response = BotResponseParser::fromJson(value);
//
//    BOOST_CHECK(response.description == L"");
//    BOOST_CHECK(response.sessionId == L"1231241241");
//    BOOST_TEST(response.code == 1);
//    BOOST_TEST(response.questionNumber == 1);
//    BOOST_CHECK(response.timestamp == L"2017-04-12'T'15:41:12.123+0300");
//}
//
//BOOST_AUTO_TEST_CASE(EmptySessionIdThrowsTest)
//{
//    web::json::value value;
//    value[U("SessionId")] = web::json::value(U(""));
//    value[U("Code")] = web::json::value(1);
//    value[U("QuestionNumber")] = web::json::value(1);
//    value[U("ResponseType")] = web::json::value(1);
//    value[U("TimeStamp")] = web::json::value(U("2017-04-12'T'15:41:12.123+0300"));
//
//    BOOST_CHECK_THROW(BotResponseParser::fromJson(value), std::exception);
//}
//
//BOOST_AUTO_TEST_CASE(NoSessionIdThrowsTest)
//{
//    web::json::value value;
//    value[U("Code")] = web::json::value(1);
//    value[U("QuestionNumber")] = web::json::value(1);
//    value[U("ResponseType")] = web::json::value(1);
//    value[U("TimeStamp")] = web::json::value(U("2017-04-12'T'15:41:12.123+0300"));
//
//    BOOST_CHECK_THROW(BotResponseParser::fromJson(value), std::exception);
//}



BOOST_AUTO_TEST_SUITE_END()
#include <iostream>
#include <thread>
#include <memory>

#include "Router\router_compatibility.h"
#include "IdGenerator.h"

class ClassifyTest
{
public:
    ClassifyTest()
        : _stop{false}
    {
        _router.RegisterConnectHandler([this](const std::wstring&, uint16_t)
            {
                this->SendS2VC_Classify();
            });
        _router.RegisterDeliverErrorHandler([](const CMessage& msg, const CLIENT_ADDRESS& from, const CLIENT_ADDRESS& to)
            {
                std::cout << "Router deliver error: " << wtos(msg.Dump()) << std::endl;
            });

        _router.RegisterStatusHandler([](DWORD dwClient, core::ClientStatus nStatus) {});

        core::SubscribeMessage(_router, L"S2VC_ClassifyAck", this, &ClassifyTest::OnS2VC_ClassifyAck);
        core::SubscribeMessage(_router, L"S2VC_ClassifyCompleted", this, &ClassifyTest::OnS2VC_ClassifyCompleted);
    }

    bool IsStopped()
    {
        return _stop;
    }

    void SendS2VC_Classify()
    {
        CMessage msg{ L"S2VC_Classify" };
        msg[L"ScriptID"] = CIdGenerator::GetInstance()->makeId();
        msg[L"Text"] = L"How are you?";

        std::cout << "[SendS2VC_Classify] send: " << wtos(msg.Dump()) << std::endl;

        _router.SendToAny(std::move(msg));
    }

private:
    void OnS2VC_ClassifyAck(const CMessage& msg, const CLIENT_ADDRESS& from, const CLIENT_ADDRESS& to)
    {
        std::cout << "[OnS2VC_ClassifyAck] Received: " << wtos(msg.Dump()) << std::endl;
    }

    void OnS2VC_ClassifyCompleted(const CMessage& msg, const CLIENT_ADDRESS& from, const CLIENT_ADDRESS& to)
    {
        std::cout << "[OnS2VC_ClassifyCompleted] Received: " << wtos(msg.Dump()) << std::endl;
        Stop();
    }

    void Stop()
    {
        _stop = true;
    }

private:
    std::atomic_bool _stop;
    CRouterManager _router;
};


int main()
{
    ClassifyTest test;

    std::thread thread{[&test]()
    {
        while (!test.IsStopped())
        {
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    } };

    thread.join();
}
